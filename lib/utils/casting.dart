import 'dart:convert';
import 'package:path/path.dart' as path;
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:varun_kanna_app/views/market/verify_main_page.dart';

class Cast {
  static dynamic objectToJson(dynamic object) {
    String jsonList = json.encode(object);
    dynamic list = json.decode(jsonList);
    return list;
  }

  static String convertLacaleDate(String date) {
    if (date == "" ||
        date.isEmpty ||
        date == "null" ||
        date == "Invalid Date") {
      return "";
    }

    final dateSplitted = date.split('-');
    final months = [
      'มกราคม',
      'กุมภาพันธ์',
      'มีนาคม',
      'เมษายน',
      'พฤษภาคม',
      'มิถุนายน',
      'กรกฏาคม',
      'สิงหาคม',
      'กันยายน',
      'ตุลาคม',
      'พฤศจิกายน',
      'ธันวาคม'
    ];
    var year = (int.parse(dateSplitted[0]) + 543).toString();
    var month = months[int.parse(dateSplitted[1]) - 1].toString();
    var day = int.parse(dateSplitted[2]).toString();
    var resultText = day + " " + month + " " + year;
    date = resultText.toString();
    return date;
  }

  static Future<File> writeImageTemp(
      dynamic base64Image, String imageName) async {
    final dir = await getTemporaryDirectory();
    await dir.create(recursive: true);
    final tempFile = File(path.join(dir.path, imageName));
    await tempFile.writeAsBytes(base64Image);
    return tempFile;
  }

  static String formatPlusCode(String address) {
    String withoutPlusCode = address;
    List stringList = [];
    if (address.contains('+')) {
      address.split("+")[1].split(" ").asMap().forEach((i, element) {
        if (i != 0) {
          stringList.add(element);
        }
      });
      withoutPlusCode =
          stringList.reduce((value, element) => value + ' ' + element);
    }
    return withoutPlusCode;
  }

  static double fahrnhitToClss(double fahrenheitNumber) {
    double celsius = (fahrenheitNumber - 32) * 5 / 9;
    return celsius;
  }

  static String mapWeatherToThaiWord(String engWeather) {
    String thaiWord = "";
    switch (engWeather) {
      case "01":
        thaiWord = "ท้องฟ้าแจ่มใส";
        break;
      case "02":
        thaiWord = "เมฆปกคลุมเล็กน้อย";
        break;
      case "03":
        thaiWord = "เมฆปกคลุมบางส่วน";
        break;
      case "04":
        thaiWord = "เมฆปกคลุมเป็นส่วนมาก";
        break;
      case "09":
        thaiWord = "ฝนตกเล็กน้อย";
        break;
      case "10":
        thaiWord = "ฝนตก";
        break;
      case "11":
        thaiWord = "ฝนฟ้าคะนอง";
        break;
      case "13":
        thaiWord = "หิมะตก";
        break;
      case "50":
        thaiWord = "หมอก";
        break;
      default:
    }
    return thaiWord;
  }

  static ActivityStatusTypes activityBEtoFEType(String status) {
    ActivityStatusTypes statusType;
    String statusText;
    switch (status) {
      case "waiting for information":
        statusText = "รอบันทึกข้อมูล";
        statusType = ActivityStatusTypes.waitingForInformation;
        break;
      case "waiting for edit identity":
        statusText = "รอแก้ไขข้อมูล";
        statusType = ActivityStatusTypes.waitingForEditIdentity;

        break;
      case "waiting for verify identity":
        statusText = "รอตรวจสอบ";
        statusType = ActivityStatusTypes.waitingForVerifyIdentity;

        break;
      case "verified":
        statusText = "สำเร็จ";
        statusType = ActivityStatusTypes.verified;

        break;
      case "verify rejected":
        statusText = "ไม่สำเร็จ";
        statusType = ActivityStatusTypes.verifyRejected;
        break;
      case "waiting for the activity log":
        statusText = "รอบันทึกกิจกรรม";
        statusType = ActivityStatusTypes.waitingForTheActivityLog;
        break;
      case "waiting for edit the activity":
        statusText = "รอแก้ไขกิจกรรม";
        statusType = ActivityStatusTypes.waitingForEditTheActivity;
        break;
      case "waiting for approve the activity":
        statusText = "รอตรวจสอบ";
        statusType = ActivityStatusTypes.waitingForApproveTheActivity;
        break;
      case "successful activity log":
        statusText = "สำเร็จ";
        statusType = ActivityStatusTypes.successfulActivityLog;

        break;
      case "activity rejected":
        statusText = "ไม่สำเร็จ";
        statusType = ActivityStatusTypes.activityRejected;
        break;
      default:
        statusText = "รอบันทึกข้อมูล";
        statusType = ActivityStatusTypes.waitingForInformation;

        break;
    }
    return statusType;
  }
}
