import 'package:flutter/material.dart';

class Success {
  int? code;
  Object? response;
  Success({this.code, this.response});
}

class Failure {
  int? code;
  Object? errorResponse;
  Failure({this.code, this.errorResponse});
}

class Screen {
  static bool isMiniScreen =
      WidgetsBinding.instance.window.physicalSize.width < 1080;
  static bool isLargeScreen =
      WidgetsBinding.instance.window.physicalSize.width >= 1080;
}

class SizeUtils {
  static Size size = WidgetsBinding.instance.window.physicalSize;
}

class ColorsUtils {
  static Color disableColor = const Color.fromARGB(182, 138, 138, 138);
}

class Messages {
  static String success = "บันทึกข้อมูลของท่านเรียบร้อยแล้ว";
  static String failure = "เกิดข้อผิดพลาด กรุณาลองใหม่อีกครั้ง";
  static String ok = "ตกลง";
  static String cancel = "ยกเลิก";
  static String noticeBackPress =
      "คุณมีการแก้ไขข้อมูล ต้องการออกโดยที่ไม่บันทึกใช่หรือไม่ ?";

  static String backPressConfirm = "ท่านต้องการออกจาหน้านี้ใช่ไหม ?";
}
