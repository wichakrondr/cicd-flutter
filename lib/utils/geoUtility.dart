import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:math' as math;

import 'package:proj4dart/proj4dart.dart';
import 'package:varun_kanna_app/model/farm_response.dart';

class GeoUtility {
  int getRai(double area) {
    return (area / 1600).floor();
  }

  int getNgran(double area, int raiValue) {
    return ((area / 1600 - raiValue) * 4).floor();
  }

  double getWha(double area, int raiValue, int ngranValue) {
    return (area / 4 - (raiValue * 400 + ngranValue * 100)).abs();
  }

  String getAreaInRaiFormat(double area) {
    int raiValue = 0;
    int ngranValue = 0;
    double wahValue = 0.0;

    raiValue = getRai(area);
    ngranValue = getNgran(area, raiValue);
    wahValue = getWha(area, raiValue, ngranValue);

    String raiText = raiValue == 0 ? "" : "${raiValue} ไร่ ";
    String ngranText = ngranValue == 0 ? "" : "${ngranValue} งาน ";
    String wahText =
        wahValue == 0 ? "" : "${wahValue.toStringAsFixed(2)} ตารางวา";
    return "${raiText}${ngranText}${wahText}";
  }

  double calcArea(List<LatLng> locations) {
    if (locations.length < 3) {
      return 0;
    }
    double radius = 6371000;
    double diameter = radius * 2;
    double circumference = diameter * math.pi;
    List listY = [];
    List listX = [];
    List listArea = [];

    double latitudeRef = locations[0].latitude;
    double longitudeRef = locations[0].longitude;

    for (var i = 1; i < locations.length; i++) {
      double latitude = locations[i].latitude;
      double longitude = locations[i].longitude;
      listY.insert(
          i - 1, calculateYSegment(latitudeRef, latitude, circumference));
      listX.insert(i - 1,
          calculateXSegment(longitudeRef, longitude, latitude, circumference));
    }

    // calculate areas for each triangle segment
    for (var i = 1; i < listX.length; i++) {
      double x1 = listX[i - 1];
      double y1 = listY[i - 1];
      double x2 = listX[i];
      double y2 = listY[i];
      listArea.insert(i - 1, calculateAreaInSquareMeters(x1, x2, y1, y2));
    }

    // sum areas of all triangle segments
    double areasSum = 0;
    listArea.forEach((area) => (areasSum = areasSum + area));

    // get abolute value of area, it can't be negative
    double areaCalc = areasSum.abs();
    //math.abs(areasSum); // Math.sqrt(areasSum * areasSum);

    return areaCalc;
  }

  double calculateYSegment(
      double latitudeRef, double latitude, double circumference) {
    return ((latitude - latitudeRef) * circumference) / 360.0;
  }

  double calculateXSegment(
    double longitudeRef,
    double longitude,
    double latitude,
    double circumference,
  ) {
    return (((longitude - longitudeRef) *
            circumference *
            math.cos(latitude * (math.pi / 180))) /
        360.0);
  }

  double calculateAreaInSquareMeters(
      double x1, double x2, double y1, double y2) {
    return (y1 * x2 - x1 * y2) / 2;
  }

  LatLng getCentroid(List<LatLng> points) {
    List lat = [];
    List long = [];

    for (var i = 0; i < points.length; i++) {
      lat.insert(i, points[i].latitude);
      long.insert(i, points[i].longitude);
    }
    lat = lat..sort();
    long = long..sort();

    double x1 = lat[0]; // the lowest x coordinate
    double y1 = long[0]; // the lowest y coordinate
    double x2 = lat[lat.length - 1]; // the highest x coordinate
    double y2 = long[long.length - 1]; // the highest y coordinate

    double centerX = x1 + ((x2 - x1) / 2);
    double centerY = y1 + ((y2 - y1) / 2);

    return LatLng(centerX, centerY);
  }

  UtmModel getUtm(LatLng centroid) {
    int zone = 0;
    double centerY = centroid.longitude;
    var projSrc = Projection.get('EPSG:4326')!;
    var pointSrc = Point(x: centroid.longitude, y: centroid.latitude);
    var projDst = Projection.get('EPSG:32647');

    if (centerY >= 96.00 && centerY <= 102.00) {
      projDst = projDst ??
          Projection.add(
              'EPSG:32647', '+proj=utm +zone=47 +datum=WGS84 units=m +no_defs');
      zone = 47;
    } else if (centerY >= 102 && centerY <= 108) {
      projDst = projDst ??
          Projection.add('EPSG:32648',
              '+proj=utm +zone=48 +datum=WGS84 +units=m +no_defs');
      zone = 48;
    } else if (centerY > 108) {
      projDst = projDst ??
          Projection.add('EPSG:32649',
              '+proj=utm +zone=49 +datum=WGS84 +units=m +no_defs');
      zone = 49;
    } else {
      projDst = projDst ??
          Projection.add('EPSG:32647',
              '+proj=utm +zone=47 +datum=WGS84 +units=m +no_defs');
      zone = 47;
    }

    var pointForward = projSrc.transform(projDst, pointSrc);
    return UtmModel(grid: "-", zone: zone, x: pointForward.x, y: pointForward.y);
  }

  LatLngBounds createBounds(List<LatLng> positions) {
    final southwestLat = positions.map((p) => p.latitude).reduce(
        (value, element) => value < element ? value : element); // smallest
    final southwestLon = positions
        .map((p) => p.longitude)
        .reduce((value, element) => value < element ? value : element);
    final northeastLat = positions.map((p) => p.latitude).reduce(
        (value, element) => value > element ? value : element); // biggest
    final northeastLon = positions
        .map((p) => p.longitude)
        .reduce((value, element) => value > element ? value : element);
    return LatLngBounds(
        southwest: LatLng(southwestLat, southwestLon),
        northeast: LatLng(northeastLat, northeastLon));
  }
}
