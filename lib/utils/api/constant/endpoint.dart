class Endpoints {
  Endpoints._();
// https://deartest.free.beeceptor.com
  // base url
  static const String baseUrl = "https://kanna-dev.varunatech.co/api/v1/";

  // receiveTimeout
  static const int receiveTimeout = 15000;

  // connectTimeout
  static const int connectionTimeout = 15000;
}
