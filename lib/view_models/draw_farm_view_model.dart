import 'dart:async';
import 'dart:collection';
import 'dart:io';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/farm_page_model/land_recomendation_model/land_recomendation_model.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/utils/geoUtility.dart';
import 'package:varun_kanna_app/view_models/land_recomendation_view_model.dart';
import 'package:varun_kanna_app/views/farms/farm_detail_page.dart';

import '../model/farm_list_response.dart';
import '../model/farm_response.dart';
import '../services/farm_service.dart';
import '../utils/api/api_result.dart';
import 'package:auto_route/auto_route.dart';

class DrawFarmViewModel extends ChangeNotifier {
  String farmId = "";
  String farmName = "";
  bool isValid = false;
  TextEditingController searchResultsController = TextEditingController();
  TextEditingController farmNameCtrl = TextEditingController();
  TextEditingController centroidCtrl = TextEditingController();
  TextEditingController utmCtrl = TextEditingController();
  Set<Polygon> _polygone = HashSet<Polygon>();
  Set<Polygon> get polygone => _polygone;
  CameraPosition? _cameraPos;
  CameraPosition? get cameraPos => _cameraPos;
  bool isFirstEdit = true;
  List<LatLng> _location = [];
  List<LatLng> get location => _location;
  MapType mapType = MapType.satellite;
  bool showXY = false;
  int indexSelected = 1;

  setXy() {
    showXY = !showXY;
    notifyListeners();
  }

  setIndexSelected(int newIndex) {
    indexSelected = newIndex;
    notifyListeners();
  }

  changepolylineColor(Color color) {
    if (_tempPolylines.isNotEmpty) {
      Polyline newPolyline =
          _tempPolylines.elementAt(0).copyWith(colorParam: color);
      _tempPolylines
          .removeWhere((line) => line.polylineId.value.toString() == uniqueID);
      _tempPolylines.add(newPolyline);
    }
    notifyListeners();
  }

  setMaptype(MapType newMapType) {
    mapType = newMapType;
    switch (newMapType) {
      case MapType.normal:
        indexSelected = 2;
        changepolylineColor(Colors.black);
        break;
      case MapType.hybrid:
        indexSelected = 3;
        changepolylineColor(Colors.white);
        break;
      case MapType.terrain:
        indexSelected = 4;
        changepolylineColor(Colors.black);
        break;
      default:
        indexSelected = 1;
        changepolylineColor(Colors.white);
    }
    notifyListeners();
  }

  setFirstEdit() {
    isFirstEdit = !isFirstEdit;
    notifyListeners();
  }

  TextEditingController getLatLngText() {
    LatLng centroid = GeoUtility().getCentroid(tempLocation);
    centroidCtrl.text =
        '${centroid.latitude.toStringAsFixed(5)}, ${centroid.longitude.toStringAsFixed(5)}';
    return centroidCtrl;
  }

  TextEditingController getUtmText() {
    LatLng centroid = GeoUtility().getCentroid(tempLocation);
    UtmModel utm = GeoUtility().getUtm(centroid);

    utmCtrl.text =
        '${utm.zone}, X ${utm.x.toStringAsFixed(2)}, Y ${utm.y.toStringAsFixed(2)}';
    return utmCtrl;
  }

  ///Property camera position
  CameraPosition? _cameraPosition;
  CameraPosition? get cameraPosition => _cameraPosition;
  resetCameraPosition() {
    _cameraPosition = null;
    notifyListeners();
  }

  ///Property my location data
  LatLng? _sourceLocation;
  LatLng? get sourceLocation => _sourceLocation;
  setSourceLocation(LatLng sourceLocation) {
    _sourceLocation = sourceLocation;

    notifyListeners();
  }

  ///Property Google Map Controller completer
  Completer<GoogleMapController> _completer = Completer();
  Completer<GoogleMapController> get completer => _completer;

  ///Property Google Map Controller
  GoogleMapController? _controller;
  GoogleMapController? get controller => _controller;

  ///Property to save all markers
  Set<Marker> _markers = {};
  Set<Marker> get markers => _markers;

  ///Property to mapStyle
  String? _mapStyle;
  String? get mapStyle => _mapStyle;

  ///Property temporary polygon list
  Set<Polygon> _tempPolygons = new Set();
  Set<Polygon> get tempPolygons => _tempPolygons;

  ///Property polygon list
  Set<Polygon> _polygons = new Set();
  Set<Polygon> get polygons => _polygons;

  //Property temporary polyline list
  Set<Polyline> _tempPolylines = new Set();
  Set<Polyline> get tempPolylines => _tempPolylines;

  Set<Polyline> _polylines = new Set();
  Set<Polyline> get polylines => _polylines;

  ///Property temporary location
  List<LatLng> _tempLocation = [];
  List<LatLng> get tempLocation => _tempLocation;
  setTempLocation(List<LatLng> tempLocation) {
    _tempLocation = tempLocation;
    notifyListeners();
  }

  ///Property to save distance location
  List<LatLng> _distanceLocation = [];
  List<LatLng> get distanceLocation => _distanceLocation;

  ///Propoerty to save end location
  LatLng? _endLoc;
  LatLng? get endLoc => _endLoc;

  ///Property to get uniqueId for markers
  String _uniqueID = "";
  String get uniqueID => _uniqueID;

  ///Property to polygon color
  Color? _polygonColor;
  Color? get polygonColor => _polygonColor;

  ///Custom key for custom marker
  GlobalKey markerKey = GlobalKey();
  GlobalKey distanceKey = GlobalKey();
  GlobalKey pinKey = GlobalKey();

  ///Value to show distance between two location
  String _distance = "0";
  String get distance => _distance;

  String _polygonId = "1";
  String get polygonId => _polygonId;

  ///Check if initialize camera success
  bool _onInitCamera = false;
  bool get onInitCamera => _onInitCamera;
  setOnInitCamera(bool onInitCamera) {
    _onInitCamera = onInitCamera;
    notifyListeners();
  }

  bool _isEditPolygon = false;
  bool get isEditPolygon => _isEditPolygon;
  setIsEditPolygon(bool isEditPolygon) {
    _isEditPolygon = isEditPolygon;
    notifyListeners();
  }

  bool _displayBtnUpdatePolygon = false;
  bool get displayBtnUpdatePolygon => _displayBtnUpdatePolygon;
  setDisplayBtnUpdatePolygon(bool isDisplay) {
    _displayBtnUpdatePolygon = isDisplay;
    notifyListeners();
  }

  ///Property temporary location
  LatLng? _currentLatLong;
  LatLng? get currentLatLong => _currentLatLong;

  bool _onDrag = false;
  bool get onDrag => _onDrag;

  List<FarmListModel> _farmList = [];

  bool _loading = true;
  bool get getLoading => _loading;
  setLoading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  bool _editMode = false;
  bool get getEditMode => _editMode;

  bool isEditing = false;
  bool isReadonly = true;
  bool isGeneralInformation = true;

  clearFarmName() {
    farmNameCtrl.text = "";
    notifyListeners();
  }

  setSearchResult(String text) {
    searchResultsController.text = text;
    searchResultsController.selection =
        const TextSelection.collapsed(offset: 0);
    notifyListeners();
  }

  setEditMode(bool editMode) {
    _editMode = editMode;
    notifyListeners();
  }

  toggleEditMode() {
    isEditing = !isEditing;
    isReadonly = !isReadonly;
    notifyListeners();
  }

  toggleTabBar() {
    isGeneralInformation = !isGeneralInformation;
    notifyListeners();
  }

  onDispose() {
    isEditing = false;
    isGeneralInformation = true;
    isReadonly = true;
    _isEditPolygon = false;
    centroidCtrl.text = "";
    utmCtrl.text = "";
    notifyListeners();
  }

  List<FarmListModel> get getFarmList => _farmList;
  setFarmList(FarmListModel farmItem) {
    _farmList.add(farmItem);
    notifyListeners();
  }

  FarmDetailModel? _farmDetail;
  FarmDetailModel get getFarmDetail => _farmDetail!;
  setFarmDetail(FarmDetailModel farmDetail) {
    _farmDetail = farmDetail;
    notifyListeners();
  }

  List<PlaceSearch>? _placeList = [];
  List<PlaceSearch> get getPlaceList => _placeList!;
  setPlaceList(List<PlaceSearch> placeList) {
    _placeList = placeList;
    notifyListeners();
  }

  clearFarmList() {
    _farmList = [];
    notifyListeners();
  }

  String _totalArea = "";
  String get totalArea => _totalArea;

  double _area = 0.0;
  double get area => _area;

  ///Function to initialize camera
  void initCamera() async {
    await initLocation();

    ///Set current location to camera
    _cameraPosition = CameraPosition(
      zoom: 16,
      target: sourceLocation!,
    );

    notifyListeners();
  }

  ///Function to init polygon color
  void setPolygonColor(Color? color) async {
    _polygonColor = await getPolyColor(color);
    notifyListeners();
  }

  ///Assign polygon color
  Future<Color?> getPolyColor(Color? color) async {
    return color;
  }

  ///Function to get current locations if permission
  ///available, otherwise set default location
  Future<void> initLocation() async {
    _onInitCamera = true;

    try {
      Position currentPos;
      final serviceEnable = await Geolocator.isLocationServiceEnabled();
      if (serviceEnable) {
        var permission = await Geolocator.checkPermission();
        if ((permission != LocationPermission.always ||
            permission != LocationPermission.whileInUse)) {
          permission = await Geolocator.requestPermission();
        }
        currentPos = await Geolocator.getCurrentPosition();
      } else {
        currentPos = Position(
            latitude: Platform.isAndroid ? 0 : -6.215412,
            longitude: Platform.isAndroid ? 0 : 106.777773,
            timestamp: DateTime.now(),
            accuracy: 0,
            altitude: 0,
            heading: 0,
            speed: 0,
            speedAccuracy: 0);
      }

      _sourceLocation = LatLng(currentPos.latitude, currentPos.longitude);
      _onInitCamera = false;
    } catch (e) {
      print(e.toString());
      initLocation();
    }

    notifyListeners();
  }

  ///Function to handle when maps created
  void onMapCreated(GoogleMapController controller) async {
    _controller = controller;
    //_completer.complete(controller);
    notifyListeners();
  }

  ///Function to change camera position
  void changeCameraPosition(LatLng location) {
    ///Moving maps camera
    _controller!.animateCamera(CameraUpdate.newLatLngZoom(
        LatLng(
          location.latitude,
          location.longitude,
        ),
        16));

    notifyListeners();
  }

  void removeLastMarker() {
    if (_markers.last.markerId.value.toString() != "pinXY") {
      _markers.remove(_markers.elementAt(_markers.length - 1));
    } else {
      _markers.remove(_markers.elementAt(_markers.length - 2));
    }
    notifyListeners();
  }

  void undoLocationNew() async {
    List<LatLng> newLocationList = [];
    if (_tempLocation.length > 0) {
      _onDrag = false;

      if (_tempLocation.length * 2 < _markers.length ||
          _tempLocation.length == 2 && _markers.length > 3 ||
          _tempLocation.length == 1 && _markers.length > 1) {
        //_markers.remove(_markers.elementAt(_markers.length - 1));
        removeLastMarker();
        _tempPolylines.clear();
        _polylines.clear();
      }

      _markers.removeWhere((mark) =>
          mark.position == _tempLocation.last &&
          mark.markerId.value.toString() != "pinXy");

      if (_markers.length > 0) {
        removeMarker(_endLoc);
      }

      _tempLocation.removeLast();
      _polygonId = (_tempLocation.length + 1).toString();

      if (_tempLocation.length > 2) {
        _area = GeoUtility().calcArea(_tempLocation);
        _totalArea = GeoUtility().getAreaInRaiFormat(_area);
        notifyListeners();
      } else {
        _area = 0.0;
        _totalArea = "";
        notifyListeners();
      }

      if (_tempLocation.length == 0) {
        _tempPolygons.clear();
        _tempPolylines.clear();
        _polylines.clear();
        // _markers.clear();
        _markers
            .removeWhere((mark) => mark.markerId.value.toString() != "pinXY");
        notifyListeners();
      } else {
        newLocationList.add(_tempLocation.last);
        newLocationList.add(_currentLatLong!);

        if (_tempLocation.length > 2) {
          //var deleteItem = (_markers.length - (_tempLocation.length * 2)) + 1;
          Set<Marker> _newMarkers = _markers
              .where((d) => d.markerId.value.toString() != "pinXY")
              .toSet();
          Set<Marker> _pinMarkers = _markers
              .where((d) => d.markerId.value.toString() == "pinXY")
              .toSet();
          var deleteItem =
              (_newMarkers.length - (_tempLocation.length * 2)) + 1;

          for (var i = 0; i < deleteItem; i++) {
            _newMarkers.remove(_newMarkers.elementAt(_newMarkers.length - 1));
          }
          _markers = {..._newMarkers, ..._pinMarkers}.toSet();

          LatLng center =
              await getCenterLatLong([_tempLocation[0], _tempLocation.last]);
          String dist =
              await calculateDistance(_tempLocation[0], _tempLocation.last);

          ///Create distance marker function
          Uint8List? distanceIcon = await getUint8List(distanceKey);
          _markers.add(Marker(
            markerId: MarkerId("${uniqueID + _distance}"),
            position: center,
            draggable: false,
            icon: BitmapDescriptor.fromBytes(distanceIcon!),
            anchor: const Offset(0.5, 0.5),
          ));

          notifyListeners();

          LatLng center2 =
              await getCenterLatLong([_tempLocation.last, _currentLatLong!]);

          _markers.add(Marker(
            markerId: MarkerId("${uniqueID + dist}"),
            position: center2,
            draggable: false,
            icon: BitmapDescriptor.fromBytes(distanceIcon),
            anchor: const Offset(0.5, 0.5),
          ));

          notifyListeners();

          setNewTempToPolyline(newLocationList);
        } else if (_tempLocation.length == 2) {
          if (_markers.length > 3) {
            Set<Marker> _newMarkers = _markers
                .where((d) => d.markerId.value.toString() != "pinXY")
                .toSet();
            Set<Marker> _pinMarkers = _markers
                .where((d) => d.markerId.value.toString() == "pinXY")
                .toSet();
            var deleteItem = _newMarkers.length - 3;
            for (var i = 0; i < deleteItem; i++) {
              _newMarkers.remove(_newMarkers.elementAt(_newMarkers.length - 1));
            }
            _markers = {..._newMarkers, ..._pinMarkers}.toSet();
            _tempPolygons.clear();
            _polygons.clear();

            _tempPolylines.add(
              Polyline(
                polylineId:
                    PolylineId(uniqueID + newLocationList.length.toString()),
                points: _tempLocation,
                width: 2,
                color: const Color(0xFF2BAD8A),
              ),
            );
            _polylines = _tempPolylines;
            notifyListeners();

            createNewDistanceMarker(_tempLocation.last, _currentLatLong!);

            _tempPolylines.add(
              Polyline(
                polylineId: PolylineId(uniqueID),
                points: newLocationList,
                width: 2,
                color: Colors.white,
              ),
            );
            _polylines = _tempPolylines;
            notifyListeners();
          }
        } else if (_tempLocation.length == 1) {
          if (_markers.length > 1) {
            Set<Marker> _newMarkers = _markers
                .where((d) => d.markerId.value.toString() != "pinXY")
                .toSet();
            Set<Marker> _pinMarkers = _markers
                .where((d) => d.markerId.value.toString() == "pinXY")
                .toSet();
            var deleteItem = _newMarkers.length - 1;
            for (var i = 0; i < deleteItem; i++) {
              _newMarkers.remove(_newMarkers.elementAt(_newMarkers.length - 1));
            }
            _markers = {..._newMarkers, ..._pinMarkers}.toSet();
            createNewDistanceMarker(_tempLocation.last, _currentLatLong!);
            setNewTempToPolyline(newLocationList);
          }
        }
      }
      notifyListeners();
    }
  }

  ///Function to create distance marker
  Future<void> createDistanceMarker(
      LatLng startLocation, LatLng _location) async {
    LatLng center = await getCenterLatLong([startLocation, _location]);
    String dist = await calculateDistance(startLocation, _location);
    _distance = dist;
    _distanceLocation.add(center);
    notifyListeners();

    ///Create distance marker function
    await Future.delayed((Duration(milliseconds: 100)));
    Uint8List? distanceIcon = await getUint8List(distanceKey);

    setMarkerLocation(dist, center, distanceIcon);
    notifyListeners();
  }

  ///Function to set end location marker
  Future<void> createEndLoc(LatLng startLocation, LatLng _location) async {
    LatLng center = await getCenterLatLong([startLocation, _location]);
    String dist = await calculateDistance(startLocation, _location);
    _distance = dist;
    _endLoc = center;
    notifyListeners();

    ///Create distance marker function
    await Future.delayed((Duration(milliseconds: 100)));
    Uint8List? distanceIcon = await getUint8List(distanceKey);
    setMarkerLocation(distance, center, distanceIcon);
    notifyListeners();
  }

  Future<void> removeMarker(LatLng? _loc) async {
    _markers.removeWhere((mark) =>
        mark.position == _loc && mark.markerId.value.toString() != "pinXy");
  }

  void onEditPolygon(FarmDetailModel farmDetailModel) async {
    _onDrag = false;
    _loading = true;
    _tempLocation = [];
    _location = [];
    _polygons.clear();
    _tempPolygons.clear();
    _polygonId = "1";
    _markers.clear();

    await initLocation();

    var latLngCoordinates = farmDetailModel.polygon!
        .map<LatLng>((coord) => LatLng(coord[0], coord[1]))
        .toList();
    _tempLocation = latLngCoordinates;
    setTempToPolygon();

    LatLng centroid = GeoUtility().getCentroid(tempLocation);
    _cameraPosition = CameraPosition(
      target: centroid,
      zoom: 18,
    );

    if (_uniqueID == "") {
      _uniqueID = Random().nextInt(10000).toString();
    }

    for (var i = 0; i < _tempLocation.length; i++) {
      if (i > 0) {
        await createDistanceMarker(_tempLocation[i - 1], _tempLocation[i]);
        if (i > 1) {
          await removeMarker(_endLoc);
          await createEndLoc(_tempLocation[0], _tempLocation[i]);
        }
        _polygonId = (int.parse(_polygonId) + 1).toString();
      }
      await Future.delayed((Duration(milliseconds: 100)));
      Uint8List? markerIcon = await getUint8List(markerKey);
      setMarkerLocation((i + 1).toString(), _tempLocation[i], markerIcon);
    }

    _area = GeoUtility().calcArea(_tempLocation);
    _totalArea = GeoUtility().getAreaInRaiFormat(area);

    LatLngBounds newPos = GeoUtility().createBounds(_tempLocation);

    _loading = false;
    _polygonId = (int.parse(_polygonId) + 1).toString();
    isFirstEdit = false;

    if (newPos.northeast.toString().isNotEmpty &&
        newPos.southwest.toString().isNotEmpty) {
      Future.delayed(const Duration(milliseconds: 500), () {
        _controller!.animateCamera(
          CameraUpdate.newLatLngBounds(newPos, 30),
        );
      });
    }
    notifyListeners();
  }

  ///Function to handle onTap Map and get location
  void onTapMap(LatLng _location) async {
    _onDrag = false;

    ///Find center position between two coordinate
    if (_tempLocation.length > 0) {
      _tempPolylines.clear();
      _polylines.clear();
      if (_markers.length > 0) {
        removeLastMarker();
        //_markers.remove(_markers.elementAt(_markers.length - 1));
      }

      if (_tempLocation.length > 1) {
        ///Remove previous distance first point to last point
        await removeMarker(_endLoc);

        ///Create distance marker for first point to last point
        await createEndLoc(_tempLocation[0], _location);
      }
      await createDistanceMarker(_tempLocation.last, _location);
    }

    _tempLocation.add(_location);
    _polygonId = (int.parse(_polygonId) + 1).toString();

    if (_tempLocation.length > 2) {
      _area = GeoUtility().calcArea(_tempLocation);
      _totalArea = GeoUtility().getAreaInRaiFormat(area);
      notifyListeners();
    }

    if (_uniqueID == "") {
      _uniqueID = Random().nextInt(10000).toString();
    }

    ///Create marker point
    Uint8List? markerIcon = await getUint8List(markerKey);
    setMarkerLocation(_tempLocation.length.toString(), _location, markerIcon);

    if (Platform.isIOS && _tempLocation.length < 3) {
      setTempToPolyline();
    } else {
      setTempToPolygon();
    }

    notifyListeners();
  }

  void onMoveMap(LatLng _currentLocation) async {
    List<LatLng> _newLocation = [];
    _currentLatLong = _currentLocation;
    notifyListeners();

    if (_tempLocation.length > 0) {
      _newLocation.add(_tempLocation.last);
      _newLocation.add(_currentLocation);

      ///Create distance marker for last positions
      await createNewDistanceMarker(_tempLocation.last, _currentLocation);
      setNewTempToPolyline(_newLocation);

      notifyListeners();
    }
  }

  ///Function to create distance marker
  Future<void> createNewDistanceMarker(
      LatLng startLocation, LatLng _location) async {
    LatLng center = await getCenterLatLong([startLocation, _location]);
    String dist = await calculateDistance(startLocation, _location);

    _distance = dist;
    notifyListeners();

    ///Create distance marker function
    await Future.delayed((Duration(milliseconds: 100)));
    Uint8List? distanceIcon = await getUint8List(distanceKey);

    if (_onDrag) {
      if (_markers.length > 0) {
        removeLastMarker();
        // _markers.remove(_markers.elementAt(_markers.length - 1));
      }
      _markers.add(Marker(
        markerId: MarkerId("${uniqueID + dist}"),
        position: center,
        draggable: false,
        icon: BitmapDescriptor.fromBytes(distanceIcon!),
        anchor: const Offset(0.5, 0.5),
      ));
    } else if (!_onDrag) {
      _markers.add(Marker(
        markerId: MarkerId("${uniqueID + dist}"),
        position: center,
        draggable: false,
        icon: BitmapDescriptor.fromBytes(distanceIcon!),
        anchor: const Offset(0.5, 0.5),
      ));
      _onDrag = true;
    }

    notifyListeners();
  }

  void setNewTempToPolyline(List<LatLng> _location) {
    _tempPolylines
        .removeWhere((line) => line.polylineId.value.toString() == uniqueID);

    _tempPolylines.add(
      Polyline(
        polylineId: PolylineId(uniqueID),
        points: _location,
        width: 2,
        color: mapType.name == "satellite" || mapType.name == "hybrid"
            ? Colors.white
            : Colors.black,
      ),
    );
    _polylines = _tempPolylines;
    notifyListeners();
  }

  ///Function to set marker locations
  void setMarkerLocation(String id, LatLng _location, Uint8List? markerIcon,
      {String? title}) async {
    _markers.add(Marker(
      markerId: MarkerId("${uniqueID + id}"),
      position: _location,
      draggable: false,
      icon: BitmapDescriptor.fromBytes(markerIcon!),
      anchor: const Offset(0.5, 0.5),
    ));

    notifyListeners();
  }

  ///Function to set temporary polygons to polygons
  void setTempToPolygon() {
    _tempPolygons.removeWhere((poly) => poly.polygonId.toString() == uniqueID);

    _tempPolygons.add(Polygon(
        polygonId: PolygonId(uniqueID),
        points: _tempLocation,
        strokeWidth: 3,
        fillColor: _polygonColor!.withOpacity(0.3),
        strokeColor: _polygonColor!));

    _polygons = _tempPolygons;
    notifyListeners();
  }

  void setTempToPolyline() {
    _tempPolylines
        .removeWhere((line) => line.polylineId.toString() == uniqueID);

    _tempPolylines.add(
      Polyline(
        polylineId: PolylineId(uniqueID + _tempPolylines.length.toString()),
        points: _tempLocation,
        width: 3,
        color: _polygonColor!,
      ),
    );
    _polylines = _tempPolylines;
    notifyListeners();
  }

  ///Function to save tracking points to database
  void saveTracking(BuildContext context) async {
    if (tempLocation.length >= 3) {
      //isEditing
      _polygone.clear();
      setPolygone(tempLocation);
      LatLng centroid = GeoUtility().getCentroid(tempLocation);
      _cameraPos = CameraPosition(
        target: centroid,
        zoom: 15,
      );

      if (_editMode) {
        _displayBtnUpdatePolygon = true;
      }

      _location = _tempLocation;
      notifyListeners();

      if (isEditing) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => FarmDetailPage()),
        );
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => FarmDetailPage()),
        );
      }
    }
  }

  ///Converting Widget to PNG
  Future<Uint8List?> getUint8List(GlobalKey widgetKey) async {
    RenderRepaintBoundary boundary =
        widgetKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
    var image = await boundary.toImage(pixelRatio: 2.0);
    ByteData? byteData = await (image.toByteData(format: ImageByteFormat.png));
    return byteData?.buffer.asUint8List();
  }

  ///Function to get center location between two coordinate
  Future<LatLng> getCenterLatLong(List<LatLng> latLongList) async {
    double pi = math.pi / 180;
    double xpi = 180 / math.pi;
    double x = 0, y = 0, z = 0;

    if (latLongList.length == 1) {
      return latLongList[0];
    }

    for (int i = 0; i < latLongList.length; i++) {
      double latitude = latLongList[i].latitude * pi;
      double longitude = latLongList[i].longitude * pi;
      double c1 = math.cos(latitude);
      x = x + c1 * math.cos(longitude);
      y = y + c1 * math.sin(longitude);
      z = z + math.sin(latitude);
    }

    int total = latLongList.length;
    x = x / total;
    y = y / total;
    z = z / total;

    double centralLongitude = math.atan2(y, x);
    double centralSquareRoot = math.sqrt(x * x + y * y);
    double centralLatitude = math.atan2(z, centralSquareRoot);

    return LatLng(centralLatitude * xpi, centralLongitude * xpi);
  }

  ///Calculate distance between two location
  Future<String> calculateDistance(
      LatLng firstLocation, LatLng secondLocation) async {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 -
        c((secondLocation.latitude - firstLocation.latitude) * p) / 2 +
        c(firstLocation.latitude * p) *
            c(secondLocation.latitude * p) *
            (1 - c((secondLocation.longitude - firstLocation.longitude) * p)) /
            2;
    var distance = 12742 * asin(sqrt(a));

    if (distance < 1) {
      return (double.parse(distance.toStringAsFixed(3)) * 1000)
              .toString()
              .split(".")[0] +
          " ม.";
    } else {
      return double.parse(distance.toStringAsFixed(2)).toString() + " กม.";
    }
  }

  saveFarm(FarmModel farmModel, Uint8List img, BuildContext context) async {
    setLoading(true);
    ApiResult<FarmResponse> response = await saveFarmService(farmModel);
    response.when(
      success: (data) {
        FarmModelRes? rest = data.data;
        savePolygonImage(farmModel.userId, rest!.id!, img, context);
      },
      failure: (error) {
        setLoading(false);
        print(error);
      },
    );
  }

  savePolygonImage(
      String userId, String farmId, Uint8List img, BuildContext context) async {
    ApiResult<FarmResponse> response =
        await savePolygonImgageService(userId, farmId, img);
    response.when(
      success: (data) {
        if (!_isEditPolygon) {
          clearFarmList();
          getFarm(userId);
          context.router.popUntilRoot();
          context.router.navigate(FarmsRouter());
        }
        _isEditPolygon = false;
        notifyListeners();
      },
      failure: (error) {
        print(error);
      },
    );
  }

  getFarm(String userId) async {
    setLoading(true);
    ApiResult<FarmListResponse> response = await fetchFarmList(userId);
    response.when(
      success: (data) {
        _farmList = data.data!;
        setLoading(false);
        notifyListeners();
      },
      failure: (error) {
        setLoading(false);
        print(error);
      },
    );
  }

  getFarmById(String farmId, String type, BuildContext context,
      bool navigateBack) async {
    ApiResult<FarmDetailResponse> response = await fetchFarmById(farmId, type);
    List<FarmDetailModel?> farmDetailList;
    setLoading(true);

    response.when(
      success: (data) {
        farmDetailList = data.data;
        setFarmDetail(farmDetailList[0]!);

        List<LatLng> polygon = farmDetailList[0]!
            .polygon!
            .map<LatLng>((coord) => LatLng(coord[0], coord[1]))
            .toList();

        _polygone.clear();
        _location = [];
        setTempLocation(polygon);

        _polygone.clear();
        setPolygone(polygon);

        LatLng centroid = GeoUtility().getCentroid(polygon);
        _cameraPos = CameraPosition(
          target: centroid,
          zoom: 15,
        );
        isFirstEdit = true;
        farmNameCtrl.text = farmDetailList.first!.farmName!;
        _displayBtnUpdatePolygon = false;
        showXY = false;
        mapType = MapType.satellite;
        indexSelected = 1;
        notifyListeners();

        onFarmNameChange(farmDetailList.first!.farmName!);
        this.farmId = farmId;

        LandRecomnViewModel landRecomnViewModel =
            Provider.of<LandRecomnViewModel>(context, listen: false);
        landRecomnViewModel.fetchRecomendationList(
            farmId, LandRecomendationType.suggest);
        if (navigateBack) {
          Navigator.of(context).pop();
          LatLngBounds newPos = GeoUtility().createBounds(polygon);

          if (newPos.northeast.toString().isNotEmpty &&
              newPos.southwest.toString().isNotEmpty) {
            Future.delayed(const Duration(milliseconds: 500), () {
              _mapController!.animateCamera(
                CameraUpdate.newLatLngBounds(newPos, 30),
              );
            });
          }
        } else {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => FarmDetailPage()),
          );
        }
        setLoading(false);
      },
      failure: (error) {
        setLoading(false);
        print(error);
      },
    );
  }

  void setPolygone(List<LatLng> _point) {
    _polygone.add(Polygon(
        polygonId: PolygonId(_polygone.length.toString()),
        points: _point,
        strokeColor: const Color(0xFF2BAD8A),
        strokeWidth: 3,
        fillColor: const Color(0xFF2BAD8A).withOpacity(0.2)));
    notifyListeners();
  }

  onFarmNameChange(String text) {
    farmName = text;
    isValid = farmName.isEmpty ? false : true;
    notifyListeners();
  }

  Future editFarmName({required String farmName}) async {
    setLoading(true);
    final ApiResult<CommonResponse> result =
        await updateFarmName(farmId: farmId, farmName: farmName);
    setLoading(false);
    print(result);
  }

  Future<String> onDeleteFarm() async {
    var statusMessage;
    setLoading(true);
    String result = "";
    final ApiResult<CommonResponse> response = await deleteFarm(farmId: farmId);
    response.when(
      success: (data) {
        result = data.message;
      },
      failure: (error) {
        statusMessage = error;
        result = statusMessage.error;
        setLoading(false);
      },
    );
    setLoading(false);
    notifyListeners();
    return result;
  }

  clearAll() {
    // clear polygon
    _tempPolygons.clear();
    _polygons.clear();

    // clear polyline
    _polylines.clear();
    _tempPolylines.clear();

    _tempLocation = [];
    _location = [];

    _markers.clear();
    _totalArea = "";

    _isEditPolygon = false;
    _cameraPosition = null;
    _onInitCamera = false;
    isFirstEdit = true;
    showXY = false;
    mapType = MapType.satellite;
    indexSelected = 1;

    notifyListeners();
  }

  searchPlaces(String searchTerm) async {
    ApiResult<List<PlaceSearch>> response = await getAutocomplete(searchTerm);
    response.when(
        success: (data) {
          setPlaceList(data);
        },
        failure: (error) {});
    notifyListeners();
  }

  setSelectedLocation(String placeId) async {
    ApiResult<Place> response = await searchLocationById(placeId);
    response.when(
        success: (data) {
          _placeList = [];
          _controller!.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(
                  target: LatLng(data.geometry!.location!.lat!,
                      data.geometry!.location!.lng!),
                  zoom: 16.0),
            ),
          );
        },
        failure: (error) {});
    notifyListeners();
  }

  setNewLocation(LatLng latLng) async {
    _controller!.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(target: latLng, zoom: 16.0),
      ),
    );
  }

  updatePolygonImg(
      FarmModel farmModel, Uint8List img, BuildContext context) async {
    //setLoading(true);
    ApiResult<FarmResponse> response =
        await savePolygonImgageService(farmModel.userId, farmId, img);
    response.when(
      success: (data) {
        String imgPath = data.data!.path!;
        updatePolygon(farmModel, img, context, imgPath);
      },
      failure: (error) {
        //setLoading(false);
        print(error);
      },
    );
    notifyListeners();
  }

  updatePolygon(FarmModel farmModel, Uint8List img, BuildContext context,
      String imgPath) async {
    ApiResult<CommonResponse> response =
        await updatepolygon(farmModel, farmId, imgPath);
    response.when(
      success: (data) {
        savePolygonImage(farmModel.userId, farmId, img, context);
        // setLoading(false);
      },
      failure: (error) {
        _isEditPolygon = false;
        // setLoading(false);
        print(error);
      },
    );
    notifyListeners();
  }

  GoogleMapController? _mapController;
  GoogleMapController? get mapController => _mapController;
  setMapController(GoogleMapController controller) {
    _mapController = controller;
    notifyListeners();
  }

  void mapCreated(
      GoogleMapController controller, List<LatLng> positions) async {
    List<LatLng> polygon = [];
    if (_farmDetail != null) {
      polygon = _farmDetail!.polygon!
          .map<LatLng>((coord) => LatLng(coord[0], coord[1]))
          .toList();
    }

    List<LatLng> boundList = _location.isEmpty ? polygon : _location;

    LatLngBounds bound =
        GeoUtility().createBounds(boundList); //createBounds(positions);

    _mapController = controller;

    if (bound.northeast.toString().isNotEmpty &&
        bound.southwest.toString().isNotEmpty) {
      Future.delayed(const Duration(milliseconds: 500), () {
        _mapController!.animateCamera(
          CameraUpdate.newLatLngBounds(bound, 30),
        );
      });
    }
    notifyListeners();
  }

  Future<void> fetchAllFarm(
      DrawFarmViewModel provider, BuildContext context) async {
    final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    provider.getFarm(prefs.getString('userId').toString());

    LandRecomnViewModel landRecomnViewModel =
        Provider.of<LandRecomnViewModel>(context, listen: false);
    landRecomnViewModel.onPageChange(0);
    await landRecomnViewModel.fetchRecomendationList(
        provider.getFarmDetail.id!, LandRecomendationType.suggest);
  }

  // for save new farm , edit polygone
  Future<void> onSave(List<LatLng> tempLoc, DrawFarmViewModel provider,
      BuildContext context) async {
    Uint8List? uin8list;
    final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    List locationList = [];
    uin8list = await _mapController!.takeSnapshot();

    for (int i = 0; i < tempLoc.length; i++) {
      List list = [tempLoc[i].latitude, tempLoc[i].longitude];
      locationList.insert(i, list);
    }

    LatLng centroid = GeoUtility().getCentroid(tempLoc);
    UtmModel utm = GeoUtility().getUtm(centroid);
    double area = GeoUtility().calcArea(tempLoc);

    int raiValue = GeoUtility().getRai(area);
    int ngranValue = GeoUtility().getNgran(area, raiValue);
    double wahValue = GeoUtility().getWha(area, raiValue, ngranValue);

    FarmModel farmModel = FarmModel(
      userId: prefs.getString('userId').toString(),
      farmName: provider.farmNameCtrl.text,
      centroidUTM: UtmModel(grid: "-", zone: utm.zone, x: utm.x, y: utm.y),
      squareWah: double.parse(wahValue.toStringAsFixed(2)),
      ngan: ngranValue < 0 ? 0 : ngranValue,
      rai: raiValue,
      centroidGLO: LatLngModel(lat: centroid.latitude, lng: centroid.longitude),
      polygon: locationList,
    );

    if (provider.isEditPolygon) {
      provider.updatePolygonImg(farmModel, uin8list!, context);
    } else {
      provider.saveFarm(farmModel, uin8list!, context);
    }
  }

  markXYPin(LatLng point) async {
    Uint8List? pinIcon = await getUint8List(pinKey);
    if (!showXY) {
      _markers.removeWhere((mark) => mark.markerId.value.toString() == "pinXY");
      notifyListeners();
    } else {
      _markers.add(Marker(
        markerId: const MarkerId("pinXY"),
        position: point,
        draggable: false,
        icon: BitmapDescriptor.fromBytes(pinIcon!),
      ));
    }
    notifyListeners();
  }
}
