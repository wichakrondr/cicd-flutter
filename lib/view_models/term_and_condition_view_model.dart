import 'package:flutter/material.dart';
import 'package:varun_kanna_app/services/term_and_condtion_service.dart';
import 'package:varun_kanna_app/model/term_and_condition_response.dart';

import '../utils/api/api_result.dart';

class TermAndContionViewModel extends ChangeNotifier {
  static late TermAndContionViewModel instance;
  bool _loading = false;
  List<TermAndConditionModel>? _termAndConditionResponse;
  TermAndConditionModel? _termAndConditionDeviceResponse;
  List<TermAndConditionModel>? _termAndConditionByIdResponse;

  ConsentModel? _consentModel;
  TermAndConditionModel? _lastConsentVersion;

  bool get getLoading => _loading;
  List<TermAndConditionModel>? get getTermAndCondtion =>
      _termAndConditionResponse;

  TermAndConditionModel? get getTermAndCondtionDevice =>
      _termAndConditionDeviceResponse;

  List<TermAndConditionModel>? get getTermAndCondtionById =>
      _termAndConditionByIdResponse;
  setLoading(bool loading) async {
    _loading = loading;
    notifyListeners();
  }

  getTermAndCondtionInfo() async {
    setLoading(true);
    ApiResult<List<TermAndConditionModel>> response =
        await fetchTermAndCondition();

    response.when(
      success: (data) => {_termAndConditionResponse = data},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }

  getTermAndCondtionDeviceServive() async {
    setLoading(true);
    ApiResult<TermAndConditionModel> response =
        await fetchTermAndConditionDevice();

    response.when(
      success: (data) => {_termAndConditionDeviceResponse = data},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }

  getCheckUserConsentService(String tel, String type) async {
    setLoading(true);
    ApiResult<ConsentModel> response = await fetchCheckUserConsent(tel, type);

    response.when(
      success: (data) => {_consentModel = data},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }

  ConsentModel? getCheckUserConsentInfo() {
    return _consentModel;
  }

  //get last consent

  getCheckLastConsentService(String type) async {
    setLoading(true);
    ApiResult<TermAndConditionModel> response =
        await fetchCheckLastConsent(type);

    response.when(
      success: (data) => {_lastConsentVersion = data},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }

  TermAndConditionModel? getCheckLastConsentInfo() {
    return _lastConsentVersion;
  }

  // get last consent \\

  getTermAndCondtionByIdService(String version, String type) async {
    setLoading(true);
    ApiResult<List<TermAndConditionModel>> response =
        await fetchTermAndConditionById(version, type);

    response.when(
      success: (data) => {_termAndConditionByIdResponse = data},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }
}
