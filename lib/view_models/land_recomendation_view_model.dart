import 'package:flutter/material.dart';
import 'package:varun_kanna_app/model/farm_page_model/land_recomendation_model/land_recomendation_model.dart';
import 'package:varun_kanna_app/services/plants_service.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';

class LandRecomnViewModel extends ChangeNotifier {
  bool _loading = false;
  List<RecommendationResponseModel>? _landRecomendationList = [];
  List<PlantModel>? _plantList;
  bool get getLoading => _loading;
  setLoading(bool loading) {
    _loading = loading;
    notifyListeners();
  }

  ValueNotifier<int> index = ValueNotifier<int>(0);

  void onPageChange(int index) {
    this.index.value = index;
    notifyListeners();
  }

  List<RecommendationResponseModel>? get getLandRecomendationList =>
      _landRecomendationList;

  Future fetchRecomendationList(
      String farmId, LandRecomendationType type) async {
    setLoading(true);
    ApiResult<List<RecommendationResponseModel>> response =
        await fetchRecommendationListService(farmId, type);
    response.when(
      success: (data) => {setLandRecommendationList(data)},
      failure: (error) => {print("fetchRecomendation err " + error.toString())},
    );
    setLoading(false);
    notifyListeners();
  }

  void setLandRecommendationList(
      List<RecommendationResponseModel> landRecomendationList) {
    _landRecomendationList = landRecomendationList;
    notifyListeners();
  }

  List<PlantModel>? get getPlantList => _plantList;

  Future fetchPlantList(String project_name) async {
    setLoading(true);
    ApiResult<List<PlantModel>> response =
        project_name == "โครงการปลูกข้าวเปียกสลับแห้ง"
            ? await fetchPlantListService()
            : await fetchPlantListByProjectService(project_name);
    response.when(
      success: (data) => {setPlantList(data)},
      failure: (error) => {print("fetchPlantList err " + error.toString())},
    );
    setLoading(false);
    notifyListeners();
  }

  void setPlantList(List<PlantModel> landRecomendationList) {
    _plantList = landRecomendationList;
    notifyListeners();
  }

  List<RecommendationResponseModel>? get getMockUp => _mockUpPlantsList;

  final List<RecommendationResponseModel>? _mockUpPlantsList = [
    RecommendationResponseModel(crop_recommendation: [
      PlantModel(
          id: '1',
          name_th: 'ข้าว',
          name_en: 'ข้าว',
          suitable_point: 2,
          suitable_status: 'ปานกลาง',
          iconPath: 'assets/images/ic_rice.svg'),
      PlantModel(
          id: '2',
          name_th: 'อ้อย',
          name_en: 'อ้อย',
          suitable_point: 1,
          suitable_status: 'น้อย',
          iconPath: 'assets/images/ic_sugarkane.svg'),
      PlantModel(
          id: '3',
          name_th: 'มันสําปะหลัง',
          name_en: 'มันสําปะหลัง',
          suitable_point: 3,
          suitable_status: 'มาก',
          iconPath: 'assets/images/ic_cassava.svg'),
      PlantModel(
          id: '4',
          name_th: 'ข้าวโพด',
          name_en: 'ข้าวโพด',
          suitable_point: 1,
          suitable_status: 'น้อย',
          iconPath: 'assets/images/ic_corn.svg'),
      PlantModel(
          id: '5',
          name_th: 'ปาล์มนํ้ามัน',
          name_en: 'ปาล์มนํ้ามัน',
          suitable_point: 0,
          suitable_status: 'ไม่มีข้อมูล',
          iconPath: 'assets/images/ic_oilpalm.svg'),
      PlantModel(
          id: '6',
          name_th: 'ยางพารา',
          name_en: 'ยางพารา',
          suitable_point: 0,
          suitable_status: 'ไม่เหมาะสม',
          iconPath: 'assets/images/ic_pararubber.svg'),
    ], soil_series: [], id: ''),
  ];
}
