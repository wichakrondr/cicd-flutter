import 'package:flutter/material.dart';
import 'package:varun_kanna_app/model/district_response.dart';
import 'package:varun_kanna_app/model/image_response.dart';
import 'package:varun_kanna_app/model/profile_model.dart';
import 'package:varun_kanna_app/model/province_response.dart';
import 'package:varun_kanna_app/model/sub_district_response.dart';
import 'package:varun_kanna_app/services/image_service.dart';
import 'package:varun_kanna_app/services/profile_service.dart';
import 'package:varun_kanna_app/services/user_service.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';

import '../services/address_service.dart';

class ProfileViewModel extends ChangeNotifier {
  bool _loading = false;
  bool _isLogin = false;
  List<Province> _provinceList = [];
  ProfileModel? _profileInfo;
  ProfileModel? _signDeviceInfo;
  
  List<DistrictModel> _districtList = [];

  List<SubDistrictModel> _subDistrictList = [];

  ImageModel? _imageInfo;

  double? _scrollOffset = 0.0;

  ProfileViewModel() {
    getProvince();
  }

  bool get getLoading => _loading;
  double get scrollOffset => _scrollOffset!;

  List<Province> get getProvinceList => _provinceList;
  List<DistrictModel> get getDistrictList => _districtList;
  List<SubDistrictModel> get getSubDistrictList => _subDistrictList;

  ProfileModel? get getProfile => _profileInfo;
  bool get getLogin => _isLogin;

  setScrollOffset(double scrollOffset) {
    _scrollOffset = scrollOffset;
    notifyListeners();
  }

  setLogin(bool isLogin) {
    _isLogin = isLogin;
    notifyListeners();
  }

  setLoading(bool loading) async {
    _loading = loading;
    notifyListeners();
  }

  setProvinceList(List<Province> provinceList) {
    _provinceList = provinceList;
    notifyListeners();
  }

  getProvince() async {
    setLoading(true);
    ApiResult<List<Province>> response = await fetchProvince();
    response.when(
      success: (data) => {setProvinceList(data)},
      failure: (error) => {print(error)},
    );
    setLoading(false);
  }

  //*** district part\\
  setDistrictList(List<DistrictModel> DistrictList) {
    _districtList = DistrictList;
    notifyListeners();
  }

  getDistrict(int provinceId) async {
    setLoading(true);
    ApiResult<List<DistrictModel>> response = await fetchDistrict(provinceId);
    response.when(
      success: (data) => {setDistrictList(data)},
      failure: (error) => {print(error)},
    );

    setLoading(false);
  }
  // district part *** \\

  //*** sub district part\\
  setSubDistrictList(List<SubDistrictModel> SubDistrictList) {
    _subDistrictList = SubDistrictList;
    notifyListeners();
  }

  getSubDistrict(int amphureId) async {
    setLoading(true);
    ApiResult<List<SubDistrictModel>> response =
        await fetchSubDistrict(amphureId);
    response.when(
      success: (data) => {setSubDistrictList(data)},
      failure: (error) => {print(error)},
    );

    setLoading(false);
  }
  // sub district part *** \\

  setProfile(ProfileModel? profile) {
    _profileInfo = profile;
    notifyListeners();
  }

  getProfileService(String phoneNumber) async {
    setLoading(true);
    ApiResult<ProfileModel> response = await fetchProfile(phoneNumber);
    response.when(
      success: (data) {
        setProfile(data);
        fetchImageSerive(_profileInfo?.url_img ?? "");
      },
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }

  //*** saveProfileService\\
  saveProfileService(ProfileModel profileModel) async {
    setLoading(true);
    ApiResult<ProfileResponse> response = await signConsent(profileModel);

    response.when(
      success: (data) => {_profileInfo = data.data},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }

  signConsentDeviceService(ProfileModel profileModel) async {
    setLoading(true);
    ApiResult<ProfileResponse> response = await signConsentDevice(profileModel);

    response.when(
      success: (data) => {_signDeviceInfo = data.data},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }
  // saveProfileService *** \\

  fetchImageSerive(String path) async {
    setLoading(true);
    ApiResult<ImageModel> response = await fetchImage(path);
    response.when(
      success: (data) => {setImageService(data)},
      failure: (error) => {print(error)},
    );

    setLoading(false);
    notifyListeners();
  }

  ImageModel? getImage() {
    return _imageInfo;
  }

  void setImageService(ImageModel? imageModel) {
    _imageInfo = imageModel;
    notifyListeners();
  }

  Future<bool> deleteProfileService(String userId) async {
    setLoading(true);
    bool isSuccess = false;
    ApiResult<ProfileDeleteResponse> response = await deleteProfile(userId);
    response.when(
      success: (data) => {isSuccess = data.status!},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
    return isSuccess;
  }

  Future<bool> updateAddressService(ProfileModel profileModel) async {
    setLoading(true);
    bool isSuccess = false;
    ApiResult<ProfileDeleteResponse> response =
        await updateUserAddress(profileModel);
    response.when(
      success: (data) => {isSuccess = data.status!},
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
    return isSuccess;
  }

  void clearState() {
    _profileInfo = null;
    notifyListeners();
  }
}
