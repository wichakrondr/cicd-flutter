import 'package:flutter/material.dart';

class NavigateIndexViewModel extends ChangeNotifier {
  int activeIndex = 0;

  int getActiveIndex() {
    return activeIndex;
  }

  void setActiveIndex(int index) {
    activeIndex = index;
    notifyListeners();
  }
}
