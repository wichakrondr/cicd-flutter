import 'package:flutter/material.dart';
import 'package:varun_kanna_app/model/otp_response.dart';
import 'package:varun_kanna_app/model/term_and_condition_response.dart';
import 'package:varun_kanna_app/services/user_service.dart';

import '../utils/api/api_result.dart';

class LoginViewModel extends ChangeNotifier {
  bool _loading = false;
  bool get getLoading => _loading;

  TermAndConditionResponse? _termAndConditionResponse;
  TermAndConditionResponse? get getTermAndCondtion => _termAndConditionResponse;

  RegisterResponse? _registerResponse;
  RegisterResponse? get getOTPResponse => _registerResponse;

  RegisterResponse? _verifyOTPResponse;
  RegisterResponse? get getVerifyOTPResponse => _verifyOTPResponse;

  String _refCode = "";
  bool? _isActice;
  String _phoneNumber = "";

  String _userId = "";

  bool? _isRedirect;

  setLoading(bool loading) async {
    _loading = loading;
    notifyListeners();
  }

  loginWithPhoneNumService(String tel) async {
    setLoading(true);
    ApiResult<RegisterResponse> response = await loginWithPhoneNumber(tel);

    response.when(
      success: (data) => {_registerResponse = data},
      failure: (error) => {_registerResponse?.status = false},
    );
    setLoading(false);
    notifyListeners();
  }

  resentOTPService(String phoneNumber) async {
    setLoading(true);
    ApiResult<RegisterResponse> response = await resentOTP(phoneNumber);

    response.when(
      success: (data) => {_registerResponse = data},
      failure: (error) => {_registerResponse?.status = false},
    );
    setLoading(false);
    notifyListeners();
  }

  verifyOTPService(
    String tel,
    String otp,
    String refCode,
    bool isActive,
  ) async {
    var statusMessage;
    setLoading(true);
    ApiResult<RegisterResponse> response = isActive
        ? await verifyOTP(tel, otp, refCode)
        : await verifyOTPNewUser(tel, otp, refCode);

    response.when(
      success: (data) => {_registerResponse = data},
      failure: (error) => {
        _registerResponse?.status = false,
        statusMessage = error,
        statusMessage = statusMessage.error,
        if (statusMessage == "otp expire")
          {_registerResponse!.data!.isExpire = true}
      },
    );
    setLoading(false);
    notifyListeners();
  }

  registerService(String tel) async {
    setLoading(true);
    ApiResult<RegisterResponse> response = await registerWithPhoneNumber(tel);

    response.when(
      success: (data) => {_registerResponse = data},
      failure: (error) => {_registerResponse?.status = false},
    );
    setLoading(false);
    notifyListeners();
  }

  String getRefCode() {
    return _refCode;
  }

  bool? getIsActive() {
    return _isActice;
  }

  void setIsActive(bool isActiveUser) {
    _isActice = isActiveUser;
    notifyListeners();
  }

  String getPhoneNumber() {
    return _phoneNumber;
  }

  void setRefCode(String refCode) {
    _refCode = refCode;
    notifyListeners();
  }

  void setPhoneNumber(String phoneNumber) {
    _phoneNumber = phoneNumber;
    notifyListeners();
  }

  void setUserId(String userId) {
    _userId = userId;
    notifyListeners();
  }

  String getUserId() {
    return _userId;
  }

  bool? getIsRedirect() {
    return _isRedirect;
  }

  void setIsRedirect(bool isRedirect) {
    _isRedirect = isRedirect;
    notifyListeners();
  }
}
