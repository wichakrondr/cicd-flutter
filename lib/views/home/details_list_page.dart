import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/news_content.dart';
import 'package:varun_kanna_app/services/news_content_service.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:varun_kanna_app/widget/carousel_cards.dart';

class DetailsListPage extends StatefulWidget {
  final String title;

  const DetailsListPage({required this.title, Key? key}) : super(key: key);

  @override
  _DetailsListPageState createState() => _DetailsListPageState();
}

class _DetailsListPageState extends State<DetailsListPage> {
  int indexFilter = 0;

  Widget filterBar1() {
    return Container(
        margin: const EdgeInsets.only(top: 10),
        width: MediaQuery.of(context).size.width * 0.9,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          TextButton(
              onPressed: () {
                setState(() {
                  indexFilter = 0;
                });
              },
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap),
              child: Container(
                  padding: const EdgeInsets.all(7),
                  alignment: Alignment.center,
                  width: 100,
                  child: Row(children: [
                    Icon(Icons.filter_list,
                        color: indexFilter == 0 ? Colors.white : Colors.black,
                        size: 24.0),
                    Text('ทั้งหมด',
                        style: TextStyle(
                            color: indexFilter == 0
                                ? Colors.white
                                : Colors.black)),
                  ]),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                      color: indexFilter == 0 ? Colors.black : Colors.white))),
          TextButton(
              onPressed: () {
                setState(() {
                  indexFilter = 1;
                });
              },
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap),
              child: Container(
                  padding: const EdgeInsets.all(10),
                  alignment: Alignment.center,
                  width: 100,
                  child: Text('ที่ถูกใจไว้',
                      style: TextStyle(
                          color:
                              indexFilter == 1 ? Colors.white : Colors.black)),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                      color: indexFilter == 1 ? Colors.black : Colors.white))),
          TextButton(
              onPressed: () {
                setState(() {
                  indexFilter = 2;
                });
              },
              style: TextButton.styleFrom(
                  padding: EdgeInsets.zero,
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap),
              child: Container(
                  padding: const EdgeInsets.all(10),
                  alignment: Alignment.center,
                  width: 100,
                  child: Text('สภาพอากาศ',
                      style: TextStyle(
                          color:
                              indexFilter == 2 ? Colors.white : Colors.black)),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      border: Border.all(color: Colors.black),
                      color: indexFilter == 2 ? Colors.black : Colors.white)))
        ]));
  }

  Widget filterBar2() {
    return Container(
      padding: const EdgeInsets.only(left: 38, right: 40),
      margin: const EdgeInsets.only(top: 10),
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.centerLeft,
      child: TextButton(
          onPressed: () {
            setState(() {
              indexFilter = 3;
            });
          },
          style: TextButton.styleFrom(
              padding: EdgeInsets.zero,
              tapTargetSize: MaterialTapTargetSize.shrinkWrap),
          child: Container(
              padding: const EdgeInsets.all(10),
              alignment: Alignment.center,
              width: 100,
              child: Text('ราคาตลาด',
                  style: TextStyle(
                      color: indexFilter == 3 ? Colors.white : Colors.black)),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: Colors.black),
                  color: indexFilter == 3 ? Colors.black : Colors.white))),
    );
  }

  Widget AppBar() {
    return Container(
      padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
      height: MediaQuery.of(context).size.height * 0.13,
      decoration: const BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(
            style: TextButton.styleFrom(
              padding: EdgeInsets.zero,
              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            ),
            child: const Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {
              context.router.pop();
            },
          ),
          Expanded(child: LayoutBuilder(builder: (context, constrait) {
            final padding =
                (MediaQuery.of(context).size.width - constrait.maxWidth) - 30;
            return Padding(
              padding: EdgeInsets.only(right: padding),
              child: Center(
                  child: Text(widget.title,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 24,
                        fontFamily: 'Sarabun',
                      ))),
            );
          })),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    NewsContentService newsContentService =
        Provider.of<NewsContentService>(context, listen: false);
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        children: [
          AppBar(),
          // filterBar1(),
          // filterBar2(),
          Consumer<NewsContentService>(builder: (context, item, child) {
            return Expanded(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                child: ListView.builder(
                    physics: const ClampingScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: indexFilter == 1
                        ? item.getFavList().length
                        : item.getSelectedItemList().length,
                    itemBuilder: (context, index) {
                      NewsContent data;
                      indexFilter == 1
                          ? data = item.getFavList()[index]
                          : data = item.getSelectedItemList()[index];

                      return CardList(
                          text: data.text,
                          imageName: data.imageName,
                          title: data.title,
                          isFav: data.isFav,
                          onPressed: () {
                            newsContentService.selectedItem = data;
                            context.router
                                .push(DetailsRoute(onFavPressed: () {}));
                          });
                    }),
              ),
            );
          }),
        ],
      ),
    );
  }
}
