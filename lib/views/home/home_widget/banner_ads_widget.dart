import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class BannerAdsWidget extends StatelessWidget {
  const BannerAdsWidget({
    Key? key,
    required this.width,
    this.height,
    required this.imageUrl,
    required this.embeddedLinkUrl,
  }) : super(key: key);

  final double width;
  final double? height;
  final String imageUrl;
  final String embeddedLinkUrl;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        if (await canLaunchUrl(Uri.parse(embeddedLinkUrl))) {
          await launchUrl(
            Uri.parse(embeddedLinkUrl),
            mode: LaunchMode.inAppWebView,
            webViewConfiguration:
                const WebViewConfiguration(enableJavaScript: true),
          );
        } else {
          throw 'Could not launch $embeddedLinkUrl ';
        }
      },
      child: Container(
        width: width,
        decoration: BoxDecoration(
            color: Color.fromARGB(255, 232, 232, 232),
            borderRadius: BorderRadius.all(
                Radius.circular(height != null ? 22.5 : 12.5))),
        alignment: Alignment.center,
        margin: const EdgeInsets.all(20),
        child: ClipRRect(
          borderRadius:
              BorderRadius.all(Radius.circular(height != null ? 22.5 : 12.5)),
          child: CachedNetworkImage(
            imageUrl: imageUrl,
            height: height ?? 200,
            width: width,
            fit: BoxFit.fill,
          ),
        ),
      ),
    );
  }
}
