import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:varun_kanna_app/model/news_content.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/services/news_content_service.dart';
import 'package:auto_route/auto_route.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:varun_kanna_app/widget/carousel_cards.dart';
import 'package:varun_kanna_app/widget/custom_app_bar.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';

import '../../widget/custom_dot_indicator.dart';
import 'home_widget/banner_ads_widget.dart';

import '../../widget/custom_dot_indicator.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final FirebaseRemoteConfig _remoteConfig = FirebaseRemoteConfig.instance;
  final _pageController = PageController();
  final _currentPpage = ValueNotifier<int>(0);
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences? prefs;
  bool isCurrentVersion = true;
  // Fetching, caching, and activating remote config
  void _initConfig() async {
    prefs = await _prefs;
    setState(() {
      isCurrentVersion = prefs!.getBool("isCurrentVersion")!;
    });
  }

  @override
  void initState() {
    _initConfig();
    super.initState();
    startTime();
  }

  startTime() async {
    const duration = const Duration(seconds: 2);
    return Timer(duration, checkNotiPermission);
  }

  void checkNotiPermission() async {
    Map<Permission, PermissionStatus> statuses =
        await [Permission.notification].request();

    //print("location: " + statuses[Permission.notification].toString());
    //checkLocationPermission();
  }

  void checkLocationPermission() async {
    Map<Permission, PermissionStatus> statuses =
        await [Permission.location].request();
    //print("location: " + statuses[Permission.location].toString());
  }

  void update() {
    if (Platform.isAndroid || Platform.isIOS) {
      final appId = Platform.isAndroid ? 'varuna.agritech.kanna' : "1635506824";
      final url = Uri.parse(
        Platform.isAndroid
            ? "https://play.google.com/store/apps/details?id=$appId"
            : "https://apps.apple.com/app/id$appId",
      );
      launchUrl(
        url,
        mode: LaunchMode.externalApplication,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    NewsContentService newsContentService =
        Provider.of<NewsContentService>(context, listen: false);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    String latest_version = _remoteConfig.getString('latest_version');
    return Scaffold(
      appBar: const CustomAppBar(title: "KANNA"),
      body: Consumer<NewsContentService>(builder: (context, provider, _) {
        return WillPopScope(
          onWillPop: _onWillPop,
          child: Stack(
            children: [
              SingleChildScrollView(
                physics: const ClampingScrollPhysics(),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                            // padding:
                            //     EdgeInsets.only(top: height * 0.15), //height * 0.08
                            child: Column(children: [
                          // weatherInfo(),
                          provider.isLoading
                              ? Container(
                                  width: width * 0.92,
                                  height: 170,
                                  alignment: Alignment.center,
                                  margin: const EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                    color: Color.fromARGB(255, 232, 232, 232),
                                    borderRadius: BorderRadius.circular(12.5),
                                  ),
                                )
                              : bannerSlider(newsContentService),
                          CustomCircleDotIndicators(
                              itemCount: provider.isLoading
                                  ? 1
                                  : newsContentService.adsList.length,
                              index: provider.isLoading
                                  ? ValueNotifier<int>(0)
                                  : newsContentService.index),
                          // menuBar(),
                          CarouselCards(
                            title: "ความรู้",
                            cardList:
                                newsContentService.getknowledgeList().length > 4
                                    ? newsContentService
                                        .getknowledgeList()
                                        .sublist(0, 5)
                                    : newsContentService.getknowledgeList(),
                            onPressed: (NewsContent selectedknowledge) {
                              newsContentService.selectedItem =
                                  selectedknowledge;
                              context.router.push(DetailsRoute());
                            },
                            onSeeAllPressed: () {
                              //newsContentService.setTitleText ("ความรู้");
                              newsContentService.setSelectedItemList(
                                  newsContentService.getknowledgeList());
                              context.router
                                  .push(DetailsListRoute(title: "ความรู้"));
                            },
                          ),
                          CarouselCards(
                            title: "ข่าวสาร",
                            cardList: newsContentService.getNewsList().length >
                                    4
                                ? newsContentService.getNewsList().sublist(0, 5)
                                : newsContentService.getNewsList(),
                            onPressed: (NewsContent selectedNews) {
                              newsContentService.selectedItem = selectedNews;
                              context.router
                                  .push(DetailsRoute(onFavPressed: () {}));
                            },
                            onSeeAllPressed: () {
                              newsContentService.setSelectedItemList(
                                  newsContentService.getNewsList());
                              context.router
                                  .push(DetailsListRoute(title: "ข่าวสาร"));
                            },
                          )
                        ])),
                      ],
                    ),
                    SizedBox(height: MediaQuery.of(context).size.height * 0.125)
                  ],
                ),
              ),
              !isCurrentVersion
                  ? Center(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        padding: const EdgeInsets.only(bottom: 60),
                        color: Colors.white.withOpacity(0.5),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            AlertDialog(
                              shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20.0))),
                              icon: SvgPicture.asset(
                                "assets/images/ic_update.svg",
                                width: 120,
                                height: 140,
                              ),
                              // <-- SEE HERE
                              title: const Text(''),
                              content: SingleChildScrollView(
                                child: ListBody(
                                  children: <Widget>[
                                    Text(
                                      'อัพเดตแอปของคุณเป็นเวอร์ชั่นใหม่?',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      'เราได้มีการเพิ่มและปรับปรุงฟีเจอร์ต่างๆ กรุณากด "อัพเดตตอนนี้"',
                                      style: TextStyle(),
                                      textAlign: TextAlign.center,
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          TextButton(
                                            style: TextButton.styleFrom(
                                              primary: Colors.white,
                                              textStyle:
                                                  const TextStyle(fontSize: 20),
                                              backgroundColor: Colors.black,
                                              padding:
                                                  const EdgeInsets.all(15.0),
                                              side: const BorderSide(
                                                  color: Color(0XFFCBD5E0),
                                                  width: 1),
                                              shape:
                                                  const RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  12.5))),
                                            ),
                                            onPressed: () {
                                              update();
                                            },
                                            child: const Text(
                                              "อัพเดตตอนนี้",
                                              style: TextStyle(fontSize: 18),
                                            ),
                                          ),
                                        ])
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : SizedBox(),
            ],
          ),
        );
      }),
    );
  }

  Widget weatherInfo() {
    return Container(
        decoration: const BoxDecoration(
            color: Color.fromARGB(255, 232, 232, 232),
            borderRadius: BorderRadius.all(Radius.circular(30))),
        alignment: Alignment.center,
        margin: const EdgeInsets.all(20),
        width: MediaQuery.of(context).size.width,
        height: 200,
        child: const Text("Weather Station"));
  }

  Widget bannerSlider(NewsContentService newsContentService) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return CarouselSlider.builder(
      itemCount: newsContentService.adsList.length,
      itemBuilder: ((context, index, realIndex) {
        return BannerAdsWidget(
          embeddedLinkUrl: newsContentService.adsList[index].link ?? "",
          imageUrl: newsContentService.adsList[index].path ?? "",
          width: width > 800 ? width * 0.8 : width,
          height: width > 800 ? 350 : null,
        );
      }),
      options: CarouselOptions(
        height: width > 800 ? height * 0.27 : 210,
        aspectRatio: 16 / 9,
        viewportFraction: 1,
        initialPage: 0,
        enableInfiniteScroll: false,
        reverse: false,
        autoPlay: true,
        autoPlayInterval: const Duration(seconds: 3),
        autoPlayAnimationDuration: const Duration(milliseconds: 1000),
        autoPlayCurve: Curves.fastOutSlowIn,
        enlargeCenterPage: true,
        onPageChanged: (index, c) {
          newsContentService.onPageChange(index);
        },
        scrollDirection: Axis.horizontal,
      ),
    );
  }

  Widget menuBar() {
    return Container(
      decoration: const BoxDecoration(
          color: Color.fromARGB(255, 232, 232, 232),
          borderRadius: BorderRadius.all(Radius.circular(30))),
      margin: const EdgeInsets.only(top: 5, left: 20, right: 20),
      width: MediaQuery.of(context).size.width,
      height: 100,
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Container(
          width: 50,
          height: 50,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 203, 202, 202),
              borderRadius: BorderRadius.all(Radius.circular(10))),
        ),
        Container(
          width: 50,
          height: 50,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 203, 202, 202),
              borderRadius: BorderRadius.all(Radius.circular(10))),
        ),
        Container(
          width: 50,
          height: 50,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 203, 202, 202),
              borderRadius: BorderRadius.all(Radius.circular(10))),
        ),
        Container(
          width: 50,
          height: 50,
          decoration: const BoxDecoration(
              color: Color.fromARGB(255, 203, 202, 202),
              borderRadius: BorderRadius.all(Radius.circular(10))),
        )
      ]),
    );
  }

  Future<bool> _onWillPop() async {
    return false;
  }
}
