import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:varun_kanna_app/model/news_content.dart';
import 'package:varun_kanna_app/services/news_content_service.dart';
import 'package:varun_kanna_app/widget/fav_icon.dart';

class DetailsPage extends StatefulWidget {
  final Function()? onFavPressed;
  const DetailsPage({this.onFavPressed, Key? key}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  ScrollController scrollController = ScrollController();
  bool showbtn = false;
  int selectedIndex = 0;
  NewsContent? selectedItem;

  void onItemTapped(int index) {
    if (index == 2) {
      return;
    }
    setState(() {
      selectedIndex = index;
    });
  }

  @override
  void initState() {
    scrollController.addListener(() {
      //scroll listener
      double showoffset =
          10.0; //Back to top botton will show on scroll offset 10.0
      setState(() {
        showbtn = scrollController.offset > showoffset;
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose(); // dispose the controller
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    NewsContentService newsContentService =
        Provider.of<NewsContentService>(context, listen: false);
    selectedItem = newsContentService.selectedItem;

    return Scaffold(
      body: Consumer<NewsContentService>(builder: (context, item, child) {
        return Stack(
          alignment: Alignment.bottomRight,
          children: [
            Container(
              child: (Column(
                children: [
                  Container(
                    height: 42.0,
                    color: Colors.black,
                  ),
                  Stack(
                    children: [
                      Container(
                        height: 200,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image:
                                    AssetImage(item.selectedItem!.imageName))),
                      ),
                      Align(
                        alignment: Alignment.topLeft,
                        child: (GestureDetector(
                          onTap: () => Navigator.pop(context),
                          child: (Container(
                            height: 30,
                            width: 30,
                            margin: const EdgeInsets.only(left: 20, top: 20),
                            decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: const Icon(Icons.navigate_before,
                                color: Colors.white),
                          )),
                        )),
                      ),
                      // Align(
                      //   alignment: Alignment.topRight,
                      //   child: FavIcon(
                      //       isFav: false, onFavPressed: widget.onFavPressed!),
                      // ),
                    ],
                  ),
                  Expanded(
                    flex: 1,
                    child: SingleChildScrollView(
                      controller: scrollController,
                      child: (Container(
                        padding:
                            const EdgeInsets.only(left: 15, right: 15, top: 10),
                        child: (Column(
                          children: [
                            Text(item.selectedItem!.title,
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16,
                                    fontFamily: 'Sarabun',
                                    height: 1.4)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Text("ผู้เขียน: " + item.selectedItem!.writer!,
                                    textAlign: TextAlign.left,
                                    style: const TextStyle(
                                        color: Colors.black38,
                                        fontSize: 14,
                                        fontFamily: 'Sarabun',
                                        height: 1.4)),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/ic_content_date.svg',
                                        color: Colors.black38,
                                        width: 16,
                                        height: 16,
                                      ),
                                      const SizedBox(width: 5),
                                      Text(item.selectedItem!.createDate!,
                                          textAlign: TextAlign.left,
                                          style: const TextStyle(
                                              color: Colors.black38,
                                              fontSize: 14,
                                              fontFamily: 'Sarabun',
                                              height: 1.4)),
                                      const SizedBox(width: 10),
                                      /* SvgPicture.asset(
                                        'assets/images/ic_see.svg',
                                        color: Colors.black38,
                                        width: 16,
                                        height: 16,
                                      ),
                                      const SizedBox(width: 5),
                                      const Text("377 การดู",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              color: Colors.black38,
                                              fontSize: 14,
                                              fontFamily: 'Sarabun',
                                              height: 1.4)),*/
                                    ]),
                                // FavIcon(isFav: false),
                              ],
                            ),
                            const SizedBox(height: 5),
                            Linkify(
                                onOpen: (link) async {
                                  // ignore: deprecated_member_use
                                  if (await canLaunch(link.url)) {
                                    await launch(link.url,
                                        forceSafariVC: true,
                                        forceWebView: true,
                                        enableJavaScript: true);
                                  } else {
                                    throw 'Could not launch $link';
                                  }
                                },
                                text: "   " + item.selectedItem!.text,
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontFamily: 'Sarabun',
                                    fontSize: 14,
                                    height: 1.4)),
                            const SizedBox(height: 130),
                          ],
                        )),
                      )),
                    ),
                  )
                ],
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: showbtn == false
                  ? null
                  : SizedBox(
                      height: 40,
                      width: 40,
                      child: FittedBox(
                        child: (FloatingActionButton(
                            onPressed: () {
                              scrollController.animateTo(
                                  //go to top of scroll
                                  0, //scroll offset to go
                                  duration: const Duration(
                                      milliseconds: 500), //duration of scroll
                                  curve: Curves.fastOutSlowIn //scroll type
                                  );
                            },
                            child: const Icon(
                              Icons.keyboard_arrow_up,
                              color: Colors.white,
                            ),
                            heroTag: "btn2",
                            backgroundColor: Colors.black)),
                      )),
            ),
          ],
        );
      }),
    );
  }
}
