import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/views/market/verification_identity_page/verification_identity_page_provider.dart';
import 'package:varun_kanna_app/views/market/verify_main_page.dart';
import 'package:varun_kanna_app/views/market/verify_main_page_provider.dart';
import 'package:varun_kanna_app/widget/custom_camera/custom_cupertino_pop_up.dart';
import 'package:varun_kanna_app/widget/input_text.dart';
import 'package:varun_kanna_app/widget/multi_box_with_img.dart';
import 'package:varun_kanna_app/widget/preview_image.dart';

class VerifyPage extends StatelessWidget {
  const VerifyPage({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ConsentPageProvider(),
      child: Consumer2<ConsentPageProvider, VerifyMainPageProvider>(builder: (
        context,
        provider,
        mainPageProvider,
        _,
      ) {
        void validField() {
          if (mainPageProvider.firstNameCtrl.text.isEmpty ||
              mainPageProvider.lastNameCtrl.text.isEmpty ||
              (mainPageProvider.idCardSignUrlImg.isEmpty &&
                  mainPageProvider.idCardImgPath.isEmpty) ||
              (mainPageProvider.copyHouseRegistrationSignUrlImg.isEmpty &&
                  mainPageProvider.copyHouseRegistrationImgPath.isEmpty)) {
            mainPageProvider.setIsEnableNextBtnPage0(false);
          } else {
            mainPageProvider.setIsEnableNextBtnPage0(true);
          }
        }

        showImageSource(BuildContext context, SourceImgType source) async {
          final result = await CustomCupertinoPopUp.showActionSheet(context);
          if (result.isEmpty) {
            return;
          }
          switch (source) {
            case SourceImgType.idCardBookImg:
              await mainPageProvider.saveImage(
                  result, SourceImgType.idCardBookImg, "id_card_img");
              validField();
              break;
            case SourceImgType.houseRegBookImg:
              await mainPageProvider.saveImage(
                  result, SourceImgType.houseRegBookImg, "house_reg_img");
              validField();
              break;
            default:
          }
        }

        Future<bool> _onWillPop() async {
          return false;
        }

        return WillPopScope(
            onWillPop: _onWillPop,
            child: GestureDetector(
                child: SizedBox(
              child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      Column(children: [
                        Container(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, bottom: 20),
                            child: Column(
                              children: [
                                InputText(
                                    isOnlyLetters: true,
                                    isEnable: mainPageProvider.getEnableFields,
                                    onChanged: (value) => validField(),
                                    autoFocus: false,
                                    isRequire: true,
                                    title: "ชื่อจริง",
                                    hintText: "กรอก",
                                    controller: mainPageProvider.firstNameCtrl),
                                InputText(
                                    isOnlyLetters: true,
                                    isEnable: mainPageProvider.getEnableFields,
                                    onChanged: (value) => validField(),
                                    autoFocus: false,
                                    isRequire: true,
                                    title: "นามสกุล",
                                    hintText: "กรอก",
                                    controller: mainPageProvider.lastNameCtrl),
                                Image.asset(
                                  'assets/images/img_id_card.png',
                                  width: 200,
                                  height: 200,
                                ),
                                MultiBoxWithImg(
                                    isEnable: mainPageProvider.getEnableFields,
                                    isRequire: true,
                                    showInFo: false,
                                    imgPath: "assets/images/ic_id_card.svg",
                                    onTap: () async {
                                      mainPageProvider.setScrollOffset(
                                          mainPageProvider
                                              .scrollController.offset);

                                      await showImageSource(
                                          context, SourceImgType.idCardBookImg);
                                    },
                                    hintText: "เพิ่มรูปภาพ",
                                    title: "รูปบัตรประชาชน",
                                    widgetImage: mainPageProvider
                                            .idCardSignUrlImg.isEmpty
                                        ? null
                                        : [
                                            for (int i = 0;
                                                i <
                                                    mainPageProvider
                                                        .idCardSignUrlImg
                                                        .length;
                                                i++)
                                              PreviewImageWidget(
                                                isEnable: mainPageProvider
                                                    .getEnableFields,
                                                clearImage: () {
                                                  mainPageProvider
                                                      .clearIdCardImg(i);
                                                  validField();
                                                },
                                                imagePathString:
                                                    mainPageProvider
                                                        .idCardSignUrlImg[i],
                                                imagePath: Uint8List(0),
                                              )
                                          ],
                                    maxImage: 5,
                                    imagePath:
                                        mainPageProvider.idCardSignUrlImg),
                                MultiBoxWithImg(
                                    isEnable: mainPageProvider.getEnableFields,
                                    isRequire: true,
                                    imgPath: "assets/images/ic_document.svg",
                                    onTap: () async {
                                      mainPageProvider.setScrollOffset(
                                          mainPageProvider
                                              .scrollController.offset);
                                      await showImageSource(context,
                                          SourceImgType.houseRegBookImg);
                                    },
                                    hintText: "เพิ่มรูปภาพ",
                                    title: "สำเนาทะเบียนบ้าน",
                                    onTapInFoIcon: () async {
                                      await PreviewImageWidget(
                                        clearImage: () {},
                                        imagePath: mainPageProvider.imagePath,
                                        isInfoTapped: true,
                                        infoPath:
                                            'assets/images/house_reg_ic.png',
                                      ).dialogPreview(context);
                                    },
                                    widgetImage: mainPageProvider
                                            .copyHouseRegistrationSignUrlImg
                                            .isEmpty
                                        ? null
                                        : [
                                            for (int i = 0;
                                                i <
                                                    mainPageProvider
                                                        .copyHouseRegistrationSignUrlImg
                                                        .length;
                                                i++)
                                              PreviewImageWidget(
                                                isEnable: mainPageProvider
                                                    .getEnableFields,
                                                clearImage: () {
                                                  mainPageProvider
                                                      .clearHouseRegImage(i);
                                                  validField();
                                                },
                                                imagePathString: mainPageProvider
                                                    .copyHouseRegistrationSignUrlImg[i],
                                                imagePath: Uint8List(0),
                                              )
                                          ],
                                    maxImage: 5,
                                    imagePath: mainPageProvider
                                        .copyHouseRegistrationSignUrlImg)
                              ],
                            )),
                        const SizedBox(
                          height: 15,
                        ),
                      ]),
                    ],
                  )),
            )));
      }),
    );
  }
}
