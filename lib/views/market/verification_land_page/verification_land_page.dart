import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/view_models/otp_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/views/market/verification_land_page/verification_land_page_provider.dart';
import 'package:varun_kanna_app/views/market/verify_main_page.dart';
import 'package:varun_kanna_app/views/market/verify_main_page_provider.dart';
import 'package:varun_kanna_app/widget/bottom_sheet_address.dart';
import 'package:varun_kanna_app/widget/custom_camera/custom_cupertino_pop_up.dart';
import 'package:varun_kanna_app/widget/input_radio_btn.dart';
import 'package:varun_kanna_app/widget/input_text_rai_ngan_wa.dart';
import 'package:varun_kanna_app/widget/input_text.dart';
import 'package:varun_kanna_app/widget/multi_box_with_img.dart';
import 'dart:math' as math;

import 'package:varun_kanna_app/widget/preview_image.dart';

class VerificationLandPage extends StatelessWidget {
  VerificationLandPage({super.key});
  final String _addressImg = "assets/images/ic_address_book.svg";

  ScrollController scrollController = ScrollController();

  TextEditingController firstNameCtrl = TextEditingController();
  TextEditingController lastNameCtrl = TextEditingController();

  ProfileViewModel? get profileViewModel => null;

  @override
  Widget build(BuildContext context) {
    Function filterById(dynamic data) {
      return (id) => id["id"] == int.parse(data.toString());
    }

    Future pickImage(ImageSource source) async {
      try {
        final image = await ImagePicker().pickImage(source: source);
        if (image == null) return;
        final imageTemporary = File(image.path);
        LoginViewModel loginResponseModel =
            Provider.of<LoginViewModel>(context, listen: false);
      } on PlatformException catch (e) {
        print('Failed to pick image : $e');
      }
    }

    return ChangeNotifierProvider(
      create: (context) => ConsentPageProvider()
        ..initialData(scrollController: scrollController),
      child: Consumer2<ConsentPageProvider, VerifyMainPageProvider>(
          builder: (context, provider, mainPageProvider, _) {
        validField() {
          bool validLand = (mainPageProvider.raiCtrl.text.isNotEmpty ||
                  mainPageProvider.nganCtrl.text.isNotEmpty ||
                  mainPageProvider.waCtrl.text.isNotEmpty) &&
              (mainPageProvider.validNgan && mainPageProvider.validWa);

          if (!validLand ||
              mainPageProvider.typeDocumentCtrl.text.isEmpty ||
              mainPageProvider.documentNoCtrl.text.isEmpty ||
              mainPageProvider.provinceCtrl.text.isEmpty ||
              mainPageProvider.districtCtrl.text.isEmpty ||
              mainPageProvider.subDistrictCtrl.text.isEmpty ||
              (mainPageProvider.letterConsentParicipateProjectImgPath.isEmpty &&
                  mainPageProvider
                      .letterConsentParicipateProjectSignUrlImg.isEmpty) ||
              (mainPageProvider.titleDeedSignUrlImg.isEmpty &&
                  mainPageProvider.titleDeedImgPath.isEmpty)) {
            mainPageProvider.setIsEnableNextBtnPage1(false);
          } else {
            mainPageProvider.setIsEnableNextBtnPage1(true);
          }
        }

        showImageSource(BuildContext context, SourceImgType source) async {
          final result = await CustomCupertinoPopUp.showActionSheet(context);
          if (result.isEmpty) {
            return;
          }
          switch (source) {
            case SourceImgType.powerAttorneyBookImg:
              await mainPageProvider.saveImage(result,
                  SourceImgType.powerAttorneyBookImg, "letter_attorney_img");
              validField();

              break;
            case SourceImgType.landDocumentImage:
              await mainPageProvider.saveImage(
                  result, SourceImgType.landDocumentImage, "land_book_img");
              validField();

              break;
            case SourceImgType.farmerBookImage:
              await mainPageProvider.saveImage(
                  result, SourceImgType.farmerBookImage, "farm_book_img");
              validField();

              break;
            case SourceImgType.farmerFamImage:
              await mainPageProvider.saveImage(
                  result, SourceImgType.farmerFamImage, "member_book_img");
              validField();

              break;
            case SourceImgType.farmerBookLandImage:
              await mainPageProvider.saveImage(
                  result, SourceImgType.farmerBookLandImage, "land_book_img");
              validField();

              break;
            case SourceImgType.rentingLandBookImg:
              await mainPageProvider.saveImage(result,
                  SourceImgType.rentingLandBookImg, "contract_leases_img");
              validField();

              break;
            default:
          }
        }

        Widget _provinceBottom(ProfileViewModel? profileViewModel) {
          return SizedBox(
              height: MediaQuery.of(context).size.height * 0.85,
              child: BottomSheetAddress(
                  autoFocus: false,
                  title: "เลือกจังหวัด",
                  item: mainPageProvider.provinceList,
                  groupValue: mainPageProvider.getProvinceValue,
                  onClosePress: () {
                    Navigator.of(context).pop();
                  },
                  valueChanged: (Object data) async {
                    mainPageProvider.setProvinceValue(data);

                    mainPageProvider.provinceCtrl.text = mainPageProvider
                            .provinceList[int.parse(data.toString()) - 1]
                        ["name_th"];

                    await mainPageProvider.getDistrictListById(
                        context, int.parse(data.toString()));
                    mainPageProvider.clearState("province");
                    validField();
                    Navigator.of(context).pop();
                  }));
        }

        Widget _typeDocumentBottom(List item) {
          var documentList = item;

          return SizedBox(
              height: MediaQuery.of(context).size.height * 0.85,
              child: BottomSheetAddress(
                  autoFocus: false,
                  title: "เลือกประเภทเอกสารสิทธ์",
                  item: documentList,
                  groupValue: mainPageProvider.getTypeDocumentValue,
                  onClosePress: () {
                    Navigator.of(context).pop();
                  },
                  valueChanged: (Object data) {
                    mainPageProvider.setTypeDocumentValue(data);

                    mainPageProvider.typeDocumentCtrl.text =
                        documentList[int.parse(data.toString()) - 1]["name_th"];

                    validField();
                    Navigator.of(context).pop();
                  }));
        }

        Widget _districtBottom(ProfileViewModel? profileViewModel) {
          return SizedBox(
              height: MediaQuery.of(context).size.height * 0.85,
              child: BottomSheetAddress(
                  autoFocus: false,
                  title: "เลือกอำเภอ/เขต",
                  item: mainPageProvider.districtList,
                  groupValue: mainPageProvider.getDistrictValue,
                  onClosePress: () {
                    Navigator.of(context).pop();
                  },
                  valueChanged: (Object data) async {
                    mainPageProvider.setDistrictValue(data);

                    var selectedDisTctId = mainPageProvider.districtList
                        .where(filterById(data))
                        .toList();

                    mainPageProvider.districtCtrl.text =
                        selectedDisTctId[0]["name_th"];

                    await mainPageProvider.getSubDistrictListById(context,
                        int.parse(selectedDisTctId[0]["id"].toString()));
                    mainPageProvider.clearState("district");
                    validField();

                    Navigator.of(context).pop();
                  }));
        }

        Widget _subDistrictBottom(ProfileViewModel? profileViewModel) {
          return SizedBox(
              height: MediaQuery.of(context).size.height * 0.85,
              child: BottomSheetAddress(
                  autoFocus: false,
                  title: "เลือกตำบล/แขวง",
                  item: mainPageProvider.subDistrictList,
                  groupValue: mainPageProvider.getSubDistrictValue,
                  onClosePress: () {
                    Navigator.of(context).pop();
                  },
                  valueChanged: (Object data) {
                    mainPageProvider.setSubDistrictValue(data);

                    var selectedSubDisTctId = mainPageProvider.subDistrictList
                        .where(filterById(data))
                        .toList();

                    mainPageProvider.subDistrictCtrl.text =
                        selectedSubDisTctId[0]["name_th"];
                    validField();

                    Navigator.of(context).pop();
                  }));
        }

        Future<bool> _onWillPop() async {
          mainPageProvider.setProcessPage(mainPageProvider.getProcessPage - 1);
          return false;
        }

//render
        return WillPopScope(
            onWillPop: _onWillPop,
            child: SizedBox(
              child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      Column(children: [
                        Container(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, bottom: 20),
                            child: Column(
                              children: [
                                SizedBox(
                                    child: RadioButton(
                                  isEnable: mainPageProvider.getEnableFields,
                                  onChange: (index) => {
                                    mainPageProvider.setIsAuthority(index!),
                                    validField()
                                  },
                                  isRequire: true,
                                  title: "สิทธิ์ในที่ดิน",
                                  radioBtn: const [
                                    "เป็นเจ้าของที่ดิน",
                                    "เป็นผู้เช่าที่ดิน"
                                  ],
                                  selectedIndex:
                                      mainPageProvider.getIsAuthority,
                                )),
                                InputText(
                                  isEnable: mainPageProvider.getEnableFields,
                                  isRequire: true,
                                  onChanged: (value) => validField(),
                                  autoFocus: false,
                                  title: "ประเภทเอกสารสิทธิ์",
                                  controller: mainPageProvider.typeDocumentCtrl,
                                  hintText: "เลือกประเภทเอกสารสิทธ์",
                                  suffixIcon: const Icon(
                                    Icons.expand_more,
                                    color: Color(0XFFA0AEC0),
                                  ),
                                  readOnly: mainPageProvider.getEnableFields,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  onTap: () {
                                    showModalBottomSheet(
                                        isScrollControlled: true,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        context: context,
                                        builder: (BuildContext context) {
                                          return _typeDocumentBottom(
                                              mainPageProvider
                                                  .getDocumentList());
                                        });
                                  },
                                ),
                                InputText(
                                    isEnable: mainPageProvider.getEnableFields,
                                    onChanged: (value) => validField(),
                                    autoFocus: false,
                                    isRequire: true,
                                    title: "เลขโฉนดที่ดิน",
                                    hintText: "กรอก",
                                    controller:
                                        mainPageProvider.documentNoCtrl),
                                InputText(
                                  isEnable: mainPageProvider.getEnableFields,
                                  onChanged: (value) => validField(),
                                  isRequire: true,
                                  autoFocus: false,
                                  title: "จังหวัดที่ตั้งแปลง",
                                  controller: mainPageProvider.provinceCtrl,
                                  hintText: "เลือกจังหวัด",
                                  suffixIcon: const Icon(
                                    Icons.expand_more,
                                    color: Color(0XFFA0AEC0),
                                  ),
                                  readOnly: mainPageProvider.getEnableFields,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  onTap: () {
                                    showModalBottomSheet(
                                        isScrollControlled: true,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        context: context,
                                        builder: (BuildContext context) {
                                          return _provinceBottom(
                                              profileViewModel);
                                        });
                                  },
                                ),
                                InputText(
                                  isEnable: mainPageProvider.getEnableFields,
                                  onChanged: (value) => validField(),
                                  fillColor: mainPageProvider.disableDistrict
                                      ? ColorsUtils.disableColor
                                          .withOpacity(0.2)
                                      : null,
                                  isRequire: true,
                                  autoFocus: false,
                                  title: "อำเภอ/เขตที่ตั้งแปลง",
                                  controller: mainPageProvider.districtCtrl,
                                  hintText: "เลือกอำเภอ",
                                  suffixIcon: const Icon(
                                    Icons.expand_more,
                                    color: Color(0XFFA0AEC0),
                                  ),
                                  readOnly: mainPageProvider.getEnableFields,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  onTap: () {
                                    mainPageProvider.disableDistrict
                                        ? null
                                        : showModalBottomSheet(
                                            isScrollControlled: true,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            context: context,
                                            builder: (BuildContext context) {
                                              return _districtBottom(
                                                  profileViewModel);
                                            });
                                  },
                                ),
                                InputText(
                                  isEnable: mainPageProvider.getEnableFields,
                                  onChanged: (value) => validField(),
                                  fillColor: mainPageProvider.disableSubDistrict
                                      ? ColorsUtils.disableColor
                                          .withOpacity(0.2)
                                      : null,
                                  isRequire: true,
                                  autoFocus: false,
                                  title: "ตำบล/แขวงที่ตั้งแปลง",
                                  controller: mainPageProvider.subDistrictCtrl,
                                  hintText: "เลือกตำบล",
                                  suffixIcon: const Icon(
                                    Icons.expand_more,
                                    color: Color(0XFFA0AEC0),
                                  ),
                                  readOnly: mainPageProvider.getEnableFields,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  onTap: () {
                                    mainPageProvider.disableSubDistrict
                                        ? null
                                        : showModalBottomSheet(
                                            isScrollControlled: true,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            context: context,
                                            builder: (BuildContext context) {
                                              return _subDistrictBottom(
                                                  profileViewModel);
                                            });
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputTextRaiNganWa(
                                    isEnable: mainPageProvider.getEnableFields,
                                    validNgan: mainPageProvider.validNgan,
                                    validWa: mainPageProvider.validWa,
                                    isNumber: true,
                                    onChangedRai: (value) => {validField()},
                                    onChangedNgan: (value) => {
                                          if (int.parse(value) > 3 ||
                                              value == "")
                                            {
                                              mainPageProvider.validNgan =
                                                  false,
                                            }
                                          else
                                            {mainPageProvider.validNgan = true},
                                          validField()
                                        },
                                    onChangedWa: (value) => {
                                          if (int.parse(value) > 99 ||
                                              value == "")
                                            {
                                              mainPageProvider.validWa = false,
                                            }
                                          else
                                            {mainPageProvider.validWa = true},
                                          validField()
                                        },
                                    autoFocus: false,
                                    isRequire: true,
                                    title: "พื้นที่ปลูกจริง",
                                    hintText1: "0",
                                    hintText2: "0",
                                    hintText3: "0",
                                    controllerRai: mainPageProvider.raiCtrl,
                                    controllerNgan: mainPageProvider.nganCtrl,
                                    controllerWa: mainPageProvider.waCtrl),
                                // this
                                MultiBoxWithImg(
                                    isEnable: mainPageProvider.getEnableFields,
                                    showInFo: false,
                                    imgPath: 'assets/images/ic_document.svg',
                                    onTap: () async {
                                      await showImageSource(context,
                                          SourceImgType.powerAttorneyBookImg);
                                    },
                                    isRequire: true,
                                    hintText: "เพิ่มรูปภาพ",
                                    title: "หนังสือยินยอมเข้าร่วมโครงการ",
                                    onTapInFoIcon: () async {
                                      mainPageProvider.setScrollOffset(
                                          mainPageProvider
                                              .scrollController.offset);
                                      await PreviewImageWidget(
                                        clearImage: () {},
                                        imagePath: mainPageProvider.imagePath,
                                        isInfoTapped: true,
                                        infoPath:
                                            'assets/images/land_document.png',
                                      ).dialogPreview(context);
                                    },
                                    widgetImage: mainPageProvider
                                            .letterConsentParicipateProjectSignUrlImg
                                            .isEmpty
                                        ? null
                                        : [
                                            for (int i = 0;
                                                i <
                                                    mainPageProvider
                                                        .letterConsentParicipateProjectSignUrlImg
                                                        .length;
                                                i++)
                                              PreviewImageWidget(
                                                isEnable: mainPageProvider
                                                    .getEnableFields,
                                                clearImage: () {
                                                  mainPageProvider
                                                      .clearLetterConsentParicipateProjectImg(
                                                          i);
                                                  validField();
                                                },
                                                imagePathString: mainPageProvider
                                                    .letterConsentParicipateProjectSignUrlImg[i],
                                                imagePath: Uint8List(0),
                                              )
                                          ],
                                    maxImage: 5,
                                    imagePath: mainPageProvider
                                        .letterConsentParicipateProjectSignUrlImg),

                                MultiBoxWithImg(
                                    isEnable: mainPageProvider.getEnableFields,
                                    showInFo: false,
                                    imgPath: 'assets/images/ic_document.svg',
                                    onTap: () async {
                                      mainPageProvider.setScrollOffset(
                                          mainPageProvider
                                              .scrollController.offset);

                                      await showImageSource(context,
                                          SourceImgType.rentingLandBookImg);
                                    },
                                    hintText: "เพิ่มรูปภาพ",
                                    title:
                                        "หนังสือรับรองการใช้ประโยชน์ที่ดิน และ",
                                    titleLine2:
                                        "สำเนาบัตรประชาชน,ทะเบียนบ้าน ของเจ้าของที่",
                                    onTapInFoIcon: () async {
                                      await PreviewImageWidget(
                                        clearImage: () {},
                                        imagePath: mainPageProvider.imagePath,
                                        isInfoTapped: true,
                                        infoPath:
                                            'assets/images/land_document.png',
                                      ).dialogPreview(context);
                                    },
                                    widgetImage: mainPageProvider
                                            .certificateLandUtilizationSignUrlImg
                                            .isEmpty
                                        ? null
                                        : [
                                            for (int i = 0;
                                                i <
                                                    mainPageProvider
                                                        .certificateLandUtilizationSignUrlImg
                                                        .length;
                                                i++)
                                              PreviewImageWidget(
                                                isEnable: mainPageProvider
                                                    .getEnableFields,
                                                clearImage: () {
                                                  mainPageProvider
                                                      .clearCertificateLandUtilizationImg(
                                                          i);
                                                  validField();
                                                },
                                                imagePathString: mainPageProvider
                                                    .certificateLandUtilizationSignUrlImg[i],
                                                imagePath: Uint8List(0),
                                              )
                                          ],
                                    maxImage: 5,
                                    imagePath: mainPageProvider
                                        .certificateLandUtilizationSignUrlImg),
                                MultiBoxWithImg(
                                    isEnable: mainPageProvider.getEnableFields,
                                    imgPath:
                                        'assets/images/ic_land_document.svg',
                                    isRequire: true,
                                    onTap: () async {
                                      mainPageProvider.setScrollOffset(
                                          mainPageProvider
                                              .scrollController.offset);

                                      await showImageSource(context,
                                          SourceImgType.landDocumentImage);
                                    },
                                    hintText: "เพิ่มรูปภาพ",
                                    title: "รูปเอกสารสิทธิ์/โฉนดที่ดิน",
                                    onTapInFoIcon: () async {
                                      await PreviewImageWidget(
                                        clearImage: () {},
                                        imagePath: mainPageProvider.imagePath,
                                        isInfoTapped: true,
                                        infoPath:
                                            'assets/images/land_document.png',
                                      ).dialogPreview(context);
                                    },
                                    widgetImage: mainPageProvider
                                            .titleDeedSignUrlImg.isEmpty
                                        ? null
                                        : [
                                            for (int i = 0;
                                                i <
                                                    mainPageProvider
                                                        .titleDeedSignUrlImg
                                                        .length;
                                                i++)
                                              PreviewImageWidget(
                                                isEnable: mainPageProvider
                                                    .getEnableFields,
                                                clearImage: () {
                                                  mainPageProvider
                                                      .clearTitleDeedImage(i);
                                                  validField();
                                                },
                                                imagePathString:
                                                    mainPageProvider
                                                        .titleDeedSignUrlImg[i],
                                                imagePath: Uint8List(0),
                                              )
                                          ],
                                    maxImage: 5,
                                    imagePath:
                                        mainPageProvider.titleDeedSignUrlImg),
                                MultiBoxWithImg(
                                    isEnable: mainPageProvider.getEnableFields,
                                    imgPath: _addressImg,
                                    onTap: () async {
                                      mainPageProvider.setScrollOffset(
                                          mainPageProvider
                                              .scrollController.offset);

                                      await showImageSource(context,
                                          SourceImgType.farmerBookImage);
                                    },
                                    hintText: "เพิ่มรูปภาพ",
                                    title: "รูปสมุดทะเบียนเกษตกร",
                                    onTapInFoIcon: () async {
                                      await PreviewImageWidget(
                                        clearImage: () {},
                                        imagePath: mainPageProvider.imagePath,
                                        isInfoTapped: true,
                                        infoPath:
                                            'assets/images/farmer_book_example.png',
                                      ).dialogPreview(context);
                                    },
                                    widgetImage: mainPageProvider
                                            .farmBookSignUrlImg.isEmpty
                                        ? null
                                        : [
                                            for (int i = 0;
                                                i <
                                                    mainPageProvider
                                                        .farmBookSignUrlImg
                                                        .length;
                                                i++)
                                              PreviewImageWidget(
                                                isEnable: mainPageProvider
                                                    .getEnableFields,
                                                clearImage: () {
                                                  mainPageProvider
                                                      .clearFarmerBookImage(i);
                                                  validField();
                                                },
                                                imagePathString:
                                                    mainPageProvider
                                                        .farmBookSignUrlImg[i],
                                                imagePath: Uint8List(0),
                                              )
                                          ],
                                    maxImage: 5,
                                    imagePath:
                                        mainPageProvider.farmBookSignUrlImg),
                                MultiBoxWithImg(
                                    isEnable: mainPageProvider.getEnableFields,
                                    imgPath: _addressImg,
                                    onTap: () async {
                                      mainPageProvider.setScrollOffset(
                                          mainPageProvider
                                              .scrollController.offset);

                                      await showImageSource(context,
                                          SourceImgType.farmerFamImage);
                                    },
                                    hintText: "เพิ่มรูปภาพ",
                                    title:
                                        "รูปสมุดหน้าสมาชิกในครัวเรือนที่ช่วยทำการเกษตร",
                                    onTapInFoIcon: () async {
                                      await PreviewImageWidget(
                                        clearImage: () {},
                                        imagePath: mainPageProvider.imagePath,
                                        isInfoTapped: true,
                                        infoPath:
                                            'assets/images/farmer_fam_example.png',
                                      ).dialogPreview(context);
                                    },
                                    widgetImage: mainPageProvider
                                            .memberBookSignUrlImg.isEmpty
                                        ? null
                                        : [
                                            for (int i = 0;
                                                i <
                                                    mainPageProvider
                                                        .memberBookSignUrlImg
                                                        .length;
                                                i++)
                                              PreviewImageWidget(
                                                isEnable: mainPageProvider
                                                    .getEnableFields,
                                                clearImage: () {
                                                  mainPageProvider
                                                      .clearFarmerFamImage(i);
                                                  validField();
                                                },
                                                imagePathString: mainPageProvider
                                                    .memberBookSignUrlImg[i],
                                                imagePath: Uint8List(0),
                                              ),
                                          ],
                                    maxImage: 5,
                                    imagePath:
                                        mainPageProvider.memberBookSignUrlImg),
                                Container(
                                  alignment: Alignment.topLeft,
                                  child: const Text(
                                      "( ในกรณีที่ไม่ใช่เจ้าของสมุด )",
                                      style: TextStyle(
                                        fontSize: 14,
                                      )),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                MultiBoxWithImg(
                                  isEnable: mainPageProvider.getEnableFields,
                                  imgPath: _addressImg,
                                  onTap: () async {
                                    mainPageProvider.setScrollOffset(
                                        mainPageProvider
                                            .scrollController.offset);

                                    await showImageSource(context,
                                        SourceImgType.farmerBookLandImage);
                                  },
                                  hintText: "เพิ่มรูปภาพ",
                                  title:
                                      "รูปสมุดหน้าการถือครองที่ดินเพื่อการเกษตร",
                                  onTapInFoIcon: () async {
                                    await PreviewImageWidget(
                                      clearImage: () {},
                                      imagePath: mainPageProvider.imagePath,
                                      isInfoTapped: true,
                                      infoPath:
                                          'assets/images/farmer_book_land_example.png',
                                    ).dialogPreview(context);
                                  },
                                  widgetImage: mainPageProvider
                                          .landBookSignUrlImg.isEmpty
                                      ? null
                                      : [
                                          for (int i = 0;
                                              i <
                                                  mainPageProvider
                                                      .landBookSignUrlImg
                                                      .length;
                                              i++)
                                            PreviewImageWidget(
                                              isEnable: mainPageProvider
                                                  .getEnableFields,
                                              clearImage: () {
                                                mainPageProvider
                                                    .clearFarmerBookLandImage(
                                                        i);
                                                validField();
                                              },
                                              imagePathString: mainPageProvider
                                                  .landBookSignUrlImg[i],
                                              imagePath: Uint8List(0),
                                            )
                                        ],
                                  imagePath:
                                      mainPageProvider.landBookSignUrlImg,
                                  maxImage: 5,
                                )
                              ],
                            )),
                        const SizedBox(
                          height: 15,
                        ),
                      ]),
                    ],
                  )),
            ));
      }),
    );
  }

  Widget navigateBtn({required bool isDownBtn, required Function onPressed}) {
    return FloatingActionButton.small(
      child: Transform.rotate(
          angle: isDownBtn ? (90 * math.pi / 180) : -90 * math.pi / 180,
          child: const Icon(
            size: 40,
            color: Color.fromARGB(255, 241, 241, 241),
            Icons.navigate_before,
          )),
      backgroundColor: Color.fromARGB(255, 32, 4, 4),
      onPressed: () => onPressed(),
    );
  }
}
