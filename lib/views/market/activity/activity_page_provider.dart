import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/common_response.dart';

import 'package:varun_kanna_app/model/farm_list_response.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_widget_model/activity_fertilize_model.dart';
import 'package:varun_kanna_app/services/activity_components_service.dart';
import 'package:varun_kanna_app/services/image_service.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';

import '../../../model/market_page_model/activity_widget_model/activity_model.dart';

class ActivityPageProvider extends ChangeNotifier {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  FarmListModel? selectFarm;
  final List<Widget> _widget = [];
  List<Widget>? get getListWidget => _widget;
  LatLng? _coordinates;
  DateTime? _shootingDate;
//[GET] atitvity/add-fertilize
  List<dynamic> widgetFertList = [
    {
      "input_type": "display",
      "place_holder": "",
      "title": "ปุ๋ย ชนิดที่",
      "is_required": false,
      "disable": false,
      "index": 0,
      "value": "",
      "error_text": "ข้อมูลไม่ถูกต้อง"
    },
    {
      "title": "ชนิดปุ๋ยที่ใช้",
      "input_type": "dropdown",
      "items": [
        {"id": 0, "name_th": "ปุ๋ยเคมี"},
        {"id": 1, "name_th": "ปุ๋ยอินทรีย์"}
      ],
      "is_required": true,
      "disable": false,
      "index": 1,
      "value": "",
      "error_text": "ข้อมูลไม่ถูกต้อง",
      "place_holder": "โปรดเลือกชนิดปุ๋ย"
    },
    {
      "title": "ชนิดปุ๋ย (กรณีใช้ปุ๋ยอินทรีย์)",
      "input_type": "text_input",
      "is_required": false,
      "disable": false,
      "index": 2,
      "value": "",
      "error_text": "ข้อมูลไม่ถูกต้อง",
      "place_holder": "ชนิดปุ๋ยอินทรีย์"
    },
    {
      "title": "สูตรปุ๋ยที่ใช้ (กรณีใช้ปุ๋ยอินทรีย์กรอก 16 ทุกช่อง) ",
      "input_type": "triple_text_input",
      "place_holder": "00",
      "is_required": true,
      "disable": false,
      "index": 3,
      "value": "",
      "error_text": "ข้อมูลไม่ถูกต้อง",
      "value2": "",
      "value3": ""
    },
    {
      "title": "ปริมาณ",
      "index": 4,
      "input_type": "unit_input",
      "is_required": true,
      "disable": false,
      "place_holder": "00000",
      "value": "",
      "error_text": "ข้อมูลไม่ถูกต้อง",
      "unit_value": "",
      "items": [
        {"id": 0, "name_th": "กิโลกรัม (kg)"},
        {"id": 1, "name_th": "กรัม (g)"},
        {"id": 2, "name_th": "ลิตร (L)"},
        {"id": 3, "name_th": "ซีซี (CC)"},
        {"id": 4, "name_th": "มิลลิลิตร (ml)"}
      ]
    },
    {
      "title": "ภาพกระสอบปุ๋ย",
      "index": 5,
      "is_required": true,
      "disable": false,
      "input_type": "image_box",
      "img_value": null,
      "place_holder": "",
      "upload_endpoint":
          "https://kanna-dev.varunatech.co/api/v1/activity-components/upload/:user_id",
      "value": "",
      "error_text": "ข้อมูลไม่ถูกต้อง"
    },
    {
      "title": "ภาพใบเสร็จการซื้อปุ๋ย",
      "index": 6,
      "is_required": false,
      "disable": false,
      "input_type": "image_box",
      "img_value": null,
      "place_holder": "",
      "upload_endpoint":
          "https://kanna-dev.varunatech.co/api/v1/activity-components/upload/:user_id",
      "value": "",
      "error_text": "ข้อมูลไม่ถูกต้อง"
    },
  ];
  List<List<dynamic>> controllers = [];

  List<List<List<dynamic>>> fertControllers = [];

  List<List<ActivityFertResponseModel>> soilList = [];
  List<List<ActivityFertResponseModel>> fertValues = [];
  List<ActivityResponseModel> list = [];
  List<ActivityResponseModel> values = [];

  List<dynamic> widgetList = [];

  String selectedRadioValue = '';

  TextEditingController datePickerCtrl = TextEditingController();
  bool selectedCheckboxValue = false;
  bool? isValidated;
  bool isInitted = false;
  bool? uploadDataSuccess;
  bool isLoading = true;
  void setWidget(Widget widget) {
    _widget.add(widget);
  }

  void setSelectFarm(FarmListModel farm) {
    selectFarm = farm;
    notifyListeners();
  }

  Future fetchApi(String activityId, bool isFertilize) async {
    if (isFertilize) {
      final response = await AcctivitiComponentsService()
          .fetchActivityFertComponents(activityId);
      final datas = json.encode(response.data);

      final data = json.decode(datas) as List<dynamic>;
      List<List<dynamic>> temp = [];
      data.asMap().forEach((key, value) {
        temp.add(value);
      });

      soilList = temp
          .map((element) => element
              .map((value) => ActivityFertResponseModel.fromJson(value))
              .toList())
          .toList();

      fertValues = temp
          .map((element) => element
              .map((value) => ActivityFertResponseModel.fromJson(value))
              .toList())
          .toList();
      fertValues.asMap().forEach((index, element) {
        element.forEach((e) {
          fertControllers.add([]);
          if (e.inputType == 'unit_input') {
            if (e.unitValue == '') {
              e.unitValue = e.items![0].nameTh!;
            }
          }
          if (e.inputType == 'group_radio' && e.value.isNotEmpty) {
            selectedRadioValue = e.value;
          }
          if (e.inputType == 'triple_text_input') {
            final controller = TextEditingController(text: e.value);
            final controller2 = TextEditingController(text: e.value2);
            final controller3 = TextEditingController(text: e.value3);
            fertControllers[index].add([
              controller,
              controller2,
              controller3,
            ]);
          } else if (e.inputType == 'date_picker') {
            final controller = convertLacaleDate(e.value.split('T')[0]);
            fertControllers[index].add([controller]);
          } else if (e.inputType == 'dropdown') {
            final controller = e.value.isEmpty
                ? SingleValueDropDownController()
                : SingleValueDropDownController(
                    data: DropDownValueModel(name: e.value, value: e.value),
                  );
            fertControllers[index].add([controller]);
          } else if (e.inputType == 'unit_input') {
            final controller = TextEditingController(text: e.value);
            final controller2 = e.value.isEmpty
                ? SingleValueDropDownController(
                    data: DropDownValueModel(
                        name: e.items![0].nameTh!, value: e.items![0].nameTh!),
                  )
                : SingleValueDropDownController(
                    data: DropDownValueModel(
                        name: e.unitValue!, value: e.unitValue!),
                  );
            fertControllers[index].add([controller, controller2]);
          } else {
            final controller = TextEditingController(text: e.value);
            fertControllers[index].add([controller]);
          }
        });
      });

      soilList.asMap().forEach((index, element) {
        element.forEach((e) {
          if (e.inputType == 'unit_input') {
            if (e.unitValue == '') {
              e.unitValue = e.items![0].nameTh!;
            }
          }
          if (e.inputType == 'group_radio' && e.value.isNotEmpty) {
            selectedRadioValue = e.value;
          }
        });
      });

      // print();

      isLoading = false;
      notifyListeners();
    } else if (!isFertilize) {
      final response = await AcctivitiComponentsService()
          .fetchActivityCommonComponents(activityId);
      response.when(
        success: (data) {
          list = data.data!;
          isLoading = false;
          notifyListeners();
        },
        failure: (error) {
          print(error);
          isLoading = false;
          notifyListeners();
        },
      );
    }

    notifyListeners();
  }

  initData(String activityId, bool isFertilize) async {
    await fetchApi(activityId, isFertilize);
    if (!isFertilize) {
      list.forEach((e) {
        if (e.inputType == 'unit_input') {
          if (e.unitValue == '') {
            e.unitValue = e.items![0].nameTh!;
          }
        }
        if (e.inputType == 'group_radio' && e.value.isNotEmpty) {
          selectedRadioValue = e.value;
        }
        if (e.inputType == 'triple_text_input') {
          final controller = TextEditingController(text: e.value);
          final controller2 = TextEditingController(text: e.value2);
          final controller3 = TextEditingController(text: e.value3);
          controllers.add([
            controller,
            controller2,
            controller3,
          ]);
        }
        if (e.inputType == 'date_picker') {
          final controller = convertLacaleDate(e.value.split('T')[0]);
          controllers.add([controller]);
        } else if (e.inputType == 'dropdown') {
          final controller = e.value.isEmpty
              ? SingleValueDropDownController()
              : SingleValueDropDownController(
                  data: DropDownValueModel(name: e.value, value: e.value),
                );
          controllers.add([controller]);
        } else if (e.inputType == 'unit_input') {
          final controller = TextEditingController(text: e.value);
          final controller2 = e.value.isEmpty
              ? SingleValueDropDownController(
                  data: DropDownValueModel(
                      name: e.items![0].nameTh!, value: e.items![0].nameTh!),
                )
              : SingleValueDropDownController(
                  data: DropDownValueModel(
                      name: e.unitValue!, value: e.unitValue!),
                );
          controllers.add([controller, controller2]);
        } else {
          final controller = TextEditingController(text: e.value);
          controllers.add([controller]);
        }
      });
      values.addAll(list);
    }
  }

  onUpdate(int index, String val, ActivityResponseModel data, String inputType,
      {String? unitValue,
      String? value2,
      String? value3,
      Uint8List? imgValue,
      String? imagePath,
      String? signUrlImagePath}) async {
    values.removeWhere((e) {
      return e.index == index;
    });

    if (inputType == "group_radio") {
      selectedRadioValue = val;
      notifyListeners();
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
            id: data.id,
            value: selectedRadioValue.toString(),
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityResponseModel(
            id: data.id,
            value: selectedRadioValue.toString(),
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
          id: data.id,
          value: selectedRadioValue.toString(),
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );
        values.add(_json);
        notifyListeners();
      }
    } else if (inputType == "text_input") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
          id: data.id,
          value: val,
          index: index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );
        values.add(_json);
        notifyListeners();
      }
    } else if (inputType == "dropdown") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          values.add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
          id: data.id,
          value: val,
          index: index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
        );
        values.add(_json);
        notifyListeners();
      }
    } else if (inputType == "date_picker") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          datePickerCtrl.text = val;
          datePickerCtrl = convertLacaleDate(val.split(" ")[0]);
          values.add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          datePickerCtrl.text = val;
          datePickerCtrl = convertLacaleDate(val.split(" ")[0]);
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
          id: data.id,
          value: val,
          index: index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
        );

        datePickerCtrl.text = val;
        datePickerCtrl = convertLacaleDate(val.split(" ")[0]);
        values.add(_json);
        notifyListeners();
      }
    } else if (inputType == "check_box") {
      selectedCheckboxValue = val == "true";
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
          id: data.id,
          value: val,
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );
        values.add(_json);
        notifyListeners();
      }
    } else if (inputType == "unit_input") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            unitValue: unitValue!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        } else {
          if (val.isNotEmpty) {
            data.isValidate = null;
          }

          final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            unitValue: unitValue!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
          id: data.id,
          value: val,
          unitValue: unitValue!,
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );
        values.add(_json);
        notifyListeners();
      }
    } else if (inputType == 'triple_text_input') {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
              id: data.id,
              value: val,
              index: data.index,
              inputType: data.inputType,
              disable: data.disable,
              isRequired: data.isRequired,
              title: data.title,
              placeHolder: data.placeHolder,
              items: data.items,
              value2: value2,
              value3: value3);
          values.add(_json);
          notifyListeners();
        } else {
          if (val.isNotEmpty && value2!.isNotEmpty && value3!.isNotEmpty) {
            data.isValidate = null;
          }

          final _json = ActivityResponseModel(
              id: data.id,
              value: val,
              index: data.index,
              inputType: data.inputType,
              disable: data.disable,
              isRequired: data.isRequired,
              title: data.title,
              placeHolder: data.placeHolder,
              items: data.items,
              value2: value2,
              value3: value3);
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
            id: data.id,
            value: val,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
            value2: value2,
            value3: value3);
        values.add(_json);
        notifyListeners();
      }
    } else if (inputType == 'image_box') {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityResponseModel(
            id: data.id,
            value: imagePath!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            uploadEndpoint: data.uploadEndpoint,
            signedUrl: signUrlImagePath,
            dataStamp: shootingDate.toString(),
            lat: _coordinates!.latitude.toString(),
            lng: _coordinates!.longitude.toString(),
          );
          values.add(_json);
          notifyListeners();
        } else {
          if (val.isNotEmpty) {
            data.isValidate = null;
          }
          final _json = ActivityResponseModel(
            id: data.id,
            value: imagePath!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            uploadEndpoint: data.uploadEndpoint,
            signedUrl: signUrlImagePath,
            dataStamp: shootingDate.toString(),
            lat: _coordinates!.latitude.toString(),
            lng: _coordinates!.longitude.toString(),
          );
          values.add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityResponseModel(
          id: data.id,
          value: imagePath!,
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          uploadEndpoint: data.uploadEndpoint,
          signedUrl: signUrlImagePath,
          dataStamp: shootingDate.toString(),
          lat: _coordinates!.latitude.toString(),
          lng: _coordinates!.longitude.toString(),
        );
        values.add(_json);
        notifyListeners();
      }
    }
  }

  onUpdateFerterlizer(
    int numberOfFert,
    int index,
    String val,
    ActivityFertResponseModel data,
    String inputType, {
    String? unitValue,
    String? value2,
    String? value3,
    Uint8List? imgValue,
    String? imagePath,
    String? signUrlImagePath,
  }) async {
    fertValues[numberOfFert].removeWhere((e) {
      return e.index == index;
    });
    if (inputType == "group_radio") {
      selectedRadioValue = val;
      notifyListeners();
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
            value: selectedRadioValue.toString(),
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityFertResponseModel(
            value: selectedRadioValue.toString(),
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
          value: selectedRadioValue.toString(),
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );

        fertValues[numberOfFert].add(_json);
        notifyListeners();
      }
    } else if (inputType == "text_input") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityFertResponseModel(
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
          value: val,
          index: index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
        );
        fertValues[numberOfFert].add(_json);
        notifyListeners();
      }
    } else if (inputType == "dropdown") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityFertResponseModel(
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
          value: val,
          index: index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );
        fertValues[numberOfFert].add(_json);
        notifyListeners();
      }
    } else if (inputType == "date_picker") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          datePickerCtrl.text = val;
          datePickerCtrl = convertLacaleDate(val.split(" ")[0]);
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityFertResponseModel(
            value: val,
            index: index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
          );
          datePickerCtrl.text = val;
          datePickerCtrl = convertLacaleDate(val.split(" ")[0]);
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
          value: val,
          index: index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
        );
        datePickerCtrl.text = val;
        datePickerCtrl = convertLacaleDate(val.split(" ")[0]);
        fertValues[numberOfFert].add(_json);
        notifyListeners();
      }
    } else if (inputType == "check_box") {
      selectedCheckboxValue = val == "true";
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
            value: val,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          data.isValidate = null;
          final _json = ActivityFertResponseModel(
            value: val,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
          value: val,
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );
        fertValues[numberOfFert].add(_json);
        notifyListeners();
      }
    } else if (inputType == "unit_input") {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
            value: val,
            unitValue: unitValue!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          if (val.isNotEmpty) {
            data.isValidate = null;
          }

          final _json = ActivityFertResponseModel(
            value: val,
            unitValue: unitValue!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
          value: val,
          unitValue: unitValue!,
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          items: data.items,
        );
        fertValues[numberOfFert].add(_json);

        notifyListeners();
      }
    } else if (inputType == 'triple_text_input') {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
              value: val,
              index: data.index,
              inputType: data.inputType,
              disable: data.disable,
              isRequired: data.isRequired,
              title: data.title,
              placeHolder: data.placeHolder,
              items: data.items,
              value2: value2,
              value3: value3);
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          if (val.isNotEmpty && value2!.isNotEmpty && value3!.isNotEmpty) {
            data.isValidate = null;
          }

          final _json = ActivityFertResponseModel(
              value: val,
              index: data.index,
              inputType: data.inputType,
              disable: data.disable,
              isRequired: data.isRequired,
              title: data.title,
              placeHolder: data.placeHolder,
              items: data.items,
              value2: value2,
              value3: value3);
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
            value: val,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            items: data.items,
            value2: value2,
            value3: value3);
        fertValues[numberOfFert].add(_json);
        notifyListeners();
      }
    } else if (inputType == 'image_box') {
      if (data.isValidate != null) {
        if (data.isValidate!) {
          final _json = ActivityFertResponseModel(
            value: imagePath!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            uploadEndpoint: data.uploadEndpoint,
            signedUrl: signUrlImagePath,
            dataStamp: shootingDate.toString(),
            lat: _coordinates!.latitude.toString(),
            lng: _coordinates!.longitude.toString(),
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        } else {
          if (val.isNotEmpty) {
            data.isValidate = null;
          }
          final _json = ActivityFertResponseModel(
            value: imagePath!,
            index: data.index,
            inputType: data.inputType,
            disable: data.disable,
            isRequired: data.isRequired,
            title: data.title,
            placeHolder: data.placeHolder,
            uploadEndpoint: data.uploadEndpoint,
            signedUrl: signUrlImagePath,
            dataStamp: shootingDate.toString(),
            lat: _coordinates!.latitude.toString(),
            lng: _coordinates!.longitude.toString(),
          );
          fertValues[numberOfFert].add(_json);
          notifyListeners();
        }
      } else {
        final _json = ActivityFertResponseModel(
          value: imagePath!,
          index: data.index,
          inputType: data.inputType,
          disable: data.disable,
          isRequired: data.isRequired,
          title: data.title,
          placeHolder: data.placeHolder,
          uploadEndpoint: data.uploadEndpoint,
          signedUrl: signUrlImagePath,
          dataStamp: shootingDate.toString(),
          lat: _coordinates!.latitude.toString(),
          lng: _coordinates!.longitude.toString(),
        );

        fertValues[numberOfFert].add(_json);
        notifyListeners();
      }
    }
  }

  onAddWidget() {
    soilList.add(widgetFertList
        .map((e) => ActivityFertResponseModel.fromJson(e))
        .toList());
    fertValues.add(widgetFertList
        .map((e) => ActivityFertResponseModel.fromJson(e))
        .toList());
    fertValues.asMap().forEach((numberOfFert, element) {
      fertControllers.add([]);
      element.asMap().forEach((index, e) {
        if (numberOfFert != 0 &&
            fertControllers[numberOfFert].length !=
                fertControllers[0].length - 1) {
          if (e.inputType == 'unit_input') {
            fertValues[numberOfFert][index].unitValue = e.items![0].nameTh!;
          }
          if (e.inputType == 'group_radio' && e.value.isNotEmpty) {
            selectedRadioValue = e.value;
          }
          if (e.inputType == 'triple_text_input') {
            final controller = TextEditingController(text: e.value);
            final controller2 = TextEditingController(text: e.value2);
            final controller3 = TextEditingController(text: e.value3);
            fertControllers[numberOfFert].add([
              controller,
              controller2,
              controller3,
            ]);
          } else if (e.inputType == 'date_picker') {
            final controller = convertLacaleDate(e.value.split('T')[0]);
            fertControllers[numberOfFert].add([controller]);
          } else if (e.inputType == 'dropdown') {
            final controller = e.value.isEmpty
                ? SingleValueDropDownController()
                : SingleValueDropDownController(
                    data: DropDownValueModel(name: e.value, value: e.value),
                  );
            fertControllers[numberOfFert].add([controller]);
          } else if (e.inputType == 'unit_input') {
            final controller = TextEditingController(text: e.value);
            final controller2 = e.value.isEmpty
                ? SingleValueDropDownController(
                    data: DropDownValueModel(
                        name: e.items![0].nameTh!, value: e.items![0].nameTh!),
                  )
                : SingleValueDropDownController(
                    data: DropDownValueModel(
                        name: e.unitValue!, value: e.unitValue),
                  );
            fertControllers[numberOfFert].add([controller, controller2]);
          } else {
            final controller = TextEditingController(text: e.value);
            fertControllers[numberOfFert].add([controller]);
          }
        }
      });
    });

    soilList.forEach((element) {
      element.forEach((e) {
        if (e.inputType == 'unit_input') {
          e.unitValue = e.items![0].nameTh!;
        }
        if (e.inputType == 'group_radio' && e.value.isNotEmpty) {
          selectedRadioValue = e.value;
        }
      });
    });

    notifyListeners();
  }

  Future onDeleteWidget(int numberOfFert) async {
    fertValues.removeAt(numberOfFert);
    soilList.removeAt(numberOfFert);
    fertControllers.removeAt(numberOfFert);
    notifyListeners();
  }

  TextEditingController convertLacaleDate(String date) {
    if (date == "" ||
        date.isEmpty ||
        date == "null" ||
        date == "Invalid Date") {
      return datePickerCtrl;
    }

    final dateSplitted = date.split('-');
    final months = [
      'มกราคม',
      'กุมภาพันธ์',
      'มีนาคม',
      'เมษายน',
      'พฤษภาคม',
      'มิถุนายน',
      'กรกฏาคม',
      'สิงหาคม',
      'กันยายน',
      'ตุลาคม',
      'พฤศจิกายน',
      'ธันวาคม'
    ];
    var year = (int.parse(dateSplitted[0]) + 543).toString();
    var month = months[int.parse(dateSplitted[1]) - 1].toString();
    var day = int.parse(dateSplitted[2]).toString();
    var resultText = day + " " + month + " " + year;
    datePickerCtrl.text = resultText.toString();
    return datePickerCtrl;
  }

  Future onValidation(bool isFerterlized, String activityId) async {
    if (isFerterlized) {
      fertValues.asMap().forEach((index, fert) {
        fert.forEach((data) {
          if (data.inputType != "triple_text_input" &&
              data.value.isEmpty &&
              data.isRequired) {
            soilList[index]
                .where((e) => e.index == data.index)
                .first
                .isValidate = false;

            notifyListeners();
          } else if (data.inputType == "triple_text_input") {
            if (data.value.isEmpty ||
                data.value2!.isEmpty ||
                data.value3!.isEmpty) {
              soilList[index]
                  .where((e) => e.index == data.index)
                  .first
                  .isValidate = false;

              notifyListeners();
            }
          } else {
            soilList[index]
                .where((displayData) => displayData.index == data.index)
                .first
                .isValidate = true;
            notifyListeners();
          }
        });
      });
      isValidated = soilList.every(
          (element) => element.every((e) => e.isValidate ?? true == true));
      if (isValidated != null && isValidated!) {
        final response = await AcctivitiComponentsService()
            .saveFormFertActivity(activityId, fertValues);

        response.when(
          success: (v) {
            isLoading = false;
            uploadDataSuccess = true;
            notifyListeners();
          },
          failure: (error) {
            isLoading = false;
            uploadDataSuccess = false;
            notifyListeners();
          },
        );
      }
    } else {
      values.forEach((element) {
        if (element.value.isEmpty && element.isRequired) {
          list.where((e) => e.index == element.index).first.isValidate = false;
          notifyListeners();
        } else if (element.inputType == "triple_text_input") {
          if (element.value.isEmpty ||
              element.value2!.isEmpty ||
              element.value3!.isEmpty) {
            list.where((e) => e.index == element.index).first.isValidate =
                false;

            notifyListeners();
          }
        } else {
          list.where((e) => e.index == element.index).first.isValidate = true;
          notifyListeners();
        }
      });

      isValidated = list.every((element) => element.isValidate! == true);
      if (isValidated != null && isValidated!) {
        isLoading = true;
        final response = await AcctivitiComponentsService()
            .saveFormActivity(activityId, values);
        response.when(
          success: (v) {
            isLoading = false;
            uploadDataSuccess = true;
            notifyListeners();
          },
          failure: (error) {
            isLoading = false;
            uploadDataSuccess = false;
            notifyListeners();
          },
        );
      }
    }
  }

  void clearIdImage(List value,
      {ActivityResponseModel? data, ActivityFertResponseModel? fertData}) {
    value
        .where((element) =>
            element.index == (data == null ? fertData!.index : data.index))
        .first
        .value = "";

    value
        .where((element) =>
            element.index == (data == null ? fertData!.index : data.index))
        .first
        .signedUrl = "";

    notifyListeners();
  }

  Future<File> uint8ListToFile(Uint8List rawFile) async {
    var tempDir = await getTemporaryDirectory();
    File file = await File('${tempDir.path}/image.png').create();
    file.writeAsBytesSync(rawFile);
    return file;
  }

  Future<UploadModel?> uploadImg(Uint8List? imgValue) async {
    // isLoading = true;
    final SharedPreferences prefs = await _prefs;
    String userId = prefs.getString('userId')!;
    File imgFile = await uint8ListToFile(imgValue!);
    UploadModel? imageResponse;
    ApiResult<UploadResponse> response =
        await uploadImageUtils(imgFile, userId, "activity-components/upload/");

    response.when(
      success: (data) {
        imageResponse = data.data!;
        // isLoading = false;
      },
      failure: (error) {
        print(error);
        // isLoading = false;
      },
    );
    return imageResponse;
  }

  LatLng? get coordinates => _coordinates;
  DateTime? get shootingDate => _shootingDate;

  void setImageInfo(LatLng coordinates, DateTime shootingDate) {
    _coordinates = coordinates;
    _shootingDate = shootingDate;

    notifyListeners();
  }
}
