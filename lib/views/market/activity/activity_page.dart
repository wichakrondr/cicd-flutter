import 'dart:async';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/main.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_widget_model/activity_fertilize_model.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_widget_model/activity_model.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/views/market/activity/activity_page_provider.dart';
import 'package:varun_kanna_app/views/market/activity_list/activity_list_page.dart';
import 'package:varun_kanna_app/widget/box_with_img.dart';
import 'package:varun_kanna_app/widget/common_button.dart';
import 'package:varun_kanna_app/widget/custom_camera/custom_cupertino_pop_up.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';
import 'package:varun_kanna_app/widget/date_picker_thai.dart';
import 'package:varun_kanna_app/widget/input_text.dart';
import 'package:varun_kanna_app/widget/preview_image.dart';
import 'package:varun_kanna_app/widget/unit_input_widget.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';
import '../../../widget/app_bar_text.dart';

class ActivityPage extends StatelessWidget {
  ActivityPage({
    super.key,
    required this.farmItem,
    required this.projectId,
    required this.reqJoinProjectId,
    required this.isFerterlize,
    required this.activityId,
    required this.bannerIcon,
    required this.bannerDate,
    required this.bannerTitle,
    required this.bannerStatus,
    this.isReadOnly = false,
    this.isEdit = false,
  });

  RequestJoinProjectListModel? farmItem;
  String projectId;
  String reqJoinProjectId;
  String activityId;
  bool isFerterlize;
  String bannerIcon;
  String bannerTitle;
  String bannerDate;
  String bannerStatus;
  bool isReadOnly;
  bool isEdit;
  final ScrollController _controller = ScrollController();

  void scrollDown() {
    _controller.animateTo(
      _controller.position.maxScrollExtent + 500,
      curve: Curves.linear,
      duration: const Duration(milliseconds: 800),
    );
  }

  Future<Position> initLocation() async {
    Position? currentPos;
    try {
      final serviceEnable = await Geolocator.isLocationServiceEnabled();
      if (serviceEnable) {
        var permission = await Geolocator.checkPermission();
        if ((permission != LocationPermission.always ||
            permission != LocationPermission.whileInUse)) {
          permission = await Geolocator.requestPermission();
        }
        currentPos = await Geolocator.getCurrentPosition();
      }
    } catch (e) {
      print(e.toString());
      initLocation();
    }
    return currentPos!;
  }

  showImageSource(
    BuildContext context,
    int numberOfFert,
    int index,
    ActivityPageProvider provider, {
    ActivityResponseModel? data,
    ActivityFertResponseModel? fertData,
  }) async {
    Position pos = await initLocation();
    final result = await CustomCupertinoPopUp.showActionSheet(context,
        currentPos: pos, showLatlng: true, provider: provider);

    if (provider.soilList.isNotEmpty) {
      if (result.isNotEmpty) {
        UploadModel? imagePath = await provider.uploadImg(result);
        if (imagePath!.pathSignUrl!.isNotEmpty) {
          await provider.onUpdateFerterlizer(numberOfFert, index,
              result.toString(), fertData!, fertData.inputType,
              imgValue: result,
              imagePath: imagePath.path,
              signUrlImagePath: imagePath.pathSignUrl);
        }
      }
    } else {
      // fert
      if (result.isNotEmpty) {
        UploadModel? imagePath = await provider.uploadImg(result);
        if (imagePath!.pathSignUrl!.isNotEmpty) {
          await provider.onUpdate(
              index, result.toString(), data!, data.inputType,
              imgValue: result,
              imagePath: imagePath.path,
              signUrlImagePath: imagePath.pathSignUrl);
        }
      }
    }
  }

  late Timer _timer;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) =>
          ActivityPageProvider()..initData(activityId, isFerterlize),
      child: Consumer<ActivityPageProvider>(builder: (context, provider, _) {
        Widget farmImg() => Container(
              padding: const EdgeInsets.all(20),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(12.5),
                child: SizedBox.fromSize(
                  size: Size.fromRadius(Screen.isLargeScreen ? 60 : 53),
                  child: CachedNetworkImage(
                      width: 12,
                      imageUrl: farmItem!.farmImg,
                      fit: BoxFit.cover),
                ),
              ),
            );
        //
        Widget _bottomWidget() => Container(
              padding: const EdgeInsets.all(25.0),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(32),
                      topLeft: Radius.circular(32)),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.15), blurRadius: 10),
                  ],
                  color: Colors.white),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                margin: const EdgeInsets.symmetric(horizontal: 15),
                child: SizedBox(
                  child: CommonButton(
                    onPressed: () async {
                      await provider.onValidation(isFerterlize, activityId);
                      if (provider.uploadDataSuccess != null) {
                        if (provider.uploadDataSuccess!) {
                          await ViewDialogs.commonDialogWithIcon(
                            context: context,
                            text: "บันทึกกิจกรรมเรียบร้อยแล้ว",
                            isAutoDismiss: true,
                            autoDismiss: () {
                              _timer = Timer(const Duration(seconds: 2), () {
                                Navigator.of(context).pop();
                                Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ActivityListPage(
                                              farmItem: farmItem,
                                              projectId: projectId,
                                              reqJoinProjectId:
                                                  reqJoinProjectId,
                                            )));
                              });
                            },
                          ).then((value) {
                            _timer.cancel();
                            Navigator.of(context).pop();
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ActivityListPage(
                                          farmItem: farmItem,
                                          projectId: projectId,
                                          reqJoinProjectId: reqJoinProjectId,
                                        )));
                          });
                        } else if (provider.uploadDataSuccess!) {
                          ViewDialogs.commonDialogWithIcon(
                            context: context,
                            text: "บันทึกข้อมูลไม่สำเร็จ",
                            icon: const Icon(
                              Icons.close,
                              size: 50,
                              color: Colors.red,
                            ),
                          );
                        }
                      }
                    },
                    title: "บันทึกกิจกรรม",
                  ),
                ),
              ),
            );
        Future<bool> _onWillPop(BuildContext context) async {
          Navigator.pop(context);
          return true;
        }

        // render
        return WillPopScope(
          onWillPop: () => _onWillPop(context),
          child: Scaffold(
            backgroundColor: Colors.white,
            body: GestureDetector(
                onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
                child: Stack(
                  children: [
                    Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        AppBarText(
                          textHeader: "บันทึกกิจกรรม",
                          isback: true,
                          onBackPress: () {
                            Navigator.pop(context);
                          },
                        ),
                        Expanded(
                          child: SingleChildScrollView(
                            controller: _controller,
                            physics: const BouncingScrollPhysics(),
                            child: Container(
                              decoration: const BoxDecoration(
                                color: Colors.white,
                              ),
                              child: Column(
                                children: [
                                  Column(
                                    children: [
                                      GestureDetector(
                                        child: Container(
                                          margin: const EdgeInsets.only(
                                            top: 20,
                                            left: 20,
                                            right: 20,
                                          ),
                                          decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.black
                                                    .withOpacity(0.3),
                                                offset: const Offset(0, 1),
                                                blurRadius: 5,
                                                spreadRadius: 0,
                                              ),
                                            ],
                                          ),
                                          child: Row(
                                            children: [
                                              Container(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.5,
                                                padding:
                                                    const EdgeInsets.all(20),
                                                child: Column(
                                                  children: [
                                                    Align(
                                                      alignment:
                                                          Alignment.centerLeft,
                                                      child: Text(
                                                          farmItem!.farmName,
                                                          style:
                                                              const TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 18,
                                                          )),
                                                    ),
                                                    const SizedBox(
                                                      height: 10,
                                                    ),
                                                    Text(
                                                        'พื้นที่ ${farmItem!.displayArea}',
                                                        style: const TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 16,
                                                        )),
                                                  ],
                                                ),
                                              ),
                                              farmImg(),
                                            ],
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 10),
                                      Container(
                                        height: 100,
                                        width:
                                            MediaQuery.of(context).size.width,
                                        margin: const EdgeInsets.only(
                                          left: 15,
                                          right: 15,
                                        ),
                                        child: Card(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              const SizedBox(width: 20),
                                              SvgPicture.network(bannerIcon),
                                              const SizedBox(width: 20),
                                              Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    bannerTitle,
                                                    style: const TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 18),
                                                  ),
                                                  bannerDate.isEmpty
                                                      ? Container()
                                                      : Text(bannerDate),
                                                  const SizedBox(height: 3),
                                                  statusWidget(
                                                      bannerStatus, context),
                                                ],
                                              )
                                            ],
                                          ),
                                          color: Colors.white,
                                          elevation: 5,
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                          ),
                                        ),
                                      ),
                                      // switch render by provider {body}
                                      SingleChildScrollView(
                                        child: provider.isLoading
                                            ? const CustomLoadingWidget()
                                            : Column(
                                                children: [
                                                  Column(
                                                    children: provider
                                                            .soilList.isNotEmpty
                                                        ? List.generate(
                                                            provider.soilList
                                                                .length,
                                                            (index) {
                                                            return Column(
                                                                children:
                                                                    dynamicFormFertilizer(
                                                              provider,
                                                              context,
                                                              provider.soilList[
                                                                  index],
                                                              index,
                                                            ));
                                                          })
                                                        : dynamicForm(
                                                            provider,
                                                            context,
                                                          ),
                                                  ),
                                                  const SizedBox(height: 20),
                                                  provider.soilList
                                                              .isNotEmpty &&
                                                          !isReadOnly
                                                      ? Container(
                                                          width: MediaQuery.of(
                                                                      context)
                                                                  .size
                                                                  .width *
                                                              0.8,
                                                          child: CommonButton(
                                                              color: const Color(
                                                                  0xffE5EAEF),
                                                              icon: const Icon(
                                                                  Icons.add,
                                                                  color: Colors
                                                                      .black),
                                                              title:
                                                                  "เพิ่มปุ๋ย",
                                                              textStyle:
                                                                  const TextStyle(
                                                                      color: Colors
                                                                          .black),
                                                              onPressed:
                                                                  () async {
                                                                await provider
                                                                    .onAddWidget();
                                                                scrollDown();
                                                              }),
                                                        )
                                                      : Container(),
                                                  const SizedBox(height: 20),
                                                ],
                                              ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                )),
            // button
            bottomNavigationBar: isReadOnly ? null : _bottomWidget(),
          ),
        );
      }),
    );
  }

  Widget padingWidget(Widget widget) => Padding(
        padding: const EdgeInsets.only(left: 18, right: 17, top: 15),
        child: widget,
      );
  Widget padingGroupWidget(Widget widget) => Padding(
        padding: const EdgeInsets.only(
          left: 10,
          top: 10,
        ),
        child: widget,
      );

  List<Widget> dynamicForm(
    ActivityPageProvider provider,
    BuildContext context,
  ) {
    return List.generate(
      provider.list.length,
      (index) {
        final data = provider.list[index];
        switch (data.inputType) {
          case 'remark':
            return data.value.isEmpty
                ? Container()
                : padingWidget(Container(
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      elevation: 8,
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data.title,
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red),
                            ),
                            const SizedBox(height: 3),
                            Text(
                              data.value,
                              style: const TextStyle(
                                  fontSize: 15, color: Colors.red),
                            )
                          ],
                        ),
                      ),
                    )));

          case "text_input":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputText(
                  isEnable: !isReadOnly,
                  controller: isReadOnly || isEdit
                      ? provider.controllers[index].first
                      : null,
                  isEdit: isEdit,
                  readOnly: isReadOnly,
                  hintText: data.placeHolder,
                  onChanged: ((value) {
                    provider.onUpdate(
                      index,
                      value,
                      data,
                      data.inputType,
                    );
                  }),
                  autoFocus: data.autoFocus,
                  isRequire: data.isRequired,
                  title: data.title,
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "group_radio":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      data.title,
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    data.isRequired
                        ? const Text(
                            "*",
                            style: TextStyle(color: Colors.red),
                          )
                        : Container(),
                  ],
                ),
                Row(
                    children: List.generate(data.items!.length, (idx) {
                  final items = data.items![idx];

                  return Column(
                    children: [
                      Row(
                        children: [
                          Radio<String>(
                            groupValue: isReadOnly
                                ? data.value
                                : provider.selectedRadioValue,
                            onChanged: (value) {
                              provider.onUpdate(index, value.toString(), data,
                                  data.inputType);
                            },
                            value: items.nameTh!,
                            fillColor: MaterialStateColor.resolveWith(
                                (states) =>
                                    isReadOnly ? Colors.grey : Colors.black),
                          ),
                          Text(items.nameTh!)
                        ],
                      ),
                    ],
                  );
                })),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "unit_input":
            return padingGroupWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UnitInputWidget(
                    isReadOnly: isReadOnly,
                    isEditing: isEdit,
                    controller: provider.controllers[index].first,
                    unitValueController: provider.controllers[index][1],
                    value: data.value,
                    unitValue: data.unitValue ?? '',
                    autoFocus: data.autoFocus,
                    title: data.title,
                    placeHolder: data.placeHolder ?? "",
                    onChanged: (value) {
                      provider.onUpdate(
                        index,
                        value,
                        data,
                        data.inputType,
                        unitValue: provider.values
                            .where((element) => element.index == data.index)
                            .first
                            .unitValue,
                      );
                    },
                    onUpdate: (unitValue) {
                      provider.onUpdate(
                        index,
                        provider.values
                            .where((element) => element.index == data.index)
                            .first
                            .value,
                        data,
                        data.inputType,
                        unitValue: unitValue,
                      );
                    },
                    items: data.items!,
                    isRequired: data.isRequired),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "dropdown":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(data.title,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        )),
                    const SizedBox(width: 5),
                    if (data.isRequired) ...[
                      const Text("*",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.red))
                    ]
                  ],
                ),
                const SizedBox(height: 10),
                Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        // border: Border.all(width: 0.3, color: Colors.grey),
                        color: isReadOnly
                            ? Colors.grey.shade300
                            : Colors.transparent,
                        borderRadius: BorderRadius.circular(12),
                      ),
                      child: DropDownTextField(
                        textStyle: TextStyle(
                            color:
                                isReadOnly ? Colors.grey[600] : Colors.black),
                        isEnabled: !isReadOnly,
                        controller: provider.controllers[index].first,
                        clearOption: false,
                        dropdownRadius: 12,
                        enableSearch: false,
                        dropDownIconProperty: IconProperty(
                          icon: Icons.keyboard_arrow_down,
                          color: Colors.black,
                          size: 30,
                        ),
                        searchShowCursor: false,
                        padding: EdgeInsets.zero,
                        textFieldDecoration: InputDecoration(
                          hintText: data.placeHolder,
                          isDense: true,
                          contentPadding: const EdgeInsets.only(
                            bottom: 16,
                            top: 16,
                            left: 10,
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(12),
                          ),
                          enabledBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                          ),
                          focusedBorder: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                          ),
                        ),
                        dropDownList: data.items!
                            .map(
                              (e) => DropDownValueModel(
                                value: e.nameTh,
                                name: e.nameTh!,
                              ),
                            )
                            .toList(),
                        onChanged: isReadOnly
                            ? null
                            : (v) {
                                provider.onUpdate(index, v.value.toString(),
                                    data, data.inputType);
                              },
                      ),
                    ),
                    // Container(
                    //   decoration: BoxDecoration(
                    //     border: Border.all(width: 0.3, color: Colors.grey),
                    //     color: isReadOnly
                    //         ? Colors.grey.shade300
                    //         : Colors.transparent,
                    //     borderRadius: BorderRadius.circular(12),
                    //   ),
                    //   child: DropdownButtonFormField2(
                    //     decoration: InputDecoration(
                    //       isDense: true,
                    //       contentPadding: EdgeInsets.zero,
                    //       border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.circular(12),
                    //       ),
                    //       enabledBorder: const OutlineInputBorder(
                    //         borderRadius: BorderRadius.all(Radius.circular(12)),
                    //         borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                    //       ),
                    //       focusedBorder: const OutlineInputBorder(
                    //         borderRadius: BorderRadius.all(Radius.circular(12)),
                    //         borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                    //       ),
                    //     ),
                    //     isExpanded: true,
                    //     icon: const Icon(
                    //       Icons.keyboard_arrow_down,
                    //       color: Colors.black,
                    //     ),
                    //     value: isReadOnly || isEdit
                    //         ? data.value
                    //         : data.value.isNotEmpty
                    //             ? data.value
                    //             : null,
                    //     style: TextStyle(
                    //       color:
                    //           isReadOnly ? Colors.grey.shade500 : Colors.black,
                    //       fontSize: 16,
                    //       fontFamily: 'Sarabun',
                    //     ),
                    //     iconSize: 30,
                    //     buttonHeight: 55,
                    //     buttonPadding:
                    //         const EdgeInsets.only(left: 10, right: 10),
                    //     dropdownDecoration: BoxDecoration(
                    //       borderRadius: BorderRadius.circular(12),
                    //     ),
                    //     hint: Text(
                    //       data.placeHolder!,
                    //     ),
                    //     key: ValueKey(numberOfFert),
                    //     items: data.items!
                    //         .map(
                    //           (e) => DropdownMenuItem(
                    //             key: ValueKey(e.id),
                    //             value: e.nameTh,
                    //             child: Text(e.nameTh!),
                    //           ),
                    //         )
                    //         .toList(),
                    //     onChanged: isReadOnly
                    //         ? null
                    //         : (v) {
                    //             provider.onUpdateFerterlizer(numberOfFert,
                    //                 index, v.toString(), data, data.inputType);
                    //           },
                    //   ),
                    // ),
                  ],
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));

          case "date_picker":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputText(
                  isEnable: !isReadOnly,
                  fillColor: isReadOnly ? Colors.grey[300] : Colors.transparent,
                  controller: isReadOnly || isEdit
                      ? provider.controllers[index].first
                      : provider.datePickerCtrl,
                  readOnly: true,
                  isEdit: isEdit,
                  focusedBorderColor: const Color(0XFFEDF2F7),
                  suffixIcon: const Icon(
                    Icons.calendar_month_outlined,
                    color: Colors.black,
                  ),
                  hintText: data.placeHolder,
                  autoFocus: data.autoFocus,
                  isRequire: data.isRequired,
                  title: data.title,
                  onTap: isReadOnly
                      ? null
                      : () {
                          showModalBottomSheet(
                              backgroundColor: Colors.transparent,
                              context: context,
                              builder: (BuildContext context) {
                                return DatePickerThai(
                                  defaultAtleastYear: 0,
                                  initialDateTime: DateTime(DateTime.now().year,
                                      DateTime.now().month, DateTime.now().day),
                                  valueChanged: (value) {
                                    provider.onUpdate(
                                      index,
                                      value.toString(),
                                      data,
                                      data.inputType,
                                    );
                                  },
                                );
                              });
                        },
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "check_box":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Checkbox(
                        activeColor:
                            isReadOnly ? Colors.grey : const Color(0xff0AC898),
                        onChanged: isReadOnly
                            ? null
                            : (value) {
                                provider.onUpdate(index, value.toString(), data,
                                    data.inputType);
                              },
                        value: isReadOnly || isEdit
                            ? BoolParsing(data.value).parseBool()
                            : provider.selectedCheckboxValue),
                    Text(data.title, style: const TextStyle(fontSize: 16)),
                  ],
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "display":
            return padingWidget(Container(
                alignment: Alignment.centerLeft,
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  data.title,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                )));
          case "triple_text_input":
            return padingGroupWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Text(
                        data.title,
                        style: const TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                    Row(
                      children: [
                        const SizedBox(width: 10),
                        Flexible(
                          child: InputText(
                            isEnable: !isReadOnly,
                            controller: provider.controllers[index].first,
                            title: '',
                            isNumber: true,
                            hintText: data.placeHolder,
                            isRequire: data.isRequired,
                            onChanged: (value) {
                              provider.onUpdate(
                                index,
                                value,
                                data,
                                data.inputType,
                                value2: provider.list
                                    .where((e) => e.index == data.index)
                                    .first
                                    .value2,
                                value3: provider.list
                                    .where((e) => e.index == data.index)
                                    .first
                                    .value3,
                              );
                            },
                          ),
                        ),
                        const SizedBox(width: 10),
                        Flexible(
                          child: InputText(
                            title: "",
                            isEnable: !isReadOnly,
                            controller: provider.controllers[index][1],
                            isNumber: true,
                            hintText: data.placeHolder,
                            onChanged: (value) {
                              provider.onUpdate(
                                index,
                                provider.list
                                    .where((e) => e.index == data.index)
                                    .first
                                    .value,
                                data,
                                data.inputType,
                                value2: value,
                                value3: provider.list
                                    .where((e) => e.index == data.index)
                                    .first
                                    .value3,
                              );
                            },
                          ),
                        ),
                        const SizedBox(width: 10),
                        Flexible(
                          child: InputText(
                            isEnable: !isReadOnly,
                            controller: provider.controllers[index][2],
                            title: "",
                            isNumber: true,
                            hintText: data.placeHolder,
                            onChanged: (value) {
                              provider.onUpdate(
                                index,
                                provider.list
                                    .where((e) => e.index == data.index)
                                    .first
                                    .value,
                                data,
                                data.inputType,
                                value3: value,
                                value2: provider.list
                                    .where((e) => e.index == data.index)
                                    .first
                                    .value2,
                              );
                            },
                          ),
                        ),
                        const SizedBox(width: 10),
                      ],
                    ),
                  ],
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "image_box":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BoxWithImg(
                  isEnable: !isReadOnly,
                  isRequire: data.isRequired,
                  onTap: () async {
                    await showImageSource(context, 0, index, provider,
                        data: data);
                  },
                  hintText: isReadOnly ? "ไม่มีรูปภาพ" : "เพิ่มรูปภาพ",
                  title: data.title,
                  showInFo: false,
                  imgPath: "",
                  widgetImage: provider.values
                          .where((element) => element.index == data.index)
                          .first
                          .signedUrl!
                          .isEmpty
                      ? null
                      : PreviewImageWidget(
                          isEnable: !isReadOnly,
                          clearImage: () {
                            provider.clearIdImage(provider.values, data: data);
                          },
                          imagePath: Uint8List(0),
                          imagePathString: provider.values
                              .where((element) => element.index == data.index)
                              .first
                              .signedUrl,
                        ),
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          default:
            return padingWidget(Container(
              alignment: Alignment.centerLeft,
              child: Text(data.title, style: const TextStyle(fontSize: 16)),
            ));
        }
      },
    );
  }

  List<Widget> dynamicFormFertilizer(
      ActivityPageProvider provider,
      BuildContext context,
      List<ActivityFertResponseModel> lists,
      int numberOfFert) {
    return List.generate(
      lists.length,
      (index) {
        final data = lists[index];
        switch (data.inputType) {
          case 'remark':
            return data.value.isEmpty
                ? Container()
                : padingWidget(Container(
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      elevation: 8,
                      child: Container(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              data.title,
                              style: const TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red),
                            ),
                            const SizedBox(height: 3),
                            Text(
                              data.value,
                              style: const TextStyle(
                                  fontSize: 15, color: Colors.red),
                            )
                          ],
                        ),
                      ),
                    )));

          case "text_input":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputText(
                  controller:
                      provider.fertControllers[numberOfFert][index].first,
                  hintText: data.placeHolder,
                  onChanged: ((value) {
                    provider.onUpdateFerterlizer(
                      numberOfFert,
                      index,
                      value,
                      data,
                      data.inputType,
                    );
                  }),
                  readOnly: isReadOnly,
                  isEnable: !isReadOnly,
                  autoFocus: data.autoFocus,
                  isRequire: data.isRequired,
                  title: data.title,
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "group_radio":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(data.title,
                    style: const TextStyle(
                        fontSize: 18, fontWeight: FontWeight.w500)),
                Row(
                    children: List.generate(data.items!.length, (idx) {
                  final items = data.items![idx];
                  return Column(
                    children: [
                      Row(
                        children: [
                          Radio<String>(
                            groupValue: isReadOnly
                                ? data.value
                                : provider.selectedRadioValue,
                            onChanged: (value) {
                              provider.onUpdateFerterlizer(numberOfFert, index,
                                  value.toString(), data, data.inputType);
                            },
                            value: items.nameTh!,
                            fillColor: MaterialStateColor.resolveWith(
                                (states) =>
                                    isReadOnly ? Colors.grey : Colors.black),
                          ),
                          Text(items.nameTh!)
                        ],
                      ),
                    ],
                  );
                })),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "unit_input":
            return padingGroupWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                UnitInputWidget(
                    controller:
                        provider.fertControllers[numberOfFert][index].first,
                    unitValueController: provider.fertControllers[numberOfFert]
                        [index][1],
                    isReadOnly: isReadOnly,
                    isEditing: isEdit,
                    unitValue: data.unitValue!,
                    autoFocus: data.autoFocus,
                    title: data.title,
                    placeHolder: data.placeHolder ?? "",
                    onChanged: (value) {
                      provider.onUpdateFerterlizer(
                        numberOfFert,
                        index,
                        value,
                        data,
                        data.inputType,
                        unitValue: provider.fertValues[numberOfFert]
                            .where((element) => element.index == data.index)
                            .first
                            .unitValue,
                      );
                    },
                    onUpdate: (unitValue) {
                      provider.onUpdateFerterlizer(
                        numberOfFert,
                        index,
                        provider.fertValues[numberOfFert]
                            .where((element) => element.index == data.index)
                            .first
                            .value,
                        data,
                        data.inputType,
                        unitValue: unitValue,
                      );
                    },
                    itemsFert: data.items!,
                    isRequired: data.isRequired),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));

          case "dropdown":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(data.title,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                        )),
                    const SizedBox(width: 5),
                    if (data.isRequired) ...[
                      const Text("*",
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.red))
                    ]
                  ],
                ),
                const SizedBox(height: 10),
                Container(
                  decoration: BoxDecoration(
                    // border: Border.all(width: 0.3, color: Colors.grey),
                    color:
                        isReadOnly ? Colors.grey.shade300 : Colors.transparent,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: DropDownTextField(
                    isEnabled: !isReadOnly,
                    controller:
                        provider.fertControllers[numberOfFert][index].first,
                    clearOption: false,
                    dropdownRadius: 12,
                    textStyle: TextStyle(
                        color: isReadOnly ? Colors.grey[600] : Colors.black),
                    enableSearch: false,
                    dropDownIconProperty: IconProperty(
                      icon: Icons.keyboard_arrow_down,
                      color: Colors.black,
                      size: 30,
                    ),
                    searchShowCursor: false,
                    padding: EdgeInsets.zero,
                    textFieldDecoration: InputDecoration(
                      hintText: data.placeHolder,
                      isDense: true,
                      contentPadding: const EdgeInsets.only(
                        bottom: 16,
                        top: 16,
                        left: 10,
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                      ),
                      enabledBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                      ),
                    ),
                    dropDownList: data.items!
                        .map(
                          (e) => DropDownValueModel(
                            value: e.nameTh,
                            name: e.nameTh!,
                          ),
                        )
                        .toList(),
                    onChanged: isReadOnly
                        ? null
                        : (v) {
                            provider.onUpdateFerterlizer(numberOfFert, index,
                                v.value.toString(), data, data.inputType);
                          },
                  ),
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));

          case "date_picker":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                InputText(
                  isEnable: !isReadOnly,
                  fillColor: isReadOnly ? Colors.grey[300] : Colors.transparent,
                  controller: isReadOnly
                      ? provider.fertControllers[numberOfFert][index].first
                      : provider.datePickerCtrl,
                  readOnly: true,
                  focusedBorderColor: const Color(0XFFEDF2F7),
                  suffixIcon: const Icon(
                    Icons.calendar_month_outlined,
                    color: Colors.black,
                  ),
                  hintText: data.placeHolder,
                  autoFocus: data.autoFocus,
                  isRequire: data.isRequired,
                  title: data.title,
                  onTap: isReadOnly
                      ? null
                      : () {
                          showModalBottomSheet(
                              backgroundColor: Colors.transparent,
                              context: context,
                              builder: (BuildContext context) {
                                return DatePickerThai(
                                  defaultAtleastYear: 0,
                                  initialDateTime: DateTime(DateTime.now().year,
                                      DateTime.now().month, DateTime.now().day),
                                  valueChanged: (value) {
                                    provider.onUpdateFerterlizer(
                                      numberOfFert,
                                      index,
                                      value.toString(),
                                      data,
                                      data.inputType,
                                    );
                                  },
                                );
                              });
                        },
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "check_box":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Checkbox(
                        activeColor: const Color(0xff0AC898),
                        onChanged: (value) {
                          provider.onUpdateFerterlizer(numberOfFert, index,
                              value.toString(), data, data.inputType);
                        },
                        value: isReadOnly
                            ? BoolParsing(data.value).parseBool()
                            : provider.selectedCheckboxValue),
                    Text(data.title, style: const TextStyle(fontSize: 16)),
                  ],
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "display":
            return padingWidget(Container(
                alignment: Alignment.centerLeft,
                margin:
                    const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      data.title + " ${numberOfFert + 1}",
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    numberOfFert != 0 && !isReadOnly
                        ? InkWell(
                            onTap: () async {
                              print(provider
                                  .fertControllers[numberOfFert][index]
                                  .first
                                  .text);
                              final action =
                                  await ViewDialogs.ConfirmOrCancelDialog(
                                context,
                                "ยืนยันการลบปุ๋ย ?",
                                "หากท่านกดลบปุ๋ย ข้อมูลที่ท่านกรอกไว้จะถูกลบ ต้องการลบปุ๋ยกดยืนยัน",
                                "ยกเลิก",
                                "ยืนยัน",
                                Colors.red,
                              );

                              if (action == ViewDialogsAction.confirm) {
                                provider.onDeleteWidget(numberOfFert);
                              }
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              decoration: BoxDecoration(
                                color: const Color(0xffFFEBEB),
                                borderRadius: BorderRadius.circular(8),
                              ),
                              child: Row(
                                children: const [
                                  Icon(
                                    Icons.delete,
                                    color: Color(0xffDB1717),
                                  ),
                                  Text(
                                    "ลบปุ๋ย",
                                    style: TextStyle(
                                      color: Color(0xffDB1717),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        : Container()
                  ],
                )));
          case "triple_text_input":
            return padingGroupWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Row(
                    children: [
                      Text(
                        data.title,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      if (data.isRequired) ...[
                        const Text("*",
                            style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.red))
                      ]
                    ],
                  ),
                ),
                Row(
                  children: [
                    const SizedBox(width: 10),
                    Flexible(
                      child: InputText(
                        isEnable: !isReadOnly,
                        controller:
                            provider.fertControllers[numberOfFert][index].first,
                        hintText: data.placeHolder,
                        autoFocus: false,
                        isNumber: true,
                        onChanged: (value) {
                          provider.onUpdateFerterlizer(
                            numberOfFert,
                            index,
                            value,
                            data,
                            data.inputType,
                            value2: provider.fertValues[numberOfFert]
                                .where((e) => e.index == data.index)
                                .first
                                .value2,
                            value3: provider.fertValues[numberOfFert]
                                .where((e) => e.index == data.index)
                                .first
                                .value3,
                          );
                        },
                      ),
                    ),
                    const SizedBox(width: 10),
                    Flexible(
                      child: InputText(
                        isEnable: !isReadOnly,
                        controller: provider.fertControllers[numberOfFert]
                            [index][1],
                        autoFocus: false,
                        isNumber: true,
                        hintText: data.placeHolder,
                        onChanged: (value) {
                          provider.onUpdateFerterlizer(
                            numberOfFert,
                            index,
                            provider.fertValues[numberOfFert]
                                .where((e) => e.index == data.index)
                                .first
                                .value,
                            data,
                            data.inputType,
                            value2: value,
                            value3: provider.fertValues[numberOfFert]
                                .where((e) => e.index == data.index)
                                .first
                                .value3,
                          );
                        },
                      ),
                    ),
                    const SizedBox(width: 10),
                    Flexible(
                      child: InputText(
                        autoFocus: false,
                        isNumber: true,
                        hintText: data.placeHolder,
                        isEnable: !isReadOnly,
                        controller: provider.fertControllers[numberOfFert]
                            [index][2],
                        onChanged: (value) {
                          provider.onUpdateFerterlizer(
                            numberOfFert,
                            index,
                            provider.fertValues[numberOfFert]
                                .where((e) => e.index == data.index)
                                .first
                                .value,
                            data,
                            data.inputType,
                            value3: value,
                            value2: provider.fertValues[numberOfFert]
                                .where((e) => e.index == data.index)
                                .first
                                .value2,
                          );
                        },
                      ),
                    ),
                    const SizedBox(width: 10),
                  ],
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          case "image_box":
            return padingWidget(Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BoxWithImg(
                  isEnable: !isReadOnly,
                  isRequire: data.isRequired,
                  onTap: () async {
                    await showImageSource(
                        context, numberOfFert, index, provider,
                        fertData: data);
                  },
                  hintText: isReadOnly ? "ไม่มีรูปภาพ" : "เพิ่มรูปภาพ",
                  title: data.title,
                  showInFo: false,
                  imgPath: "",
                  widgetImage: provider.fertValues.isEmpty
                      ? Container()
                      : provider.fertValues[numberOfFert]
                              .where((element) => element.index == data.index)
                              .first
                              .signedUrl!
                              .isEmpty
                          ? null
                          : PreviewImageWidget(
                              isEnable: !isReadOnly,
                              clearImage: () {
                                provider.clearIdImage(
                                    provider.fertValues[numberOfFert],
                                    fertData: data);
                              },
                              imagePath: Uint8List(0),
                              imagePathString: provider.fertValues[numberOfFert]
                                  .where(
                                      (element) => element.index == data.index)
                                  .first
                                  .signedUrl,
                            ),
                ),
                data.isValidate == null
                    ? Container()
                    : (!data.isValidate! && data.isRequired)
                        ? Container(
                            margin: const EdgeInsets.only(
                              left: 10,
                              bottom: 10,
                              top: 10,
                            ),
                            child: Text(
                              data.errorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                          )
                        : Container(),
              ],
            ));
          default:
            return padingWidget(Container(
              alignment: Alignment.centerLeft,
              child: Text(data.title, style: const TextStyle(fontSize: 16)),
            ));
        }
      },
    );
  }

  Widget statusWidget(String status, BuildContext context) {
    Color fontColor;
    Color backgroundColor;
    String statusText = "";
    var icon = Icons.create_outlined;

    switch (status) {
      case "waiting for the activity log":
        icon = Icons.create_outlined;
        statusText = "รอบันทึกกิจกรรม";
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);
        break;
      case "waiting for edit the activity":
        icon = Icons.create_outlined;
        statusText = "รอแก้ไขกิจกรรม";
        fontColor = const Color(0xffEF7E21);
        backgroundColor = const Color(0xffFCEADB);
        break;
      case "waiting for approve the activity":
        icon = Icons.schedule;
        statusText = "รอตรวจสอบ";
        fontColor = const Color(0xffF8C309);
        backgroundColor = const Color(0xffFDF6DD);
        break;
      case "successful activity log":
        icon = Icons.check_circle_outline;
        statusText = "สำเร็จ";
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);
        break;
      case "activity rejected":
        statusText = "ไม่สำเร็จ";
        fontColor = const Color(0xffFF3E1D);
        backgroundColor = const Color(0xffFFE0DB);
        break;
      case "skipped activity":
        statusText = "กิจกรรมถูกข้าม";
        fontColor = const Color(0xff787F88);
        backgroundColor = const Color(0xffCBD5E0);
        break;

      default:
        icon = Icons.create_outlined;
        statusText = "รอบันทึกกิจกรรม";
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);
        break;
    }

    return Container(
      width: MediaQuery.of(context).size.width * 0.35,
      padding: const EdgeInsets.fromLTRB(10, 4, 10, 4),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 20,
            child: status == "activity rejected"
                ? SvgPicture.asset(
                    'assets/images/ic_failed_circle.svg',
                    width: 16,
                    height: 16,
                  )
                : Icon(
                    icon,
                    color: fontColor,
                    size: 20,
                  ),
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                statusText,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 13, color: fontColor),
              )
            ],
          )),
        ],
      ),
    );
  }
}
