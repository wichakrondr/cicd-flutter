import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/market_page_model/join_project_list_model.dart';
import 'package:varun_kanna_app/services/join_project_service.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';

import '../../model/farm_list_response.dart';
import '../../services/farm_service.dart';
import '../../utils/api/api_result.dart';

class MarketPageProvider with ChangeNotifier {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<FarmListModel> farmList = [];
  List<JoinProjectListModel> projectList = [];
  bool isLoading = true;
  bool isLoadingConsent = false;
  bool isClicked = false;
  late bool isSigned;
  String content = "";
  String consentId = "";
  initialData() async {
    await getProjectList();
  }

  setLoading(bool isLoading) {
    this.isLoading = isLoading;
    notifyListeners();
  }

  setLoadingConsent(bool isLoadingConsent) {
    this.isLoadingConsent = isLoadingConsent;
    notifyListeners();
  }

  Future<bool> checkIsEmptyFarm() async {
    isClicked = true;
    final SharedPreferences prefs = await _prefs;
    notifyListeners();
    await getFarm(prefs.getString('userId').toString());
    if (farmList.isEmpty) {
      isClicked = false;
      notifyListeners();
      setLoading(false);
      return true;
    } else {
      isClicked = false;
      notifyListeners();
      setLoading(false);
      return false;
    }
  }

  getFarm(String userId) async {
    ApiResult<FarmListResponse> response = await fetchFarmList(userId);
    response.when(
      success: (data) {
        farmList = data.data!;
        if (farmList.length == 0) {
          setLoading(false);
        }
        setLoading(false);
      },
      failure: (error) {
        setLoading(false);
        print(error);
      },
    );
  }

  Future getProjectList() async {
    setLoading(true);
    ApiResult<JoinProjectListResponse> response =
        await JoinProjectService().getProjectList();
    response.when(failure: (NetworkExceptions error) {
      setLoading(false);
    }, success: (JoinProjectListResponse data) {
      projectList = data.data!;
      setLoading(false);
    });
  }

  Future getConset({required String projectId}) async {
    isClicked = true;
    print(isClicked);
    final SharedPreferences prefs = await _prefs;

    final response = await JoinProjectService().getProjectConsent(
        userId: prefs.getString('userId').toString(), projectId: projectId);

    setLoadingConsent(true);
    notifyListeners();
    response.when(
      success: (data) {
        if (data.data == null) {
          isSigned = false;
          isClicked = false;
          notifyListeners();
        } else {
          isSigned = data.data!.signed!;
          content = data.data!.currentVersion!.content!;
          consentId = data.data!.currentVersion!.id!;

          setLoadingConsent(false);
          isClicked = false;
          notifyListeners();
        }
      },
      failure: (error) {
        isSigned = false;
        isClicked = false;
        setLoading(false);
        notifyListeners();
        print(error);
      },
    );
  }
}
