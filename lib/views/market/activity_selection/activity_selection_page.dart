import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/views/market/activity_list/activity_list_page.dart';
import 'package:varun_kanna_app/views/market/activity_selection/activity_selection_page_provider.dart';
import 'package:varun_kanna_app/views/market/landing/landing_page.dart';
import 'package:varun_kanna_app/views/market/verify_main_page.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';

import '../../../utils/casting.dart';

class ActivitySelectionPage extends StatelessWidget {
  ActivitySelectionPage({
    Key? key,
    required this.farmItem,
    required this.projectId,
    required this.projectName,
    required this.reqJoinProjectId,
    required this.headerImage,
  }) : super(key: key);

  RequestJoinProjectListModel farmItem;
  String projectId;
  String reqJoinProjectId;
  static const navigateToVerifyMainPageButtonKey =
      Key('navigateToVerifyMainPage');
  static const navigateToActivityListPageButtonKey =
      Key('navigateToActivityListPage');
  String projectName;
  String headerImage;
  @override
  Widget build(BuildContext context) {
    Future<bool> _onWillPop() async {
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => LandingPage(
              projectId: projectId,
              projectName: projectName,
              headerImage: headerImage),
        ),
      );
      return true;
    }

    return ChangeNotifierProvider(
      create: (context) =>
          ActivitySelectionPageProvider()..initialData(farmItem),
      child: Consumer<ActivitySelectionPageProvider>(
          builder: (context, activityProv, _) {
        return WillPopScope(
          onWillPop: _onWillPop,
          child: Scaffold(
              backgroundColor: Colors.white,
              body: Stack(
                children: [
                  Center(
                    child: Column(
                      children: [
                        AppBarText(
                            textHeader: "กิจกรรม",
                            isback: true,
                            onBackPress: () => _onWillPop()),
                        const SizedBox(
                          height: 10,
                        ),
                        farmInfo(context, activityProv),
                        verificationBtn(context, activityProv),
                        activityBtn(context, activityProv),
                      ],
                    ),
                  ),
                  activityProv.loading
                      ? Center(child: CustomLoadingWidget())
                      : Container()
                ],
              )),
        );
      }),
    );
  }

  Widget farmInfo(
      BuildContext context, ActivitySelectionPageProvider activityProv) {
    final width = MediaQuery.of(context).size.width;
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      margin: const EdgeInsets.only(top: 10, left: 2, right: 2, bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            offset: const Offset(0, 1),
            blurRadius: 5,
            spreadRadius: 0,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: width > 800
            ? MainAxisAlignment.center
            : MainAxisAlignment.spaceAround,
        crossAxisAlignment:
            width > 800 ? CrossAxisAlignment.center : CrossAxisAlignment.start,
        children: [
          width > 800 ? const Expanded(child: SizedBox()) : Container(),
          Container(
            width: width > 800
                ? width * 0.2
                : MediaQuery.of(context).size.width * 0.45,
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(farmItem.farmName,
                      style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      )),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text('พื้นที่ ${farmItem.displayArea}',
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    )),
              ],
            ),
          ),
          width > 800 ? const Expanded(child: SizedBox()) : Container(),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 20, 25, 20),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(18),
                child: CachedNetworkImage(imageUrl: farmItem.farmImg)),
          ),
          width > 800 ? const Expanded(child: SizedBox()) : Container(),
        ],
      ),
    );
  }

  Widget verificationStatus(
      ActivitySelectionPageProvider activityProv, BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.35,
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        color: activityProv.backgroundColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          activityProv.statusVerify == "verify rejected"
              ? SvgPicture.asset(
                  'assets/images/ic_failed_circle.svg',
                  width: 16,
                  height: 16,
                )
              : Icon(
                  activityProv.icon,
                  color: activityProv.fontColor,
                  size: 20,
                ),
          const SizedBox(
            width: 5,
          ),
          Text(
            activityProv.statusText,
            style: TextStyle(fontSize: 13, color: activityProv.fontColor),
          )
        ],
      ),
    );
  }

  Widget verificationBtn(
      BuildContext context, ActivitySelectionPageProvider activityProv) {
    return InkWell(
        key: navigateToVerifyMainPageButtonKey,
        onTap: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (_) => VerifyMainPage(
                  statusType: Cast.activityBEtoFEType(farmItem.statusVerify),
                  farmItem: farmItem,
                  projectId: projectId,
                  reqJoinProjectId: reqJoinProjectId,
                  projectName: projectName,
                  headerImage: headerImage),
            ),
          );
        },
        child: Container(
          width: MediaQuery.of(context).size.width * 0.9,
          margin: const EdgeInsets.only(top: 10, left: 2, right: 2, bottom: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: const Offset(0, 1),
                blurRadius: 5,
                spreadRadius: 0,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: MediaQuery.of(context).size.width * 0.45,
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    verificationStatus(activityProv, context),
                    const SizedBox(
                      height: 20,
                    ),
                    const Text(
                      "ยืนยันข้อมูล\nแปลงปลูก",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(30, 20, 25, 20),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(18),
                  child: SvgPicture.asset(
                    "assets/images/ic_waiting_information.svg",
                    alignment: Alignment.topCenter,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Widget activityStatus(ActivitySelectionPageProvider activityProv,
      BuildContext context, bool isDisable) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      padding: const EdgeInsets.fromLTRB(2, 4, 2, 4),
      decoration: BoxDecoration(
        color: activityProv.activityBackgroundColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          activityProv.statusActivity == "activity rejected"
              ? SvgPicture.asset(
                  'assets/images/ic_failed_circle.svg',
                  width: 16,
                  height: 16,
                )
              : Icon(
                  activityProv.activityIcon,
                  color: activityProv.activityFontColor,
                  size: 20,
                ),
          const SizedBox(
            width: 5,
          ),
          Text(
            activityProv.activityStatusText,
            style:
                TextStyle(fontSize: 13, color: activityProv.activityFontColor),
          )
        ],
      ),
    );
  }

  Widget activityBtn(
      BuildContext context, ActivitySelectionPageProvider activityProv) {
    return InkWell(
      key: navigateToActivityListPageButtonKey,
      onTap: () {
        activityProv.disableActivityBtn
            ? null
            : Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ActivityListPage(
                          farmItem: farmItem,
                          projectId: projectId,
                          reqJoinProjectId: reqJoinProjectId,
                        )));
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.9,
        margin: const EdgeInsets.only(top: 10, left: 2, right: 2, bottom: 10),
        decoration: BoxDecoration(
          color: activityProv.disableActivityBtn
              ? const Color(0XFFE5EAEF)
              : Colors.white,
          borderRadius: BorderRadius.circular(20),
          border: Border.all(
              color: activityProv.disableActivityBtn
                  ? const Color(0XFFA4AEB9)
                  : Colors.white),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.3),
              offset: const Offset(0, 1),
              blurRadius: 5,
              spreadRadius: 0,
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: MediaQuery.of(context).size.width * 0.45,
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  activityStatus(
                      activityProv, context, activityProv.disableActivityBtn),
                  const SizedBox(
                    height: 20,
                  ),
                  Center(
                    child: Text(
                      "บันทึกกิจกรรม\nการปลูกพืช",
                      style: TextStyle(
                          color: activityProv.disableActivityBtn
                              ? activityProv.activityFontColor
                              : Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: activityProv.disableActivityBtn
                  ? SvgPicture.asset(
                      "assets/images/ic_waiting_activity_disable.svg",
                      alignment: Alignment.topCenter,
                      fit: BoxFit.cover,
                    )
                  : SvgPicture.asset(
                      "assets/images/ic_waiting_activity.svg",
                      alignment: Alignment.topCenter,
                      fit: BoxFit.cover,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
