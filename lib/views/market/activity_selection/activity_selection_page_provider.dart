import 'package:flutter/material.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';

class ActivitySelectionPageProvider extends ChangeNotifier {
  bool loading = true;

  Color? fontColor;
  Color? backgroundColor;
  String statusText = "";
  String statusActivity = "";
  String statusVerify = '';

  Color? activityFontColor;
  Color? activityBackgroundColor;
  String activityStatusText = "";
  bool disableActivityBtn = true;
  var icon = Icons.create_outlined;
  var activityIcon = Icons.schedule;

  bool init = false;

  void initialData(RequestJoinProjectListModel farmItem) async {
    statusActivity = farmItem.statusActivity;
    statusVerify = farmItem.statusVerify;
    await manageStatusVerify(farmItem.statusVerify);
    await manageStatusActivity(farmItem.statusActivity);
    notifyListeners();
  }

  Future manageStatusVerify(String status) async {
    switch (status) {
      case "waiting for information":
        statusText = "รอบันทึกข้อมูล";
        icon = Icons.create_outlined;
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);

        break;
      case "waiting for edit identity":
        statusText = "รอแก้ไขข้อมูล";
        icon = Icons.create_outlined;
        fontColor = const Color(0xffEF7E21);
        backgroundColor = const Color(0xffFCEADB);

        break;
      case "waiting for verify identity":
        statusText = "รอตรวจสอบ";
        icon = Icons.schedule;
        fontColor = const Color(0xffF8C309);
        backgroundColor = const Color(0xffFDF6DD);

        break;
      case "verified":
        statusText = "สำเร็จ";
        icon = Icons.check_circle_outline;
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);

        break;
      case "verify rejected":
        statusText = "ไม่สำเร็จ";
        fontColor = const Color(0xffFF3E1D);
        backgroundColor = const Color(0xffFFE0DB);

        break;
      case "waiting for the activity log":
        statusText = "สำเร็จ";
        icon = Icons.check_circle_outline;
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);

        break;
      case "waiting for edit the activity":
        statusText = "สำเร็จ";
        icon = Icons.check_circle_outline;
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);

        break;
      case "waiting for approve the activity":
        statusText = "สำเร็จ";
        icon = Icons.check_circle_outline;
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);

        break;
      case "successful activity log":
        statusText = "สำเร็จ";
        icon = Icons.check_circle_outline;
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);

        break;
      case "activity rejected":
        statusText = "สำเร็จ";
        icon = Icons.check_circle_outline;
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);
        disableActivityBtn = false;
        break;
      default:
        statusText = "รอบันทึกข้อมูล";
        icon = Icons.create_outlined;
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);

        break;
    }
    loading = false;
    init = true;
    notifyListeners();
  }

  Future manageStatusActivity(String status) async {
    switch (status) {
      case "waiting for information":
        activityStatusText = "รออนุมัติแปลงปลูก";
        activityIcon = Icons.schedule;
        activityFontColor = const Color(0XFF787F88);
        activityBackgroundColor = const Color(0XFFCBD5E0);
        disableActivityBtn = true;

        break;
      case "waiting for edit identity":
        activityStatusText = "รออนุมัติแปลงปลูก";
        activityIcon = Icons.schedule;
        activityFontColor = const Color(0XFF787F88);
        activityBackgroundColor = const Color(0XFFCBD5E0);
        disableActivityBtn = true;

        break;
      case "waiting for verify identity":
        activityStatusText = "รออนุมัติแปลงปลูก";
        activityIcon = Icons.schedule;
        activityFontColor = const Color(0XFF787F88);
        activityBackgroundColor = const Color(0XFFCBD5E0);
        disableActivityBtn = true;

        break;
      case "verified":
        activityStatusText = "รอบันทึกกิจกรรม";
        activityIcon = Icons.create_outlined;
        activityFontColor = const Color(0XFF3D75D6);
        activityBackgroundColor = const Color(0XFFE4ECFA);
        disableActivityBtn = false;
        break;
      case "verify rejected":
        activityStatusText = "รออนุมัติแปลงปลูก";
        activityIcon = Icons.schedule;
        activityFontColor = const Color(0XFF787F88);
        activityBackgroundColor = const Color(0XFFCBD5E0);
        disableActivityBtn = true;
        break;
      case "waiting for the activity log":
        activityStatusText = "รอบันทึกกิจกรรม";
        activityIcon = Icons.create_outlined;
        activityFontColor = const Color(0XFF3D75D6);
        activityBackgroundColor = const Color(0XFFE4ECFA);
        disableActivityBtn = false;
        break;
      case "waiting for edit the activity":
        activityStatusText = "รอแก้ไขกิจกรรม";
        activityIcon = Icons.create_outlined;
        activityFontColor = const Color(0xffEF7E21);
        activityBackgroundColor = const Color(0xffFCEADB);
        disableActivityBtn = false;
        break;
      case "waiting for approve the activity":
        activityStatusText = "รอตรวจสอบ";
        activityIcon = Icons.schedule;
        activityFontColor = const Color(0xffF8C309);
        activityBackgroundColor = const Color(0xffFDF6DD);
        disableActivityBtn = false;
        break;
      case "successful activity log":
        activityStatusText = "สำเร็จ";
        activityIcon = Icons.check_circle_outline;
        activityFontColor = const Color(0xff0AC898);
        activityBackgroundColor = const Color(0xffD8F6EF);
        disableActivityBtn = false;
        break;
      case "activity rejected":
        activityStatusText = "ไม่สำเร็จ";
        activityFontColor = const Color(0xffFF3E1D);
        activityBackgroundColor = const Color(0xffFFE0DB);
        disableActivityBtn = false;
        break;
      default:
        activityStatusText = "รออนุมัติแปลงปลูก";
        activityIcon = Icons.schedule;
        activityFontColor = const Color(0XFF787F88);
        activityBackgroundColor = const Color(0XFFCBD5E0);
        disableActivityBtn = true;
        break;
    }
    loading = false;
    init = true;
    notifyListeners();
  }
}
