import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/utils/casting.dart';
import 'package:varun_kanna_app/views/market/select_plant_page/select_plant_page_provider.dart';
import 'package:varun_kanna_app/views/market/verify_main_page_provider.dart';
import 'package:varun_kanna_app/widget/bottom_sheet_address.dart';
import 'package:varun_kanna_app/widget/date_picker_thai.dart';
import 'package:varun_kanna_app/widget/input_radio_btn.dart';
import 'package:varun_kanna_app/widget/input_text.dart';

class SelectPlantPage extends StatelessWidget {
  SelectPlantPage(
      {super.key, required this.projectId, required this.projectName});
  String projectId;
  String projectName;
  ScrollController scrollController = ScrollController();

  TextEditingController birthDateCtrl = TextEditingController();

  Object? plantListValue = -1;
  late DateTime tempPickedDate =
      DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day);
  @override
  Widget build(BuildContext context) {
    bool isWDProject = projectName == "โครงการปลูกข้าวเปียกสลับแห้ง";
    //0d6dd87b-eab7-43c2-809b-dec652ac5f57 wet
    //2e812160-dfd2-427b-82fd-cf545b06b92a fire
    return ChangeNotifierProvider(
      create: (context) => SelectPlantProvider()
        ..initialData(scrollController: scrollController),
      child: Consumer2<SelectPlantProvider, VerifyMainPageProvider>(
          builder: (context, provider, mainPageProvider, _) {
        void validField() {
          // WD project do this one
          if (isWDProject) {
            if (mainPageProvider.getTypeIrrigation > 0) {
              // selected rice
              if (mainPageProvider.getSelectedPlantName == "ข้าว") {
                if (mainPageProvider.fieldTypeCtrl <= 0 ||
                    mainPageProvider.ricePlantingTypeCtrl <= 0 ||
                    mainPageProvider.awdDayCtrl.text.isEmpty ||
                    mainPageProvider.interredTypeCtrl <= 0) {
                  mainPageProvider.setIsEnableNextBtnPage2(false);
                } else {
                  mainPageProvider.setIsEnableNextBtnPage2(true);
                }
              } else if (mainPageProvider.getSelectedPlantName.isNotEmpty &&
                  mainPageProvider.getSelectedPlantName != "ข้าว") {
                mainPageProvider.setIsEnableNextBtnPage2(true);
              }
            } else {
              mainPageProvider.setIsEnableNextBtnPage2(false);
            }
          } else {
            if (mainPageProvider.isFireCtrl <= 0 ||
                (mainPageProvider.selectedPlant <= 0 && mainPageProvider.getSelectedPlantName.isEmpty ) ||
                mainPageProvider.forcastPlantingDateCtrl.text.isEmpty ||
                mainPageProvider.forcastHarvestDateCtrl.text.isEmpty ||
                mainPageProvider.interredTypeCtrl <= 0) {
              mainPageProvider.setIsEnableNextBtnPage2(false);
            } else {
              mainPageProvider.setIsEnableNextBtnPage2(true);
            }
          }
        }

        Widget switchDefaultRadio() => isWDProject
            ? SizedBox(
                child: RadioButton(
                  isEnable: mainPageProvider.getEnableFields,
                  onChange: (index) => {
                    mainPageProvider.setTypeIrrigation(index!),
                    validField()
                  },
                  isRequire: true,
                  title: "อยู่ในพื้นที่ชลประทานหรือไม่",
                  radioBtn: const ["ใช่", "ไม่"],
                  selectedIndex: mainPageProvider.getTypeIrrigation,
                ),
              )
            : SizedBox(
                child: RadioButton(
                  isEnable: mainPageProvider.getEnableFields,
                  onChange: (index) =>
                      {mainPageProvider.setIsFire(index!), validField()},
                  isRequire: true,
                  title: "มีการเผาที่แปลงเพาะปลูกนี้",
                  radioBtn: const ["ใช่", "ไม่"],
                  selectedIndex: mainPageProvider.getIsFire,
                ),
              );
        List<Widget> forcastDate() {
          List<Widget> children = [];
          if (!isWDProject) {
            children.add(const SizedBox(
              height: 15,
            ));
            children.add(
              InputText(
                isEnable: mainPageProvider.getEnableFields,
                isRequire: true,
                autoFocus: false,
                title: "คาดการณ์วันเริ่มปลูก",
                controller: mainPageProvider.forcastPlantingDateCtrl,
                readOnly: mainPageProvider.getEnableFields,
                focusedBorderColor: const Color(0XFFEDF2F7),
                suffixIcon: const Icon(
                  Icons.calendar_month_outlined,
                  color: Colors.black,
                ),
                onTap: () {
                  showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      context: context,
                      builder: (BuildContext context) {
                        return DatePickerThai(
                          defaultAtleastYear: 0,
                          initialDateTime: tempPickedDate,
                          valueChanged: (DateTime dt) {
                            tempPickedDate = dt;
                            mainPageProvider.forcastPlantingDateFormat =
                                dt.toString().substring(0, 10);
                            mainPageProvider.forcastPlantingDateCtrl.text =
                                Cast.convertLacaleDate(
                                    dt.toString().substring(0, 10));
                            validField();
                          },
                        );
                      });
                },
                hintText: "1 มกราคม 2548",
              ),
            );
            children.add(const SizedBox(
              height: 15,
            ));
            children.add(
              InputText(
                isEnable: mainPageProvider.getEnableFields,
                isRequire: true,
                autoFocus: false,
                title: "คาดการณ์วันเก็บเกี่ยว",
                controller: mainPageProvider.forcastHarvestDateCtrl,
                readOnly: mainPageProvider.getEnableFields,
                focusedBorderColor: const Color(0XFFEDF2F7),
                suffixIcon: const Icon(
                  Icons.calendar_month_outlined,
                  color: Colors.black,
                ),
                onTap: () {
                  showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      context: context,
                      builder: (BuildContext context) {
                        return DatePickerThai(
                          defaultAtleastYear: 0,
                          initialDateTime: tempPickedDate,
                          valueChanged: (DateTime dt) {
                            tempPickedDate = dt;
                            mainPageProvider.forcastHarvestDateFormat =
                                dt.toString().substring(0, 10);
                            mainPageProvider.forcastHarvestDateCtrl.text =
                                Cast.convertLacaleDate(
                                    dt.toString().substring(0, 10));
                            validField();
                          },
                        );
                      });
                },
                hintText: "1 มกราคม 2548",
              ),
            );
          }
          return children;
        }

        Widget _ricePlanField() {
          return Column(
            children: [
              SizedBox(
                child: RadioButton(
                  isEnable: mainPageProvider.getEnableFields,
                  onChange: (index) =>
                      {mainPageProvider.setfieldTypeCtrl(index!), validField()},
                  isRequire: true,
                  title: "ประเภทการปลูกข้าว",
                  radioBtn: mainPageProvider.type_of_rice_cultivation,
                  selectedIndex: mainPageProvider.fieldTypeCtrl,
                ),
              ),
              SizedBox(
                child: RadioButton(
                  isEnable: mainPageProvider.getEnableFields,
                  onChange: (index) => {
                    mainPageProvider.setRicePlantingTypeCtrl(index!),
                    validField()
                  },
                  isRequire: true,
                  title: "ประเภทการทำนา",
                  radioBtn: mainPageProvider.type_of_farming,
                  selectedIndex: mainPageProvider.ricePlantingTypeCtrl,
                ),
              ),
              InputText(
                isEnable: mainPageProvider.getEnableFields,
                isRequire: true,
                autoFocus: false,
                title: "คาดการณ์วันเริ่มปลูกข้าวในโครงการ AWD",
                controller: mainPageProvider.awdDayCtrl,
                readOnly: mainPageProvider.getEnableFields,
                focusedBorderColor: const Color(0XFFEDF2F7),
                suffixIcon: const Icon(
                  Icons.calendar_month_outlined,
                  color: Colors.black,
                ),
                onTap: () {
                  showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      context: context,
                      builder: (BuildContext context) {
                        return DatePickerThai(
                          defaultAtleastYear: 0,
                          initialDateTime: tempPickedDate,
                          valueChanged: (DateTime dt) {
                            tempPickedDate = dt;
                            mainPageProvider.awdDayDateFormat =
                                dt.toString().substring(0, 10);
                            mainPageProvider.awdDayCtrl.text =
                                Cast.convertLacaleDate(
                                    dt.toString().substring(0, 10));
                            validField();
                          },
                        );
                      });
                },
                hintText: "1 มกราคม 2548",
              ),
              SizedBox(
                child: RadioButton(
                  isEnable: mainPageProvider.getEnableFields,
                  onChange: (index) => {
                    mainPageProvider.setInterredTypeCtrl(index!),
                    validField()
                  },
                  isRequire: true,
                  title: "สนใจเข้าร่วมโครงการหรือเปล่า",
                  radioBtn: const ["สนใจ", "ไม่สนใจ"],
                  selectedIndex: mainPageProvider.interredTypeCtrl,
                ),
              ),
            ],
          );
        }

        Widget _plantListBottom(List item) {
          var plantList = item;
          return SizedBox(
              height: MediaQuery.of(context).size.height * 0.85,
              child: BottomSheetAddress(
                  autoFocus: false,
                  title: "เลือกพืช",
                  item: plantList,
                  groupValue: mainPageProvider.selectedPlant,
                  onClosePress: () {
                    Navigator.of(context).pop();
                  },
                  valueChanged: (Object data) {
                    mainPageProvider.setIconPath(
                        plantList[int.parse(data.toString()) - 1]["icon_path"]);
                    mainPageProvider.plantListCtrl.text =
                        plantList[int.parse(data.toString()) - 1]["name_th"];

                    mainPageProvider
                        .setSelectedPlant(int.parse(data.toString()));
                    mainPageProvider.setSelectedPlantName(
                        plantList[int.parse(data.toString()) - 1]["name_th"]);
                    validField();
                    Navigator.of(context).pop();
                  }));
        }

        Widget interredNoFireProject() => !isWDProject
            ? SizedBox(
                child: RadioButton(
                  isEnable: mainPageProvider.getEnableFields,
                  onChange: (index) => {
                    mainPageProvider.setInterredTypeCtrl(index!),
                    validField()
                  },
                  isRequire: true,
                  titleSize: 15,
                  title: "สนใจเข้าร่วมโครงการเราไม่เผา (V No Burn) หรือไม่",
                  radioBtn: const ["สนใจ", "ไม่สนใจ"],
                  selectedIndex: mainPageProvider.interredTypeCtrl,
                ),
              )
            : const SizedBox();

        Future<bool> _onWillPop() async {
          mainPageProvider.setProcessPage(mainPageProvider.getProcessPage - 1);
          return false;
        }

// render
        return WillPopScope(
            onWillPop: _onWillPop,
            child: SizedBox(
              child: SingleChildScrollView(
                  child: Container(
                      decoration: const BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Column(
                        children: [
                          Column(children: [
                            Container(
                                padding: const EdgeInsets.only(
                                    left: 20, right: 20, bottom: 20),
                                child: Column(
                                  children: [
                                    switchDefaultRadio(),
                                    InputText(
                                      isEnable:
                                          mainPageProvider.getEnableFields,
                                      isRequire: true,
                                      autoFocus: false,
                                      suffix: SvgPicture.network(
                                        mainPageProvider.iconPath,
                                        height: 25,
                                      ),
                                      title: "พืชที่ปลูก",
                                      controller:
                                          mainPageProvider.plantListCtrl,
                                      hintText: "กรุณาเลือกพืช",
                                      suffixIcon: const Icon(
                                        Icons.expand_more,
                                        color: Color(0XFFA0AEC0),
                                      ),
                                      readOnly:
                                          mainPageProvider.getEnableFields,
                                      focusedBorderColor:
                                          const Color(0XFFEDF2F7),
                                      onTap: () {
                                        showModalBottomSheet(
                                            isScrollControlled: true,
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            context: context,
                                            builder: (BuildContext context) {
                                              return _plantListBottom(
                                                  mainPageProvider
                                                      .getPlantList(context));
                                            });
                                      },
                                    ),
                                    ((mainPageProvider.getSelectedPlantName ==
                                                    "ข้าว" ||
                                                mainPageProvider
                                                        .plantListCtrl.text ==
                                                    "ข้าว") &&
                                            isWDProject)
                                        ? _ricePlanField()
                                        : const SizedBox(),
                                    ...forcastDate(),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    interredNoFireProject()
                                  ],
                                )),
                          ]),
                        ],
                      ))),
            ));
      }),
    );
  }
}
