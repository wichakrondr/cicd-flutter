import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';

class SelectPlantProvider with ChangeNotifier {
  int selectedPlant = 0;
  bool isDownBtn = false;
  bool isReadAll = false;
  int ricePlantingType = 0;
  int _fieldType = 0;
  int _interedType = 0;
  String iconPath = "";
  TextEditingController contentCtrl = TextEditingController();
  initialData({required ScrollController scrollController}) async {
    contentCtrl = TextEditingController(
        text:
            """Her hand was balled into a fist with her keys protruding out from between her fingers. This was the weapon her father had shown her how to make when she walked alone to her car after work. She wished that she had something a little more potent than keys between her fingers. It would have been nice to have some mace or pepper spray. He had been meaning to buy some but had never gotten around to it. As the mother bear took another step forward with her cubs in tow, she knew her fist with keys wasn't going to be an adequate defense for this situation.
Twenty-five hours had passed since the incident. It seemed to be a lot longer than that. That twenty-five hours seemed more like a week in her mind. The fact that she still was having trouble comprehending exactly what took place wasn't helping the matter. She thought if she could just get a little rest the entire incident might make a little more sense.
The blinking light caught her attention. She thought about it a bit and couldn't remember ever noticing it before. That was strange since it was obvious the flashing light had been there for years. Now she wondered how she missed it for that amount of time and what other things in her small town she had failed to notice.
Sometimes it just doesn't make sense. The man walking down the street in a banana suit. The llama standing in the middle of the road. The fairies dancing in front of the car window. The fact that all of this was actually happening and wasn't a dream.
The computer wouldn't start. She banged on the side and tried again. Nothing. She lifted it up and dropped it to the table. Still nothing. She banged her closed fist against the top. It was at this moment she saw the irony of trying to fix the machine with violence.
Finding the truth wouldn't be easy, that's for sure. Then there was the question of whether or not Jane really wanted to know the truth. That's the thing that bothered her most. It wasn't the difficulty of actually finding out what happened that was the obstacle, but having to live with that information once it was found.
One dollar and eighty-seven cents. That was all. And sixty cents of it was in pennies. Pennies saved one and two at a time by bulldozing the grocer and the vegetable man and the butcher until one’s cheeks burned with the silent imputation of parsimony that such close dealing implied. One dollar and eighty-seven cents. And the next day would be Christmas...
The bridge spanning a 100-foot gully stood in front of him as the last obstacle blocking him from reaching his destination. While people may have called it a "bridge", the reality was it was nothing more than splintered wooden planks held together by rotting ropes. It was questionable whether it would hold the weight of a child, let alone the weight of a grown man. The problem was there was no other way across the gully, and this played into his calculations of whether or not it was worth the risk of trying to cross it.
There weren't supposed to be dragons flying in the sky. First and foremost, dragons didn't exist. They were mythical creatures from fantasy books like unicorns. This was something that Pete knew in his heart to be true so he was having a difficult time acknowledging that there were actually fire-breathing dragons flying in the sky above him.
Nobody really understood Kevin. It wasn't that he was super strange or difficult. It was more that there wasn't enough there that anyone wanted to take the time to understand him. This was a shame as Kevin had many of the answers to the important questions most people who knew him had. It was even more of a shame that they'd refuse to listen even if Kevin offered to give them the answers. So, Kevin remained silent, misunderstood, and kept those important answers to life to himself.
The box sat on the desk next to the computer. It had arrived earlier in the day and business had interrupted her opening it earlier. She didn't who had sent it and briefly wondered who it might have been. As she began to unwrap it, she had no idea that opening it would completely change her life.""");
    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0) {
        isReadAll = true;
        notifyListeners();
      }
    });
    setupScrollListener(scrollController: scrollController);
  }

  setIconPath(String path) {
    iconPath = path;
    notifyListeners();
  }

  setRicePlantingType(int index) {
    ricePlantingType = index;
    notifyListeners();
  }

  fetchProvince(BuildContext context) {
    ProfileViewModel profileViewModel = context.watch<ProfileViewModel>();
  }

  setupScrollListener({required ScrollController scrollController}) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        // Reach the top of the list
        if (scrollController.position.pixels == 0) {
          isDownBtn = false;
          notifyListeners();
        }

        // Reach the bottom of the list
        else {
          isDownBtn = true;
          isReadAll = true;
          notifyListeners();
        }
      }
    });
  }

  onButtonDownPressed({required ScrollController scrollController}) {
    isDownBtn = !isDownBtn;
    isDownBtn
        ? scrollController.animateTo(scrollController.position.maxScrollExtent,
            curve: Curves.easeOut, duration: const Duration(milliseconds: 300))
        : scrollController.animateTo(0,
            curve: Curves.easeOut, duration: const Duration(milliseconds: 300));
    notifyListeners();
  }
}

// class selectPlantM {
//   final 
//   selectPlantM({required this.status, required this.message});
// }
