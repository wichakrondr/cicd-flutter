import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/views/market/select_farm/select_farm_page_provider.dart';
import 'package:varun_kanna_app/widget/common_button.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

import '../../../widget/app_bar_text.dart';
import '../landing/landing_page.dart';

class SelectFarmPage extends StatelessWidget {
  SelectFarmPage(
      {super.key,
      required this.projectId,
      required this.projectName,
      required this.headerImage});
  String projectId;
  String projectName;
  String headerImage;
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  late Timer _timer;
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return ChangeNotifierProvider(
      create: (context) =>
          SelectFarmPageProvider()..initialData(projectId: projectId),
      child: Consumer<SelectFarmPageProvider>(builder: (context, provider, _) {
        void _onLoading() async {
          await provider.getFarmList(
            projectId: projectId,
            limit: "10",
            offset: provider.farmList.length.toString(),
          );
          _refreshController.loadComplete();
        }

        return Scaffold(
          body: Stack(
            children: [
              Center(
                child: Column(
                  children: [
                    AppBarText(
                      textHeader: "เข้าร่วมโครงการ",
                      isback: false,
                      action: IconButton(
                        padding: const EdgeInsets.only(right: 30, top: 8),
                        onPressed: () async {
                          final SharedPreferences prefs = await _prefs;
                          prefs.setString("projectName", projectName);
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LandingPage(
                                      projectId: projectId,
                                      projectName: projectName,
                                      headerImage: headerImage)));
                        },
                        icon: const Icon(
                          Icons.close,
                          size: 40,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Transform.scale(
                                scale: 1.5,
                                child: Checkbox(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  value: provider.isSelectAll,
                                  onChanged: (v) =>
                                      provider.onSelectAll(v ?? false),
                                  activeColor: Colors.black,
                                ),
                              ),
                              const Text("เลือกทั้งหมด"),
                            ],
                          ),
                          Text("เลือก ${provider.count} แปลง")
                        ],
                      ),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.8,
                      child: SmartRefresher(
                        controller: _refreshController,
                        onLoading: _onLoading,
                        footer: CustomFooter(
                          builder: (context, mode) {
                            Widget body;
                            if (mode == LoadStatus.idle) {
                              body = const Text("Pull up to load more. . .");
                            } else if (mode == LoadStatus.loading) {
                              body = const CustomLoadingWidget();
                            } else if (mode == LoadStatus.failed) {
                              body = const Text("Load Failed! Click retry!");
                            } else if (mode == LoadStatus.canLoading) {
                              body = const Text("release to load more");
                            } else {
                              body = const Text("No more Data");
                            }
                            return Container(
                              height: 55.0,
                              child: Center(child: body),
                            );
                          },
                        ),
                        enablePullUp: true,
                        enablePullDown: false,
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              ListView.builder(
                                padding: EdgeInsets.zero,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: provider.farmList.length,
                                shrinkWrap: true,
                                itemBuilder: (context, index) {
                                  return InkWell(
                                    onTap: () {
                                      provider.onSelect(index);
                                    },
                                    child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.9,
                                      height: 180,
                                      margin: const EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 20),
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(12.5),
                                        color: Colors.white,
                                        boxShadow: [
                                          BoxShadow(
                                            color:
                                                Colors.black.withOpacity(0.15),
                                            blurRadius: 10,
                                          ),
                                        ],
                                      ),
                                      child: Container(
                                        child: Row(
                                          children: [
                                            width > 800
                                                ? const Expanded(
                                                    child: SizedBox())
                                                : const SizedBox(),
                                            Expanded(
                                              flex: width > 800 ? 2 : 1,
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                      height:
                                                          width > 800 ? 20 : 0),
                                                  Row(
                                                    children: [
                                                      Checkbox(
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(5),
                                                        ),
                                                        activeColor:
                                                            Colors.black,
                                                        value: provider
                                                            .farmList[index]
                                                            .isSelected,
                                                        onChanged: (v) {
                                                          provider
                                                              .onSelect(index);
                                                        },
                                                      ),
                                                      const Text(
                                                          "เลือกแปลงนี้"),
                                                    ],
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 15.0),
                                                    child: Text(
                                                      provider.farmList[index]
                                                              .farmName ??
                                                          "",
                                                      style: const TextStyle(
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                  const SizedBox(height: 20),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            left: 15.0),
                                                    child: Text(
                                                      "พื้นที่ ${provider.farmList[index].displayArea}",
                                                      style: const TextStyle(
                                                        fontSize: 18,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Container(
                                              margin: const EdgeInsets.fromLTRB(
                                                  20, 20, 25, 20),
                                              child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(18),
                                                  child: Image.network(provider
                                                          .farmList[index]
                                                          .farmImg ??
                                                      "")),
                                            ),
                                            width > 800
                                                ? const Expanded(
                                                    child: SizedBox())
                                                : const SizedBox(),
                                          ],
                                        ),
                                      ),
                                    ),
                                  );
                                },
                              ),
                              SizedBox(
                                  height:
                                      MediaQuery.of(context).size.height * 0.1)
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(20),
                  child: CommonButton(
                    title: 'เข้าร่วมโครงการ',
                    borderRadius: 16,
                    onPressed: provider.isLoading
                        ? null
                        : () async {
                            final result =
                                await provider.onSubmit(projectId: projectId);
                            if (result) {
                              await ViewDialogs.commonDialogWithIcon(
                                      autoDismiss: () {
                                        _timer = Timer(
                                            const Duration(seconds: 2), () {
                                          Navigator.pop(context);
                                        });
                                      },
                                      isAutoDismiss: true,
                                      context: context,
                                      text: "เลือกแปลงเรียบร้อยแล้ว")
                                  .then((value) {
                                if (_timer.isActive) {
                                  Navigator.pushReplacement(
                                    context,
                                    MaterialPageRoute(
                                      builder: (c) => LandingPage(
                                          projectId: projectId,
                                          projectName: projectName,
                                          headerImage: headerImage),
                                    ),
                                  );
                                  _timer.cancel();
                                }
                              });
                              Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LandingPage(
                                      projectId: projectId,
                                      projectName: projectName,
                                      headerImage: headerImage),
                                ),
                              );
                            } else {
                              await ViewDialogs.commonDialogWithIcon(
                                context: context,
                                text: "เลือกแปลงไม่สำเร็จ",
                                autoDismiss: () {
                                  _timer =
                                      Timer(const Duration(seconds: 2), () {
                                    Navigator.of(context).pop();
                                  });
                                },
                                isAutoDismiss: true,
                                icon: const Icon(
                                  Icons.close_outlined,
                                  color: Colors.red,
                                ),
                              ).then((value) {
                                if (_timer.isActive) {
                                  _timer.cancel();
                                }
                              });
                            }
                          },
                  ),
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: const Offset(0, 3),
                      ),
                    ],
                    color: Colors.white,
                    borderRadius: const BorderRadius.vertical(
                      top: Radius.circular(32.5),
                    ),
                  ),
                ),
              ),
              provider.isLoading
                  ? const Center(child: CustomLoadingWidget())
                  : Container()
            ],
          ),
        );
      }),
    );
  }
}
