import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/services/join_project_service.dart';

import '../../../model/market_page_model/farm_list_model.dart';

class SelectFarmPageProvider with ChangeNotifier {
  bool isLoading = false;
  bool isSelectAll = false;
  int count = 0;
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<JoinProjectFarmListModel> farmList = [];

  initialData({required String projectId}) {
    getFarmList(projectId: projectId);
  }

  onSelectAll(bool value) {
    isSelectAll = value;
    if (value) {
      for (var i = 0; i < farmList.length; i++) {
        farmList[i].isSelected = true;
      }
      count = farmList.length;
      notifyListeners();
    } else {
      for (var i = 0; i < farmList.length; i++) {
        farmList[i].isSelected = false;
      }
      count = 0;
      notifyListeners();
    }

    notifyListeners();
  }

  onSelect(int index) {
    farmList[index].isSelected = !farmList[index].isSelected;

    if (farmList[index].isSelected) {
      count++;
      notifyListeners();
    } else {
      count--;
      notifyListeners();
    }
    if (count == farmList.length) {
      isSelectAll = true;
      notifyListeners();
    }

    if (count != farmList.length) {
      isSelectAll = false;
      notifyListeners();
    }
    notifyListeners();
  }

  Future getFarmList(
      {required String projectId,
      String limit = "10",
      String offset = "0"}) async {
    final SharedPreferences prefs = await _prefs;
    final response = await JoinProjectService().getFarmList(
        projectId: projectId,
        userId: prefs.getString('userId').toString(),
        limit: limit,
        offset: offset);
    response.when(
      success: (data) {
        if (farmList.isEmpty) {
          farmList = data.data!;
        } else {
          if (data.data != null) {
            farmList.addAll(data.data ?? []);
          }
        }

        notifyListeners();
      },
      failure: (error) {
        print(error);
      },
    );
  }

  Future<bool> onSubmit({required String projectId}) async {
    List<JoinProjectFarmListModel> tempList = [];
    tempList = farmList.where((element) => element.isSelected == true).toList();
    List farmListId = [];
    tempList.forEach((e) {
      farmListId.add({"farm_id": e.id});
    });
    isLoading = true;
    notifyListeners();
    final SharedPreferences prefs = await _prefs;
    final response = await JoinProjectService().postCreateJoinProject(
      projectId: projectId,
      userId: prefs.getString('userId').toString(),
      farmList: farmListId,
    );
    final status = response.when(
      success: (data) {
        if (data.status) {
          isLoading = false;
          notifyListeners();
          return true;
        }
      },
      failure: (error) {
        isLoading = false;
        notifyListeners();
        return false;
      },
    );
    if (status != null) {
      return status;
    } else {
      return false;
    }
  }
}
