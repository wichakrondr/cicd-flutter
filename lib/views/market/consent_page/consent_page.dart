import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/views/market/consent_page/consent_page_provider.dart';
import 'package:varun_kanna_app/views/market/select_farm/select_fam_page.dart';
import 'package:varun_kanna_app/widget/common_button.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';
import 'dart:math' as math;
import '../../../widget/app_bar_text.dart';

class ConsentPage extends StatelessWidget {
  ConsentPage({
    super.key,
    required this.content,
    required this.consentId,
    required this.projectId,
    required this.projectName,
    required this.headerImage
  });
  String content;
  String projectId;
  String projectName;
  String consentId;
  String headerImage;
  ScrollController scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ConsentPageProvider()
        ..initialData(scrollController: scrollController, content: content),
      child: Consumer<ConsentPageProvider>(builder: (context, provider, _) {
        return Scaffold(
          body: Stack(
            children: [
              Column(
                children: [
                  AppBarText(
                    textHeader: "ข้อกำหนดและนโยบาย",
                    isback: false,
                    action: IconButton(
                      padding: const EdgeInsets.only(right: 30, top: 8),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Icon(
                        Icons.close,
                        size: 40,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 30, right: 30, top: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        SizedBox(
                          height: MediaQuery.of(context).size.height * 0.67,
                          width: double.infinity,
                          child: SingleChildScrollView(
                            controller: scrollController,
                            physics: const BouncingScrollPhysics(),
                            child: HtmlWidget(
                              // the first parameter (`html`) is required
                              provider.contentCtrl.text,

                              onErrorBuilder: (context, element, error) =>
                                  Text('$element error: $error'),
                              onLoadingBuilder:
                                  (context, element, loadingProgress) =>
                                      const CustomLoadingWidget(),

                              textStyle: const TextStyle(fontSize: 14),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.all(20),
                  child: CommonButton(
                    title: 'ยอมรับ',
                    borderRadius: 16,
                    onPressed: !provider.isReadAll
                        ? null
                        : () async {
                            await provider.signConsent(consentId);
                            if (provider.isSigned) {
                              await Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SelectFarmPage(
                                      projectId: projectId,
                                      projectName: projectName,
                                      headerImage: headerImage),
                                ),
                              );
                            }
                          },
                  ),
                  // height: MediaQuery.of(context).size.height * 0.15,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: const Offset(0, 3),
                      ),
                    ],
                    color: Colors.white,
                    borderRadius: const BorderRadius.vertical(
                      top: Radius.circular(32.5),
                    ),
                  ),
                ),
              ),
              provider.isLoading
                  ? const Center(child: CustomLoadingWidget())
                  : Container(),
            ],
          ),
          floatingActionButton: Padding(
            padding: const EdgeInsets.only(bottom: 100.0),
            child: navigateBtn(
              isDownBtn: provider.isDownBtn,
              onPressed: () => provider.onButtonDownPressed(
                  scrollController: scrollController),
            ),
          ),
        );
      }),
    );
  }

  Widget navigateBtn({required bool isDownBtn, required Function onPressed}) {
    return FloatingActionButton.small(
      child: Transform.rotate(
          angle: isDownBtn ? (90 * math.pi / 180) : -90 * math.pi / 180,
          child: const Icon(
            size: 40,
            color: Color.fromARGB(255, 241, 241, 241),
            Icons.navigate_before,
          )),
      backgroundColor: const Color.fromARGB(255, 0, 0, 0),
      onPressed: () => onPressed(),
    );
  }
}
