import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/view_models/term_and_condition_view_model.dart';

import '../../../model/term_and_condition_response.dart';
import '../../../services/join_project_service.dart';

class ConsentPageProvider with ChangeNotifier {
  bool isDownBtn = false;
  bool isReadAll = false;
  bool isSigned = false;
  bool isLoading = false;
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  TextEditingController contentCtrl = TextEditingController();
  initialData(
      {required ScrollController scrollController,
      required String content}) async {
    contentCtrl = TextEditingController(text: (content));

    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0) {
        isReadAll = true;
        notifyListeners();
      }
    });
    setupScrollListener(scrollController: scrollController);
  }

  setupScrollListener({required ScrollController scrollController}) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        // Reach the top of the list
        if (scrollController.position.pixels == 0) {
          isDownBtn = false;
          notifyListeners();
        }

        // Reach the bottom of the list
        else {
          isDownBtn = true;
          isReadAll = true;
          notifyListeners();
        }
      }
    });
  }

  setLoading(bool isLoading) {
    this.isLoading = isLoading;
    notifyListeners();
  }

  Future signConsent(String consentId) async {
    final SharedPreferences prefs = await _prefs;
    setLoading(true);
    final response = await JoinProjectService().postSignConsent(
        userId: prefs.getString('userId').toString(), consentId: consentId);
    response.when(
      success: (data) {
        isSigned = true;
        notifyListeners();
        setLoading(false);
      },
      failure: (error) {
        setLoading(false);
        print(error);
      },
    );
  }

  onButtonDownPressed({required ScrollController scrollController}) {
    isDownBtn = !isDownBtn;
    isDownBtn
        ? scrollController.animateTo(scrollController.position.maxScrollExtent,
            curve: Curves.easeOut, duration: const Duration(milliseconds: 300))
        : scrollController.animateTo(0,
            curve: Curves.easeOut, duration: const Duration(milliseconds: 300));
    notifyListeners();
  }
}
