import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/market_page_model/document_model.dart';
import 'package:varun_kanna_app/model/market_page_model/form_model.dart';
import 'package:varun_kanna_app/model/market_page_model/join_project_list_model.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/model/market_page_model/verify_page_model/verify_response.dart';
import 'package:varun_kanna_app/services/image_service.dart';
import 'package:varun_kanna_app/services/join_project_service.dart';
import 'package:varun_kanna_app/services/verify_services.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/casting.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/view_models/land_recomendation_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/views/market/activity_selection/activity_selection_page.dart';
import 'package:varun_kanna_app/views/market/verify_main_page.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

class VerifyMainPageProvider with ChangeNotifier {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  ScrollController scrollController = ScrollController();

  int _processPage = 0;
  bool _loding = false;
  bool _isEnableNextBtn = false;
  bool isEnableBackBtn = false;
  bool isEnableSubmitBtn = false;
  double _scrollOffset = 0.0;

  List<String> idCardImgPath = [];
  List<String> idCardSignUrlImg = [];
  List<String> copyHouseRegistrationImgPath = [];
  List<String> copyHouseRegistrationSignUrlImg = [];

  List<String> certificateLandUtilizationImgPath = [];
  List<String> certificateLandUtilizationSignUrlImg = [];

  List<String> titleDeedImgPath = [];
  List<String> titleDeedSignUrlImg = [];

  List<String> farmBookImgPath = [];
  List<String> farmBookSignUrlImg = [];

  List<String> memberBookImgPath = [];
  List<String> memberBookSignUrlImg = [];

  List<String> landBookImgPath = [];
  List<String> landBookSignUrlImg = [];

  List<String> letterConsentParicipateProjectImgPath = [];
  List<String> letterConsentParicipateProjectSignUrlImg = [];

  List<String> deletePath = [];

  List<VerifyResponseModel>? verifyResponse = [];

  bool disableDistrict = true;

  bool disableSubDistrict = true;

  int get getProcessPage => _processPage;

  String personal_id = "";
  String land_id = "";
  String cultivation_id = "";

  String remarkText = "";
  setProcessPage(int process) {
    _processPage = process;
    notifyListeners();
  }

  bool isEditData = false;
  bool isSaveData = false;
  bool isReadonly = false;
  bool _isEnableFields = true;

  void setEnableFields(bool isEnableFields) {
    _isEnableFields = isEnableFields;
    notifyListeners();
  }

  bool get getEnableFields => _isEnableFields;

  String _projectName = "";
  String get getProjectName => _projectName;
// - รอบันทึกข้อมูล/รอบันทึกกิจกรรม = กรอกข้อมูลได้
// - รอตรวจสอบ = ดูและแก้ไม่ได้
// - รอแก้ไขข้อมูล = แก้ได้ปกติ
// - สำเร็จ/ไม่สำเร็จ = ดูอย่างเดียว
  initialStatusField(ActivityStatusTypes statusTypes) {
    // statusTypes = ActivityStatusTypes.waitingForEditIdentity;
    // isEditData = (statusTypes == ActivityStatusTypes.waitingForInformation);
    isEditData = (statusTypes == ActivityStatusTypes.waitingForInformation ||
        statusTypes == ActivityStatusTypes.waitingForEditIdentity);

    isSaveData = statusTypes == ActivityStatusTypes.waitingForInformation;

    isReadonly = !isEditData;
    if (isEditData) {
      setEnableFields(true);
      if (statusTypes == ActivityStatusTypes.waitingForEditIdentity) {
        _isEnableNextBtnPage0 = true;
        _isEnableNextBtnPage1 = true;
        _isEnableNextBtnPage2 = true;
      }
    } else if (isReadonly) {
      _isEnableNextBtnPage0 = true;
      _isEnableNextBtnPage1 = true;
      setEnableFields(false);
    }
  }

  FormModel? form;

  initialData(BuildContext context, ActivityStatusTypes statusTypes,
      String projectId, String reqJoinProjectId) async {
    scrollController = ScrollController(initialScrollOffset: _scrollOffset);

    await fetchVerifyForm(projectId, reqJoinProjectId);
    initialStatusField(statusTypes);
    final SharedPreferences prefs = await _prefs;
    ProfileViewModel profileViewModel =
        Provider.of<ProfileViewModel>(context, listen: false);
    await profileViewModel.getProvince();
    provinceList = Cast.objectToJson(profileViewModel.getProvinceList);

    LandRecomnViewModel landRecomnViewModel =
        Provider.of<LandRecomnViewModel>(context, listen: false);

    _projectName = prefs.getString("projectName")!;
    await landRecomnViewModel.fetchPlantList(_projectName);
    notifyListeners();
  }

  void clearImageList() {
    idCardImgPath = [];
    idCardSignUrlImg = [];
    copyHouseRegistrationImgPath = [];
    copyHouseRegistrationSignUrlImg = [];

    certificateLandUtilizationImgPath = [];
    certificateLandUtilizationSignUrlImg = [];

    titleDeedImgPath = [];
    titleDeedSignUrlImg = [];

    farmBookImgPath = [];
    farmBookSignUrlImg = [];

    memberBookImgPath = [];
    memberBookSignUrlImg = [];

    landBookImgPath = [];
    landBookSignUrlImg = [];

    letterConsentParicipateProjectImgPath = [];
    letterConsentParicipateProjectSignUrlImg = [];

    deletePath = [];
    notifyListeners();
  }

  Future<void> fetchVerifyForm(
      String projectId, String reqJoinProjectId) async {
    setLoading(true);
    clearImageList();
    ApiResult<VerifyResponse> response =
        await VerifyService().getVerifyIdentity(projectId, reqJoinProjectId);
    response.when(
      success: (data) {
        verifyResponse = data.data ?? [];
        personal_id = verifyResponse![0].personalId;
        land_id = verifyResponse![0].landId!;
        cultivation_id = verifyResponse![0].cultivationId!;
// start text section
        remarkText = verifyResponse![0].remark ?? '';
        firstNameCtrl.text = verifyResponse![0].firstName!;
        lastNameCtrl.text = verifyResponse![0].lastName!;
        typeDocumentCtrl.text = verifyResponse![0].typeOfCertificateOwnership!;
        documentNoCtrl.text = verifyResponse![0].titleDeedNumber!;
        provinceCtrl.text = verifyResponse![0].province!;
        districtCtrl.text = verifyResponse![0].district!;
        subDistrictCtrl.text = verifyResponse![0].subDistrict!;
        raiCtrl.text = verifyResponse![0].rai.toString();
        nganCtrl.text = verifyResponse![0].ngan.toString();
        waCtrl.text = verifyResponse![0].squareWah == "0.000000"
            ? "0.00"
            : verifyResponse![0].squareWah!.split('.')[0];
        plantListCtrl.text = verifyResponse![0].plant!;
        _selectedPlantName = verifyResponse![0].plant!;
        //date
        forcastPlantingDateCtrl.text = Cast.convertLacaleDate(
            verifyResponse![0].expectedDatePlantingV ?? "");
        forcastHarvestDateCtrl.text = Cast.convertLacaleDate(
            verifyResponse![0].expectedDateHarvestV ?? "");
        awdDayCtrl.text =
            Cast.convertLacaleDate(verifyResponse![0].startAtRiceAwd ?? "");
        // awdDayCtrl
//end text section

//start radio btn section
        fieldTypeCtrl = type_of_rice_cultivation.indexWhere((element) =>
                element == verifyResponse![0].typeOfRiceCultivation) +
            1;

        ricePlantingTypeCtrl = type_of_farming.indexWhere(
                (element) => element == verifyResponse![0].typeOfFarming) +
            1;

        isAuthority =
            verifyResponse![0].landRights == "เป็นเจ้าของที่ดิน" ? 1 : 2;
        typeIrrigationCtrl = verifyResponse![0].irrigationArea == "Y" ? 1 : 2;
        interredTypeCtrl = verifyResponse![0].join == "Y" ? 1 : 2;

        isFireCtrl = verifyResponse![0].burnCultivationAreaV == "Y" ? 1 : 2;
// end radio btn section
        idCardSignUrlImg = verifyResponse![0].idCardImgs!;
        idCardImgPath = verifyResponse![0].idCardImgs_non_sign!;
        letterConsentParicipateProjectSignUrlImg =
            verifyResponse![0].letterConsentParticipateProjectImgs!;
        letterConsentParicipateProjectImgPath =
            verifyResponse![0].letterConsentParticipateProjectImgs_non_sign!;
        copyHouseRegistrationSignUrlImg =
            verifyResponse![0].copyHouseRegistrationImgs!;
        copyHouseRegistrationImgPath =
            verifyResponse![0].copyHouseRegistrationImgs_non_sign!;
        certificateLandUtilizationSignUrlImg =
            verifyResponse![0].certificateLandUtilizationImgs!;
        certificateLandUtilizationImgPath =
            verifyResponse![0].certificateLandUtilizationImgs_non_sign!;
        titleDeedSignUrlImg = verifyResponse![0].titleDeedImgs!;
        titleDeedImgPath = verifyResponse![0].titleDeedImgs_non_sign!;
        farmBookSignUrlImg = verifyResponse![0].farmBookImgs!;
        farmBookImgPath = verifyResponse![0].farmBookImgs_non_sign!;
        memberBookSignUrlImg = verifyResponse![0].memberBookImgs!;
        memberBookImgPath = verifyResponse![0].memberBookImgs_non_sign!;
        landBookSignUrlImg = verifyResponse![0].landBookImgs!;
        landBookImgPath = verifyResponse![0].landBookImgs_non_sign!;

        setLoading(false);
        notifyListeners();
      },
      failure: ((error) {
        setLoading(false);
        print(error);
      }),
    );
  }

  Uint8List imagePath = Uint8List(0);
  // setImage(Uint8List img) async {
  //   // idCardImage = File.fromRawPath(img);
  //   //imagePath = img;
  //   // if (img.isNotEmpty) {
  //   //   idCardImg.add(img);
  //   // }
  //   notifyListeners();
  // }

  void clearIdCardImg(int index) {
    deletePath.add(idCardImgPath[index]);
    idCardSignUrlImg.removeAt(index);
    idCardImgPath.removeAt(index);
    notifyListeners();
  }

  void clearHouseRegImage(int index) {
    deletePath.add(copyHouseRegistrationImgPath[index]);
    copyHouseRegistrationSignUrlImg.removeAt(index);
    copyHouseRegistrationImgPath.removeAt(index);
    notifyListeners();
  }

  TextEditingController firstNameCtrl = TextEditingController();
  TextEditingController lastNameCtrl = TextEditingController();
  bool _isEnableNextBtnPage0 = false;
  bool get getIsEnableNextBtnPage0 => _isEnableNextBtnPage0;
  setIsEnableNextBtnPage0(bool enable) {
    _isEnableNextBtnPage0 = enable;
    notifyListeners();
  }
//page 0

//page1
// POWER OF ATTORNEY
  int isAuthority = 0;
  int get getIsAuthority => isAuthority;

  setIsAuthority(int index) {
    isAuthority = index;
    notifyListeners();
  }

  void clearLetterConsentParicipateProjectImg(int index) {
    deletePath.add(letterConsentParicipateProjectSignUrlImg[index]);
    letterConsentParicipateProjectImgPath.removeAt(index);
    letterConsentParicipateProjectSignUrlImg.removeAt(index);
    notifyListeners();
  }

  void clearCertificateLandUtilizationImg(int index) {
    deletePath.add(certificateLandUtilizationSignUrlImg[index]);
    certificateLandUtilizationImgPath.removeAt(index);
    certificateLandUtilizationSignUrlImg.removeAt(index);
    notifyListeners();
  }

  void clearFarmerBookImage(int index) {
    deletePath.add(farmBookSignUrlImg[index]);
    farmBookImgPath.removeAt(index);
    farmBookSignUrlImg.removeAt(index);
    notifyListeners();
  }

  void clearFarmerFamImage(int index) {
    deletePath.add(memberBookSignUrlImg[index]);
    memberBookImgPath.removeAt(index);
    memberBookSignUrlImg.removeAt(index);
    notifyListeners();
  }

  void clearFarmerBookLandImage(int index) {
    deletePath.add(landBookSignUrlImg[index]);
    landBookImgPath.removeAt(index);
    landBookSignUrlImg.removeAt(index);
    notifyListeners();
  }

  void clearTitleDeedImage(int index) {
    deletePath.add(titleDeedSignUrlImg[index]);
    titleDeedImgPath.removeAt(index);
    titleDeedSignUrlImg.removeAt(index);
    notifyListeners();
  }

  Object? _typeDocumentValue = -1;
  final List<DocumentModel>? _typeDocumentList = [
    DocumentModel(
      id: "b5c0b412-3c5d-471f-b2d1-795f242cd8a3",
      short_name: "น.ส 2",
    ),
    DocumentModel(
      id: "1f04eee4-d43f-48bb-8aa4-561dfeabb727",
      short_name: "น.ส 3",
    ),
    DocumentModel(
      id: "30c7823d-c824-4969-81af-47fac378e925",
      short_name: "น.ส. 3 ก",
    ),
    DocumentModel(
      id: "15667047-9620-4286-ae25-da1ddcee381e",
      short_name: "น.ส. 3 ข",
    ),
    DocumentModel(
      id: "94ab50c3-cf5d-44b0-abf9-97ef7ded2357",
      short_name: "น.ส. 3 ค",
    ),
    DocumentModel(
      id: "55315f33-1515-4e34-b4b9-c1869bfafa66",
      short_name: "น.ส. 4",
    ),
    DocumentModel(
      id: "1819654b-049c-4d57-8645-e0475f1fbefd",
      short_name: "น.ส. 4 ก",
    ),
    DocumentModel(
      id: "2b7dea4d-2d31-4076-9a73-317aa93be9b6",
      short_name: "น.ส. 4 ข",
    ),
    DocumentModel(
      id: "03650b16-352b-4e9b-b31f-7627ad994366",
      short_name: "น.ส. 4 ค",
    ),
    DocumentModel(
      id: "cd708a5b-3b46-483e-b2a6-601cf94f5b4f",
      short_name: "น.ส. 4 ง",
    ),
    DocumentModel(
      id: "244abd85-f0c1-4c80-a7a0-b911abb7c3b9",
      short_name: "น.ส. 4 จ",
    ),
    DocumentModel(
      id: "5714195b-b6f4-4f25-a0f6-87869e4d1de1",
      short_name: "ภ.ท.บ. 5",
    ),
    DocumentModel(
      id: "36f0cbd6-62c7-476f-98c9-78d9c34483b7",
      short_name: "ก.ส.น",
    ),
    DocumentModel(
      id: "60904f04-efbf-4624-a47d-a10397d71c42",
      short_name: "น.ค.",
    ),
    DocumentModel(
      id: "c856827b-abb6-4282-9305-b3b1f59482fb",
      short_name: "ส.ท.ก",
    ),
    DocumentModel(
      id: "fa9ab5dc-2704-4804-9f2d-8e178bf60da4",
      short_name: "ประเภท 12",
    ),
    DocumentModel(
      id: "8247be6f-bb22-4b60-89f6-f1b24eb301dc",
      short_name: "ส.ค. 1",
    ),
    DocumentModel(
      id: "a55c82e3-125b-4a44-9829-04eece472cd6",
      short_name: "ส.ป.ก 4-01",
    )
  ];

  Object? get getTypeDocumentValue => _typeDocumentValue;

  void setTypeDocumentValue(Object data) {
    _typeDocumentValue = data;
    notifyListeners();
  }

  getDocumentList() {
    List items = Cast.objectToJson(_typeDocumentList);
    int fieldId = 0;
    var plantList = [];

    items.forEach(
      (element) {
        element["uuid"] = element["id"];
        element["name_th"] = element["short_name"];
        fieldId = fieldId + 1;
        element["id"] = fieldId;
        plantList.add(element);
      },
    );
    return Cast.objectToJson(plantList);
  }

  void validField(int pageState) {
    bool isWDProject = _projectName == "โครงการปลูกข้าวเปียกสลับแห้ง";

    switch (pageState) {
      case 0:
        if (firstNameCtrl.text.isEmpty ||
            lastNameCtrl.text.isEmpty ||
            (idCardSignUrlImg.isEmpty && idCardImgPath.isEmpty) ||
            (copyHouseRegistrationSignUrlImg.isEmpty &&
                copyHouseRegistrationImgPath.isEmpty)) {
          setIsEnableNextBtnPage0(false);
        } else {
          setIsEnableNextBtnPage0(true);
        }
        break;
      case 1:
        bool validLand = (raiCtrl.text.isNotEmpty ||
                nganCtrl.text.isNotEmpty ||
                waCtrl.text.isNotEmpty) &&
            (validNgan && validWa);
        if (!validLand ||
            typeDocumentCtrl.text.isEmpty ||
            documentNoCtrl.text.isEmpty ||
            provinceCtrl.text.isEmpty ||
            districtCtrl.text.isEmpty ||
            subDistrictCtrl.text.isEmpty ||
            (letterConsentParicipateProjectImgPath.isEmpty &&
                letterConsentParicipateProjectSignUrlImg.isEmpty) ||
            (titleDeedSignUrlImg.isEmpty && titleDeedImgPath.isEmpty)) {
          setIsEnableNextBtnPage1(false);
        } else {
          setIsEnableNextBtnPage1(true);
        }
        break;
      case 2:
        // WD project do this one
        if (isWDProject) {
          if (getTypeIrrigation > 0) {
            // selected rice
            if (getSelectedPlantName == "ข้าว") {
              if (fieldTypeCtrl <= 0 ||
                  ricePlantingTypeCtrl <= 0 ||
                  awdDayCtrl.text.isEmpty ||
                  interredTypeCtrl <= 0) {
                setIsEnableNextBtnPage2(false);
              } else {
                setIsEnableNextBtnPage2(true);
              }
            } else if (getSelectedPlantName.isNotEmpty &&
                getSelectedPlantName != "ข้าว") {
              setIsEnableNextBtnPage2(true);
            }
          } else {
            setIsEnableNextBtnPage2(false);
          }
        } else {
          if (isFireCtrl <= 0 ||
              selectedPlant <= 0 ||
              forcastPlantingDateCtrl.text.isEmpty ||
              forcastHarvestDateCtrl.text.isEmpty ||
              interredTypeCtrl <= 0) {
            setIsEnableNextBtnPage2(false);
          } else {
            setIsEnableNextBtnPage2(true);
          }
        }
        break;
      default:
    }
  }

  Object? _provinceValue = -1;
  Object? get getProvinceValue => _provinceValue;
  List provinceList = [];
  List get getProvinceList => provinceList;
  void setProvinceValue(Object data) {
    _provinceValue = data;
    notifyListeners();
  }

  Object? _districtValue = -1;
  Object? get getDistrictValue => _districtValue;
  dynamic districtList = [];
  List get getDistrictList => districtList;
  Future getDistrictListById(BuildContext context, int id) async {
    ProfileViewModel profileViewModel =
        Provider.of<ProfileViewModel>(context, listen: false);
    await profileViewModel.getDistrict(id);
    districtList = Cast.objectToJson(profileViewModel.getDistrictList);
    notifyListeners();
  }

  void setDistrictValue(Object data) {
    _districtValue = data;
    notifyListeners();
  }

  Object? _subDistrictValue = -1;
  Object? get getSubDistrictValue => _subDistrictValue;
  dynamic subDistrictList = [];
  List get getSubDistrictList => subDistrictList;
  Future getSubDistrictListById(BuildContext context, int id) async {
    ProfileViewModel profileViewModel =
        Provider.of<ProfileViewModel>(context, listen: false);
    await profileViewModel.getSubDistrict(id);
    subDistrictList = Cast.objectToJson(profileViewModel.getSubDistrictList);
    notifyListeners();
  }

  void setSubDistrictValue(Object data) {
    _subDistrictValue = data;
    notifyListeners();
  }

  TextEditingController typeDocumentCtrl = TextEditingController();
  TextEditingController documentNoCtrl = TextEditingController();
  TextEditingController provinceCtrl = TextEditingController();
  TextEditingController districtCtrl = TextEditingController();
  TextEditingController subDistrictCtrl = TextEditingController();
  TextEditingController raiCtrl = TextEditingController();
  TextEditingController nganCtrl = TextEditingController();
  TextEditingController waCtrl = TextEditingController();

  bool validNgan = true;
  bool validWa = true;

  bool _isEnableNextBtnPage1 = false;
  bool get getIsEnableNextBtnPage1 => _isEnableNextBtnPage1;
  setIsEnableNextBtnPage1(bool enable) {
    _isEnableNextBtnPage1 = enable;
    notifyListeners();
  }
//page1

//page2
  /** Start No Fire project Phase **/
  int isFireCtrl = 0;
  int get getIsFire => isFireCtrl;
  setIsFire(int index) {
    isFireCtrl = index;
    notifyListeners();
  }

  TextEditingController forcastPlantingDateCtrl = TextEditingController();
  String forcastPlantingDateFormat = "";
  TextEditingController forcastHarvestDateCtrl = TextEditingController();
  String forcastHarvestDateFormat = "";

  /** End No Fire project Phase **/

  int typeIrrigationCtrl = 0;
  setTypeIrrigation(int index) {
    typeIrrigationCtrl = index;
    notifyListeners();
  }

  int get getTypeIrrigation => typeIrrigationCtrl;

  TextEditingController plantListCtrl = TextEditingController();

  int selectedPlant = 0;
  setSelectedPlant(int plant) {
    selectedPlant = plant;
    notifyListeners();
  }

  String get getSelectedPlantName => _selectedPlantName;
  String _selectedPlantName = "";
  setSelectedPlantName(String plantName) {
    _selectedPlantName = plantName;
    notifyListeners();
  }

  String iconPath = "";
  setIconPath(String path) {
    iconPath = path;
    notifyListeners();
  }

  getPlantList(BuildContext context) {
    LandRecomnViewModel landRecomnViewModel =
        Provider.of<LandRecomnViewModel>(context, listen: false);
    List items = Cast.objectToJson(landRecomnViewModel.getPlantList);
    int plantId = 0;
    var plantList = [];
    items.forEach(
      (element) {
        element["uuid"] = element["id"];
        plantId = plantId + 1;
        element["id"] = plantId;
        plantList.add(element);
      },
    );
    return Cast.objectToJson(plantList);
  }

  int fieldTypeCtrl = 0;
  setfieldTypeCtrl(int index) {
    fieldTypeCtrl = index;
    notifyListeners();
  }

  int get getfieldTypeCtrl => fieldTypeCtrl;

  int ricePlantingTypeCtrl = 0;
  setRicePlantingTypeCtrl(int index) {
    ricePlantingTypeCtrl = index;
    notifyListeners();
  }

  int get getRicePlantingTypeCtrl => ricePlantingTypeCtrl;

  TextEditingController awdDayCtrl = TextEditingController();
  String awdDayDateFormat = "";

  int interredTypeCtrl = 0;
  setInterredTypeCtrl(int index) {
    interredTypeCtrl = index;
    notifyListeners();
  }

  int get getInterredTypeCtrl => interredTypeCtrl;

  bool _isEnableNextBtnPage2 = false;

  bool get getIsEnableNextBtnPage2 => _isEnableNextBtnPage2;
  setIsEnableNextBtnPage2(bool enable) {
    _isEnableNextBtnPage2 = enable;
    notifyListeners();
  }

  Future<File> uint8ListToFile(Uint8List rawFile, String fileName) async {
    var tempDir = await getTemporaryDirectory();
    File file = await File('${tempDir.path}/image_${fileName}.png').create();
    file.writeAsBytesSync(rawFile);
    return file;
  }

//page2
  List<String> type_of_rice_cultivation = ["นาปี", "นาปรัง"];
  List<String> type_of_farming = ["นาดำ", "นาหว่าน", "นาหยอด"];
  Future setForm(
      String project_id,
      String req_join_project_id,
      BuildContext context,
      RequestJoinProjectListModel? farmItem,
      String headerImage) async {
    setLoading(true);
    final SharedPreferences prefs = await _prefs;
    String userId = prefs.getString('userId')!;
    String? formId;
    ApiResult<FormResponse> formIdResponse =
        await JoinProjectService().getFormByProjectId(project_id);
    formIdResponse.when(
        success: (success) => {
              formId = success.data?[0].id,
            },
        failure: (failure) => {});

    String land_rights =
        isAuthority == 1 ? "เป็นเจ้าของที่ดิน" : "เป็นผู้เช่าที่ดิน";

    //form for submit
    bool isWDProject = _projectName == "โครงการปลูกข้าวเปียกสลับแห้ง";
    waCtrl.text = waCtrl.text == "0.00" ? "" : waCtrl.text;
    if (isWDProject) {
      if (_selectedPlantName == "ข้าว") {
        form = FormModel(
            personal_id: personal_id,
            land_id: land_id,
            cultivation_id: cultivation_id,
            copy_house_registration_imgs: copyHouseRegistrationImgPath,
            letter_consent_participate_project_imgs:
                letterConsentParicipateProjectImgPath,
            project_code: prefs.getString("projectCode"),
            form_id: formId,
            user_id: userId,
            first_name: firstNameCtrl.text,
            last_name: lastNameCtrl.text,
            id_card_imgs: idCardImgPath,
            req_join_project_id: req_join_project_id,
            project_id: project_id,
            type_of_certificate_ownership: typeDocumentCtrl.text,
            land_rights: land_rights,
            certificate_land_utilization_imgs:
                certificateLandUtilizationImgPath,
            title_deed_number: documentNoCtrl.text,
            province: provinceCtrl.text,
            district: districtCtrl.text,
            sub_district: subDistrictCtrl.text,
            square_wah: int.parse(waCtrl.text == "" ? "0" : waCtrl.text),
            ngan: int.parse(nganCtrl.text == "" ? "0" : nganCtrl.text),
            rai: int.parse(raiCtrl.text == "" ? "0" : raiCtrl.text),
            title_deed_imgs: titleDeedImgPath,
            farm_book_imgs: farmBookImgPath,
            land_book_imgs: landBookImgPath,
            member_book_imgs: memberBookImgPath,
            irrigation_area: typeIrrigationCtrl == 1 ? "Y" : "N",
            plant: _selectedPlantName,
            type_of_rice_cultivation:
                fieldTypeCtrl == 1 ? type_of_rice_cultivation[0] : "นาปรัง",
            type_of_farming: ricePlantingTypeCtrl == 1
                ? type_of_farming[0]
                : ricePlantingTypeCtrl == 2
                    ? type_of_farming[1]
                    : "นาหยอด",
            start_at_rice_awd: awdDayDateFormat,
            join: interredTypeCtrl == 1 ? "Y" : "N",
            project_name: _projectName);
      } else {
        form = FormModel(
            personal_id: personal_id,
            land_id: land_id,
            cultivation_id: cultivation_id,
            copy_house_registration_imgs: copyHouseRegistrationImgPath,
            letter_consent_participate_project_imgs:
                letterConsentParicipateProjectImgPath,
            project_code: prefs.getString("projectCode"),
            form_id: formId,
            user_id: userId,
            first_name: firstNameCtrl.text,
            last_name: lastNameCtrl.text,
            id_card_imgs: idCardImgPath,
            req_join_project_id: req_join_project_id,
            project_id: project_id,
            type_of_certificate_ownership: typeDocumentCtrl.text,
            land_rights: land_rights,
            certificate_land_utilization_imgs:
                certificateLandUtilizationImgPath,
            title_deed_number: documentNoCtrl.text,
            province: provinceCtrl.text,
            district: districtCtrl.text,
            sub_district: subDistrictCtrl.text,
            square_wah: int.parse(waCtrl.text == "" ? "0" : waCtrl.text),
            ngan: int.parse(nganCtrl.text == "" ? "0" : nganCtrl.text),
            rai: int.parse(raiCtrl.text == "" ? "0" : raiCtrl.text),
            title_deed_imgs: titleDeedImgPath,
            farm_book_imgs: farmBookImgPath,
            land_book_imgs: landBookImgPath,
            member_book_imgs: memberBookImgPath,
            irrigation_area: typeIrrigationCtrl == 1 ? "Y" : "N",
            plant: _selectedPlantName,
            join: "N",
            project_name: _projectName);
      }
    }

    /// is V not fire project
    else {
      form = FormModel(
          personal_id: personal_id,
          land_id: land_id,
          cultivation_id: cultivation_id,
          copy_house_registration_imgs: copyHouseRegistrationImgPath,
          letter_consent_participate_project_imgs:
              letterConsentParicipateProjectImgPath,
          project_code: prefs.getString("projectCode"),
          form_id: formId,
          user_id: userId,
          first_name: firstNameCtrl.text,
          last_name: lastNameCtrl.text,
          id_card_imgs: idCardImgPath,
          req_join_project_id: req_join_project_id,
          project_id: project_id,
          type_of_certificate_ownership: typeDocumentCtrl.text,
          title_deed_number: documentNoCtrl.text,
          province: provinceCtrl.text,
          district: districtCtrl.text,
          sub_district: subDistrictCtrl.text,
          square_wah: int.parse(waCtrl.text == "" ? "0" : waCtrl.text),
          ngan: int.parse(nganCtrl.text == "" ? "0" : nganCtrl.text),
          rai: int.parse(raiCtrl.text == "" ? "0" : raiCtrl.text),
          land_rights: land_rights,
          certificate_land_utilization_imgs: certificateLandUtilizationImgPath,
          title_deed_imgs: titleDeedImgPath,
          farm_book_imgs: farmBookImgPath,
          land_book_imgs: landBookImgPath,
          member_book_imgs: memberBookImgPath,
          plant: _selectedPlantName,
          join: interredTypeCtrl == 1 ? "Y" : "N",
          burn_cultivation_area_v: isFireCtrl == 1 ? "Y" : "N",
          expected_date_planting_v: forcastPlantingDateFormat,
          expected_date_harvest_v: forcastHarvestDateFormat,
          project_name: _projectName);
    }
    FormModel form4Test = FormModel(
      copy_house_registration_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3b87050.png"
      ],
      letter_consent_participate_project_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/02c2d5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3b.png"
      ],
      project_code: prefs.getString("projectCode"),
      form_id: "b7c29c45-780a-4cf2-a892-0f1dc5a6b1a9",
      user_id: "bb11ec11-1123-4dce-9f18-2a293de60ebd",
      first_name: "firstNameCtrltext",
      last_name: "lastNameCtrltext",
      id_card_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/502c2d5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3.png"
      ],
      req_join_project_id: "85b9707b-6682-41c7-ae62-a57bd90f338e",
      project_id: "0d6dd87b-eab7-43c2-809b-dec652ac5f57",
      type_of_certificate_ownership: "typeDocumentCtrl.text",
      land_rights: "เป็นเจ้าของที่ดิน",
      letter_attorney:
          "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/02c2d5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3b.png",
      certificate_land_utilization_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/2c2d5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3b8.png"!
      ],
      title_deed_number: "documentNoCtrl.text,",
      province: "provinceCtrl.text",
      district: "districtCtrl.text",
      sub_district: "subDistrictCtrl.text",
      square_wah: int.parse(waCtrl.text == "" ? "0" : "2"),
      ngan: int.parse(nganCtrl.text == "" ? "0" : "1"),
      rai: int.parse(raiCtrl.text == "" ? "0" : "2"),
      title_deed_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/c2d5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3b87.png"
      ],
      farm_book_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/2d5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3b870.png"!
      ],
      land_book_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/f1ae7d9b7386b13bfb2fdd81a76de2db0c13ff5ea40335e5c8.png"!
      ],
      member_book_imgs: [
        "public/image/user/bb11ec11-1123-4dce-9f18-2a293de60ebd/application_form/d5f4916db695c1b4fc1e0b4b639c451e26c3c3e8e7ac3b8705.png"!
      ],
      irrigation_area: "N",
      plant: "มันสําปะหลัง",
      join: "N",
      project_name: "มันสําปะหลัง",
    );
    await deleteImagePath(userId);
    // log("message ===> ${json.encode(form)}");
    late Timer _timer;
    ApiResult<JoinProjectListResponse> submitResponse = isSaveData
        ? await JoinProjectService().submitForm(form!)
        : await JoinProjectService().updateForm(form!);
    submitResponse.when(
        success: (success) async => {
              await ViewDialogs.commonDialogWithIcon(
                  autoDismiss: () {
                    _timer = Timer(const Duration(seconds: 1), () async {
                      ApiResult<RequestJoinProjectListResponse>
                          resReqJoinProject = await JoinProjectService()
                              .requestJoinProjectById(req_join_project_id);
                      resReqJoinProject.when(
                          success: (farmItem) async => {
                                setLoading(false),
                                await redirectToActivityPage(
                                    context,
                                    project_id,
                                    farmItem,
                                    req_join_project_id,
                                    headerImage)
                              },
                          failure: (failure) async => {
                                setLoading(false),
                                await showErrorDialog(context),
                              });
                    });
                  },
                  isAutoDismiss: true,
                  context: context,
                  text: Messages.success),
              // Navigator.pushReplacement(
              //   context,
              //   MaterialPageRoute(
              //     builder: (_) => ActivitySelectionPage(
              //         projectId: project_id,
              //         farmItem: farmItem!,
              //         reqJoinProjectId: req_join_project_id),
              //   ),
              // )
            },
        failure: (failure) async => {
              setLoading(false),
              await showErrorDialog(context),
            });
  }

  Future<void> deleteImagePath(String userId) async {
    setLoading(true);
    ApiResult<CommonResponse> response =
        await VerifyService().deleteVerifyIdentityImage(userId, deletePath);
    response.when(
      success: (data) {
        setLoading(false);
        notifyListeners();
      },
      failure: ((error) {
        setLoading(false);
        print(error);
      }),
    );
  }

///////////////////////////
  Future showErrorDialog(BuildContext context) =>
      ViewDialogs.commonDialogWithIcon(
          icon: const Icon(
            Icons.close,
            color: Colors.red,
            size: 100,
          ),
          autoDismiss: () {
            Timer _timer = Timer(const Duration(seconds: 2), () {});
          },
          isAutoDismiss: true,
          context: context,
          text: Messages.failure);

  Future redirectToActivityPage(
          BuildContext context,
          String project_id,
          RequestJoinProjectListResponse farmItem,
          String req_join_project_id,
          String headerImage) =>
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (_) => ActivitySelectionPage(
              projectId: project_id,
              farmItem: farmItem.data![0],
              reqJoinProjectId: req_join_project_id,
              projectName: _projectName,
              headerImage: headerImage),
        ),
      );

  bool get getIsEnableNextBtn => _isEnableNextBtn;

  setIsEnableNextBtn(bool enable) {
    _isEnableNextBtn = enable;
    notifyListeners();
  }

  setIsEnableBackBtn(bool enable) {
    isEnableBackBtn = enable;
    notifyListeners();
  }

  setIsEnableSubmitBtn(bool enable) {
    isEnableSubmitBtn = enable;
    notifyListeners();
  }

  void clearState(String field) {
    switch (field) {
      case "province":
        {
          disableDistrict = false;
          disableSubDistrict = true;

          districtCtrl.clear();
          subDistrictCtrl.clear();
          notifyListeners();
        }
        break;

      case "district":
        {
          disableSubDistrict = false;

          subDistrictCtrl.clear();
          notifyListeners();
        }

        break;

      default:
        {}
        break;
    }
  }

  bool get getLoading => _loding;

  void setLoading(bool loading) {
    _loding = loading;
    notifyListeners();
  }

  Future<void> saveImage(
      Uint8List imageFile, SourceImgType source, String fileName) async {
    setLoading(true);

    String imgPath = "";
    String imgSignUrl;
    final SharedPreferences prefs = await _prefs;
    String userId = prefs.getString('userId')!;

    File fileImg = await uint8ListToFile(imageFile, fileName);
    ApiResult<UploadResponse> response =
        await uploadImageUtils(fileImg, userId, "verify-identity/upload/");

    response.when(success: (success) {
      imgPath = success.data?.path ?? "";
      imgSignUrl = success.data?.pathSignUrl ?? "";
      switch (source) {
        case SourceImgType.idCardBookImg:
          idCardImgPath.add(imgPath);
          idCardSignUrlImg.add(imgSignUrl);
          break;
        case SourceImgType.houseRegBookImg:
          copyHouseRegistrationImgPath.add(imgPath);
          copyHouseRegistrationSignUrlImg.add(imgSignUrl);
          break;
        case SourceImgType.landDocumentImage:
          titleDeedImgPath.add(imgPath);
          titleDeedSignUrlImg.add(imgSignUrl);
          break;
        case SourceImgType.farmerBookImage:
          farmBookImgPath.add(imgPath);
          farmBookSignUrlImg.add(imgSignUrl);
          break;
        case SourceImgType.farmerFamImage:
          memberBookImgPath.add(imgPath);
          memberBookSignUrlImg.add(imgSignUrl);
          break;
        case SourceImgType.farmerBookLandImage:
          landBookImgPath.add(imgPath);
          landBookSignUrlImg.add(imgSignUrl);
          break;
        case SourceImgType.powerAttorneyBookImg:
          letterConsentParicipateProjectImgPath.add(imgPath);
          letterConsentParicipateProjectSignUrlImg.add(imgSignUrl);
          break;
        case SourceImgType.rentingLandBookImg:
          certificateLandUtilizationImgPath.add(imgPath);
          certificateLandUtilizationSignUrlImg.add(imgSignUrl);
          break;
        default:
      }
      scrollController = ScrollController(initialScrollOffset: _scrollOffset);
      notifyListeners();
    }, failure: (error) {
      setLoading(false);
      print(error);
    });

    setLoading(false);
    notifyListeners();
  }

  double get scrollOffset => _scrollOffset;
  setScrollOffset(double scrollOffset) {
    _scrollOffset = scrollOffset;
    notifyListeners();
  }
}
