import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/views/market/consent_page/consent_page.dart';
import 'package:varun_kanna_app/views/market/landing/landing_page.dart';
import 'package:varun_kanna_app/views/market/market_page_provider.dart';
import 'package:varun_kanna_app/views/market/select_farm/select_fam_page.dart';
import 'package:varun_kanna_app/widget/custom_app_bar.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';

import '../../routes/router.gr.dart';
import '../../view_models/navigate_index_view_model.dart';
import '../../widget/view_dialogs.dart';

class MarketPage extends StatelessWidget {
  const MarketPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final router = context.router;
    NavigateIndexViewModel navigateIndexService =
        Provider.of<NavigateIndexViewModel>(context, listen: true);
    final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    return ChangeNotifierProvider(
      create: (context) => MarketPageProvider()..initialData(),
      child: Consumer<MarketPageProvider>(builder: (context, provider, _) {
        return Scaffold(
          appBar: const CustomAppBar(title: "KANNA"),
          body: provider.isLoading
              ? SizedBox(
                  height: MediaQuery.of(context).size.height,
                  child: const CustomLoadingWidget(),
                )
              : Center(
                  child: SizedBox(
                    height: MediaQuery.of(context).size.height,
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: provider.projectList.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: provider.isClicked
                                    ? null
                                    : () async {
                                        String projectName =
                                            provider.projectList[index].name!;
                                        String projectCode = provider
                                            .projectList[index].projectCode!;
                                        final SharedPreferences prefs =
                                            await _prefs;

                                        prefs.setString(
                                            "projectName", projectName);
                                        prefs.setString(
                                            "projectCode", projectCode);
                                        final isEmpty =
                                            await provider.checkIsEmptyFarm();
                                        if (isEmpty) {
                                          final action = await ViewDialogs
                                              .ConfirmOrCancelDialog(
                                                  context,
                                                  "ยังไม่มีแปลงปลูกใช่ไหม ?",
                                                  "กรุณาเพิ่มแปลงปลูกก่อนที่จะเข้าร่วมโครงการเพิ่มแปลงปลูกกด ยืนยัน",
                                                  "ยกเลิก",
                                                  "ยืนยัน",
                                                  Colors.black);
                                          if (action ==
                                              ViewDialogsAction.confirm) {
                                            router.navigate(FarmsRouter());
                                            navigateIndexService
                                                .setActiveIndex(2);
                                          }
                                        } else {
                                          await provider.getConset(
                                              projectId: provider
                                                      .projectList[index].id ??
                                                  "");
                                          if (!provider.isSigned) {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) => ConsentPage(
                                                        projectName: provider
                                                                .projectList[
                                                                    index]
                                                                .name ??
                                                            "",
                                                        projectId: provider
                                                                .projectList[
                                                                    index]
                                                                .id ??
                                                            "",
                                                        content:
                                                            provider.content,
                                                        consentId:
                                                            provider.consentId,
                                                        headerImage: provider
                                                                .projectList[
                                                                    index]
                                                                .headerImage ??
                                                            "")));
                                          } else {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      LandingPage(
                                                        projectId: provider
                                                                .projectList[
                                                                    index]
                                                                .id ??
                                                            "",
                                                        projectName: provider
                                                                .projectList[
                                                                    index]
                                                                .name ??
                                                            "",
                                                        headerImage: provider
                                                                .projectList[
                                                                    index]
                                                                .headerImage ??
                                                            "",
                                                      )),
                                            );
                                          }
                                        }
                                      },
                                child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  height:
                                      MediaQuery.of(context).size.width * 0.38,
                                  margin: const EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(12.5),
                                    color: Colors.grey,
                                    image: DecorationImage(
                                      image: CachedNetworkImageProvider(
                                        provider.projectList[index]
                                                .bannerImage ??
                                            "",
                                      ),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: Align(
                                    alignment: Alignment.bottomRight,
                                    child: Container(
                                        margin: const EdgeInsets.all(10),
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 15, vertical: 5),
                                        decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        child: const Text(
                                          "เข้าร่วม",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              fontSize: 16),
                                        )),
                                  ),
                                ),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ),
        );
      }),
    );
  }
}
