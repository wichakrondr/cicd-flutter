import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/views/market/activity_selection/activity_selection_page.dart';
import 'package:varun_kanna_app/views/market/select_plant_page/select_plant_page.dart';
import 'package:varun_kanna_app/views/market/verification_identity_page/verification_identity_page.dart';
import 'package:varun_kanna_app/views/market/verification_land_page/verification_land_page.dart';
import 'package:varun_kanna_app/views/market/verify_main_page_provider.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';

import 'package:varun_kanna_app/widget/time_line_widget.dart';
import '../../../widget/app_bar_text.dart';

enum SourceImgType {
  farmerBookImage,
  farmerFamImage,
  farmerBookLandImage,
  landDocumentImage,
  powerAttorneyBookImg,
  rentingLandBookImg,
  houseRegBookImg,
  idCardBookImg
}

enum ActivityStatusTypes {
  waitingForInformation,
  waitingForEditIdentity,
  waitingForVerifyIdentity,
  verified,
  verifyRejected,
  waitingForTheActivityLog,
  waitingForEditTheActivity,
  waitingForApproveTheActivity,
  successfulActivityLog,
  activityRejected,
}
// - รอบันทึกข้อมูล/รอบันทึกกิจกรรม = กรอกข้อมูลได้
// - รอตรวจสอบ = ดูและแก้ไม่ได้
// - รอแก้ไขข้อมูล = แก้ได้ปกติ
// - สำเร็จ/ไม่สำเร็จ = ดูอย่างเดียว

class VerifyMainPage extends StatelessWidget {
  VerifyMainPage(
      {super.key,
      required this.farmItem,
      required this.projectId,
      required this.projectName,
      required this.reqJoinProjectId,
      required this.statusType,
      required this.headerImage});
  String reqJoinProjectId;
  RequestJoinProjectListModel? farmItem;
  String projectId;
  String projectName;
  String headerImage;
  ActivityStatusTypes statusType;
  List<String> listProcess = [
    "ยืนยันตัวตัวตน",
    "ยืนยันแปลงปลูก",
    "พืชที่ปลูก",
  ];
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => VerifyMainPageProvider()
        ..initialData(context, statusType, projectId, reqJoinProjectId),
      child: Consumer<VerifyMainPageProvider>(builder: (context, provider, _) {
        bool isHideNextBtn = provider.getProcessPage >= listProcess.length - 1;
        bool isHideBackBtn = provider.getProcessPage == 0;
        bool isHideSubmitBtn =
            provider.getProcessPage != listProcess.length - 1;
        List<Widget> _nextBtn() {
          List<Widget> children = [];
          children.add(const Spacer());
          children.add(
            SizedBox(
              width: MediaQuery.of(context).size.width /
                  (provider.getProcessPage == 1 ? 2.5 : 1.2),
              child: TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Colors.white,
                  textStyle: const TextStyle(fontSize: 20),
                  backgroundColor: provider.getIsEnableNextBtnPage0 &&
                          provider.getProcessPage == 0
                      ? Colors.black
                      : provider.getIsEnableNextBtnPage1 &&
                              provider.getProcessPage == 1
                          ? Colors.black
                          : const Color(0XFFE5EAEF),
                  padding: const EdgeInsets.all(15.0),
                  side: const BorderSide(color: Color(0XFFCBD5E0), width: 1),
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.5))),
                ),
                onPressed: () {
                  provider.getIsEnableNextBtnPage0 &&
                          provider.getProcessPage == 0
                      ? provider.setProcessPage(provider.getProcessPage + 1)
                      : provider.getIsEnableNextBtnPage1 &&
                              provider.getProcessPage == 1
                          ? provider.setProcessPage(provider.getProcessPage + 1)
                          : provider.getIsEnableNextBtnPage2 &&
                                  provider.getProcessPage == 2
                              ? provider
                                  .setProcessPage(provider.getProcessPage + 1)
                              : null;
                },
                child: const Text(
                  "ถัดไป",
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
          );
          // set full size next btn
          provider.getProcessPage == 0 ? children.removeAt(0) : null;
          isHideNextBtn ? children.removeRange(1, 2) : null;
          return children;
        }

        Widget remarkBox(String remarkText) {
          return Container(
            margin: const EdgeInsets.only(
              top: 20,
              left: 20,
              right: 20,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.3),
                  offset: const Offset(0, 1),
                  blurRadius: 5,
                  spreadRadius: 0,
                ),
              ],
            ),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: const Text(
                    "หมายเหตุ",
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  padding: const EdgeInsets.only(
                      top: 5, left: 20, bottom: 20, right: 20),
                  child: Text(
                      style: TextStyle(
                        fontSize: 14.2,
                        color: Colors.red,
                      ),
                      remarkText),
                ),
              ],
            ),
          );
        }

        List<Widget> _backBtn() {
          List<Widget> children = [];
          children.add(
            SizedBox(
              width: MediaQuery.of(context).size.width /
                  (provider.getProcessPage == 0 ? 1.4 : 2.5),
              child: TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Colors.black,
                  textStyle: const TextStyle(fontSize: 20),
                  backgroundColor: !false ? Colors.white : Colors.black,
                  padding: const EdgeInsets.all(15.0),
                  side: const BorderSide(color: Colors.black, width: 1),
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.5))),
                ),
                onPressed: () {
                  provider.setProcessPage(provider.getProcessPage - 1);
                },
                child: const Text(
                  "ย้อนกลับ",
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
          );
          isHideBackBtn ? children.removeLast() : null;
          return children;
        }

        List<Widget> _submitBtn() {
          List<Widget> children = [];
          children.add(
            SizedBox(
              width: MediaQuery.of(context).size.width / 2.5,
              child: TextButton(
                style: TextButton.styleFrom(
                  foregroundColor: Colors.white,
                  textStyle: const TextStyle(fontSize: 20),
                  backgroundColor: provider.getIsEnableNextBtnPage2
                      ? Colors.black
                      : const Color(0XFFE5EAEF),
                  padding: const EdgeInsets.all(15.0),
                  side: const BorderSide(color: Color(0XFFCBD5E0), width: 1),
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.5))),
                ),
                onPressed: () async {
                  provider.getIsEnableNextBtnPage2
                      ? {
                          await provider.setForm(projectId, reqJoinProjectId,
                              context, farmItem, headerImage),
                        }
                      : null;
                },
                child: const Text(
                  "เสร็จสิ้น",
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
          );
          isHideSubmitBtn ? children.removeLast() : null;
          return children;
        }

        //
        Widget _bottomWidget() => Container(
            padding: const EdgeInsets.all(25.0),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(32),
                    topLeft: Radius.circular(32)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.15), blurRadius: 10),
                ],
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [..._backBtn(), ..._nextBtn(), ..._submitBtn()],
            ));

        // render
        return provider.getLoading
            ? Scaffold(
                backgroundColor: Colors.white,
                body: Container(
                  height: MediaQuery.of(context).size.height,
                  child: const CustomLoadingWidget(),
                ),
              )
            : WillPopScope(
                onWillPop: (() async {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ActivitySelectionPage(
                          projectId: projectId,
                          farmItem: farmItem!,
                          reqJoinProjectId: reqJoinProjectId,
                          projectName: projectName,
                          headerImage: headerImage),
                    ),
                  );
                  return true;
                }),
                child: Scaffold(
                    backgroundColor: Colors.white,
                    body: GestureDetector(
                        onTap: () =>
                            FocusManager.instance.primaryFocus?.unfocus(),
                        child: Stack(
                          children: [
                            Column(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                AppBarText(
                                  textHeader: "ยืนยันข้อมูล",
                                  action: InkWell(
                                      onTap: () => {
                                            Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                builder: (_) =>
                                                    ActivitySelectionPage(
                                                        projectId: projectId,
                                                        farmItem: farmItem!,
                                                        reqJoinProjectId:
                                                            reqJoinProjectId,
                                                        projectName:
                                                            projectName,
                                                        headerImage:
                                                            headerImage),
                                              ),
                                            )
                                          },
                                      child: const Icon(
                                        Icons.close,
                                        color: Colors.white,
                                        size: 30,
                                      )),
                                  isback: false,
                                ),
                                Expanded(
                                    child: SingleChildScrollView(
                                        controller: provider.scrollController,
                                        physics: const BouncingScrollPhysics(),
                                        child: Container(
                                          decoration: const BoxDecoration(
                                            color: Colors.white,
                                          ),
                                          child: Column(children: [
                                            Container(
                                              margin: const EdgeInsets.only(
                                                top: 20,
                                                left: 20,
                                                right: 20,
                                              ),
                                              decoration: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.black
                                                        .withOpacity(0.3),
                                                    offset: const Offset(0, 1),
                                                    blurRadius: 5,
                                                    spreadRadius: 0,
                                                  ),
                                                ],
                                              ),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.5,
                                                    padding:
                                                        const EdgeInsets.all(
                                                            20),
                                                    child: Column(
                                                      children: [
                                                        Align(
                                                          alignment: Alignment
                                                              .centerLeft,
                                                          child: Text(
                                                              farmItem!
                                                                  .farmName,
                                                              style:
                                                                  const TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                                fontSize: 18,
                                                              )),
                                                        ),
                                                        const SizedBox(
                                                          height: 10,
                                                        ),
                                                        Text(
                                                            'พื้นที่ ${farmItem!.displayArea}',
                                                            style:
                                                                const TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 16,
                                                            )),
                                                      ],
                                                    ),
                                                  ),
                                                  Container(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            20),
                                                    child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              12.5),
                                                      child: SizedBox.fromSize(
                                                        size: Size.fromRadius(
                                                            Screen.isLargeScreen
                                                                ? 60
                                                                : 53),
                                                        child:
                                                            CachedNetworkImage(
                                                                width: 12,
                                                                imageUrl:
                                                                    farmItem!
                                                                        .farmImg,
                                                                fit: BoxFit
                                                                    .cover),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            provider.remarkText.isNotEmpty
                                                ? remarkBox(provider.remarkText)
                                                : const SizedBox(),
                                            ProcessTimeline(
                                              height: 0.14,
                                              space: 1,
                                              dotSize: 27,
                                              listProcess: listProcess,
                                              itemCount: 3,
                                              processIndex:
                                                  provider.getProcessPage,
                                            ),
                                            // switch render verify state page
                                            provider.getProcessPage == 0
                                                ? const VerifyPage()
                                                : provider.getProcessPage == 1
                                                    ? VerificationLandPage()
                                                    : provider.getProcessPage ==
                                                            2
                                                        ? SelectPlantPage(
                                                            projectName: provider
                                                                .getProjectName,
                                                            projectId:
                                                                projectId,
                                                          )
                                                        : Container(),
                                          ]),
                                        ))),
                              ],
                            ),
                          ],
                        )),
                    bottomNavigationBar: _bottomWidget()),
              );
      }),
    );
  }
}
