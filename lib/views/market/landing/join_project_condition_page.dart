import 'package:flutter/material.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';

class JoinProjectConditionPage extends StatelessWidget {
  JoinProjectConditionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(color: Colors.white),
        child: Column(
          children: [
            AppBarText(
              textHeader: "เงื่อนไขการเข้าร่วมโครงการ",
              isback: false,
              action: IconButton(
                padding: const EdgeInsets.only(right: 30, top: 8),
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: const Icon(
                  Icons.close,
                  size: 40,
                  color: Colors.white,
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width * 0.9,
                margin: const EdgeInsets.only(top: 30),
                child: SingleChildScrollView(
                  child: Column(
                    children: const [
                      Text(
                        "เงื่อนไขการเข้าร่วมโครงการทำนาแบบเปียกสลับแห้ง\n" +
                            " 1.พื้นที่ปลูกข้าวอยู่ในเขตชลประทาน\n" +
                            " 2.เปลี่ยนการจัดการน้ำจากการปล่อยน้ำให้ท่วมขังตลอดฤดูเพาะปลูก เป็นแบบเปียกสลับแห้ง\n" +
                            " 3. เปลี่ยนวิธีการปลูกข้าวจาก \"นาดำ\" เป็น \"นาหว่าน หรือ นาหยอด\"\n"
                                " 4. เปลี่ยนวิธีการปลูกข้าวจากนาดำที่เพาะกล้าในแปลงรวม เป็น นาดำที่เพาะกล้าในถาดเพาะ\n"
                                " 5. มีอุปกรณ์หรือเครื่องมือในการควบคุมการนําน้ำเข้าและระบายน้ำที่สามารถควบคุมน้ำได้ทั้งฤดูฝนและฤดูแล้ง\n" +
                            " 6. ติดตั้งท่อวัดระดับน้ำในแปลงนา\n" +
                            " 7. สามารถเข้ารับการฝึกอบรมหรือการสนับสนุนความรู้เชิงเทคนิคในการทำนาแบบเปียกสลับแห้ง\n" +
                            " 8. มีการวิเคราะห์ธาตุอาหารในดินก่อนการเพาะปลูก และใช้ปุ๋ยตามค่าวิเคราะห์ดิน\n",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                            height: 1.6),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
