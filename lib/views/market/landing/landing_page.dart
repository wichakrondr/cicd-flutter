import 'dart:async';

import 'package:auto_route/auto_route.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/views/market/activity_selection/activity_selection_page.dart';
import 'package:varun_kanna_app/views/market/landing/join_project_condition_page.dart';
import 'package:varun_kanna_app/views/market/landing/landing_page_provider.dart';
import 'package:varun_kanna_app/views/market/select_farm/select_fam_page.dart';
import 'package:varun_kanna_app/widget/slidable_button.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

import 'joint_project_hand_book_page.dart';

class LandingPage extends StatelessWidget {
  LandingPage(
      {Key? key,
      required this.projectId,
      required this.projectName,
      required this.headerImage})
      : super(key: key);

  String projectId;
  String projectName;
  String headerImage;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) =>
            LandingPageProvider()..initData(projectId, projectName),
        child: Consumer<LandingPageProvider>(builder: (contex, landingProv, _) {
          return Scaffold(
            body: landingProv.loading
                ? const Center(
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))),
                  )
                : Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        varunaBackground(context),
                        Align(
                            alignment: Alignment.topCenter,
                            child: Container(
                                margin: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.08,
                                ),
                                child: Column(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.05,
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.05),
                                      child: Column(
                                        children: [
                                          varunaIcon(context),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          profileInfo(landingProv),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          joinProjectBtn(context),
                                          const SizedBox(
                                            height: 15,
                                          ),
                                          landingProv.projectName ==
                                                  "โครงการปลูกข้าวเปียกสลับแห้ง"
                                              ? InkWell(
                                                  onTap: () => Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              JoinProjectConditionPage())),
                                                  child: const Text(
                                                      "เงื่อนไขการเข้าร่วมโครงการ",
                                                      style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        decoration:
                                                            TextDecoration
                                                                .underline,
                                                      )),
                                                )
                                              : const SizedBox(),
                                          const SizedBox(
                                            height: 15,
                                          ),
                                          landingProv.projectName ==
                                                  "โครงการปลูกข้าวเปียกสลับแห้ง"
                                              ? InkWell(
                                                  onTap: () => Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              JoinProjectHandBookPage(
                                                                galleryItems: const [
                                                                  'assets/images/awd_hand_book/1.png',
                                                                  'assets/images/awd_hand_book/2.png',
                                                                  'assets/images/awd_hand_book/3.png',
                                                                  'assets/images/awd_hand_book/4.png',
                                                                  'assets/images/awd_hand_book/5.png',
                                                                  'assets/images/awd_hand_book/6.png',
                                                                ],
                                                                initPage: 0,
                                                              ))),
                                                  child: const Text(
                                                      "ขั้นตอนการลงทะเบียน",
                                                      style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        decoration:
                                                            TextDecoration
                                                                .underline,
                                                      )),
                                                )
                                              : const SizedBox(),
                                          const SizedBox(
                                            height: 15,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Expanded(
                                        child: Container(
                                      child: ClipRRect(
                                        borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(25),
                                          topRight: Radius.circular(25),
                                        ),
                                        child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              boxShadow: [
                                                BoxShadow(
                                                    color: Colors.black
                                                        .withOpacity(0.2),
                                                    blurRadius: 10),
                                              ],
                                              borderRadius:
                                                  const BorderRadius.only(
                                                topLeft: Radius.circular(25),
                                                topRight: Radius.circular(25),
                                              ),
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 20, left: 20, right: 20),
                                              child: Column(
                                                children: [
                                                  filterTab(landingProv),
                                                  const SizedBox(
                                                    height: 20,
                                                  ),
                                                  farmList(landingProv, context,
                                                      projectId),
                                                ],
                                              ),
                                            )),
                                      ),
                                    ))
                                  ],
                                ))),
                      ],
                    ),
                  ),
          );
        }));
  }

  Widget varunaBackground(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Container(
        height: width < 800
            ? MediaQuery.of(context).size.height * 0.47
            : MediaQuery.of(context).size.height * 0.56,
        width: double.infinity,
        child: CachedNetworkImage(imageUrl: headerImage, fit: BoxFit.cover));
  }

  Widget varunaIcon(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        InkWell(
          onTap: () {
            Navigator.popUntil(context, (route) => route.isFirst);
            context.router.navigate(MarketRouter());
          },
          child: const Icon(
            Icons.close,
            size: 32.0,
            color: Colors.white,
          ),
        )
      ],
    );
  }

  Widget profileInfo(LandingPageProvider prov) {
    Color fontColor = projectName == "โครงการปลูกข้าวเปียกสลับแห้ง"
        ? Colors.black
        : Colors.white;
    return Column(
      children: [
        const SizedBox(
          height: 5,
        ),
        Text(
          projectName,
          style: TextStyle(
              fontSize: 18, fontWeight: FontWeight.w500, color: fontColor),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Text(
            prov.firstName + " " + prov.lastName,
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.w700, color: fontColor),
          ),
        ),
        const SizedBox(
          height: 5,
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Text(
            "${prov.mobilePhone.substring(0, 3)}-${prov.mobilePhone.substring(3, 6)}-${prov.mobilePhone.substring(6, 10)}",
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.w700, color: fontColor),
          ),
        )
      ],
    );
  }

  Widget joinProjectBtn(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => SelectFarmPage(
                      projectId: projectId,
                      projectName: projectName,
                      headerImage: headerImage,
                    )));
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(
            Icons.add,
            color: Colors.black,
          ),
          SizedBox(
            width: 5,
          ),
          Text("เพิ่มแปลงเข้าร่วมโครงการ")
        ],
      ),
      style: ElevatedButton.styleFrom(
          padding: const EdgeInsets.all(15),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12.5),
          ),
          textStyle: const TextStyle(
              fontWeight: FontWeight.w500, fontSize: 18, height: 1.4),
          backgroundColor: Colors.white,
          foregroundColor: Colors.black),
    );
  }

  Widget filterTab(LandingPageProvider prov) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: () {
              prov.setSelectIndex(1);
              prov.clearList();
              prov.requestJoinProjectsList(projectId);
            },
            child: Column(
              children: [
                SvgPicture.asset(
                  "assets/images/ic_filter_all.svg",
                  color: prov.selectIndex == 1
                      ? const Color(0xFF0AC898)
                      : Colors.black,
                  width: 30,
                  height: 30,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "ทั้งหมด",
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: prov.selectIndex == 1
                        ? const Color(0xFF0AC898)
                        : Colors.black,
                  ),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              prov.setSelectIndex(2);
              prov.clearList();
              prov.requestJoinProjectsList(projectId);
            },
            child: Column(
              children: [
                SvgPicture.asset(
                  "assets/images/ic_filter_fill_out.svg",
                  color: prov.selectIndex == 2
                      ? const Color(0xFF0AC898)
                      : Colors.black,
                  width: 30,
                  height: 30,
                ),
                const SizedBox(
                  height: 10,
                ),
                Text(
                  "กรอกข้อมูล",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w500,
                      color: prov.selectIndex == 2
                          ? const Color(0xFF0AC898)
                          : Colors.black),
                ),
              ],
            ),
          ),
          InkWell(
              onTap: () {
                prov.setSelectIndex(3);
                prov.clearList();
                prov.requestJoinProjectsList(projectId);
              },
              child: Column(
                children: [
                  SvgPicture.asset(
                    "assets/images/ic_filter_waiting.svg",
                    color: prov.selectIndex == 3
                        ? const Color(0xFF0AC898)
                        : Colors.black,
                    width: 30,
                    height: 30,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "รอตรวจ",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: prov.selectIndex == 3
                            ? const Color(0xFF0AC898)
                            : Colors.black),
                  ),
                ],
              )),
          InkWell(
              onTap: () {
                prov.setSelectIndex(4);
                prov.clearList();
                prov.requestJoinProjectsList(projectId);
              },
              child: Column(
                children: [
                  prov.selectIndex == 4
                      ? SvgPicture.asset(
                          "assets/images/ic_filter_success.svg",
                          width: 30,
                          height: 30,
                          color: const Color(0xFF0AC898),
                        )
                      : SvgPicture.asset(
                          "assets/images/ic_filter_success.svg",
                          width: 30,
                          height: 30,
                        ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "สำเร็จ",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: prov.selectIndex == 4
                            ? const Color(0xFF0AC898)
                            : Colors.black),
                  ),
                ],
              )),
          InkWell(
              onTap: () {
                prov.setSelectIndex(5);
                prov.clearList();
                prov.requestJoinProjectsList(projectId);
              },
              child: Column(
                children: [
                  prov.selectIndex == 5
                      ? SvgPicture.asset(
                          "assets/images/ic_filter_fail.svg",
                          width: 30,
                          height: 30,
                          color: const Color(0xFF0AC898),
                        )
                      : SvgPicture.asset(
                          "assets/images/ic_filter_fail.svg",
                          width: 30,
                          height: 30,
                        ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    "ไม่สำเร็จ",
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: prov.selectIndex == 5
                            ? const Color(0xFF0AC898)
                            : Colors.black),
                  ),
                ],
              ))
        ]);
  }

  Widget farmList(
      LandingPageProvider prov, BuildContext ctx, String projectId) {
    late Timer _timer;
    final width = MediaQuery.of(ctx).size.width;
    return Expanded(
        child: SmartRefresher(
      enablePullDown: false,
      enablePullUp: true,
      controller: prov.loadmoreController,
      onLoading: prov.onLoading,
      footer: CustomFooter(
        builder: (BuildContext context, LoadStatus? mode) {
          if (mode == LoadStatus.failed) {
            return Container();
          } else if (mode == LoadStatus.canLoading) {
            return Container(
              height: 50,
              child: const Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8)))),
            );
          } else {
            return Container();
          }
        },
      ),
      child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Container(
            child: Column(
              children: [
                SlidableAutoCloseBehavior(
                  child: ListView.builder(
                      physics: const NeverScrollableScrollPhysics(),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: prov.joinProjectlist.length,
                      padding: EdgeInsets.zero,
                      itemBuilder: (context, index) {
                        return Container(
                            margin: EdgeInsets.only(
                                top: index == 0 ? 2 : 10,
                                left: 2,
                                right: 2,
                                bottom: 10),
                            child: Stack(
                              children: [
                                Positioned.fill(
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Container(
                                          decoration: BoxDecoration(
                                            color: Colors.red,
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.black
                                                    .withOpacity(0.3),
                                                offset: const Offset(0, 1),
                                                blurRadius: 5,
                                                spreadRadius: 0,
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Slidable(
                                  groupTag: '0',
                                  key: ValueKey(
                                      prov.joinProjectlist[index].farmId),
                                  direction: Axis.horizontal,
                                  endActionPane: ActionPane(
                                    motion: const ScrollMotion(),
                                    extentRatio: 0.27,
                                    dragDismissible: false,
                                    children: [
                                      Expanded(
                                        child: SlidableButton(
                                            icon: Icons.delete_outline,
                                            label: 'ออกจาก\nโครงการ',
                                            onPressed: (context) async {
                                              final action = await ViewDialogs
                                                  .ConfirmOrCancelDialog(
                                                context,
                                                "ต้องการที่จะออกจากโครงการใช่ไหม ?",
                                                "หากคุณกดยืนยันการออกจากโครงการ\nข้อมูลต่างๆ เกี่ยวกับแปลงปลูก\nที่คุณเข้าโครงการจะหายไป\nคุณยืนยันที่จะออกจากโครงการใช่ไหม ?",
                                                "ยกเลิก",
                                                "ยืนยัน",
                                                Colors.red,
                                              );
                                              if (action ==
                                                  ViewDialogsAction.confirm) {
                                                bool result = await prov
                                                    .cancelJoinProject(
                                                        prov
                                                            .joinProjectlist[
                                                                index]
                                                            .reqJoinProjectId,
                                                        projectId);
                                                if (result) {
                                                  await ViewDialogs
                                                          .commonDialogWithIcon(
                                                              context: context,
                                                              isAutoDismiss:
                                                                  true,
                                                              autoDismiss: () {
                                                                _timer = Timer(
                                                                    const Duration(
                                                                        seconds:
                                                                            2),
                                                                    () {
                                                                  Navigator.of(
                                                                          ctx)
                                                                      .pop();
                                                                });
                                                              },
                                                              icon: const Icon(
                                                                Icons
                                                                    .check_circle_outline,
                                                                color: Color(
                                                                    0xFF25C8A8),
                                                                size: 100,
                                                              ),
                                                              text:
                                                                  "ลบแปลงของท่านเรียบร้อยแล้ว")
                                                      .then((value) {
                                                    if (_timer.isActive) {
                                                      _timer.cancel();
                                                    }
                                                  });
                                                } else {
                                                  await ViewDialogs
                                                          .commonDialogWithIcon(
                                                              context: context,
                                                              isAutoDismiss:
                                                                  true,
                                                              autoDismiss: () {
                                                                _timer = Timer(
                                                                    const Duration(
                                                                        seconds:
                                                                            2),
                                                                    () {
                                                                  Navigator.of(
                                                                          ctx)
                                                                      .pop();
                                                                });
                                                              },
                                                              icon: const Icon(
                                                                Icons
                                                                    .close_outlined,
                                                                color:
                                                                    Colors.red,
                                                                size: 100,
                                                              ),
                                                              text:
                                                                  "ลบแปลงไม่สำเร็จ กรุณาลองใหม่อีกครั้ง")
                                                      .then((value) {
                                                    if (_timer.isActive) {
                                                      _timer.cancel();
                                                      Slidable.of(context)
                                                          ?.close();
                                                    }
                                                  });
                                                }
                                              }
                                              Slidable.of(context)?.close();
                                            }),
                                      )
                                    ],
                                  ),
                                  child: Builder(builder: (context) {
                                    SlidableController? _controller =
                                        Slidable.of(context);
                                    return ValueListenableBuilder<int>(
                                      valueListenable: _controller?.direction ??
                                          ValueNotifier<int>(0),
                                      builder: (context, value, _) {
                                        return GestureDetector(
                                          onHorizontalDragStart: (value) {
                                            _controller?.openEndActionPane();
                                          },
                                          onTap: () {
                                            Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ActivitySelectionPage(
                                                            farmItem:
                                                                prov.joinProjectlist[
                                                                    index],
                                                            reqJoinProjectId: prov
                                                                .joinProjectlist[
                                                                    index]
                                                                .reqJoinProjectId,
                                                            projectId:
                                                                projectId,
                                                            projectName:
                                                                projectName,
                                                            headerImage:
                                                                headerImage)));
                                          },
                                          child: Container(
                                            padding: const EdgeInsets.all(15),
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  width: width > 800
                                                      ? width * 0.15
                                                      : 0,
                                                ),
                                                Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.45,
                                                  padding:
                                                      const EdgeInsets.all(20),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      statusWidget(
                                                          prov
                                                              .joinProjectlist[
                                                                  index]
                                                              .status,
                                                          context,
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      Text(
                                                          prov
                                                              .joinProjectlist[
                                                                  index]
                                                              .farmName,
                                                          style:
                                                              const TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.w700,
                                                            fontSize: 18,
                                                          )),
                                                      const SizedBox(
                                                        height: 10,
                                                      ),
                                                      Text(
                                                          'พื้นที่ ${prov.joinProjectlist[index].displayArea}',
                                                          style:
                                                              const TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 16,
                                                          )),
                                                    ],
                                                  ),
                                                ),
                                                Container(
                                                  // margin:
                                                  //     const EdgeInsets.fromLTRB(
                                                  //         0, 20, 25, 20),
                                                  child: ClipRRect(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              18),
                                                      child: CachedNetworkImage(
                                                        imageUrl: prov
                                                            .joinProjectlist[
                                                                index]
                                                            .farmImg,
                                                        placeholder:
                                                            (context, url) =>
                                                                Container(
                                                          margin:
                                                              const EdgeInsets
                                                                  .all(5),
                                                          width: 40,
                                                          height: 40,
                                                          child: const CircularProgressIndicator(
                                                              valueColor:
                                                                  AlwaysStoppedAnimation<
                                                                          Color>(
                                                                      Color(
                                                                          0xFF25C8A8))),
                                                        ),
                                                      )),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  }),
                                )
                              ],
                            ));
                      }),
                ),
              ],
            ),
          )),
    ));
  }

  Widget statusWidget(String status, BuildContext context, double width) {
    Color fontColor;
    Color backgroundColor;
    String statusText = "";
    var icon = Icons.create_outlined;

    switch (status) {
      case "waiting for information":
        icon = Icons.create_outlined;
        statusText = "กรอกข้อมูล";
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);
        break;
      case "pending":
        icon = Icons.schedule;
        statusText = "รอยืนยัน";
        fontColor = const Color(0xffF8C309);
        backgroundColor = const Color(0xffFDF6DD);
        break;
      case "approved":
        icon = Icons.check_circle_outline;
        statusText = "สำเร็จ";
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);
        break;
      case "rejected":
        statusText = "ไม่สำเร็จ";
        fontColor = const Color(0xffFF3E1D);
        backgroundColor = const Color(0xffFFE0DB);
        break;
      default:
        icon = Icons.create_outlined;
        statusText = "รอบันทึกข้อมูล";
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);
        break;
    }

    return Container(
      width: MediaQuery.of(context).size.width * 0.33,
      padding: const EdgeInsets.fromLTRB(6, 4, 6, 4),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        mainAxisAlignment: width > 800
            ? MainAxisAlignment.center
            : MainAxisAlignment.spaceEvenly,
        children: [
          status == "verify rejected" || status == "activity rejected"
              ? SvgPicture.asset(
                  'assets/images/ic_failed_circle.svg',
                  width: 16,
                  height: 16,
                )
              : Icon(
                  icon,
                  color: fontColor,
                  size: 20,
                ),
          SizedBox(
            width: width > 800 ? 20 : 0,
          ),
          Text(
            statusText,
            style: TextStyle(fontSize: 13, color: fontColor),
          )
        ],
      ),
    );
  }
}
