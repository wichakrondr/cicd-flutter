import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/model/profile_model.dart';
import 'package:varun_kanna_app/services/join_project_service.dart';
import 'package:varun_kanna_app/services/profile_service.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';

class LandingPageProvider extends ChangeNotifier {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  RefreshController loadmoreController =
      RefreshController(initialRefresh: false);
  bool loading = true;
  String projectName = "";

  int _selectIndex = 1;
  int get selectIndex => _selectIndex;

  String firstName = "";
  String lastName = "";
  String mobilePhone = "";
  String userId = "";
  String projectId = "";
  int limit = 10;
  int offset = 0;
  bool hideLoadmore = true;

  List<RequestJoinProjectListModel> joinProjectlist = [];

  setLoading(bool isloading) {
    loading = isloading;
    notifyListeners();
  }

  setOffset(int curr) {
    offset = curr;
    notifyListeners();
  }

  void setSelectIndex(int index) {
    _selectIndex = index;
    notifyListeners();
  }

  void clearList() {
    joinProjectlist = [];
    notifyListeners();
  }

  Future<void> initData(String projectId, String projectName) async {
    this.projectId = projectId;
    final SharedPreferences prefs = await _prefs;
    mobilePhone = prefs.getString("phoneNumber").toString();
    this.projectName = prefs.getString("projectName") ?? projectName;
    await getProfile(mobilePhone);
    await requestJoinProjectsList(projectId);
    notifyListeners();
  }

  Future<bool> requestJoinProjectsList(String id) async {
    bool result = false;
    setLoading(true);
    String status = "";
    switch (_selectIndex) {
      case 1:
        status = "all";
        break;
      case 2:
        status = "waiting for information";
        break;
      case 3:
        status = "pending";
        break;
      case 4:
        status = "approved";
        break;
      case 5:
        status = "reject";
        break;
      default:
        status = "all";
    }
    setOffset(joinProjectlist.length);

    final SharedPreferences prefs = await _prefs;
    userId = prefs.getString('userId').toString();

    ApiResult<RequestJoinProjectListResponse> response =
        await JoinProjectService()
            .requestJoinProjectList(status, id, userId, limit, offset);

    response.when(
      success: (data) {
        joinProjectlist = data.data!;
        setLoading(false);
        result = true;
      },
      failure: (error) {
        setLoading(false);
        print(error);
      },
    );
    notifyListeners();
    return result;
  }

  onLoading() async {
    String status = "";
    var statusMessage;
    switch (_selectIndex) {
      case 1:
        status = "all";
        break;
      case 2:
        status = "waiting for information,waiting for edit identity";
        break;
      case 3:
        status = "waiting for verify identity";
        break;
      case 4:
        status = "verified";
        break;
      case 5:
        status = "verify rejected";
        break;
      default:
        status = "all";
    }

    await Future.delayed(const Duration(milliseconds: 1000));
    setOffset(joinProjectlist.length);
    ApiResult<RequestJoinProjectListResponse> response =
        await JoinProjectService()
            .requestJoinProjectList(status, projectId, userId, limit, offset);

    response.when(
      success: (data) {
        joinProjectlist = [...joinProjectlist, ...data.data!];
        loadmoreController.loadComplete();
        notifyListeners();
      },
      failure: (error) => {
        statusMessage = error,
        statusMessage = statusMessage.error,
        if (statusMessage == "can not found data farm list")
          {
            loadmoreController.loadNoData(),
          }
        else
          {
            loadmoreController.loadFailed(),
          },
        notifyListeners(),
      },
    );
  }

  void onDispose() {
    _selectIndex = 1;
    firstName = "";
    lastName = "";
    mobilePhone = "";
    joinProjectlist = [];

    notifyListeners();
  }

  getProfile(String phoneNumber) async {
    ApiResult<ProfileModel> response = await fetchProfile(phoneNumber);
    response.when(
      success: (data) {
        firstName = data.firstName!;
        lastName = data.lastName!;
      },
      failure: (error) => {},
    );
    setLoading(false);
    notifyListeners();
  }

  Future<bool> cancelJoinProject(
      String requestJoinProjectID, String projectId) async {
    bool result = false;
    final response =
        await JoinProjectService().cancelJoinProject(requestJoinProjectID);
    response.when(
      success: (data) async {
        result = data.status;
        if (result) {
          if (joinProjectlist.length == 1) {
            clearList();
          } else {
            clearList();
            await requestJoinProjectsList(projectId);
          }
        }
      },
      failure: (error) {
        setLoading(false);
      },
    );

    notifyListeners();
    return result;
  }
}
