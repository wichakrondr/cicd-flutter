import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';

class JoinProjectHandBookPage extends StatelessWidget {
  JoinProjectHandBookPage(
      {super.key, required this.galleryItems, required this.initPage});
  List<String> galleryItems;
  int initPage;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          AppBarText(
            textHeader: "ตัวอย่างรูปภาพ",
            isback: false,
            action: IconButton(
              padding: const EdgeInsets.only(right: 30, top: 8),
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.close,
                size: 40,
                color: Colors.white,
              ),
            ),
          ),
          Expanded(
            child: Container(
                child: PhotoViewGallery.builder(
              scrollPhysics: const BouncingScrollPhysics(),
              builder: (BuildContext context, int index) {
                return PhotoViewGalleryPageOptions(
                  imageProvider: AssetImage(galleryItems[index]),
                  initialScale: PhotoViewComputedScale.contained,
                  heroAttributes:
                      PhotoViewHeroAttributes(tag: galleryItems[index]),
                );
              },
              itemCount: galleryItems.length,
              loadingBuilder: (context, event) => Center(
                child: Container(
                  width: 20.0,
                  height: 20.0,
                  child: CircularProgressIndicator(
                    value: event == null
                        ? 0
                        : event.cumulativeBytesLoaded /
                            event.expectedTotalBytes!,
                  ),
                ),
              ),
              backgroundDecoration: BoxDecoration(color: Colors.white),
              pageController: PageController(initialPage: initPage),
            )),
          ),
        ],
      ),
    );
  }
}
