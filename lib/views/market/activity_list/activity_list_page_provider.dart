import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_selection_model.dart';
import 'package:varun_kanna_app/services/activity_list_service.dart';

class ActivityListPageProvider extends ChangeNotifier {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<ActivitySelectionModel> activityList = [];
  bool isLoading = true;
  bool isDisable = false;
  String projectName = "";

  void initialData(String reqJoinProjectId) async {
    isLoading = true;
    final SharedPreferences prefs = await _prefs;
    projectName = prefs.getString("projectName") ?? projectName;
    final response =
        await ActivityListService().fetchActivityList(reqJoinProjectId);
    response.when(
      success: (data) {
        activityList = data.data!;
        isLoading = false;
        notifyListeners();
      },
      failure: (error) {
        print(error);
        isLoading = false;
        notifyListeners();
      },
    );
  }

  Future<bool> skipActivity(String id, String reqJoinProjectId) async {
    final response = await ActivityListService().skipActivity(id);
    bool isSuccess = false;
    response.when(
      success: (data) {
        if (data.status) {
          initialData(reqJoinProjectId);
          isSuccess = true;
        }
      },
      failure: (error) {
        print(error);
      },
    );
    notifyListeners();
    return isSuccess;
  }
}
