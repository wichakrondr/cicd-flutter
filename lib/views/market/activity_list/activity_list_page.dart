import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_selection_model.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/views/market/activity/activity_page.dart';
import 'package:varun_kanna_app/views/market/activity_list/activity_list_page_provider.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:varun_kanna_app/widget/slidable_button.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

class ActivityListPage extends StatefulWidget {
  ActivityListPage({
    Key? key,
    required this.farmItem,
    required this.projectId,
    required this.reqJoinProjectId,
  }) : super(key: key);

  RequestJoinProjectListModel? farmItem;
  String projectId;
  String reqJoinProjectId;

  @override
  State<ActivityListPage> createState() => _ActivityListPageState();
}

class _ActivityListPageState extends State<ActivityListPage> {
  double _borderRadius = 20.0;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) =>
          ActivityListPageProvider()..initialData(widget.reqJoinProjectId),
      child:
          Consumer<ActivityListPageProvider>(builder: (context, provider, _) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: provider.isLoading
              ? const Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))),
                )
              : Stack(
                  children: [
                    Center(
                      child: Column(
                        children: [
                          AppBarText(
                              textHeader: "บันทึกกิจกรรม",
                              isback: true,
                              onBackPress: () => Navigator.pop(context)),
                          const SizedBox(
                            height: 10,
                          ),
                          farmInfo(context),
                          activityList(context, provider)
                        ],
                      ),
                    ),
                  ],
                ),
        );
      }),
    );
  }

  Widget farmInfo(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      margin: const EdgeInsets.only(top: 10, left: 2, right: 2, bottom: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            offset: const Offset(0, 1),
            blurRadius: 5,
            spreadRadius: 0,
          ),
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.45,
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(widget.farmItem!.farmName,
                      style: const TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                      )),
                ),
                const SizedBox(
                  height: 10,
                ),
                Text('พื้นที่ ${widget.farmItem!.displayArea}',
                    style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    )),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 20, 25, 20),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(18),
                child: CachedNetworkImage(imageUrl: widget.farmItem!.farmImg)),
          ),
        ],
      ),
    );
  }

  bool isEnable(int index, String status, String projectName) {
    if (projectName == "โครงการปลูกข้าวเปียกสลับแห้ง") {
      return index == 0 || status != "waiting for the activity log";
    } else {
      return true;
    }
  }

  Widget activityList(BuildContext ctx, ActivityListPageProvider provider) {
    late Timer _timer;
    return Expanded(
        child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              child: Column(
                children: [
                  SlidableAutoCloseBehavior(
                    child: ListView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: provider.activityList.length,
                        padding: EdgeInsets.zero,
                        itemBuilder: (context, index) {
                          String currentStatus =
                              provider.activityList[index].status!;

                          bool _isEnable = isEnable(
                              index,
                              provider.activityList[0].status!,
                              provider.projectName);
                          return Container(
                              margin: const EdgeInsets.only(
                                  top: 10, left: 2, right: 2, bottom: 10),
                              child: Stack(
                                children: [
                                  Positioned.fill(
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: Colors.red,
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(20),
                                                  bottomLeft:
                                                      Radius.circular(20),
                                                  topRight: Radius.circular(
                                                      _borderRadius),
                                                  bottomRight: Radius.circular(
                                                      _borderRadius)),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.black
                                                      .withOpacity(0.3),
                                                  offset: const Offset(0, 1),
                                                  blurRadius: 5,
                                                  spreadRadius: 0,
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Slidable(
                                    groupTag: '0',
                                    enabled:
                                        provider.activityList[index].required!
                                            ? false
                                            : true,
                                    key: ValueKey(
                                        provider.activityList[index].id!),
                                    direction: Axis.horizontal,
                                    endActionPane: ActionPane(
                                      motion: const ScrollMotion(),
                                      extentRatio: 0.25,
                                      dragDismissible: false,
                                      children: [
                                        Expanded(
                                            child: SlidableButton(
                                                label: 'กดข้าม\nกิจกรรม',
                                                onPressed: (context) async {
                                                  final action =
                                                      await ViewDialogs
                                                          .ConfirmOrCancelDialog(
                                                    context,
                                                    "ยืนยันการข้ามกิจกรรม",
                                                    "หากท่านกดข้ามกิจกรรม อาจมีผลกระทบ\nต่อค่าคาร์บอนเครดิตที่คุณจะได้รับ\nข้ามกิจกรรม กดยืนยัน",
                                                    "ยกเลิก",
                                                    "ยืนยัน",
                                                    Colors.red,
                                                  );
                                                  if (action ==
                                                      ViewDialogsAction
                                                          .confirm) {
                                                    bool isSuccess = await provider
                                                        .skipActivity(
                                                            provider
                                                                .activityList[
                                                                    index]
                                                                .id!,
                                                            widget
                                                                .reqJoinProjectId);
                                                    if (isSuccess) {
                                                      await ViewDialogs
                                                              .commonDialogWithIcon(
                                                                  context:
                                                                      context,
                                                                  isAutoDismiss:
                                                                      true,
                                                                  autoDismiss:
                                                                      () {
                                                                    _timer = Timer(
                                                                        const Duration(
                                                                            seconds:
                                                                                2),
                                                                        () {
                                                                      Navigator.of(
                                                                              ctx)
                                                                          .pop();
                                                                    });
                                                                  },
                                                                  icon:
                                                                      const Icon(
                                                                    Icons
                                                                        .check_circle_outline,
                                                                    color: Color(
                                                                        0xFF25C8A8),
                                                                    size: 100,
                                                                  ),
                                                                  text:
                                                                      "ข้ามกิจกรรมเรียบร้อยแล้ว")
                                                          .then((value) {
                                                        if (_timer.isActive) {
                                                          _timer.cancel();
                                                        }
                                                      });
                                                    } else {
                                                      await ViewDialogs
                                                              .commonDialogWithIcon(
                                                                  context:
                                                                      context,
                                                                  isAutoDismiss:
                                                                      true,
                                                                  autoDismiss:
                                                                      () {
                                                                    _timer = Timer(
                                                                        const Duration(
                                                                            seconds:
                                                                                2),
                                                                        () {
                                                                      Navigator.of(
                                                                              ctx)
                                                                          .pop();
                                                                    });
                                                                  },
                                                                  icon:
                                                                      const Icon(
                                                                    Icons
                                                                        .close_outlined,
                                                                    color: Colors
                                                                        .red,
                                                                    size: 100,
                                                                  ),
                                                                  text:
                                                                      "ข้ามกิจกรรมไม่สำเร็จ กรุณาลองใหม่อีกครั้ง")
                                                          .then((value) {
                                                        if (_timer.isActive) {
                                                          _timer.cancel();
                                                          Slidable.of(context)
                                                              ?.close();
                                                        }
                                                      });
                                                    }
                                                  }
                                                  Slidable.of(context)?.close();
                                                }))
                                      ],
                                    ),
                                    child: Builder(builder: (context) {
                                      SlidableController? _controller =
                                          Slidable.of(context);
                                      return ValueListenableBuilder<int>(
                                        valueListenable:
                                            _controller?.direction ??
                                                ValueNotifier<int>(0),
                                        builder: (context, value, _) {
                                          return GestureDetector(
                                            onHorizontalDragStart: (value) {
                                              if (!provider.activityList[index]
                                                      .required! &&
                                                  currentStatus !=
                                                      "skipped activity") {
                                                if (_isEnable) {
                                                  _controller
                                                      ?.openEndActionPane();
                                                }
                                              }
                                            },
                                            onTap: () {
                                              final model =
                                                  provider.activityList[index];
                                              if (currentStatus ==
                                                      "waiting for approve the activity" ||
                                                  currentStatus ==
                                                      "successful activity log") {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        ActivityPage(
                                                      isReadOnly: true,
                                                      farmItem: widget.farmItem,
                                                      projectId:
                                                          widget.projectId,
                                                      reqJoinProjectId: widget
                                                          .reqJoinProjectId,
                                                      isFerterlize: provider
                                                          .activityList[index]
                                                          .name!
                                                          .contains("ใส่ปุ๋ย"),
                                                      activityId: model.id!,
                                                      bannerStatus:
                                                          model.status ?? '',
                                                      bannerDate:
                                                          model.periodDate ??
                                                              "",
                                                      bannerIcon:
                                                          model.iconPath ?? '',
                                                      bannerTitle:
                                                          model.name ?? '',
                                                    ),
                                                  ),
                                                );
                                              }
                                              // else if (currentStatus ==
                                              //     "waiting for edit the activity") {
                                              //   Navigator.push(
                                              //     context,
                                              //     MaterialPageRoute(
                                              //       builder: (context) =>
                                              //           ActivityPage(
                                              //         isEdit: true,
                                              //         farmItem: widget.farmItem,
                                              //         projectId:
                                              //             widget.projectId,
                                              //         reqJoinProjectId: widget
                                              //             .reqJoinProjectId,
                                              //         isFerterlize: provider
                                              //             .activityList[index]
                                              //             .name!
                                              //             .contains("ใส่ปุ๋ย"),
                                              //         activityId: model.id!,
                                              //         bannerStatus:
                                              //             model.status ?? '',
                                              //         bannerDate:
                                              //             model.periodDate ??
                                              //                 "",
                                              //         bannerIcon:
                                              //             model.iconPath ?? '',
                                              //         bannerTitle:
                                              //             model.name ?? '',
                                              //       ),
                                              //     ),
                                              //   );
                                              // }
                                              else if (currentStatus !=
                                                  "waiting for the activity log") {
                                                null;
                                              }
                                              if (currentStatus ==
                                                  "waiting for the activity log") {
                                                if (_isEnable) {
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder:
                                                              (context) =>
                                                                  ActivityPage(
                                                                    farmItem: widget
                                                                        .farmItem,
                                                                    projectId:
                                                                        widget
                                                                            .projectId,
                                                                    reqJoinProjectId:
                                                                        widget
                                                                            .reqJoinProjectId,
                                                                    isFerterlize: provider
                                                                        .activityList[
                                                                            index]
                                                                        .name!
                                                                        .contains(
                                                                            "ใส่ปุ๋ย"),
                                                                    activityId: provider
                                                                        .activityList[
                                                                            index]
                                                                        .id!,
                                                                    bannerStatus:
                                                                        provider.activityList[index].status ??
                                                                            '',
                                                                    bannerDate:
                                                                        provider.activityList[index].periodDate ??
                                                                            '',
                                                                    bannerIcon:
                                                                        provider.activityList[index].iconPath ??
                                                                            '',
                                                                    bannerTitle:
                                                                        provider.activityList[index].name ??
                                                                            '',
                                                                  )));
                                                }
                                              }
                                            },
                                            child: Container(
                                              padding: const EdgeInsets.all(15),
                                              decoration: BoxDecoration(
                                                color: _isEnable
                                                    ? Colors.white
                                                    : const Color(0xffE5EAEF),
                                                borderRadius:
                                                    BorderRadius.circular(20),
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.15,
                                                    height: 50,
                                                    child: SvgPicture.network(
                                                      provider
                                                          .activityList[index]
                                                          .iconPath!,
                                                      color: _isEnable
                                                          ? Colors.black
                                                          : provider
                                                                      .activityList[
                                                                          index]
                                                                      .status! ==
                                                                  "successful activity log"
                                                              ? const Color(
                                                                  0xff0AC898)
                                                              : const Color(
                                                                  0xff787F88),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            provider
                                                                .activityList[
                                                                    index]
                                                                .name!,
                                                            style: TextStyle(
                                                                color: _isEnable
                                                                    ? Colors
                                                                        .black
                                                                    : const Color(
                                                                        0xff787F88),
                                                                fontSize: 16,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700),
                                                          ),
                                                          const SizedBox(
                                                            height: 5,
                                                          ),
                                                          provider
                                                                      .activityList[
                                                                          index]
                                                                      .periodDate ==
                                                                  null
                                                              ? const SizedBox(
                                                                  height: 5,
                                                                )
                                                              : Text(
                                                                  provider
                                                                      .activityList[
                                                                          index]
                                                                      .periodDate!,
                                                                  style: const TextStyle(
                                                                      color: Colors
                                                                          .black,
                                                                      fontSize:
                                                                          12,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w500),
                                                                ),
                                                          const SizedBox(
                                                            height: 5,
                                                          ),
                                                          statusWidget(
                                                              provider
                                                                  .activityList[
                                                                      index]
                                                                  .status!,
                                                              context),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  const SizedBox(
                                                    width: 10,
                                                  ),
                                                  Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.25,
                                                    height: 40,
                                                    child: activityBtn(
                                                        context,
                                                        provider
                                                            .activityList[index]
                                                            .status!,
                                                        provider
                                                            .activityList[index]
                                                            .name!
                                                            .contains(
                                                                "ใส่ปุ๋ย"),
                                                        provider
                                                            .activityList[index]
                                                            .id!,
                                                        _isEnable,
                                                        provider.activityList[
                                                            index]),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    }),
                                  )
                                ],
                              ));
                        }),
                  ),
                  SizedBox(height: MediaQuery.of(context).size.height * 0.12),
                ],
              ),
            )));
  }

  Widget statusWidget(String status, BuildContext context) {
    Color fontColor;
    Color backgroundColor;
    String statusText = "";
    var icon = Icons.create_outlined;

    switch (status) {
      case "waiting for the activity log":
        icon = Icons.create_outlined;
        statusText = "รอบันทึกกิจกรรม";
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);
        break;
      case "waiting for edit the activity":
        icon = Icons.create_outlined;
        statusText = "รอแก้ไขกิจกรรม";
        fontColor = const Color(0xffEF7E21);
        backgroundColor = const Color(0xffFCEADB);
        break;
      case "waiting for approve the activity":
        icon = Icons.schedule;
        statusText = "รอตรวจสอบ";
        fontColor = const Color(0xffF8C309);
        backgroundColor = const Color(0xffFDF6DD);
        break;
      case "successful activity log":
        icon = Icons.check_circle_outline;
        statusText = "สำเร็จ";
        fontColor = const Color(0xff0AC898);
        backgroundColor = const Color(0xffD8F6EF);
        break;
      case "activity rejected":
        statusText = "ไม่สำเร็จ";
        fontColor = const Color(0xffFF3E1D);
        backgroundColor = const Color(0xffFFE0DB);
        break;
      case "skipped activity":
        statusText = "กิจกรรมถูกข้าม";
        fontColor = const Color(0xff787F88);
        backgroundColor = const Color(0xffCBD5E0);
        break;

      default:
        icon = Icons.create_outlined;
        statusText = "รอบันทึกกิจกรรม";
        fontColor = const Color(0xff3273E4);
        backgroundColor = const Color(0xffE4ECFA);
        break;
    }

    return Container(
      width: MediaQuery.of(context).size.width * 0.35,
      padding: const EdgeInsets.fromLTRB(10, 4, 10, 4),
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Row(
        children: [
          SizedBox(
            width: 20,
            child: status == "activity rejected"
                ? SvgPicture.asset(
                    'assets/images/ic_failed_circle.svg',
                    width: 16,
                    height: 16,
                  )
                : Icon(
                    icon,
                    color: fontColor,
                    size: 20,
                  ),
          ),
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                statusText,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 13, color: fontColor),
              )
            ],
          )),
        ],
      ),
    );
  }

  Widget activityBtn(BuildContext context, String status, bool isFerterlize,
      String activityId, bool isEnable, ActivitySelectionModel model) {
    return status == "skipped activity"
        ? Container()
        : ElevatedButton(
            onPressed: () {
              if (status == "waiting for approve the activity" ||
                  status == "successful activity log") {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ActivityPage(
                      isReadOnly: true,
                      farmItem: widget.farmItem,
                      projectId: widget.projectId,
                      reqJoinProjectId: widget.reqJoinProjectId,
                      isFerterlize: isFerterlize,
                      activityId: activityId,
                      bannerStatus: model.status ?? '',
                      bannerDate: model.periodDate ?? "",
                      bannerIcon: model.iconPath ?? '',
                      bannerTitle: model.name ?? '',
                    ),
                  ),
                );
              }
              //else if (status == "waiting for edit the activity") {
              //   Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //       builder: (context) => ActivityPage(
              //         isEdit: true,
              //         farmItem: widget.farmItem,
              //         projectId: widget.projectId,
              //         reqJoinProjectId: widget.reqJoinProjectId,
              //         isFerterlize: isFerterlize,
              //         activityId: activityId,
              //         bannerStatus: model.status ?? '',
              //         bannerDate: model.periodDate ?? "",
              //         bannerIcon: model.iconPath ?? '',
              //         bannerTitle: model.name ?? '',
              //       ),
              //     ),
              //   );
              // }
              else if (status != "waiting for the activity log") {
                null;
              } else {
                if (isEnable) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ActivityPage(
                                farmItem: widget.farmItem,
                                projectId: widget.projectId,
                                reqJoinProjectId: widget.reqJoinProjectId,
                                isFerterlize: isFerterlize,
                                activityId: activityId,
                                bannerStatus: model.status ?? '',
                                bannerDate: model.periodDate ?? "",
                                bannerIcon: model.iconPath ?? '',
                                bannerTitle: model.name ?? '',
                              )));
                }
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "ทำกิจกรรม",
                  style: TextStyle(
                    color: isEnable ? Colors.white : const Color(0xff787F88),
                    fontWeight: FontWeight.w500,
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                )
              ],
            ),
            style: ElevatedButton.styleFrom(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.5),
                ),
                textStyle:
                    const TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                backgroundColor:
                    isEnable ? Colors.black : const Color(0xffCBD5E0),
                foregroundColor: Colors.white),
          );
  }
}
