import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/term_and_condition_response.dart';
import 'package:varun_kanna_app/view_models/term_and_condition_view_model.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';

class TermServicePolicyPage extends StatefulWidget {
  TermServicePolicyPage({Key? key}) : super(key: key);

  @override
  State<TermServicePolicyPage> createState() => _TermServicePolicyPageState();
}

class _TermServicePolicyPageState extends State<TermServicePolicyPage> {
  TextEditingController contentCtrl = TextEditingController();
  String? consentDeviceId;
  String? consentVersion;
  String title = "";
  String content = "";

  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    TermAndContionViewModel termAndContionViewModel =
        context.read<TermAndContionViewModel>();
    // TermAndContionViewModel termAndContionViewModel =
    //     Provider.of<TermAndContionViewModel>(context, listen: false);

    await termAndContionViewModel.getTermAndCondtionDeviceServive();

    TermAndConditionModel? termAndCondition =
        termAndContionViewModel.getTermAndCondtionDevice;

    setState(() {
      consentDeviceId = termAndCondition!.id;
      title = termAndCondition.title ?? "ข้อกำหนดการใช้งาน";
      content = termAndCondition.content ?? "";
      consentVersion = termAndCondition.version!;
      contentCtrl = TextEditingController(text: "${content}");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          AppBarText(
              fontSize: 17,
              textHeader: "ข้อกำหนดและนโยบายการให้บริการ",
              isback: true,
              onBackPress: () => Navigator.pop(context)),
          Expanded(
            flex: 1,
            child: SingleChildScrollView(
              physics: const ClampingScrollPhysics(),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.9,
                margin: const EdgeInsets.only(top: 30),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    HtmlWidget(
                      contentCtrl.text,
                      onErrorBuilder: (context, element, error) =>
                          Text('$element error: $error'),
                      // onLoadingBuilder: (context, element, loadingProgress) =>
                      //     const CircularProgressIndicator(),
                      textStyle: const TextStyle(fontSize: 14),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10),
                      child: _groupCheckBoxConsent(),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ));
  }

  Widget _groupCheckBoxConsent() {
    return Column(
      children: [
        const Divider(
          height: 20,
          thickness: 1,
        ),
        // use isReadAll case when scroll until buttom show checkbox
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Expanded(
              // add this
              child: Text(
                'ข้าพเจ้ายินยอมให้บริษัทฯ ประมวลผลข้อมูล ส่วนบุคคลของข้าพเจ้าเพื่อนำเสนอ เชิญชวน และส่งข่าวสารเกี่ยวกับโครงการให้แก่ข้าพเจ้า',
                maxLines: 99, // you can change it accordingly
                overflow: TextOverflow.ellipsis, // and this
              ),
            ),
          ],
        ),
        const Divider(
          height: 20,
          thickness: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Expanded(
              // add this
              child: Text(
                "ข้าพเจ้ายินยอมให้บริษัทฯ เปิดเผยข้อมูลส่วนบุคคล เกี่ยวกับชื่อ ข้อมูลติดต่อ ข้อมูลแปลงเพาะปลูก และข้อมูลที่ตั้งของข้าพเจ้าให้แก่บุคคลภายนอก เพื่อวัตถุประสงค์ทางด้านการตลาดของบุคคล ดังกล่าวในการนำเสนอสินค้าและบริการของบุคคล เหล่านั้นที่ท่านอาจสนใจ",
                maxLines: 99, // you can change it accordingly
                overflow: TextOverflow.ellipsis, // and this
              ),
            ),
          ],
        ),
        const Divider(
          height: 20,
          thickness: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Expanded(
              // add this
              child: Text(
                "ยอมรับว่าผู้ใช้มีอายุเกิน 20 ปี",
                maxLines: 99, // you can change it accordingly
                overflow: TextOverflow.ellipsis, // and this
              ),
            ),
          ],
        ),
      ],
    );
  }
}
