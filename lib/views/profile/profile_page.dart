import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/view_models/navigate_index_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/views/profile/contact_page.dart';
import 'package:varun_kanna_app/views/profile/edit_profile_page.dart';
import 'package:varun_kanna_app/views/profile/login_page.dart';
import 'package:varun_kanna_app/views/profile/terms_service_policy_page.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:auto_route/auto_route.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  SharedPreferences? prefs;

  String userId = "";
  String firstName = "";
  String lastName = "";
  String urlImg = "";

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      ProfileViewModel profileModel = context.read<ProfileViewModel>();
      prefs = await _prefs;
      userId = prefs!.getString('userId') ?? "";
      profileModel.setLogin(userId != "");
      String phoneNumber = prefs!.getString('phoneNumber').toString();
      await Provider.of<ProfileViewModel>(context, listen: false)
          .getProfileService(phoneNumber);
      await initData();
      await Provider.of<ProfileViewModel>(context, listen: false).getProvince();
    });
    super.initState();
  }

  Future<void> initData() async {
    ProfileViewModel profileModel = context.read<ProfileViewModel>();
    prefs = await _prefs;
    if (profileModel.getProfile != null) {
      prefs!.setString("urlImg", profileModel.getProfile!.url_img ?? "");
    }
  }

  void clearProvider() {
    ProfileViewModel profileViewModel =
        Provider.of<ProfileViewModel>(context, listen: false);
    profileViewModel.clearState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<ProfileViewModel>(builder: (contex, profileModel, _) {
      firstName = profileModel.getProfile?.firstName ?? firstName;
      urlImg = profileModel.getProfile?.url_img ?? urlImg;
      lastName = profileModel.getProfile?.lastName ?? lastName;
      return Column(
        children: [
          const AppBarText(
            textHeader: "โปรไฟล์",
            isback: false,
          ),
          profileModel.getLoading
              ? SizedBox(
                  height: MediaQuery.of(context).size.height * 0.75,
                  child: const Center(
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))),
                  ),
                )
              : Container(
                  width: MediaQuery.of(context).size.width * 0.9,
                  padding: const EdgeInsets.only(top: 20),
                  child: Column(
                    children: [
                      profileModel.getLogin
                          ? (GestureDetector(
                              onTap: () {
                                Navigator.of(context, rootNavigator: true).push(
                                  MaterialPageRoute(
                                    builder: (_) => const EditProfilePage(),
                                  ),
                                );
                              },
                              child: Container(
                                padding: const EdgeInsets.all(20),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(12.5),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.3),
                                      offset: const Offset(0, 1),
                                      blurRadius: 5,
                                      spreadRadius: 0,
                                    ),
                                  ],
                                ),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    (urlImg.isNotEmpty &&
                                            urlImg !=
                                                "https://s3.ap-southeast-1.amazonaws.com/" &&
                                            urlImg != "")
                                        ? ClipOval(
                                            child: Image.network(
                                              urlImg,
                                              width: 90,
                                              height: 90,
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        : Container(
                                            padding: EdgeInsets.all(20),
                                            decoration: BoxDecoration(
                                              color: const Color(0XFFE5EAEF),
                                              borderRadius:
                                                  BorderRadius.circular(50),
                                            ),
                                            child: SvgPicture.asset(
                                              "assets/images/ic_profile.svg",
                                              width: 30,
                                              height: 30,
                                              color: const Color(0xFFCBD5E0),
                                            ),
                                          ),
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            margin: EdgeInsets.only(top: 5),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.4,
                                            child: Column(
                                              children: [
                                                Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    firstName + " " + lastName,
                                                    style: const TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                  ),
                                                ),
                                                Align(
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  child: Text(
                                                    profileModel
                                                            .getProfile?.tel ??
                                                        "",
                                                    style: const TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.w500),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SvgPicture.asset(
                                            "assets/images/ic_edit_outline.svg",
                                            width: 26,
                                            height: 26,
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              )))
                          : (SizedBox(
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context, rootNavigator: true)
                                      .push(
                                    MaterialPageRoute(
                                      builder: (_) => LoginPage(),
                                    ),
                                  );
                                  // context.router.push(LoginRoute());
                                },
                                child: const Text("เข้าสู่ระบบ/สมัครสมาชิก"),
                                style: ElevatedButton.styleFrom(
                                    padding: const EdgeInsets.all(15),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(
                                          12.5), // <-- Radius
                                    ),
                                    textStyle: const TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                        fontFamily: 'Sarabun',
                                        height: 1.4),
                                    backgroundColor: Colors.black,
                                    foregroundColor: Colors.white),
                              ),
                            )),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12.5),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.3),
                              offset: const Offset(0, 1),
                              blurRadius: 5,
                              spreadRadius: 0,
                            ),
                          ],
                        ),
                        child: Column(
                          children: [
                            ButtonText(
                              btnText: "ขอความช่วยเหลือ",
                              onTap: () {
                                Navigator.of(context, rootNavigator: true).push(
                                  MaterialPageRoute(
                                    builder: (_) => ContactPage(),
                                  ),
                                );
                              },
                            ),
                            ButtonText(
                              btnText: "ข้อกำหนดและนโยบายการให้บริการ",
                              onTap: () {
                                Navigator.of(context, rootNavigator: true).push(
                                  MaterialPageRoute(
                                    builder: (_) => TermServicePolicyPage(),
                                  ),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      profileModel.getLogin
                          ? SizedBox(
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () async {
                                  final action =
                                      await ViewDialogs.ConfirmOrCancelDialog(
                                          context,
                                          "ท่านต้องการออกจากระบบใช่หรือไม่ ?",
                                          "",
                                          "ไม่",
                                          " ใช่",
                                          Colors.red);
                                  if (action == ViewDialogsAction.confirm) {
                                    //prefs!.clear();
                                    prefs!.setString('userId', "");
                                    prefs!.setString('phoneNumber', "");
                                    clearProvider();
                                    profileModel.setLogin(false);
                                  }
                                },
                                child: const Text("ออกจากระบบ"),
                                style: ElevatedButton.styleFrom(
                                    side: const BorderSide(color: Colors.black),
                                    padding: const EdgeInsets.all(15),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(12.5),
                                    ),
                                    textStyle: const TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 18,
                                        fontFamily: 'Sarabun',
                                        height: 1.4),
                                    backgroundColor: Colors.white,
                                    foregroundColor: Colors.black),
                              ),
                            )
                          : const SizedBox(),
                      profileModel.getLogin
                          ? const SizedBox(
                              height: 30,
                            )
                          : const SizedBox(),
                      profileModel.getLogin
                          ? Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const Text(
                                  "ต้องการลบบัญชีผู้ใช้งานกด",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                      color: Color(0xFF787F88)),
                                ),
                                const SizedBox(
                                  width: 5,
                                ),
                                TextButton(
                                  onPressed: () async {
                                    final action =
                                        await ViewDialogs.ConfirmOrCancelDialog(
                                            context,
                                            "ต้องการลบข้อมูลโปรไฟล์ใช่ไหม ?",
                                            "ระบบจะทำการส่งคำขอลบข้อมูล ภายใน 7 วัน หากท่านกดยืนยันแล้ว ข้อมูลของท่านจะถูกลบทั้งหมด และไม่สามารถกลับมาใช้ข้อมูลเดิมได้อีก หากกลับมาใช้งานอีกครั้ง ท่านจะเป็นผู้ใช้งานใหม่",
                                            "ยกเลิก",
                                            "ลบข้อมูล",
                                            Colors.red);
                                    if (action == ViewDialogsAction.confirm) {
                                      profileModel.setLogin(false);
                                      if (await profileModel
                                          .deleteProfileService(userId)) {
                                        prefs!.setString('userId', "");
                                        prefs!.setString('phoneNumber', "");
                                        prefs!.setString('Version', "");
                                        //prefs!.clear();
                                        profileModel.setLogin(false);
                                      }
                                    }
                                  },
                                  child: const Text("ลบโปรไฟล์",
                                      style: TextStyle(
                                          decoration: TextDecoration.underline,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black)),
                                ),
                              ],
                            )
                          : const SizedBox(),
                    ],
                  ),
                )
        ],
      );
    });
  }
}

class ButtonText extends StatelessWidget {
  final String btnText;
  final Function()? onTap;
  const ButtonText({Key? key, required this.btnText, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding:
            const EdgeInsets.only(top: 15, bottom: 15, left: 20, right: 20),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Text(
            btnText,
            style: const TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 18,
            ),
          ),
          const Icon(
            Icons.navigate_next,
            color: Colors.black,
          )
        ]),
      ),
    );
  }
}
