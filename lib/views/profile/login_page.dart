import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/view_models/otp_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/views/profile/login/login_with_phoneNumber.dart';
import 'package:varun_kanna_app/views/profile/login/term_and_condition_page.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/view_models/term_and_condition_view_model.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  bool isVaildPhoneNum = false;
  bool isShowing = false;
  bool isLoading = false;
  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<TermAndContionViewModel>(context, listen: false)
          .getTermAndCondtionInfo();
    });
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  void signInWithPhoneNumber(BuildContext context) async {
    setState(() {
      isLoading = true;
    });
    FocusScope.of(context).unfocus();
    TermAndContionViewModel termAndContionViewModel =
        Provider.of<TermAndContionViewModel>(context, listen: false);
    ProfileViewModel profileModel =
        Provider.of<ProfileViewModel>(context, listen: false);
    try {
      // if (isNotlogin) {
      //   Provider.of<TermAndContionViewModel>(context, listen: false)
      //       .getTermAndCondtionInfo();
      //   context.router.navigate(HomeRouter());
      //   Provider.of<NavigateIndexViewModel>(context, listen: false)
      //       .setActiveIndex(0);
      // } else

      final SharedPreferences prefs = await _prefs;

      LoginViewModel loginResponseModel =
          Provider.of<LoginViewModel>(context, listen: false);
      String? phoneNumber = loginResponseModel.getPhoneNumber();
      await profileModel.getProfileService(phoneNumber);

      await termAndContionViewModel.getCheckLastConsentService("application");

      int? consentsCurrentVersion = int.parse(
          termAndContionViewModel.getCheckLastConsentInfo()!.version!);

      prefs.setInt("version", consentsCurrentVersion);
      bool? isActiveUser;
// catch when หลังบ้าน return not found user ในระบบ คือไม่ี user ในระบบ
      try {
        isActiveUser = profileModel.getProfile!.id!.isNotEmpty;
      } catch (e) {
        isActiveUser = false;
      }

      loginResponseModel.setIsActive(isActiveUser);

      if (isActiveUser) {
        await termAndContionViewModel.getCheckUserConsentService(
            phoneNumber, "application");
        await loginResponseModel.loginWithPhoneNumService(phoneNumber);
        loginResponseModel
            .setRefCode(loginResponseModel.getOTPResponse!.data!.refCode ?? "");
//is redirect == appecpt last consent
        loginResponseModel.setIsRedirect(termAndContionViewModel
                .getCheckUserConsentInfo()!
                .isUserSignCurrentVersion ??
            false);
        // bool? isRedirect = termAndContionViewModel
        //         .getCheckUserConsentInfo()!
        //         .isUserSignCurrentVersion! &&
        //     profileModel.getProfile!.firstName!.isNotEmpty;

        // loginResponseModel.setIsRedirect(isRedirect);
        Navigator.of(context, rootNavigator: true).push(
          MaterialPageRoute(
            builder: (_) => const LoginWithPhoneNumberPage(),
          ),
        );
      } else {
        loginResponseModel.setIsRedirect(false);
        await loginResponseModel.registerService(phoneNumber);

        //new user new phone number
        loginResponseModel
            .setRefCode(loginResponseModel.getOTPResponse!.data!.refCode ?? "");
        loginResponseModel
            .setUserId(loginResponseModel.getOTPResponse!.data!.userId ?? "");
        //รอ หลังบ้านดีพลอย consentsCurrentVersion filed
        Navigator.of(context, rootNavigator: true).push(
          MaterialPageRoute(
            builder: (_) => const LoginWithPhoneNumberPage(),
          ),
        );
      }

      // else {
      //   Navigator.of(context, rootNavigator: true).push(
      //     MaterialPageRoute(builder: (_) => TermAndConditionPage()),
      //   );
      // }
    } on PlatformException catch (e) {
      print(e.stacktrace);
    }
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    TermAndContionViewModel termAndContionViewModel =
        Provider.of<TermAndContionViewModel>(context, listen: false);
    LoginViewModel loginResponseModel =
        Provider.of<LoginViewModel>(context, listen: false);
    LoginViewModel profileModel = context.watch<LoginViewModel>();
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
            resizeToAvoidBottomInset: true,
            body: SingleChildScrollView(
              reverse: true,
              controller: scrollController,
              physics: const NeverScrollableScrollPhysics(),
              child: Column(
                children: [
                  isLoading
                      ? SizedBox(
                          height: MediaQuery.of(context).size.height * 1,
                          child: const Center(
                            child: CircularProgressIndicator(
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Color(0xFF25C8A8))),
                          ),
                        )
                      : Column(
                          children: [
                            const LoginAppBar(),
                            Center(
                              child: SvgPicture.asset(
                                "assets/images/ic_login.svg",
                                height:
                                    MediaQuery.of(context).size.height * 0.25,
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.only(
                                    left: 30, right: 30, top: 20),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: const [
                                        Text(
                                          "เบอร์โทรศัพท์มือถือ",
                                          style: TextStyle(
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16,
                                            fontFamily: 'Sarabun',
                                          ),
                                        ),
                                      ],
                                    ),
                                    TextField(
                                      maxLength: 10,
                                      keyboardType:
                                          const TextInputType.numberWithOptions(
                                              decimal: true),
                                      inputFormatters: <TextInputFormatter>[
                                        FilteringTextInputFormatter.allow(
                                            RegExp('([0]+(\[0-9]+)?)')),
                                      ],
                                      onChanged: (value) => {
                                        if (value.length == 10)
                                          {
                                            setState(
                                                () => {isVaildPhoneNum = true})
                                          }
                                        else
                                          {
                                            setState(() => {
                                                  isVaildPhoneNum = false,
                                                  isShowing = true
                                                })
                                          },
                                        profileModel
                                            .setPhoneNumber(value.toString())
                                      },
                                      style: const TextStyle(
                                          height: 0.8, color: Colors.black),
                                      decoration: InputDecoration(
                                        counter: const Offstage(),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(12.5),
                                        ),
                                        hintText:
                                            'กรุณากรอกเบอร์โทรศัพท์มือถือ',
                                        fillColor: Colors.black87,
                                        focusedBorder: const OutlineInputBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12.5)),
                                          borderSide:
                                              BorderSide(color: Colors.black87),
                                        ),
                                      ),
                                    ),
                                    !isVaildPhoneNum && isShowing
                                        ? const Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              "เบอร์โทรศัพท์มือถือไม่ถูกต้อง",
                                              style: TextStyle(
                                                color: Colors.red,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14,
                                                fontFamily: 'Sarabun',
                                              ),
                                            ),
                                          )
                                        : const Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              "",
                                              style: TextStyle(
                                                color: Colors.red,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14,
                                                fontFamily: 'Sarabun',
                                              ),
                                            ),
                                          ),
                                    const SizedBox(
                                      height: 12,
                                    ),
                                    Container(
                                      child: const Divider(
                                        color: Color(0xffCBD5E0),
                                      ),
                                    ),
                                    // Row(children: <Widget>[
                                    //   Expanded(
                                    //     child: Container(
                                    //         margin: const EdgeInsets.only(
                                    //             left: 10.0, right: 20.0),
                                    //         child: const Divider(
                                    //           color: Color(0xffCBD5E0),
                                    //         )),
                                    //   ),
                                    //   const Text(
                                    //     "หรือ",
                                    //     style: TextStyle(
                                    //       fontWeight: FontWeight.w500,
                                    //       fontSize: 16,
                                    //       fontFamily: 'Sarabun',
                                    //     ),
                                    //   ),
                                    //   Expanded(
                                    //     child: Container(
                                    //         margin: const EdgeInsets.only(
                                    //             left: 20.0, right: 10.0),
                                    //         child: const Divider(
                                    //           color: Color(0xffCBD5E0),
                                    //         )),
                                    //   ),
                                    // ]),
                                    const SizedBox(
                                      height: 12,
                                    ),
                                    SizedBox(
                                      width: double.infinity,
                                      child: ElevatedButton(
                                        onPressed: () => {
                                          isVaildPhoneNum
                                              ? signInWithPhoneNumber(context)
                                              : "",
                                        },
                                        child: const Text("เข้าสู่ระบบ"),
                                        style: ElevatedButton.styleFrom(
                                            side: const BorderSide(
                                                color: Color(0xffCBD5E0)),
                                            padding: const EdgeInsets.all(15),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(12.5),
                                            ),
                                            textStyle: const TextStyle(
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18,
                                                fontFamily: 'Sarabun',
                                                height: 1.4),
                                            primary: isVaildPhoneNum
                                                ? const Color.fromARGB(
                                                    255, 0, 0, 0)
                                                : const Color(0xffE5EAEF),
                                            onPrimary: Colors.white),
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 100,
                                    ),
                                  ],
                                )),
                          ],
                        ),
                ],
              ),
              padding: const EdgeInsets.only(left: 0, right: 0, top: 0),
            )));
  }
}

class LoginAppBar extends StatelessWidget {
  const LoginAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
      height: MediaQuery.of(context).size.height * 0.13,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
              child: const Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 30,
              ),
              onPressed: () {
                Navigator.pop(context);
              }),
          Expanded(child: LayoutBuilder(builder: (context, constrait) {
            final padding =
                (MediaQuery.of(context).size.width - constrait.maxWidth);
            return Padding(
              padding: EdgeInsets.only(right: padding),
              child: Center(
                child: SvgPicture.asset(
                  "assets/images/ic_kanna_black.svg",
                  width: 30,
                  height: 30,
                ),
              ),
            );
          })),
        ],
      ),
    );
  }
}
