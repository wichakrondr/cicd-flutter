import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/profile_model.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/services/image_service.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:varun_kanna_app/widget/bottom_sheet_address.dart';
import 'package:varun_kanna_app/widget/date_picker_thai.dart';
import 'package:varun_kanna_app/widget/input_text.dart';
import 'package:image_picker/image_picker.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  TextEditingController firstNameCtrl = TextEditingController();
  TextEditingController lastNameCtrl = TextEditingController();
  TextEditingController emailCtrl = TextEditingController();
  TextEditingController mobileCtrl = TextEditingController();
  TextEditingController addressCtrl = TextEditingController();
  TextEditingController birthDateCtrl = TextEditingController();
  TextEditingController provinceCtrl = TextEditingController();
  TextEditingController districtCtrl = TextEditingController();
  TextEditingController subDistrictCtrl = TextEditingController();
  TextEditingController postCodeCtrl = TextEditingController();

  String? urlImg = "";
  Messages messages = Messages();
  File? image;
  late int counterText = 0;
  late DateTime tempPickedDate = DateTime(
      DateTime.now().year - 20, DateTime.now().month, DateTime.now().day);
  List provinceItem = [];

  Object? provinceValue = -1;
  Object? districtValue = -1;
  Object? subDistrictValue = -1;

  Object? selectedProvince;
  Object? selectedDistrict;
  Object? selectedSubdistrict;

  dynamic provinceList;
  dynamic districtList;
  dynamic subDistrictList;
  String subDistrictId = "";

  String userId = "";
  dynamic zipCodeTxt;

  bool disableDistrict = true;
  bool disableSubDistrict = true;
  bool disablePostCode = true;

  bool isValidEmail = true;
  bool isEditPoscode = false;

  String? birthDay = "";

  ScrollController scrollController = ScrollController();

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  SharedPreferences? prefs;
  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData() async {
    prefs = await _prefs;

    ProfileViewModel profileViewModel = context.read<ProfileViewModel>();
    String? prefsUrlImg = prefs!.getString("urlImg");
    userId = profileViewModel.getProfile!.id!;
    setState(() {
      urlImg = prefsUrlImg;
    });
    firstNameCtrl =
        TextEditingController(text: profileViewModel.getProfile?.firstName);
    lastNameCtrl =
        TextEditingController(text: profileViewModel.getProfile?.lastName);
    emailCtrl = TextEditingController(text: profileViewModel.getProfile?.email);
    mobileCtrl = TextEditingController(text: profileViewModel.getProfile?.tel);
    addressCtrl = TextEditingController(
        text: profileViewModel.getProfile?.address?.address);
    provinceCtrl = TextEditingController(
        text: profileViewModel.getProfile?.address?.province);
    districtCtrl = TextEditingController(
        text: profileViewModel.getProfile?.address?.district);
    subDistrictCtrl = TextEditingController(
        text: profileViewModel.getProfile?.address?.subdistrict);
    postCodeCtrl = TextEditingController(
        text: profileViewModel.getProfile?.address?.postcode);
    subDistrictId = postCodeCtrl.text;
    String birthDateString =
        profileViewModel.getProfile?.birthDay.toString() ?? "";
    convertLacaleDate(birthDateString);

    setState(() {
      counterText = addressCtrl.text.length;
      if (provinceCtrl.text.isNotEmpty) {
        String provinceId = findIdByName(provinceList, provinceCtrl.text);
        provinceValue = int.parse(provinceId);
      }
    });

    initDisableField(
        profileViewModel.getProfile?.address?.province ?? "",
        profileViewModel.getProfile?.address?.district ?? "",
        profileViewModel.getProfile?.address?.subdistrict ?? "",
        profileViewModel.getProfile?.address?.postcode ?? "");

    tempPickedDate = (birthDateString != "" &&
            birthDateString.isNotEmpty &&
            birthDateString != "null" &&
            birthDateString != "Invalid Date")
        ? DateTime.parse(profileViewModel.getProfile!.birthDay.toString())
        : tempPickedDate;
    birthDay = tempPickedDate.toString().substring(0, 10);
  }

  void initDisableField(
      String province, String district, String subDistrict, String postCode) {
    if (province.isNotEmpty) {
      setState(() {});
    }

    if (district.isNotEmpty) {
      setState(() {
        // disableDistrict = false;
      });
    }
    if (subDistrict.isNotEmpty) {
      setState(() {
        // disableSubDistrict = false;
      });
    }

    if (postCode.isNotEmpty) {
      setState(() {
        // disablePostCode = false;
      });
    }
  }

  @override
  void dispose() {
    firstNameCtrl.dispose();
    lastNameCtrl.dispose();
    emailCtrl.dispose();
    mobileCtrl.dispose();
    addressCtrl.dispose();
    birthDateCtrl.dispose();
    provinceCtrl.dispose();
    districtCtrl.dispose();
    subDistrictCtrl.dispose();
    postCodeCtrl.dispose();
    scrollController.dispose();
    super.dispose();
  }

  bool checkEmailChange(ProfileViewModel profileViewModel) {
    if (profileViewModel.getProfile?.email == null) {
      if (emailCtrl.text != "") {
        return true;
      } else {
        return false;
      }
    } else if (profileViewModel.getProfile?.email != emailCtrl.text) {
      return true;
    } else {
      return false;
    }
  }

  bool checkValueChange() {
    ProfileViewModel profileViewModel = context.read<ProfileViewModel>();
    // if (birthDateCtrl.text != "") {
    //   return true;
    // }
    if (checkEmailChange(profileViewModel)) {
      return true;
    }
// was edited profile
    if (profileViewModel.getProfile?.address?.address != null &&
        profileViewModel.getProfile?.address?.province != null &&
        profileViewModel.getProfile?.address?.district != null &&
        profileViewModel.getProfile?.address?.subdistrict != null &&
        profileViewModel.getProfile?.address?.postcode != null &&
        profileViewModel.getProfile?.birthDay != null) {
      if (firstNameCtrl.text != profileViewModel.getProfile?.firstName ||
          lastNameCtrl.text != profileViewModel.getProfile?.lastName ||
          mobileCtrl.text != profileViewModel.getProfile?.tel ||
          addressCtrl.text != profileViewModel.getProfile?.address?.address ||
          provinceCtrl.text != profileViewModel.getProfile?.address?.province ||
          districtCtrl.text != profileViewModel.getProfile?.address?.district ||
          subDistrictCtrl.text !=
              profileViewModel.getProfile?.address?.subdistrict ||
          postCodeCtrl.text != profileViewModel.getProfile?.address?.postcode ||
          birthDay != profileViewModel.getProfile?.birthDay) {
        return true;
      } else {
        return false;
      }
    }
// never edit profile
    else {
      if (firstNameCtrl.text != profileViewModel.getProfile?.firstName ||
          lastNameCtrl.text != profileViewModel.getProfile?.lastName ||
          emailCtrl.text != "" ||
          addressCtrl.text != "" ||
          provinceCtrl.text != "" ||
          districtCtrl.text != "" ||
          subDistrictCtrl.text != "" ||
          postCodeCtrl.text != "" ||
          birthDateCtrl.text != "") {
        return true;
      } else {
        return false;
      }
    }
  }

  bool validateEmailField() {
    if (emailCtrl.text.isNotEmpty) {
      setState(() {
        isValidEmail = RegExp(
                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
            .hasMatch(emailCtrl.text);
      });
      return isValidEmail;
    } else {
      setState(() {
        isValidEmail = true;
      });
      return true;
    }
  }

  bool validateField() {
    validateEmailField();
    bool isVaild = true;

    // firstNameCtrl.text.isEmpty;
    // lastNameCtrl.text.isEmpty;
    // addressCtrl.text.isEmpty;
    // districtCtrl.text.isEmpty;
    // subDistrictCtrl.text.isEmpty;
    // provinceCtrl.text.isEmpty;
    // postCodeCtrl.text.isEmpty;

    if (!isValidEmail ||
        firstNameCtrl.text.isEmpty ||
        lastNameCtrl.text.isEmpty) {
      isVaild = false;
      return isVaild;
    }

    if (firstNameCtrl.text.isEmpty) {
      jumpScroll(0.0);
    } else if (lastNameCtrl.text.isEmpty) {
      jumpScroll(0.2);
    } else if (!isValidEmail) {
      jumpScroll(0.3);
    }

    return isVaild;
  }

  void onSubmit(BuildContext context) async {
    ProfileViewModel profileViewModel = context.read<ProfileViewModel>();
    profileViewModel.setLoading(true);
    if (validateField()) {
      AddressModel addressModel = AddressModel(
          address: addressCtrl.text,
          district: districtCtrl.text,
          subdistrict: subDistrictCtrl.text,
          province: provinceCtrl.text,
          postcode: postCodeCtrl.text);

      ///Set Profile
      /// in case email is empty
      ProfileModel profileModel = ProfileModel();

      if (emailCtrl.text.isEmpty) {
        profileModel = ProfileModel(
            id: userId,
            firstName: firstNameCtrl.text,
            lastName: lastNameCtrl.text,
            birthDay: DateFormat('yyyy-MM-dd').format(tempPickedDate),
            tel: mobileCtrl.text,
            address: addressModel,
            province: provinceCtrl.text,
            district: districtCtrl.text,
            subDistrict: subDistrictCtrl.text,
            postCode: postCodeCtrl.text);
      } else {
        profileModel = ProfileModel(
            email: emailCtrl.text,
            id: userId,
            firstName: firstNameCtrl.text,
            lastName: lastNameCtrl.text,
            birthDay: DateFormat('yyyy-MM-dd').format(tempPickedDate),
            tel: mobileCtrl.text,
            address: addressModel,
            province: provinceCtrl.text,
            district: districtCtrl.text,
            subDistrict: subDistrictCtrl.text,
            postCode: postCodeCtrl.text);
      }
      context.read<ProfileViewModel>().setProfile(profileModel);
      await uploadImage(image, userId);

      bool updateAddresRes =
          await profileViewModel.updateAddressService(profileModel);
      if (updateAddresRes) {
        await ViewDialogs.successDialog(context, Messages.success, "");
// fix in case new user set new img profile not show when redirect to profile page
        String phoneNumber = prefs!.getString('phoneNumber').toString();
        await Provider.of<ProfileViewModel>(context, listen: false)
            .getProfileService(phoneNumber);
        ProfileViewModel profileModel = context.read<ProfileViewModel>();

        prefs!.setString("urlImg", profileModel.getProfile!.url_img ?? "");

        context.router.popForced();
      }
    }
  }

  TextEditingController convertLacaleDate(String date) {
    if (date == "" ||
        date.isEmpty ||
        date == "null" ||
        date == "Invalid Date") {
      return birthDateCtrl;
    }

    final dateSplitted = date.split('-');
    final months = [
      'มกราคม',
      'กุมภาพันธ์',
      'มีนาคม',
      'เมษายน',
      'พฤษภาคม',
      'มิถุนายน',
      'กรกฏาคม',
      'สิงหาคม',
      'กันยายน',
      'ตุลาคม',
      'พฤศจิกายน',
      'ธันวาคม'
    ];
    var year = (int.parse(dateSplitted[0]) + 543).toString();
    var month = months[int.parse(dateSplitted[1]) - 1].toString();
    var day = int.parse(dateSplitted[2]).toString();
    var resultText = day + " " + month + " " + year;
    birthDateCtrl.text = resultText.toString();
    return birthDateCtrl;
  }

  Future<ImageSource?> showImageSource(BuildContext context) async {
    if (Platform.isIOS) {
      return showCupertinoModalPopup<ImageSource>(
          context: context,
          builder: (context) => CupertinoActionSheet(
                actions: [
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.camera);
                        pickImage(ImageSource.camera);
                      },
                      child: const Text('ถ่ายรูป')),
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.gallery);
                        pickImage(ImageSource.gallery);
                      },
                      child: const Text('เลือกจากอัลบั้ม'))
                ],
              ));
    } else {
      return showModalBottomSheet(
          context: context,
          builder: (context) => Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                      leading: const Icon(Icons.camera_alt),
                      title: const Text('ถ่ายรูป'),
                      onTap: () async {
                        var status = await Permission.camera.status;

                        if (status.isDenied) {
                          Map<Permission, PermissionStatus> statuses = await [
                            Permission.camera,
                          ].request();
                          statuses.forEach((key, value) {
                            if (value.isGranted) {
                              Navigator.of(context).pop(ImageSource.camera);
                              pickImage(ImageSource.camera);
                            }
                          });
                        }
                        if (status.isGranted) {
                          Navigator.of(context).pop(ImageSource.camera);
                          pickImage(ImageSource.camera);
                        }
                      }),
                  ListTile(
                      leading: const Icon(Icons.image),
                      title: const Text('เลือกจากอัลบั้ม'),
                      onTap: () async {
                        var status = await Permission.storage.status;
                        if (status.isDenied) {
                          Map<Permission, PermissionStatus> statuses = await [
                            Permission.storage,
                          ].request();
                          statuses.forEach((key, value) {
                            if (value.isGranted) {
                              Navigator.of(context).pop(ImageSource.gallery);
                              pickImage(ImageSource.gallery);
                            }
                          });
                        }
                        if (status.isGranted) {
                          Navigator.of(context).pop(ImageSource.gallery);
                          pickImage(ImageSource.gallery);
                        }
                        // if (status.isRestricted) {
                        //   print("status.isRestricted ");
                        // }
                        // if (status.isLimited) {
                        //   print("status.isLimited ");
                        // }
                        // if (status.isPermanentlyDenied) {
                        //   print("status.isPermanentlyDenied ");
                        // }

                        // print(statuses[Permission.location]);
                      }),
                ],
              ));
    }
  }

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker()
          .pickImage(source: source, maxHeight: 360, maxWidth: 640);
      if (image == null) return;

      final imageTemporary = File(image.path);

      setState(() {
        this.image = imageTemporary;
      });
    } on PlatformException catch (e) {
      print('Failed to pick image : $e');
    }
  }

  dynamic castObjectToJson(dynamic object) {
    String jsonList = json.encode(object);
    dynamic list = json.decode(jsonList);
    return list;
  }

  Function filterById(dynamic data) {
    return (id) => id["id"] == int.parse(data.toString());
  }

  String findIdByName(dynamic list, String key) {
    final foundName =
        list.where((element) => element["name_th"] == key.toString());
    if (foundName.isNotEmpty) {
      return foundName.first["id"].toString();
    }
    return "";
  }

  void clearState(String field) {
    switch (field) {
      case "province":
        {
          setState(() {
            disableDistrict = false;
            disableSubDistrict = true;
            disablePostCode = true;
          });
          districtCtrl.clear();
          subDistrictCtrl.clear();
          postCodeCtrl.clear();
          subDistrictId = "";
        }
        break;

      case "district":
        {
          setState(() {
            disableSubDistrict = false;
            disablePostCode = true;
          });
          subDistrictCtrl.clear();
          postCodeCtrl.clear();
          subDistrictId = "";
        }

        break;
      case "subDistrict":
        {
          setState(() {
            disablePostCode = false;
          });
          postCodeCtrl.clear();
        }

        break;
      default:
        {}
        break;
    }
  }

  Widget _provinceBottom(ProfileViewModel profileViewModel) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.85,
        child: BottomSheetAddress(
            autoFocus: false,
            title: "เลือกจังหวัด",
            item: provinceList,
            groupValue: provinceValue,
            onClosePress: () {
              Navigator.of(context).pop();
            },
            valueChanged: (Object data) {
              provinceValue = data;

              provinceCtrl.text =
                  provinceList[int.parse(data.toString()) - 1]["name_th"];

              profileViewModel.getDistrict(int.parse(data.toString()));
              clearState("province");
              Navigator.of(context).pop();
            }));
  }

  Widget _districtBottom(ProfileViewModel profileViewModel) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.85,
        child: BottomSheetAddress(
            autoFocus: false,
            title: "เลือกอำเภอ/เขต",
            item: districtList,
            groupValue: districtValue,
            onClosePress: () {
              Navigator.of(context).pop();
            },
            valueChanged: (Object data) {
              districtValue = data;

              var selectedDisTctId =
                  districtList.where(filterById(data)).toList();

              districtCtrl.text = selectedDisTctId[0]["name_th"];

              profileViewModel.getSubDistrict(
                  int.parse(selectedDisTctId[0]["id"].toString()));
              clearState("district");
              Navigator.of(context).pop();
            }));
  }

  Widget _subDistrictBottom(ProfileViewModel profileViewModel) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.85,
        child: BottomSheetAddress(
            autoFocus: false,
            title: "เลือกตำบล/แขวง",
            item: subDistrictList,
            groupValue: subDistrictValue,
            onClosePress: () {
              Navigator.of(context).pop();
            },
            valueChanged: (Object data) {
              subDistrictValue = data;

              var selectedSubDisTctId =
                  subDistrictList.where(filterById(data)).toList();

              subDistrictCtrl.text = selectedSubDisTctId[0]["name_th"];

              setState(
                () {
                  subDistrictId = selectedSubDisTctId[0]["zip_code"].toString();
                  isEditPoscode = false;
                },
              );
              clearState("subDistrict");
              Navigator.of(context).pop();
            }));
  }

  void jumpScroll(double position) {
    scrollController.jumpTo(MediaQuery.of(context).size.height * position);
  }

  Future<bool> _onBackPressed() async {
    if (checkValueChange()) {
      final action = await ViewDialogs.ConfirmOrCancelDialog(
          context,
          "ท่านต้องการออกจาหน้านี้ใช่ไหม ?",
          "คุณมีการแก้ไขข้อมูล ต้องการออกโดยที่ไม่บันทึกใช่หรือไม่",
          "ยกเลิก",
          " ยืนยัน",
          Colors.black);
      if (action == ViewDialogsAction.confirm) {
        //prefs!.clear();
        Navigator.pop(context);
      }
    } else {
      Navigator.pop(context);
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    ProfileViewModel profileViewModel = context.watch<ProfileViewModel>();
    //validate text when keyboard appears

    if (MediaQuery.of(context).viewInsets.bottom > 0.0) {
      validateField();
    } else {
      validateField();
    }
    setState(() {
      provinceList = castObjectToJson(profileViewModel.getProvinceList);
      districtList = castObjectToJson(profileViewModel.getDistrictList);
      subDistrictList = castObjectToJson(profileViewModel.getSubDistrictList);
      if (!isEditPoscode) {
        postCodeCtrl.text = subDistrictId.toString();
      }

      scrollController =
          ScrollController(initialScrollOffset: profileViewModel.scrollOffset);
    });

    return WillPopScope(
        onWillPop: () => _onBackPressed(),
        child: GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              children: [
                AppBarText(
                  textHeader: "โปรไฟล์",
                  isback: true,
                  onBackPress: () async {
                    if (checkValueChange()) {
                      final action = await ViewDialogs.ConfirmOrCancelDialog(
                          context,
                          Messages.backPressConfirm,
                          Messages.noticeBackPress,
                          Messages.cancel,
                          Messages.ok,
                          Colors.black);
                      if (action == ViewDialogsAction.confirm) {
                        Navigator.pop(context);
                      }
                    } else {
                      Navigator.pop(context);
                    }
                  },
                ),
                profileViewModel.getLoading
                    ? SizedBox(
                        height: MediaQuery.of(context).size.height * 0.7,
                        child: const Center(
                          child: CircularProgressIndicator(
                              valueColor: AlwaysStoppedAnimation<Color>(
                                  Color(0xFF25C8A8))),
                        ),
                      )
                    : Expanded(
                        flex: 1,
                        child: SingleChildScrollView(
                          controller: scrollController,
                          physics: const ClampingScrollPhysics(),
                          child: Container(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, top: 10),
                            child: Column(
                              children: [
                                InkWell(
                                  onTap: () async {
                                    await showImageSource(context);
                                  },
                                  child: Stack(
                                    children: [
                                      //user choosing new img profile
                                      (image != null)
                                          ? ClipOval(
                                              child: Image.file(
                                                image!,
                                                width: 160,
                                                height: 160,
                                                fit: BoxFit.cover,
                                              ),
                                            )
                                          :
                                          //user have img profile
                                          (urlImg!.isNotEmpty &&
                                                  urlImg !=
                                                      "https://s3.ap-southeast-1.amazonaws.com/" &&
                                                  urlImg != "")
                                              ? ClipOval(
                                                  child: Image.network(
                                                    urlImg!,
                                                    width: 160,
                                                    height: 160,
                                                    fit: BoxFit.cover,
                                                  ),
                                                )
                                              : Container(
                                                  padding:
                                                      const EdgeInsets.all(45),
                                                  decoration: BoxDecoration(
                                                      color: const Color(
                                                          0XFFE5EAEF),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100),
                                                      border: Border.all(
                                                          color: const Color(
                                                              0XFFCBD5E0))),
                                                  child: SvgPicture.asset(
                                                    "assets/images/ic_profile.svg",
                                                    width: 60,
                                                    height: 60,
                                                    color:
                                                        const Color(0xFFCBD5E0),
                                                  )),
                                      Positioned(
                                          right: 5.0,
                                          bottom: 0.0,
                                          child: Container(
                                              padding: EdgeInsets.all(8),
                                              decoration: BoxDecoration(
                                                  color:
                                                      const Color(0XFFE5EAEF),
                                                  borderRadius:
                                                      BorderRadius.circular(50),
                                                  border: Border.all(
                                                      color: const Color(
                                                          0XFFCBD5E0))),
                                              child: Center(
                                                child: SvgPicture.asset(
                                                  "assets/images/ic_camera.svg",
                                                  width: 25,
                                                  height: 25,
                                                  color:
                                                      const Color(0xFFCBD5E0),
                                                ),
                                              ))),
                                    ],
                                  ),
                                ),
                                InputText(
                                    autoFocus: false,
                                    isRequire: true,
                                    title: "ชื่อจริง",
                                    controller: firstNameCtrl),
                                const SizedBox(
                                  height: 15,
                                ),
                                InputText(
                                    autoFocus: false,
                                    isRequire: true,
                                    title: "นามสกุล",
                                    controller: lastNameCtrl),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputText(
                                  autoFocus: false,
                                  title: "วัน เดือน ปีเกิด",
                                  controller: birthDateCtrl,
                                  readOnly: true,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  suffixIcon: const Icon(
                                    Icons.calendar_month_outlined,
                                    color: Colors.black,
                                  ),
                                  onTap: () {
                                    showModalBottomSheet(
                                        backgroundColor: Colors.transparent,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return DatePickerThai(
                                            initialDateTime: tempPickedDate,
                                            valueChanged: (DateTime dt) {
                                              tempPickedDate = dt;
                                              setState(() {
                                                birthDay = dt
                                                    .toString()
                                                    .substring(0, 10);
                                              });
                                              convertLacaleDate(
                                                  DateFormat('yyyy-MM-dd')
                                                      .format(dt));
                                            },
                                          );
                                        });
                                  },
                                  hintText: "1 มกราคม 2548",
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputText(
                                  autoFocus: false,
                                  title: "อีเมล์",
                                  isRequire: false,
                                  controller: emailCtrl,
                                  hintText: "email@kanna.co.th",
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Row(
                                    children: [
                                      if (!validateEmailField()) ...[
                                        SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.05,
                                        ),
                                        (const Text("กรุณาป้อนอีเมล์ให้ถูกต้อง",
                                            style: TextStyle(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.red)))
                                      ],
                                    ],
                                  ),
                                ),
                                InputText(
                                    autoFocus: false,
                                    isEnable: false,
                                    fillColor: ColorsUtils.disableColor
                                        .withOpacity(0.2),
                                    title: "เบอร์มือถือ",
                                    isRequire: false,
                                    controller: mobileCtrl),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputText(
                                    autoFocus: false,
                                    title: "ที่อยู่ปัจจุบัน",
                                    controller: addressCtrl,
                                    maxLines: 5,
                                    maxLength: 350,
                                    counterText:
                                        counterText.toString() + '/' + '350',
                                    hintText: "ที่อยู่เกษตรกร",
                                    onChanged: (String value) {
                                      setState(() {
                                        counterText = value.length;
                                      });
                                    }),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputText(
                                  autoFocus: false,
                                  title: "จังหวัด",
                                  controller: provinceCtrl,
                                  hintText: "เลือกจังหวัด",
                                  suffixIcon: const Icon(
                                    Icons.expand_more,
                                    color: Color(0XFFA0AEC0),
                                  ),
                                  readOnly: true,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  onTap: () {
                                    profileViewModel.setScrollOffset(
                                        scrollController.offset);
                                    showModalBottomSheet(
                                        isScrollControlled: true,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20),
                                        ),
                                        context: context,
                                        builder: (BuildContext context) {
                                          return _provinceBottom(
                                              profileViewModel);
                                        });
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputText(
                                  autoFocus: false,
                                  fillColor: disableDistrict
                                      ? ColorsUtils.disableColor
                                          .withOpacity(0.2)
                                      : null,
                                  title: "อำเภอ/เขต",
                                  controller: districtCtrl,
                                  hintText: "เลือกอำเภอ/เขต",
                                  suffixIcon: const Icon(
                                    Icons.expand_more,
                                    color: Color(0XFFA0AEC0),
                                  ),
                                  readOnly: true,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  onTap: () {
                                    disableDistrict
                                        ? null
                                        : {
                                            profileViewModel.setScrollOffset(
                                                scrollController.offset),
                                            showModalBottomSheet(
                                                isScrollControlled: true,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return _districtBottom(
                                                      profileViewModel);
                                                })
                                          };
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputText(
                                  autoFocus: false,
                                  fillColor: disableSubDistrict
                                      ? ColorsUtils.disableColor
                                          .withOpacity(0.2)
                                      : null,
                                  title: "ตำบล/แขวง",
                                  controller: subDistrictCtrl,
                                  hintText: "เลือกตำบล/แขวง",
                                  suffixIcon: const Icon(
                                    Icons.expand_more,
                                    color: Color(0XFFA0AEC0),
                                  ),
                                  readOnly: true,
                                  focusedBorderColor: const Color(0XFFEDF2F7),
                                  onTap: () {
                                    disableSubDistrict
                                        ? null
                                        : {
                                            profileViewModel.setScrollOffset(
                                                scrollController.offset),
                                            showModalBottomSheet(
                                                isScrollControlled: true,
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(20),
                                                ),
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return _subDistrictBottom(
                                                      profileViewModel);
                                                })
                                          };
                                  },
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                InputText(
                                    autoFocus: false,
                                    maxLength: 5,
                                    isEnable: disablePostCode ? false : true,
                                    fillColor: disablePostCode
                                        ? ColorsUtils.disableColor
                                            .withOpacity(0.2)
                                        : null,
                                    isNumber: true,
                                    title: "รหัสไปรษณีย์",
                                    controller: postCodeCtrl,
                                    hintText: "ใส่รหัสไปรษณีย์",
                                    onChanged: (String value) {
                                      setState(() {
                                        isEditPoscode = true;
                                      });
                                    }),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            ),
                          ),
                        ))
              ],
            ),
            bottomNavigationBar: profileViewModel.getLoading
                ? Container(
                    padding: const EdgeInsets.all(25.0),
                    child: TextButton(
                        style: TextButton.styleFrom(
                          foregroundColor: Colors.white,
                          textStyle: const TextStyle(fontSize: 20),
                          padding: const EdgeInsets.all(15.0),
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12.5))),
                        ),
                        onPressed: () {},
                        child: const Text(
                          "",
                          style: TextStyle(fontSize: 18),
                        )),
                  )
                : Container(
                    padding: const EdgeInsets.all(25.0),
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(32),
                            topLeft: Radius.circular(32)),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(0.15),
                              blurRadius: 10),
                        ],
                        color: Colors.white),
                    child: TextButton(
                        style: TextButton.styleFrom(
                          foregroundColor: Colors.white,
                          textStyle: const TextStyle(fontSize: 20),
                          backgroundColor: !validateField()
                              ? const Color(0XFFE5EAEF)
                              : Colors.black,
                          padding: const EdgeInsets.all(15.0),
                          side: const BorderSide(
                              color: Color(0XFFCBD5E0), width: 1),
                          shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(12.5))),
                        ),
                        onPressed: () {
                          if (validateField()) {
                            onSubmit(context);
                          }
                        },
                        child: const Text(
                          "บันทึก",
                          style: TextStyle(fontSize: 18),
                        ))),
          ),
        ));
  }
}
