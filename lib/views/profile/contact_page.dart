import 'package:flutter/material.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';

class ContactPage extends StatelessWidget {
  const ContactPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(color: Colors.white),
        child: Column(
          children: [
            AppBarText(
                textHeader: "ขอความช่วยเหลือ",
                isback: true,
                onBackPress: () => Navigator.pop(context)),
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              margin: const EdgeInsets.only(top: 30),
              child: Column(
                children: [
                  const Text(
                    "ขอความช่วยเหลือ",
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Linkify(
                      text:
                          "หากติดปัญหาในการใช้งาน สามารถส่งอีเมลล์มาที่\nvaruna.contact@varuna.co.th\nและสามารถติดต่อทาง facebook\nwww.facebook.com/varunatech\nหรือสามารถติดต่อตามที่อยู่Varuna (Thailand) Co., Ltd. 33, 31, 31/1 อาคารภิรัชทาวเวอร์ สาทร,ถนนสาทรใต้, ยานนาวา,สาทร กรุงเทพ 10120",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          height: 1.6)),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        "assets/images/ic_kanna_black.svg",
                        width: 40,
                        height: 40,
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      SvgPicture.asset(
                        "assets/images/ic_varuna.svg",
                        width: 40,
                        height: 40,
                      ),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
