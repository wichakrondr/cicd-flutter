import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/view_models/navigate_index_view_model.dart';
import 'package:varun_kanna_app/view_models/otp_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';

import 'package:varun_kanna_app/widget/app_bar_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter/gestures.dart';

import 'package:pinput/pinput.dart';
import 'package:varun_kanna_app/views/profile/login/term_and_condition_page.dart';

class LoginWithPhoneNumberPage extends StatefulWidget {
  const LoginWithPhoneNumberPage({Key? key}) : super(key: key);

  @override
  State<LoginWithPhoneNumberPage> createState() => _LoginWithPhoneNumberState();
}

class _LoginWithPhoneNumberState extends State<LoginWithPhoneNumberPage> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  String phoneNumber = "";
  String? refCode = "";
  bool isKeyboardVisible = false;
  bool pressAttention = false;
  bool validOtp = true;
  bool isExpireOTP = false;
  String otpCode = "";
  SharedPreferences? prefs;

  final enableTextInputList = <bool?>[true, false, false, false];
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    LoginViewModel loginResponseModel =
        Provider.of<LoginViewModel>(context, listen: false);
    phoneNumber = loginResponseModel.getPhoneNumber();
    refCode = loginResponseModel.getOTPResponse?.data?.refCode;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        reverse: true,
        controller: scrollController,
        physics: const NeverScrollableScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            loginResponseModel.getLoading
                ? SizedBox(
                    height: MediaQuery.of(context).size.height * 1,
                    child: const Center(
                      child: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))),
                    ),
                  )
                : InkWell(
                    onTap: () => {FocusScope.of(context).unfocus()},
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(17)),
                      child: Column(
                        children: [
                          const AppBarImage(
                            isback: false,
                          ),
                          Center(
                            child: SvgPicture.asset(
                              "assets/images/ic_login.svg",
                              height: MediaQuery.of(context).size.height * 0.25,
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 30,
                                  right: 30,
                                  top: 20,
                                  bottom: MediaQuery.of(context).size.height *
                                      0.03),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Text(
                                        "เราได้ส่งข้อความพร้อมรหัสยืนยันไปที่เบอร์",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          fontFamily: 'Sarabun',
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 12,
                                      ),
                                      Text(
                                        "${phoneNumber.substring(0, 3)}" +
                                            " xxx xx "
                                                "${phoneNumber.substring(8)}",
                                        style: const TextStyle(
                                          color: Colors.green,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          fontFamily: 'Sarabun',
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 12,
                                      ),
                                      const Text(
                                        "กรอกรหัส 4 ตัวที่คุณได้รับลงในช่องด้านล่าง",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16,
                                          fontFamily: 'Sarabun',
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 12,
                                      ),
                                      Text(
                                        "(รหัสอ้างอิง : ${refCode})",
                                        style: const TextStyle(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14,
                                          fontFamily: 'Sarabun',
                                        ),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  SizedBox(
                                    width: double.infinity,
                                    child: _otpTextField(),
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  SizedBox(
                                    width: double.infinity,
                                    child: ElevatedButton(
                                      onPressed: () => {
                                        pressAttention
                                            ? onSubmit()
                                            : setState(() => {})
                                      },
                                      child: const Text("ยืนยัน"),
                                      style: ElevatedButton.styleFrom(
                                          side: const BorderSide(
                                              color: Color(0xffCBD5E0)),
                                          padding: const EdgeInsets.all(15),
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(12.5),
                                          ),
                                          textStyle: const TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 18,
                                              fontFamily: 'Sarabun',
                                              height: 1.4),
                                          primary: pressAttention
                                              ? Colors.black
                                              : const Color(0xffE5EAEF),
                                          onPrimary: Colors.white),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 12,
                                  ),
                                  Center(
                                      child: RichText(
                                    text: TextSpan(
                                      text: 'ไม่ได้รับรหัส?กด ',
                                      style: const TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15,
                                      ),
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: 'ส่งใหม่อีกครั้ง ',
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () => resendOtp(),
                                            style: const TextStyle(
                                              color: Colors.black,
                                              decoration:
                                                  TextDecoration.underline,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16,
                                              fontFamily: 'Sarabun',
                                            )),
                                      ],
                                    ),
                                  )),
                                ],
                              )),
                        ],
                      ),
                    ),
                  )
          ],
        ),
      ),
    );
  }

  void resendOtp() async {
    LoginViewModel loginResponseModel =
        Provider.of<LoginViewModel>(context, listen: false);
    bool isActiveUser = loginResponseModel.getIsActive() ?? false;
    if (isActiveUser) {
      await loginResponseModel.loginWithPhoneNumService(phoneNumber);
    } else {
      await loginResponseModel
          .resentOTPService(loginResponseModel.getPhoneNumber());
    }
    setState(() => {});
  }

  void onSubmit() async {
    prefs = await _prefs;

    FocusScope.of(context).unfocus();
    LoginViewModel loginResponseModel =
        Provider.of<LoginViewModel>(context, listen: false);
    ProfileViewModel profileViewModel = context.read<ProfileViewModel>();
    bool? isRedirect = loginResponseModel.getIsRedirect() ?? false;
    bool? isActiveUser = loginResponseModel.getIsActive();
    String userId = loginResponseModel.getUserId();
    await loginResponseModel.verifyOTPService(
        loginResponseModel.getPhoneNumber(),
        otpCode,
        refCode ?? "",
        isActiveUser!);
    bool otpRes = loginResponseModel.getOTPResponse?.status ?? false;

    isExpireOTP = loginResponseModel.getOTPResponse!.data?.isExpire ?? false;

    if (otpRes) {
      ProfileViewModel profileModel =
          Provider.of<ProfileViewModel>(context, listen: false);
      await profileModel.getProfileService(phoneNumber);
      prefs!.setString('userId', profileModel.getProfile!.id!);

      await profileViewModel.getProfileService(phoneNumber);
      if (isRedirect) {
        prefs!.setString('phoneNumber', phoneNumber);

        context.router.popUntilRoot();
        context.router.push(MainRoute());
        Provider.of<NavigateIndexViewModel>(context, listen: false)
            .setActiveIndex(0);
        context.read<ProfileViewModel>().setLogin(true);
      } else {
        Navigator.of(context, rootNavigator: true).push(
          MaterialPageRoute(
            builder: (_) => const TermAndConditionPage(),
          ),
        );
      }
    } else if (isExpireOTP) {
      setState(() => isExpireOTP = true);
    } else {
      setState(() => validOtp = false);
    }
  }

  Widget _otpTextField() {
    double refCodeBoxW = 60;
    double refCodeBoxH = 80;

    final defaultPinTheme = PinTheme(
      width: refCodeBoxW,
      height: refCodeBoxH,
      textStyle: const TextStyle(
          fontSize: 20,
          color: const Color.fromARGB(255, 14, 15, 15),
          fontWeight: FontWeight.w600),
      decoration: BoxDecoration(
        border: Border.all(
            color: validOtp
                ? const Color.fromARGB(255, 173, 149, 149)
                : Colors.red),
        borderRadius: BorderRadius.circular(10),
      ),
    );

    final focusedPinTheme = defaultPinTheme.copyDecorationWith(
      border: Border.all(color: const Color.fromARGB(255, 30, 30, 31)),
      borderRadius: BorderRadius.circular(8),
    );

    final submittedPinTheme = defaultPinTheme.copyWith(
      decoration: defaultPinTheme.decoration?.copyWith(
        color: const Color.fromRGBO(234, 239, 243, 1),
      ),
    );

    ///Lib pinput for otp text field

    return Container(
        child: Column(
      children: [
        Pinput(
          inputFormatters: [
            FilteringTextInputFormatter.allow(RegExp('((\[0-9]+)?)')),
          ],
          defaultPinTheme: defaultPinTheme,
          focusedPinTheme: focusedPinTheme,
          submittedPinTheme: submittedPinTheme,
          pinputAutovalidateMode: PinputAutovalidateMode.onSubmit,
          showCursor: true,
          onChanged: ((value) => {
                if (value.runes.length == 4)
                  {setState(() => pressAttention = true)}
                else
                  {
                    setState(() => {pressAttention = false, validOtp = true})
                  }
              }),
          onCompleted: (pin) => {
            print("pin " + pin),
            setState(() => {otpCode = pin})
          },
        ),
        !validOtp
            ? Container(
                child: const Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "รหัส OTP ไม่ถูกต้อง",
                    style: TextStyle(
                      color: Colors.red,
                      fontWeight: FontWeight.w500,
                      fontSize: 14,
                      fontFamily: 'Sarabun',
                    ),
                  ),
                ),
                margin: const EdgeInsets.only(left: 35.0, top: 10),
              )
            : isExpireOTP
                ? Container(
                    child: const Align(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        "รหัส OTP หมดอายุ กรุณาส่งใหม่อีกครั้ง",
                        style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          fontFamily: 'Sarabun',
                        ),
                      ),
                    ),
                    margin: const EdgeInsets.only(left: 35.0, top: 10),
                  )
                : Container()
      ],
    ));
  }
}
