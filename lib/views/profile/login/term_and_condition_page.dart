import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/profile_model.dart';
import 'package:varun_kanna_app/model/term_and_condition_response.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/view_models/navigate_index_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'setup_profile_page.dart';
import 'package:varun_kanna_app/view_models/term_and_condition_view_model.dart';

import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart';

import 'package:provider/provider.dart';

import 'package:flutter/scheduler.dart';
import 'dart:math' as math;

enum ConsentType { isAgeConsent, isNewsConsent, isPersonalDataConsent }

class TermAndConditionPage extends StatefulWidget {
  const TermAndConditionPage({Key? key}) : super(key: key);

  @override
  State<TermAndConditionPage> createState() => _TermAndConditionState();
}

class _TermAndConditionState extends State<TermAndConditionPage> {
  TextEditingController titleCtrl = TextEditingController();
  TextEditingController contentCtrl = TextEditingController();
  ScrollController scrollController = ScrollController();

  bool buildCalledYet = false;

  bool isReadAll = false;
  bool isAgeConsent = false;
  bool isNewsConsent = false;
  bool isPersonalDataConsent = false;
  bool showbtn = false;
  String title = "";
  String content = "";
  String _consentId = "";

  bool isDownBtn = false;

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      // Provider.of<ProfileViewModel>(context, listen: false).getProfileService();
    });
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.extentAfter == 0) {
        setState(() {
          isReadAll = true;
        });
      }
    });

    initData();
  }

  void initData() {}

  @override
  void dispose() {
    titleCtrl.dispose();
    contentCtrl.dispose();
    scrollController.dispose();
    super.dispose();
  }

  Future<bool> _onWillPop() async {
    return false; //<-- SEE HERE
  }

  @override
  Widget build(BuildContext context) {
    if (!buildCalledYet) {
      buildCalledYet = true;
      SchedulerBinding.instance.addPostFrameCallback((_) async {
        final SharedPreferences prefs = await _prefs;
        TermAndContionViewModel termAndContionViewModel =
            Provider.of<TermAndContionViewModel>(context, listen: false);
        List<TermAndConditionModel>? termAndConditionList =
            termAndContionViewModel.getTermAndCondtion;
        int version = prefs.getInt("version")!;
        await termAndContionViewModel.getCheckLastConsentService("application");

        TermAndConditionModel? termAndCondition =
            termAndContionViewModel.getCheckLastConsentInfo();
        //term and

        setState(() {
          _consentId = termAndCondition!.id!;
          title = termAndCondition.title ?? "ข้อกำหนดการใช้งาน";
          content = termAndCondition.content ?? "";
          // isReadAll = !(scrollController.position.maxScrollExtent > 0);
        });
      });
    }

    titleCtrl = TextEditingController(text: title);
    contentCtrl = TextEditingController(text: "${content}");

    setupScrollListener(
      scrollController: scrollController,
      onAtTop: () {
        setState(() {
          isDownBtn = false;
        });
        print('At top');
      },
      onAtBottom: () {
        setState(() {
          isDownBtn = true;
          isReadAll = true;
        });
        print('At bottom');
      },
    );
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          body: Column(
            children: [
              AppBarText(
                textHeader: titleCtrl.text,
                isback: false,
              ),
              Padding(
                  padding: const EdgeInsets.only(left: 30, right: 30, top: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.7,
                        width: double.infinity,
                        child: SingleChildScrollView(
                            controller: scrollController,
                            physics: const BouncingScrollPhysics(),
                            child: Column(
                              children: [
                                HtmlWidget(
                                  // the first parameter (`html`) is required
                                  contentCtrl.text,

                                  onErrorBuilder: (context, element, error) =>
                                      Text('$element error: $error'),
                                  // onLoadingBuilder:
                                  //     (context, element, loadingProgress) =>
                                  //         const CircularProgressIndicator(),

                                  textStyle: const TextStyle(fontSize: 14),
                                ),
                                isReadAll ? _groupCheckBoxConsent() : SizedBox()
                              ],
                            )),
                      ),
                    ],
                  )),
            ],
          ),
          floatingActionButton: Padding(
            padding: const EdgeInsets.only(bottom: 0),
            child: _navigateBtn(),
          ),
          bottomNavigationBar: Container(
            padding: const EdgeInsets.all(15.0),
            decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(32),
                    topLeft: Radius.circular(32)),
                boxShadow: [
                  BoxShadow(
                      color: Colors.black.withOpacity(0.15), blurRadius: 10),
                ],
                color: Colors.white),
            child: TextButton(
                style: TextButton.styleFrom(
                  backgroundColor:
                      isAgeConsent && isNewsConsent && isPersonalDataConsent
                          ? const Color.fromARGB(255, 0, 0, 0)
                          : ColorsUtils.disableColor,
                  primary: Colors.white,
                  textStyle: const TextStyle(fontSize: 20),
                  padding: const EdgeInsets.all(15.0),
                  side: const BorderSide(color: Color(0XFFCBD5E0), width: 1),
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(12.5))),
                ),
                onPressed: () => {handleOnsubmit()},
                child: const Text(
                  "ยอมรับทั้งหมด",
                  style: TextStyle(fontSize: 18),
                )),
          )),
    );
  }

  Widget _groupCheckBoxConsent() {
    return Column(
      children: [
        const Divider(
          height: 20,
          thickness: 1,
        ),
        // use isReadAll case when scroll until buttom show checkbox
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Checkbox(
              activeColor: const Color(0xff0AC898),
              value: !isReadAll ? false : isNewsConsent,
              onChanged: (value) {
                handleCheckBox(value!, ConsentType.isNewsConsent);
              },
            ),
            const Expanded(
              // add this
              child: Text(
                'ข้าพเจ้ายินยอมให้บริษัทฯ ประมวลผลข้อมูล ส่วนบุคคลของข้าพเจ้าเพื่อนำเสนอ เชิญชวน และส่งข่าวสารเกี่ยวกับโครงการให้แก่ข้าพเจ้า',
                maxLines: 99, // you can change it accordingly
                overflow: TextOverflow.ellipsis, // and this
              ),
            ),
          ],
        ),
        const Divider(
          height: 20,
          thickness: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Checkbox(
              activeColor: const Color(0xff0AC898),
              value: !isReadAll ? false : isPersonalDataConsent,
              onChanged: (value) {
                handleCheckBox(value!, ConsentType.isPersonalDataConsent);
              },
            ),
            const Expanded(
              // add this
              child: Text(
                "ข้าพเจ้ายินยอมให้บริษัทฯ เปิดเผยข้อมูลส่วนบุคคล เกี่ยวกับชื่อ ข้อมูลติดต่อ ข้อมูลแปลงเพาะปลูก และข้อมูลที่ตั้งของข้าพเจ้าให้แก่บุคคลภายนอก เพื่อวัตถุประสงค์ทางด้านการตลาดของบุคคล ดังกล่าวในการนำเสนอสินค้าและบริการของบุคคล เหล่านั้นที่ท่านอาจสนใจ",
                maxLines: 99, // you can change it accordingly
                overflow: TextOverflow.ellipsis, // and this
              ),
            ),
          ],
        ),
        const Divider(
          height: 20,
          thickness: 1,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Checkbox(
              activeColor: const Color(0xff0AC898),
              value: !isReadAll ? false : isAgeConsent,
              onChanged: (value) {
                handleCheckBox(value!, ConsentType.isAgeConsent);
              },
            ),
            const Expanded(
              // add this
              child: Text(
                "ยอมรับว่าผู้ใช้มีอายุเกิน 20 ปี",
                maxLines: 99, // you can change it accordingly
                overflow: TextOverflow.ellipsis, // and this
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget _navigateBtn() {
    return FloatingActionButton.small(
        child: Transform.rotate(
            angle: isDownBtn ? (90 * math.pi / 180) : -90 * math.pi / 180,
            child: new Icon(
              size: 40,
              color: const Color.fromARGB(255, 241, 241, 241),
              Icons.navigate_before,
            )),
        backgroundColor: const Color.fromARGB(255, 0, 0, 0),
        onPressed: () {
          setState(() {
            isDownBtn = !isDownBtn;
            isDownBtn
                ? scrollController.animateTo(
                    scrollController.position.maxScrollExtent +
                        (isReadAll ? 0 : 350),
                    curve: Curves.easeOut,
                    duration: const Duration(milliseconds: 300))
                : scrollController.animateTo(0,
                    curve: Curves.easeOut,
                    duration: const Duration(milliseconds: 300));
          });
        });
  }

  void handleCheckBox(bool value, ConsentType type) {
    // scrollController.animateTo(
    //     //go to top of scroll
    //     2, //scroll offset to go
    //     duration: const Duration(milliseconds: 500), //duration of scroll
    //     curve: Curves.fastOutSlowIn //scroll type
    //     );

    setState(() {
      switch (type) {
        case ConsentType.isNewsConsent:
          isReadAll ? isNewsConsent = value : "";
          break;
        case ConsentType.isPersonalDataConsent:
          isReadAll ? isPersonalDataConsent = value : "";

          break;
        case ConsentType.isAgeConsent:
          isReadAll ? isAgeConsent = value : "";

          break;
        default:
      }
    });
  }

  void handleOnsubmit() async {
    final SharedPreferences prefs = await _prefs;

    ProfileViewModel profileModel =
        Provider.of<ProfileViewModel>(context, listen: false);

    if (isAgeConsent && isNewsConsent && isPersonalDataConsent) {
      if (profileModel.getProfile!.firstName != null) {
        ProfileModel profileModelReq = ProfileModel(
            tel: profileModel.getProfile!.tel!,
            firstName: profileModel.getProfile!.firstName,
            lastName: profileModel.getProfile!.lastName,
            isAcceptedLegalAge: true,
            consentId: _consentId);
        profileModel.saveProfileService(profileModelReq);
        profileModel.setLogin(true);
        context.read<ProfileViewModel>().setLogin(true);
        prefs.setString('phoneNumber', profileModel.getProfile!.tel!);
        Navigator.popUntil(
            context, ModalRoute.withName(Navigator.defaultRouteName));
        context.router.popUntilRoot();
        context.router.push(MainRoute());
        Provider.of<NavigateIndexViewModel>(context, listen: false)
            .setActiveIndex(0);
        context.read<ProfileViewModel>().setLogin(true);
      } else {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const SetupProfilePage(),
          ),
        );
      }
    } else {
      null;
    }
  }

  void setupScrollListener(
      {required ScrollController scrollController,
      Function? onAtTop,
      Function? onAtBottom}) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        // Reach the top of the list
        if (scrollController.position.pixels == 0) {
          onAtTop?.call();
        }

        // Reach the bottom of the list
        else {
          onAtBottom?.call();
        }
      }
    });
  }
}
