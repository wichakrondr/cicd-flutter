import 'dart:async';
import 'dart:io';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/profile_model.dart';
import 'package:varun_kanna_app/model/term_and_condition_response.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/services/image_service.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/view_models/navigate_index_view_model.dart';
import 'package:varun_kanna_app/view_models/otp_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/view_models/term_and_condition_view_model.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:varun_kanna_app/widget/input_text.dart';
import 'package:image_picker/image_picker.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

class SetupProfilePage extends StatefulWidget {
  const SetupProfilePage({Key? key}) : super(key: key);

  @override
  State<SetupProfilePage> createState() => _SetupProfilePageState();
}

class _SetupProfilePageState extends State<SetupProfilePage> {
  TextEditingController firstNameCtrl = TextEditingController();
  TextEditingController lastNameCtrl = TextEditingController();
  TextEditingController emailCtrl = TextEditingController();
  TextEditingController mobileCtrl = TextEditingController();
  TextEditingController addressCtrl = TextEditingController();
  TextEditingController birthDateCtrl = TextEditingController();
  TextEditingController provinceCtrl = TextEditingController();
  TextEditingController districtCtrl = TextEditingController();
  TextEditingController subDistrictCtrl = TextEditingController();
  TextEditingController postCodeCtrl = TextEditingController();

  bool isValidTextField = false;
  bool isLoading = false;

  File? image;
  late int counterText = 0;
  late DateTime tempPickedDate = DateTime(
      DateTime.now().year - 20, DateTime.now().month, DateTime.now().day);
  List provinceItem = [];

  ScrollController scrollController = ScrollController();
  Object? provinceValue = -1;
  Object? districtValue = -1;
  Object? subDistrictValue = -1;

  Object? selectedProvince;
  Object? selectedDistrict;
  Object? selectedSubdistrict;

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  var focusNode = FocusNode();

  @override
  void initState() {
    focusNode.addListener(() {
      print("focusNode.hasFocus " + focusNode.hasFocus.toString());
    });
    super.initState();
    initData();
  }

  void initData() {
    ProfileViewModel profileViewModel = context.read<ProfileViewModel>();
    firstNameCtrl =
        TextEditingController(text: profileViewModel.getProfile?.firstName);
    lastNameCtrl =
        TextEditingController(text: profileViewModel.getProfile?.lastName);
    emailCtrl = TextEditingController(text: profileViewModel.getProfile?.email);
    mobileCtrl = TextEditingController(text: profileViewModel.getProfile?.tel);
    addressCtrl = TextEditingController(
        text: profileViewModel.getProfile?.address?.address);
    provinceCtrl =
        TextEditingController(text: profileViewModel.getProfile?.province);
    districtCtrl =
        TextEditingController(text: profileViewModel.getProfile?.district);
    subDistrictCtrl =
        TextEditingController(text: profileViewModel.getProfile?.subDistrict);
    postCodeCtrl =
        TextEditingController(text: profileViewModel.getProfile?.postCode);

    // String birthDateString = profileViewModel.getProfile!.birthDate.toString();
    // convertLacaleDate(birthDateString);
    // tempPickedDate = birthDateString != ""
    //     ? DateTime.parse(profileViewModel.getProfile!.birthDate.toString())
    //     : tempPickedDate;
    setState(() {
      if (firstNameCtrl.text.isNotEmpty && lastNameCtrl.text.isNotEmpty) {
        isValidTextField = true;
      }
    });
  }

  @override
  void dispose() {
    firstNameCtrl.dispose();
    lastNameCtrl.dispose();
    emailCtrl.dispose();
    mobileCtrl.dispose();
    addressCtrl.dispose();
    birthDateCtrl.dispose();
    provinceCtrl.dispose();
    districtCtrl.dispose();
    subDistrictCtrl.dispose();
    postCodeCtrl.dispose();
    super.dispose();
  }

  void onSubmit(BuildContext context) async {
    setState(() {
      isLoading = true;
    });
    ProfileViewModel profileViewModel = context.read<ProfileViewModel>();

    final SharedPreferences prefs = await _prefs;
    int version = prefs.getInt("version")!;
    TermAndContionViewModel termAndContionViewModel =
        context.read<TermAndContionViewModel>();
    List<TermAndConditionModel>? termAndConditionList =
        termAndContionViewModel.getTermAndCondtion;
    TermAndConditionModel? termAndCondition;
    //term and
    termAndConditionList?.forEach((element) {
      if (element.version == version.toString() &&
          element.type == "application") {
        termAndCondition = element;
      }
    });

    LoginViewModel loginResponseModel =
        Provider.of<LoginViewModel>(context, listen: false);
    String? phoneNumber = loginResponseModel.getPhoneNumber();

    ProfileModel profileModel = ProfileModel(
        firstName: firstNameCtrl.text,
        lastName: lastNameCtrl.text,
        birthDay: DateFormat('yyyy-MM-dd').format(tempPickedDate),
        province: provinceCtrl.text,
        district: districtCtrl.text,
        subDistrict: subDistrictCtrl.text,
        postCode: postCodeCtrl.text,
        tel: phoneNumber,
        isAcceptedLegalAge: true,
        consentId: termAndCondition!.id);
    context.read<ProfileViewModel>().setProfile(profileModel);
    context.read<ProfileViewModel>().saveProfileService(profileModel);
    context.read<ProfileViewModel>().setLogin(true);
    await context.read<ProfileViewModel>().getProfileService(phoneNumber);
    String userId = loginResponseModel.getUserId();

    prefs.setString(
        'userId',
        userId == ""
            ? profileViewModel.getProfile!.id!.toString()
            : userId.toString());

    prefs.setString('phoneNumber', phoneNumber);
    setState(() {
      isLoading = false;
    });

    await ViewDialogs.commonDialogWithIcon(
        autoDismiss: () {
          late Timer _timer;
          _timer = Timer(const Duration(seconds: 1), () {
            Provider.of<NavigateIndexViewModel>(context, listen: false)
                .setActiveIndex(0);
            Navigator.popUntil(
                context, ModalRoute.withName(Navigator.defaultRouteName));
            context.router.popUntilRoot();
            context.router.push(MainRoute());
          });
        },
        isAutoDismiss: true,
        context: context,
        text: Messages.success);
  }

  TextEditingController convertLacaleDate(String date) {
    if (date == "") return birthDateCtrl;

    final dateSplitted = date.split('-');
    final months = [
      'มกราคม',
      'กุมภาพันธ์',
      'มีนาคม',
      'เมษายน',
      'พฤษภาคม',
      'มิถุนายน',
      'กรกฏาคม',
      'สิงหาคม',
      'กันยายน',
      'ตุลาคม',
      'พฤศจิกายน',
      'ธันวาคม'
    ];
    var year = (int.parse(dateSplitted[0]) + 543).toString();
    var month = months[int.parse(dateSplitted[1]) - 1].toString();
    var day = int.parse(dateSplitted[2]).toString();
    var resultText = day + " " + month + " " + year;
    birthDateCtrl.text = resultText.toString();
    return birthDateCtrl;
  }

  Future<ImageSource?> showImageSource(BuildContext context) async {
    if (Platform.isIOS) {
      return showCupertinoModalPopup<ImageSource>(
          context: context,
          builder: (context) => CupertinoActionSheet(
                actions: [
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.camera);
                        pickImage(ImageSource.camera);
                      },
                      child: const Text('ถ่ายรูป')),
                  CupertinoActionSheetAction(
                      onPressed: () {
                        Navigator.of(context).pop(ImageSource.gallery);
                        pickImage(ImageSource.gallery);
                      },
                      child: const Text('เลือกจากอัลบั้ม'))
                ],
              ));
    } else {
      return showModalBottomSheet(
          context: context,
          builder: (context) => Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  ListTile(
                      leading: const Icon(Icons.camera_alt),
                      title: const Text('ถ่ายรูป'),
                      onTap: () async {
                        var status = await Permission.camera.status;

                        if (status.isDenied) {
                          Map<Permission, PermissionStatus> statuses = await [
                            Permission.camera,
                          ].request();
                          statuses.forEach((key, value) {
                            if (value.isGranted) {
                              Navigator.of(context).pop(ImageSource.camera);
                              pickImage(ImageSource.camera);
                            }
                          });
                        }
                        if (status.isGranted) {
                          Navigator.of(context).pop(ImageSource.camera);
                          pickImage(ImageSource.camera);
                        }
                      }),
                  ListTile(
                      leading: const Icon(Icons.image),
                      title: const Text('เลือกจากอัลบั้ม'),
                      onTap: () async {
                        var status = await Permission.storage.status;
                        if (status.isDenied) {
                          Map<Permission, PermissionStatus> statuses = await [
                            Permission.storage,
                          ].request();
                          statuses.forEach((key, value) {
                            if (value.isGranted) {
                              Navigator.of(context).pop(ImageSource.gallery);
                              pickImage(ImageSource.gallery);
                            }
                          });
                        }
                        if (status.isGranted) {
                          Navigator.of(context).pop(ImageSource.gallery);
                          pickImage(ImageSource.gallery);
                        }
                        // if (status.isRestricted) {
                        //   print("status.isRestricted ");
                        // }
                        // if (status.isLimited) {
                        //   print("status.isLimited ");
                        // }
                        // if (status.isPermanentlyDenied) {
                        //   print("status.isPermanentlyDenied ");
                        // }

                        // print(statuses[Permission.location]);
                      }),
                ],
              ));
    }
  }

  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker()
          .pickImage(source: source, maxHeight: 360, maxWidth: 640);
      if (image == null) return;
      final imageTemporary = File(image.path);

      ProfileViewModel profileModel =
          Provider.of<ProfileViewModel>(context, listen: false);
      setState(() {
        this.image = imageTemporary;
      });
      await uploadImage(image, profileModel.getProfile!.id!);
    } on PlatformException catch (e) {
      print('Failed to pick image : $e');
    }
  }

  Future<bool> _onWillPop() async {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    ProfileViewModel profileViewModel = context.watch<ProfileViewModel>();
    if (WidgetsBinding.instance.window.viewInsets.bottom > 0.0) {
      // if (scrollController.hasClients) {
      //   scrollController.jumpTo(MediaQuery.of(context).size.height * 0.1);
      // }
    } else {
      if (scrollController.hasClients) {
        // scrollController.jumpTo(
        //   0,
        // );
      }
    }
    return GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: isLoading
            ? const Scaffold(
                backgroundColor: Colors.white,
                body: Center(
                  child: CircularProgressIndicator(
                      valueColor:
                          AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))),
                ))
            : GestureDetector(
                onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
                child: Scaffold(
                    backgroundColor: Colors.white,
                    resizeToAvoidBottomInset: true,
                    body: Column(
                      children: [
                        const AppBarText(
                          textHeader: "โปรไฟล์",
                          isback: false,
                        ),
                        Expanded(
                          flex: 1,
                          child: SingleChildScrollView(
                              controller: scrollController,
                              physics: const NeverScrollableScrollPhysics(),
                              child: Container(
                                padding: const EdgeInsets.only(
                                    left: 20, right: 20, top: 10, bottom: 100),
                                child: Column(
                                  children: [
                                    InkWell(
                                      onTap: () async {
                                        final source =
                                            await showImageSource(context);
                                      },
                                      child: Stack(
                                        children: [
                                          (image != null)
                                              ? ClipOval(
                                                  child: Image.file(
                                                    image!,
                                                    width: 160,
                                                    height: 160,
                                                    fit: BoxFit.cover,
                                                  ),
                                                )
                                              : Container(
                                                  padding: EdgeInsets.all(45),
                                                  decoration: BoxDecoration(
                                                      color: const Color(
                                                          0XFFE5EAEF),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              100),
                                                      border: Border.all(
                                                          color: Color(
                                                              0XFFCBD5E0))),
                                                  child: SvgPicture.asset(
                                                    "assets/images/ic_profile.svg",
                                                    width: 60,
                                                    height: 60,
                                                    color:
                                                        const Color(0xFFCBD5E0),
                                                  ),
                                                ),
                                          Positioned(
                                              right: 5.0,
                                              bottom: 0.0,
                                              child: Container(
                                                  padding: EdgeInsets.all(8),
                                                  decoration: BoxDecoration(
                                                      color: const Color(
                                                          0XFFE5EAEF),
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              50),
                                                      border: Border.all(
                                                          color: Color(
                                                              0XFFCBD5E0))),
                                                  child: Center(
                                                    child: SvgPicture.asset(
                                                      "assets/images/ic_camera.svg",
                                                      width: 25,
                                                      height: 25,
                                                      color: const Color(
                                                          0xFFCBD5E0),
                                                    ),
                                                  ))),
                                        ],
                                      ),
                                    ),
                                    InputText(
                                      autoFocus: false,
                                      onTap: () => scrollController.jumpTo(
                                          MediaQuery.of(context).size.height *
                                              0.0),
                                      hintText: "ชื่อ",
                                      title: "ชื่อจริง",
                                      isOnlyLetters: true,
                                      isRequire: true,
                                      controller: firstNameCtrl,
                                      onChanged: (value) => {
                                        if (value.isNotEmpty &&
                                            lastNameCtrl.text.isNotEmpty)
                                          {
                                            setState(
                                                () => {isValidTextField = true})
                                          }
                                        else
                                          {
                                            setState(() =>
                                                {isValidTextField = false})
                                          }
                                      },
                                    ),
                                    const SizedBox(
                                      height: 15,
                                    ),
                                    InputText(
                                      autoFocus: false,
                                      onTap: () => Screen.isLargeScreen
                                          ? scrollController.jumpTo(
                                              MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.02)
                                          : "",
                                      hintText: "นามสกุล",
                                      title: "นามสกุล",
                                      isOnlyLetters: true,
                                      isRequire: true,
                                      controller: lastNameCtrl,
                                      onChanged: (value) => {
                                        if (value.isNotEmpty &&
                                            firstNameCtrl.text.isNotEmpty)
                                          {
                                            setState(
                                                () => {isValidTextField = true})
                                          }
                                        else
                                          {
                                            setState(() =>
                                                {isValidTextField = false})
                                          }
                                      },
                                    ),
                                  ],
                                ),
                              )),
                        ),
                      ],
                    ),
                    bottomNavigationBar: Container(
                      padding: const EdgeInsets.all(15.0),
                      decoration: BoxDecoration(
                          borderRadius: const BorderRadius.only(
                              topRight: Radius.circular(32),
                              topLeft: Radius.circular(32)),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.15),
                                blurRadius: 10),
                          ],
                          color: Colors.white),
                      child: TextButton(
                          style: TextButton.styleFrom(
                            primary: Colors.white,
                            textStyle: const TextStyle(fontSize: 20),
                            backgroundColor: isValidTextField
                                ? Colors.black
                                : const Color(0XFFE5EAEF),
                            padding: const EdgeInsets.all(15.0),
                            side: const BorderSide(
                                color: Color(0XFFCBD5E0), width: 1),
                            shape: const RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(12.5))),
                          ),
                          onPressed: () {
                            if (isValidTextField) {
                              onSubmit(context);
                            } else
                              null;
                          },
                          child: const Text(
                            "บันทึก",
                            style: TextStyle(fontSize: 18),
                          )),
                    )),
              ));
  }
}
