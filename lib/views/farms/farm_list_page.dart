import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';
import 'package:varun_kanna_app/views/farms/draw_farm_page.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:varun_kanna_app/widget/input_text.dart';

class FarmListPage extends StatefulWidget {
  FarmListPage({Key? key}) : super(key: key);

  @override
  State<FarmListPage> createState() => _FarmListPageState();
}

class _FarmListPageState extends State<FarmListPage> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  TextEditingController searchCtrl = TextEditingController();

  @override
  void initState() {
    super.initState();
    getFarmList();
  }

  Future<void> getFarmList() async {
    final SharedPreferences prefs = await _prefs;
    DrawFarmViewModel drawFarmViewModel = context.read<DrawFarmViewModel>();
    drawFarmViewModel.clearFarmList();
    drawFarmViewModel.getFarm(prefs.getString('userId').toString());
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: Scaffold(
          body: Consumer<DrawFarmViewModel>(builder: (contex, provider, _) {
            return provider.getLoading
                ? const Center(
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))),
                  )
                : Container(
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    child: Column(
                      children: [
                        const AppBarText(
                          textHeader: "แปลงปลูก",
                          isback: false,
                        ),
                        Expanded(
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.9,
                            child: Column(
                              children: [
                                searchFarm(),
                                const SizedBox(
                                  height: 20,
                                ),
                                addFarmButton(provider),
                                const SizedBox(
                                  height: 20,
                                ),
                                provider.getFarmList.length == 0
                                    ? noFarm()
                                    : allFarm(provider, context)
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
          }),
        ),
      ),
    );
  }

  void goToDrawFram() {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => const DrawFarmPage()));
  }

  Widget noFarm() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      child: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          SvgPicture.asset('assets/images/ic_no_farm.svg')
        ],
      ),
    );
  }

  Future<bool> _onWillPop() async {
    return false;
  }

  Widget searchFarm() {
    return InputText(
      title: "",
      isRequire: false,
      controller: searchCtrl,
      hintText: "ค้นหา",
      prefixIcon: const Icon(
        Icons.search,
        color: Color(0XFFA0AEC0),
      ),
      onChanged: (String text) {},
    );
  }

  Widget addFarmButton(DrawFarmViewModel drawFarmProvider) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {
          drawFarmProvider.setEditMode(false);
          drawFarmProvider.farmNameCtrl.text = "";
          drawFarmProvider.clearAll();
          goToDrawFram();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Icon(Icons.add),
            SizedBox(
              width: 5,
            ),
            Text("เพิ่มแปลงปลูก")
          ],
        ),
        style: ElevatedButton.styleFrom(
            padding: const EdgeInsets.all(15),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12.5),
            ),
            textStyle: const TextStyle(
                fontWeight: FontWeight.w500, fontSize: 18, height: 1.4),
            backgroundColor: Colors.black,
            foregroundColor: Colors.white),
      ),
    );
  }

  Widget allFarm(DrawFarmViewModel provider, BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return Expanded(
      child: SingleChildScrollView(
        physics: const BouncingScrollPhysics(),
        child: Column(
          children: [
            ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: provider.getFarmList.length,
                padding: EdgeInsets.zero,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      provider.setEditMode(true);
                      provider.getFarmById(provider.getFarmList[index].id!,
                          "base", context, false);
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                          top: index == 0 ? 2 : 10,
                          left: 2,
                          right: 2,
                          bottom: 10),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black.withOpacity(0.3),
                            offset: const Offset(0, 1),
                            blurRadius: 5,
                            spreadRadius: 0,
                          ),
                        ],
                      ),
                      child: Row(
                        children: [
                          width > 800
                              ? const Expanded(child: SizedBox())
                              : const SizedBox(),
                          Expanded(
                            flex: width > 800 ? 2 : 1,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: width > 800 ? 0 : 15),
                                  child: Text(
                                      provider.getFarmList[index].farmName!,
                                      style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 18,
                                      )),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: width > 800 ? 0 : 15),
                                  child: Text(
                                      'พื้นที่ ${provider.getFarmList[index].displayArea}',
                                      style: const TextStyle(
                                        color: Colors.black,
                                        fontSize: 18,
                                      )),
                                ),
                              ],
                            ),
                          ),
                          // width > 800
                          //     ? const Expanded(child: SizedBox())
                          //     : const SizedBox(),
                          provider.getFarmList[index].farmImg! ==
                                  "https://s3.ap-southeast-1.amazonaws.com/"
                              ? const SizedBox()
                              : Container(
                                  // padding:
                                  //     const EdgeInsets.fromLTRB(5, 20, 0, 20),
                                  margin:
                                      const EdgeInsets.fromLTRB(20, 20, 25, 20),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20),
                                    child: SizedBox.fromSize(
                                      size: const Size.fromRadius(60),
                                      child: CachedNetworkImage(
                                          imageUrl: provider
                                              .getFarmList[index].farmImg!.path,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                                ),
                          width > 800
                              ? const Expanded(child: SizedBox())
                              : const SizedBox(),
                        ],
                      ),
                    ),
                  );
                }),
            SizedBox(height: MediaQuery.of(context).size.height * 0.12),
          ],
        ),
      ),
    );
  }
}
