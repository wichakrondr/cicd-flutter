import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/utils/geoUtility.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';
import 'package:varun_kanna_app/widget/copy_button.dart';
import 'package:varun_kanna_app/widget/input_text.dart';

class GeneralInformationPage extends StatelessWidget {
  const GeneralInformationPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<DrawFarmViewModel>(builder: (contex, prov, _) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "ข้อมูลพื้นฐาน",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                ),
                GestureDetector(
                  onTap: () {
                    prov.isEditPolygon ? null :
                    prov.toggleEditMode();
                  },
                  child: Container(
                      padding: const EdgeInsets.all(5),
                      decoration: const BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      child: SvgPicture.asset(
                        "assets/images/ic_edit_outline.svg",
                        width: 26,
                        height: 26,
                        color: Colors.white,
                      )),
                )
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            InputText(
              title: "ID แปลง",
              controller:
                  TextEditingController(text: prov.getFarmDetail.refCode!),
              readOnly: true,
              focusedBorderColor: const Color(0XFFEDF2F7),
            ),
            const SizedBox(
              height: 20,
            ),
            InputText(
              title: "ชื่อแปลง",
              isRequire: true,
              controller: prov.farmNameCtrl,
              onChanged: (String value) {
               prov.onFarmNameChange(value);
              },
              fillColor: prov.getEditMode
                  ? (prov.isEditing ? Colors.white : const Color(0XFFEDF2F7))
                  : Colors.white,
              focusedBorderColor: prov.getEditMode
                  ? const Color(0XFFEDF2F7)
                  : const Color(0xFF101010),
              isNumberAndLetters: true,
              readOnly: prov.getEditMode ? prov.isReadonly : false,
            ),
            const SizedBox(
              height: 20,
            ),
            Stack(
              alignment: Alignment.bottomRight,
              children: [
                InputText(
                  title: "ละติจูด ลองจิจูด",
                  controller: prov.getLatLngText(),
                  readOnly: true,
                  focusedBorderColor: const Color(0XFFEDF2F7),
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: CopyButton(copyText: prov.centroidCtrl.text))
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Stack(
              alignment: Alignment.bottomRight,
              children: [
                InputText(
                  title: "ค่าพิกัด",
                  controller: prov.getUtmText(),
                  readOnly: true,
                  focusedBorderColor: const Color(0XFFEDF2F7),
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: CopyButton(copyText: prov.utmCtrl.text))
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  child: Row(
                children: const [
                  Text("ขนาดพื้นที่",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                ],
              )),
            ),
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  child: Row(
                children: [
                  Text(
                      GeoUtility().getAreaInRaiFormat(
                          GeoUtility().calcArea(prov.tempLocation)),
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.w500)),
                ],
              )),
            ),
            SizedBox(
              height: prov.isEditing ? 20 : 40,
            ),
          ],
        );
      }),
    );
  }
}
