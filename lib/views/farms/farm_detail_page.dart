import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/utils/geoUtility.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';
import 'package:varun_kanna_app/views/farms/create_farm_detail_page.dart';
import 'package:varun_kanna_app/views/farms/draw_farm_page.dart';
import 'package:varun_kanna_app/views/farms/general_information_page.dart';
import 'package:varun_kanna_app/views/farms/land_recomendation_screen.dart';
import 'package:varun_kanna_app/widget/app_bar_text.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';

class FarmDetailPage extends StatelessWidget {
  FarmDetailPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Consumer<DrawFarmViewModel>(builder: (contex, provider, _) {
      return WillPopScope(
          onWillPop: _onWillPop,
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: provider.getLoading
                ? const Center(
                    child: CircularProgressIndicator(
                        valueColor:
                            AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))))
                : Stack(
                    children: [
                      Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                        ),
                        child: Column(
                          children: [
                            AppBarText(
                                textHeader: "แปลงปลูก",
                                isback: true,
                                onBackPress: () async {
                                  if (provider.getEditMode) {
                                    await provider.fetchAllFarm(
                                        provider, context);
                                    provider.onDispose();
                                  }
                                  provider.setFirstEdit();
                                  provider.clearFarmName();
                                  if (provider.getEditMode) {
                                    Navigator.of(context)
                                        .popUntil((route) => route.isFirst);
                                  } else {
                                    Navigator.of(context).pop();
                                  }
                                }),
                            const SizedBox(
                              height: 20,
                            ),
                            Expanded(
                                flex: 1,
                                child: SingleChildScrollView(
                                  physics: const ClampingScrollPhysics(),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.9,
                                    child: Column(
                                      children: [
                                        Stack(
                                          children: [
                                            Center(
                                                child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(32),
                                              child: Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.9,
                                                  height: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.25,
                                                  child: GoogleMap(
                                                      myLocationButtonEnabled:
                                                          false,
                                                      myLocationEnabled: false,
                                                      compassEnabled: false,
                                                      tiltGesturesEnabled:
                                                          false,
                                                      mapType:
                                                          MapType.satellite,
                                                      initialCameraPosition:
                                                          provider.cameraPos!,
                                                      onMapCreated: (controller) =>
                                                          provider.mapCreated(
                                                              controller,
                                                              provider
                                                                  .tempLocation),
                                                      mapToolbarEnabled: true,
                                                      polygons:
                                                          provider.polygone,
                                                      zoomControlsEnabled:
                                                          false,
                                                      zoomGesturesEnabled:
                                                          false,
                                                      scrollGesturesEnabled: false)),
                                            )),
                                            if (provider.getEditMode) ...[
                                              editPolygonButton(
                                                  provider, context),
                                            ]
                                          ],
                                        ),
                                        if (provider.getEditMode) ...[
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          tabBar(provider, context),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          provider.isGeneralInformation
                                              ? Column(
                                                  children: [
                                                    const GeneralInformationPage(),
                                                    provider.isEditing ||
                                                            provider
                                                                .displayBtnUpdatePolygon
                                                        ? deleteFarmButton(
                                                            provider, context)
                                                        : Container(),
                                                  ],
                                                )
                                              : LandRecommendationScreen()
                                        ] else ...[
                                          const CreateFarmDetailPage(),
                                        ]
                                      ],
                                    ),
                                  ),
                                )),
                          ],
                        ),
                      ),
                      provider.isEditing ||
                              provider.displayBtnUpdatePolygon ||
                              !provider.getEditMode
                          ? saveUpdateFarmButton(provider, context)
                          : Container()
                    ],
                  ),
          ));
    });
  }

  Future<bool> _onWillPop() async {
    return true;
  }

  Widget tabBar(DrawFarmViewModel provider, BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(left: 1, right: 1),
          width: MediaQuery.of(context).size.width * 0.89,
          height: MediaQuery.of(context).size.height * 0.09,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(21),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: const Offset(0, 1),
                blurRadius: 5,
                spreadRadius: 0,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  if (!provider.isGeneralInformation) {
                    provider.toggleTabBar();
                  }
                },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.44,
                  height: MediaQuery.of(context).size.height * 0.1,
                  decoration: BoxDecoration(
                      color: provider.isGeneralInformation
                          ? Colors.black
                          : Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Text(
                      "ข้อมูลพื้นฐาน",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w500,
                          color: !provider.isGeneralInformation
                              ? Colors.black
                              : Colors.white),
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  if (provider.isGeneralInformation) {
                    provider.toggleTabBar();
                  }
                },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.44,
                  height: MediaQuery.of(context).size.height * 0.1,
                  decoration: BoxDecoration(
                      color: !provider.isGeneralInformation
                          ? Colors.black
                          : Colors.white,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Text(
                      "คำแนะนำแปลง",
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: provider.isGeneralInformation
                            ? Colors.black
                            : Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget deleteFarmButton(DrawFarmViewModel provider, BuildContext context) {
    late Timer _timer;
    return Container(
      margin: const EdgeInsets.only(bottom: 90),
      child: TextButton(
        onPressed: () async {
          final action = await ViewDialogs.ConfirmOrCancelDialog(
            context,
            "ต้องการที่จะลบแปลงใช่ไหม ?",
            "หากคุณกดยืนยันการลบแปลง ข้อมูลต่างๆเกี่ยวกับแปลงปลูกที่คุณบันทึกไว้จะหายไปคุณยืนยันที่จะลบแปลงใช่ไหม ?",
            "ยกเลิก",
            "ยืนยัน",
            Colors.red,
          );

          if (action == ViewDialogsAction.confirm) {
            String message = await provider.onDeleteFarm();
            if (message == "Successfully.") {
              // case delete success
              await ViewDialogs.commonDialogWithIcon(
                      context: context,
                      isAutoDismiss: true,
                      autoDismiss: () {
                        _timer = Timer(const Duration(seconds: 2), () {
                          Navigator.of(context).pop();
                        });
                      },
                      icon: const Icon(
                        Icons.check_circle_outline,
                        color: Color(0xff0AC898),
                        size: 100,
                      ),
                      text: "ลบแปลงของท่านเรียบร้อยแล้ว")
                  .then((value) {
                if (_timer.isActive) {
                  _timer.cancel();
                }
              });
              await provider.fetchAllFarm(provider, context);
              Navigator.of(context).popUntil((route) => route.isFirst);
              provider.onDispose();
            } else if (message ==
                "Can not delete this farm because farm joining on project.") {
              await ViewDialogs.confirmDialog(
                  context: context,
                  title: "ไม่สามารถลบแปลงได้",
                  textButton: "ปิด",
                  text:
                      " เนื่องจากแปลงนี้ กำลังเข้าร่วมโครงการ\nหากต้องการลบแปลง ",
                  customText: const Text(
                    "กรุณาออกจากโครงการ",
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontSize: 16, height: 1.5, color: Colors.red),
                  )).then((value) {
                if (_timer.isActive) {
                  _timer.cancel();
                }
              });
            } else {
              await ViewDialogs.commonDialogWithIcon(
                      context: context,
                      isAutoDismiss: true,
                      autoDismiss: () {
                        _timer = Timer(const Duration(seconds: 2), () {
                          Navigator.of(context).pop();
                        });
                      },
                      icon: const Icon(
                        Icons.close_outlined,
                        color: Colors.red,
                        size: 100,
                      ),
                      text: "ลบแปลงไม่สำเร็จ กรุณาลองใหม่อีกครั้ง")
                  .then((value) {
                if (_timer.isActive) {
                  _timer.cancel();
                }
              });
            }
          }
        },
        child: const Text(
          "ลบแปลง",
          style: TextStyle(color: Colors.red, fontSize: 18),
        ),
      ),
    );
  }

  // save farm, edit polygone and edit farm name
  Widget saveUpdateFarmButton(
      DrawFarmViewModel provider, BuildContext context) {
    late Timer _timer;
    int countClickSave = 0;
    return !provider.isGeneralInformation
        ? Container()
        : Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              margin: const EdgeInsets.only(bottom: 25),
              child: ElevatedButton(
                onPressed: !provider.isValid
                    ? null
                    : () async {
                        if (countClickSave == 0) {
                          if (!provider.getEditMode) {
                            // create new farm
                            await provider.onSave(
                                provider.tempLocation, provider, context);
                          } else if (provider.isEditPolygon) {
                            // edit polygon
                            await provider.onSave(
                                provider.tempLocation, provider, context);
                            await ViewDialogs.commonDialogWithIcon(
                                    context: context,
                                    isAutoDismiss: true,
                                    autoDismiss: () {
                                      _timer =
                                          Timer(const Duration(seconds: 2), () {
                                        Navigator.of(context).pop();
                                      });
                                    },
                                    text: "บันทึกข้อมูลของท่านเรียบร้อยแล้ว")
                                .then((value) {
                              if (_timer.isActive) {
                                _timer.cancel();
                              }
                              provider.setDisplayBtnUpdatePolygon(false);
                            });
                          } else {
                            // edit farm name
                            await provider.editFarmName(
                                farmName: provider.farmName);
                            await ViewDialogs.commonDialogWithIcon(
                                    context: context,
                                    isAutoDismiss: true,
                                    autoDismiss: () {
                                      _timer =
                                          Timer(const Duration(seconds: 2), () {
                                        Navigator.of(context).pop();
                                      });
                                    },
                                    text: "บันทึกข้อมูลของท่านเรียบร้อยแล้ว")
                                .then((value) {
                              if (_timer.isActive) {
                                _timer.cancel();
                              }
                            });
                            provider.toggleEditMode();
                          }
                        }
                      },
                child: const Text("บันทึก"),
                style: ElevatedButton.styleFrom(
                  disabledBackgroundColor: const Color(0xffE5EAEF),
                  side: const BorderSide(color: Colors.black),
                  padding: const EdgeInsets.all(15),
                  backgroundColor: Colors.black,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  textStyle: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18,
                      fontFamily: 'Sarabun',
                      height: 1.4),
                ),
              ),
            ),
          );
  }

  Widget editPolygonButton(DrawFarmViewModel provider, BuildContext context) {
    return Align(
      alignment: Alignment.topRight,
      child: InkWell(
        onTap: () {
          if (provider.isEditing) {
            return null;
          }
          if (provider.isFirstEdit) {
            provider.resetCameraPosition();
            provider.setOnInitCamera(false);
            provider.setIsEditPolygon(true);
            provider.setDisplayBtnUpdatePolygon(false);
            Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => const DrawFarmPage()));
          } else {
            provider.setIsEditPolygon(true);
            provider.setDisplayBtnUpdatePolygon(false);
            Navigator.pop(context);
            LatLngBounds newPos =
                GeoUtility().createBounds(provider.tempLocation);

            if (newPos.northeast.toString().isNotEmpty &&
                newPos.southwest.toString().isNotEmpty) {
              Future.delayed(const Duration(milliseconds: 200), () {
                provider.controller!.animateCamera(
                  CameraUpdate.newLatLngBounds(newPos, 30),
                );
              });
            }
          }
        },
        child: Container(
            padding: const EdgeInsets.all(5),
            margin: const EdgeInsets.fromLTRB(0, 10, 10, 0),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(50))),
            child: SvgPicture.asset(
              "assets/images/ic_edit_outline.svg",
              width: 30,
              height: 30,
              color: Colors.black,
            )),
      ),
    );
  }
}
