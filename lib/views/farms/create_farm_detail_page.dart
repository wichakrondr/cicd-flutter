import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/utils/geoUtility.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';
import 'package:varun_kanna_app/widget/copy_button.dart';
import 'package:varun_kanna_app/widget/input_text.dart';

class CreateFarmDetailPage extends StatelessWidget {
  const CreateFarmDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Consumer<DrawFarmViewModel>(builder: (contex, prov, _) {
        return Column(
          children: [
            InputText(
                title: "ชื่อแปลง",
                isRequire: true,
                controller: prov.farmNameCtrl,
                onChanged: (String value) {
                  prov.onFarmNameChange(value);
                },
                fillColor: Colors.white,
                focusedBorderColor: const Color(0xFF101010),
                isNumberAndLetters: true),
            const SizedBox(
              height: 20,
            ),
            Stack(
              alignment: Alignment.bottomRight,
              children: [
                InputText(
                  title: "ละติจูด ลองจิจูด",
                  controller: prov.getLatLngText(),
                  readOnly: true,
                  focusedBorderColor: const Color(0XFFEDF2F7),
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: CopyButton(copyText: prov.centroidCtrl.text))
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Stack(
              alignment: Alignment.bottomRight,
              children: [
                InputText(
                  title: "ค่าพิกัด",
                  controller: prov.getUtmText(),
                  readOnly: true,
                  focusedBorderColor: const Color(0XFFEDF2F7),
                ),
                Align(
                    alignment: Alignment.bottomRight,
                    child: CopyButton(copyText: prov.utmCtrl.text))
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  child: Row(
                children: const [
                  Text("ขนาดพื้นที่",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w500)),
                ],
              )),
            ),
            const SizedBox(
              height: 20,
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                  child: Row(
                children: [
                  Text(
                      GeoUtility().getAreaInRaiFormat(
                          GeoUtility().calcArea(prov.tempLocation)),
                      style: const TextStyle(
                          fontSize: 16, fontWeight: FontWeight.w500)),
                ],
              )),
            ),
            const SizedBox(
              height: 100,
            ),
          ],
        );
      }),
    );
  }
}
