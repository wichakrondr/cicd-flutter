import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/farm_page_model/land_recomendation_model/land_recomendation_model.dart';
import 'package:varun_kanna_app/utils/constants.dart';
import 'package:varun_kanna_app/view_models/land_recomendation_view_model.dart';
import 'package:varun_kanna_app/widget/custom_dot_indicator.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:varun_kanna_app/widget/time_line_widget.dart';

class LandRecommendationScreen extends StatelessWidget {
  LandRecommendationScreen({super.key});
  TextEditingController dirtPackageController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    LandRecomnViewModel landRecomnViewModel =
        Provider.of<LandRecomnViewModel>(context, listen: false);
    return Column(
      children: [
        Container(
          alignment: Alignment.topLeft,
          child: const Text(
            "คำแนะนำสำหรับแปลงนี้",
            style: TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10, bottom: 15),
          child: Container(
            width: MediaQuery.of(context).size.width * 0.9,
            height: 60,
            child: Row(
              children: [
                Row(
                  children: [
                    const SizedBox(width: 20),
                    SvgPicture.asset('assets/images/ic_dirt_packages_name.svg'),
                    const SizedBox(width: 10),
                    const Text(
                      "ชื่อชุดดิน",
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                ),
                const SizedBox(width: 20),
                Expanded(
                  child: Text(
                    '${landRecomnViewModel.getLandRecomendationList![0].soil_series[0].soil_seriesname} (${landRecomnViewModel.getLandRecomendationList![0].soil_series[0].soil_soilseries})',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: Screen.isLargeScreen ? 18 : 14,
                        color: const Color(0xff0AC898)),
                  ),
                ),
              ],
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12.5),
              border: Border.all(
                color: const Color(0XFFEDF2F7),
              ),
            ),
          ),
        ),
        CarouselSlider(
          options: CarouselOptions(
            // autoPlayInterval: const Duration(seconds: 3),
            // autoPlay: true,
            enlargeCenterPage: true,
            onPageChanged: (index, reason) => {
              landRecomnViewModel.onPageChange(index),
            },
            aspectRatio: 2.1,
            initialPage: 0,
          ),
          items: [1, 2, 3, 4, 5, 6].map((i) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                    margin: const EdgeInsets.symmetric(horizontal: 1),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 0.15),
                      borderRadius: BorderRadius.circular(12.5),
                      color: Colors.white,
                    ),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: SvgPicture.network(
                              placeholderBuilder: (context) {
                            return SvgPicture.asset(
                                width: 100,
                                height: 50,
                                'assets/images/ic_rice.svg');
                          },
                              landRecomnViewModel.getLandRecomendationList![0]
                                  .crop_recommendation[i - 1].iconPath!,
                              height:
                                  MediaQuery.of(context).size.height * 0.07),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              landRecomnViewModel.getLandRecomendationList![0]
                                      .crop_recommendation[i - 1].name_th ??
                                  "ไม่มีข้อมูล",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: Screen.isLargeScreen ? 22 : 20),
                            ),
                            Text(
                              "พื้นที่นี้เหมาะสำหรับการปลูก",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal,
                                  fontSize: Screen.isLargeScreen ? 17 : 13),
                            ),
                            Container(
                              //width of the recommendation text
                              width: MediaQuery.of(context).size.width * 0.40,
                              alignment: Alignment.bottomCenter,
                              child: Text(
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                landRecomnViewModel
                                        .getLandRecomendationList![0]
                                        .crop_recommendation[i - 1]
                                        .suitable_status ??
                                    "ไม่มีข้อมูล",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: Screen.isLargeScreen ? 22 : 17),
                              ),
                            ),
                            TimeLinesWidget(
                              itemCount: 3,
                              processIndex: landRecomnViewModel
                                      .getLandRecomendationList![0]
                                      .crop_recommendation[i - 1]
                                      .suitable_point! -
                                  2,
                              boxHeight: 0.03,
                            ),
                          ],
                        )
                      ],
                    ));
              },
            );
          }).toList(),
        ),
        Padding(
          padding: const EdgeInsets.all(7),
          child: CustomCircleDotIndicators(
            dotSpacing: 12,
            selectedSize: 9,
            dotsSize: 9,
            itemCount: 6,
            index: landRecomnViewModel.index,
          ),
        )
      ],
    );
  }
}
