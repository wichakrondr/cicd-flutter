import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';

class SearchLocation extends StatefulWidget {
  SearchLocation({Key? key}) : super(key: key);

  @override
  State<SearchLocation> createState() => _SearchLocationState();
}

class _SearchLocationState extends State<SearchLocation> {
  @override
  Widget build(BuildContext context) {
    final drawFarmViewModel = Provider.of<DrawFarmViewModel>(context);

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.black,
        toolbarHeight: MediaQuery.of(context).size.height * 0.1,
        title: Container(
            width: double.infinity,
            height: 50,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(12.5)),
            child: Center(
              child: TextField(
                onChanged: (value) => drawFarmViewModel.searchPlaces(value),
                decoration: InputDecoration(
                    prefixIcon: IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: const Icon(
                        Icons.arrow_back,
                        color: Colors.black,
                      ),
                    ),
                    suffixIcon: const Icon(
                      Icons.search,
                      color: Colors.black,
                    ),
                    hintText: 'ค้นหาสถานที่ / ค้นหาพิกัด ',
                    hintStyle:
                        const TextStyle(color: Color(0xFFA0AEC0), fontSize: 16),
                    border: InputBorder.none),
              ),
            )),
        automaticallyImplyLeading: false,
      ),
      body: searchPlace(),
    );
  }

  Widget searchPlace() {
    return Builder(builder: (context) {
      return Consumer<DrawFarmViewModel>(builder: (context, mapProv, _) {
        return Container(
          decoration: const BoxDecoration(color: Colors.white),
          child: SafeArea(
              child: Stack(children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 5),
                child: ListView.builder(
                    itemCount: mapProv.getPlaceList.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5.0),
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).pop(
                                    mapProv.getPlaceList[index].description);
                                FocusManager.instance.primaryFocus?.unfocus();
                                mapProv.setSelectedLocation(
                                    mapProv.getPlaceList[index].placeId!);
                              },
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: SvgPicture.asset(
                                        "assets/images/ic_location.svg"),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Text(
                                      mapProv.getPlaceList[index].description!,
                                      style: const TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Divider(
                            height: 0,
                            thickness: 1,
                          ),
                        ],
                      );
                    }),
              ),
            )
          ])),
        );
      });
    });
  }
}
