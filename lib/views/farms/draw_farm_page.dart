import 'dart:io';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:proj4dart/proj4dart.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/farm_response.dart';
import 'package:varun_kanna_app/utils/geoUtility.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';
import 'package:varun_kanna_app/views/farms/farm_detail_page.dart';
import 'package:varun_kanna_app/views/farms/search_location_page.dart';
import 'package:varun_kanna_app/widget/bottom_sheet_map.dart';
import 'package:varun_kanna_app/widget/input_text.dart';

class DrawFarmPage extends StatefulWidget {
  final Color toolColor = Colors.black87;
  final Color polygonColor = const Color(0xFF2BAD8A);
  final IconData iconLocation = Icons.my_location;
  final IconData iconEditMode = Icons.edit_location;
  final IconData? iconCloseEdit = Icons.close;
  final IconData? iconDoneEdit = Icons.check;
  final IconData? iconUndoEdit = Icons.undo;
  final IconData? iconGPSPoint = Icons.add_location;

  const DrawFarmPage({Key? key}) : super(key: key);

  @override
  _DrawFarmPageState createState() => _DrawFarmPageState();
}

class _DrawFarmPageState extends State<DrawFarmPage> {
  TextEditingController xCtrl = TextEditingController();
  TextEditingController yCtrl = TextEditingController();
  TextEditingController latLngCtrl = TextEditingController();

  LatLng? currentLocation;
  bool isCameraMove = false;
  int topCenter = Platform.isIOS ? 10 : 5;
  Set<Polyline> polylines = new Set();
  bool selectLatLng = false;
  String zone = "";
  bool invalidLatLng = false;

  @override
  void dispose() {
    xCtrl.dispose();
    yCtrl.dispose();
    latLngCtrl.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Consumer<DrawFarmViewModel>(
        builder: (contex, mapProv, _) {
          //Get first location
          if (mapProv.cameraPosition == null && mapProv.onInitCamera == false) {
            mapProv.isEditPolygon
                ? mapProv.onEditPolygon(mapProv.getFarmDetail)
                : mapProv.initCamera();
            mapProv.setPolygonColor(widget.polygonColor);

            return const Center(
              child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xFF25C8A8))),
            );
          }

          double mapHeight = MediaQuery.of(context).size.height * 0.87;
          return Stack(
            alignment: Alignment.center,
            children: [
              Column(
                children: [
                  searchFarm(mapProv),
                  Center(
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned(top: -300, child: mapDistance()),
                        Positioned(top: -300, child: mapIcon()),
                        if (mapProv.cameraPosition != null &&
                            mapProv.getLoading == false) ...[
                          Positioned(top: -300, child: mapXY())
                        ],
                        mapProv.cameraPosition != null &&
                                mapProv.getLoading == false
                            ? Container(
                                width: MediaQuery.of(context).size.width,
                                height:
                                    MediaQuery.of(context).size.height * 0.87,
                                child: GoogleMap(
                                  myLocationButtonEnabled: false,
                                  myLocationEnabled: false,
                                  compassEnabled: true,
                                  tiltGesturesEnabled: false,
                                  markers: mapProv.markers,
                                  mapType: mapProv.mapType,
                                  initialCameraPosition:
                                      mapProv.cameraPosition!,
                                  onMapCreated: mapProv.onMapCreated,
                                  mapToolbarEnabled: true,
                                  polygons: mapProv.polygons,
                                  polylines: mapProv.polylines,
                                  onCameraMove: (position) {
                                    mapProv.onMoveMap(position.target);
                                    setState(() {
                                      currentLocation = position.target;
                                      isCameraMove = true;
                                    });
                                  },
                                ),
                              )
                            : Container(
                                width: MediaQuery.of(context).size.width,
                                height:
                                    MediaQuery.of(context).size.height * 0.87,
                                child: const Center(
                                  child: CircularProgressIndicator(
                                      valueColor: AlwaysStoppedAnimation<Color>(
                                          Color(0xFF25C8A8))),
                                )),
                        mapProv.cameraPosition != null
                            ? Positioned(
                                top: (mapHeight / 2) - 20,
                                right: (MediaQuery.of(context).size.width / 2) -
                                    20,
                                child: SvgPicture.asset(
                                  'assets/images/ic_compass.svg',
                                  width: 40,
                                  height: 40,
                                  color: mapProv.mapType.name == "satellite" ||
                                          mapProv.mapType.name == "hybrid"
                                      ? Colors.white
                                      : Colors.black,
                                ),
                              )
                            : const SizedBox(),
                      ],
                    ),
                  )
                ],
              ),
              if (mapProv.cameraPosition != null) ...[
                SafeArea(
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.topLeft,
                        child: Padding(
                          padding: EdgeInsets.only(right: 10, top: MediaQuery.of(context).size.height * 0.17),
                          child: Column(
                            children: [
                              const SizedBox(height: 5,),
                              mapType(context),
                              const SizedBox(height: 10,),
                              currentLoc(),
                              const SizedBox(height: 10,),
                              xyPin(context),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                totalArea(),
                toolsList()
              ]
            ],
          );
        },
      ),
    );
  }

  Widget searchFarm(DrawFarmViewModel provider) {
    double h = MediaQuery.of(context).size.height * 0.035;
    return Container(
      padding: EdgeInsets.only(top: h, left: 10, right: 10),
      height: MediaQuery.of(context).size.height * 0.13,
      color: Colors.black,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          IconButton(
            icon: const Icon(
              Icons.arrow_back,
              color: Colors.white,
              size: 30,
            ),
            onPressed: () {
              if (!provider.isEditPolygon) {
                provider.clearAll();
              } else if (provider.isEditPolygon &&
                  provider.tempLocation.length == 0) {
                provider.getFarmById(
                    provider.getFarmDetail.id!, "base", context, true);
              } else if (provider.isEditPolygon) {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => FarmDetailPage()),
                );
              } else {
                Navigator.of(context).pop();
              }
              provider.setSearchResult('');
              provider.setFirstEdit();
              if (!provider.getEditMode) {
                Navigator.of(context).pop();
              }
            },
          ),
          Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: 50,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12.5)),
              child: Center(
                child: TextField(
                  showCursor: false,
                  keyboardType: TextInputType.none,
                  controller: provider.searchResultsController,
                  onTap: () =>
                      provider.getLoading ? null : selectSearchType(provider),
                  onChanged: (value) => provider.searchPlaces(value),
                  decoration: const InputDecoration(
                      prefixIcon: Icon(
                        Icons.search,
                        color: Color(0XFFA0AEC0),
                      ),
                      hintText: 'ค้นหา / ค้นหาพิกัด / พิกัดมุมแปลง',
                      hintStyle:
                          TextStyle(color: Color(0xFFA0AEC0), fontSize: 16),
                      border: InputBorder.none),
                ),
              ))
        ],
      ),
    );
  }

  ///Widget for custom marker
  Widget mapIcon() {
    return Consumer<DrawFarmViewModel>(
      builder: (context, mapProv, _) {
        return RepaintBoundary(
          key: mapProv.markerKey,
          child: Container(
            width: 36,
            height: 36,
            decoration: BoxDecoration(
              color: Colors.black,
              shape: BoxShape.circle,
              border: Border.all(
                color: const Color(0xFF2BAD8A),
                width: 3.0,
                style: BorderStyle.solid,
              ),
            ),
            child: Center(
              child: Text(
                mapProv.isEditPolygon
                    ? (mapProv.polygonId).toString()
                    : (mapProv.tempLocation.length + 1).toString(),
                style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                    fontSize: 16),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget mapDistance() {
    return Consumer<DrawFarmViewModel>(
      builder: (context, mapProv, _) {
        return RepaintBoundary(
          key: mapProv.distanceKey,
          child: Container(
            width: mapProv.distance.length > 6
                ? (mapProv.distance.length >= 9 ? 100 : 80)
                : 64,
            height: 32,
            decoration: BoxDecoration(
                color: widget.toolColor.withOpacity(0.5),
                borderRadius: BorderRadius.circular(10)),
            child: Center(
              child: Text(mapProv.distance,
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      color: Colors.white)),
            ),
          ),
        );
      },
    );
  }

  Widget mapXY() {
    return Consumer<DrawFarmViewModel>(
      builder: (context, mapProv, _) {
        LatLng point =
            isCameraMove ? currentLocation! : mapProv.sourceLocation!;
        UtmModel utm = GeoUtility().getUtm(point);
        return RepaintBoundary(
            key: mapProv.pinKey,
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    color: widget.toolColor,
                    border:
                        Border.all(color: const Color(0xFF2BAD8A), width: 2),
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  child: Center(
                    child: Text(
                        '${utm.zone}, X ${utm.x.toStringAsFixed(2)}, Y ${utm.y.toStringAsFixed(2)}',
                        style: const TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.normal,
                            color: Colors.white)),
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Center(
                  child: SvgPicture.asset(
                    'assets/images/ic_pin_xy.svg',
                    width: 60,
                    height: 60,
                  ),
                )
              ],
            ));
      },
    );
  }

  Future<void> selectSearchType(DrawFarmViewModel farmProv) {
    return showDialog(
        context: context,
        barrierColor: Colors.transparent,
        builder: (BuildContext context) {
          return Align(
            alignment: const Alignment(0, -0.7),
            child: Material(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0)),
              child: Padding(
                  padding: const EdgeInsets.all(32.0),
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.5,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        TextButton(
                          style: TextButton.styleFrom(
                            textStyle: Theme.of(context).textTheme.labelMedium,
                          ),
                          child: const Text('ค้นหาจากสถานที่',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                              )),
                          onPressed: () {
                            Navigator.of(context).pop();
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SearchLocation()),
                            ).then((value) {
                              farmProv.setSearchResult(value ?? "");
                            });
                          },
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            textStyle: Theme.of(context).textTheme.labelMedium,
                          ),
                          child: const Text('ค้นหาจากพิกัด',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 16,
                              )),
                          onPressed: () {
                            Navigator.of(context).pop();
                            searchCoordinates();
                          },
                        ),
                      ],
                    ),
                  )),
            ),
          );
        });
  }

  Widget tabBar(StateSetter setState) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          margin: const EdgeInsets.only(left: 1, right: 1, top: 15),
          width: MediaQuery.of(context).size.width * 0.66,
          height: MediaQuery.of(context).size.height * 0.09,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(21),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: const Offset(0, 1),
                blurRadius: 5,
                spreadRadius: 0,
              ),
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              InkWell(
                onTap: () {
                  setState(() {
                    selectLatLng = false;
                  });
                },
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.33,
                  decoration: BoxDecoration(
                      color: selectLatLng ? Colors.white : Colors.black,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SvgPicture.asset(
                          'assets/images/ic_coordinate_search.svg',
                          width: 30,
                          height: 30,
                          color: selectLatLng ? Colors.black : Colors.white,
                        ),
                        Text(
                          "พิกัด X, Y",
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              color:
                                  selectLatLng ? Colors.black : Colors.white),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  setState(() {
                    selectLatLng = true;
                  });
                },
                child: Container(
                    width: MediaQuery.of(context).size.width * 0.33,
                    decoration: BoxDecoration(
                        color: selectLatLng ? Colors.black : Colors.white,
                        borderRadius: BorderRadius.circular(20)),
                    child: Center(
                      child: Wrap(
                        children: [
                          SvgPicture.asset(
                            'assets/images/ic_lat_lng_search.svg',
                            width: 30,
                            height: 30,
                            color: selectLatLng ? Colors.white : Colors.black,
                          ),
                          Text("พิกัด lat,long",
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  color: selectLatLng
                                      ? Colors.white
                                      : Colors.black)),
                        ],
                      ),
                    )),
              ),
            ],
          ),
        )
      ],
    );
  }

  final List<String> zoneItems = [
    '47',
    '48',
  ];

  String? selectedValue;

  void convertUtmToLatLng(DrawFarmViewModel farmProv) {
    var pointSrc =
        Point(x: double.parse(xCtrl.text), y: double.parse(yCtrl.text));
    var projSrc = Projection.get('EPSG:4326')!;
    var projDst = Projection.get('EPSG:32647');

    if (zone == "48") {
      projDst = Projection.add(
          'EPSG:32648', '+proj=utm +zone=48 +datum=WGS84 +units=m +no_defs');
    } else {
      projDst = Projection.add(
          'EPSG:32647', '+proj=utm +zone=47 +datum=WGS84 units=m +no_defs');
    }

    var pointInverse = projDst.transform(projSrc, pointSrc);

    farmProv.setNewLocation(LatLng(pointInverse.y, pointInverse.x));
    farmProv.setSearchResult("$zone,  X ${xCtrl.text}, Y ${yCtrl.text}");
    Navigator.of(context).pop();
  }

  Widget coordinateInput() {
    return Consumer<DrawFarmViewModel>(
      builder: (context, mapProv, _) {
        return Container(
          margin: const EdgeInsets.only(top: 20),
          child: Column(
            children: [
              Row(
                children: [
                  const Text(
                    "Zone",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: SizedBox(
                    child: DropdownButtonFormField2(
                      decoration: InputDecoration(
                        isDense: true,
                        contentPadding: EdgeInsets.zero,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(20)),
                          borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                        ),
                      ),
                      isExpanded: true,
                      hint: const Text(
                        'ระบุ Zone',
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w300,
                            color: Color(0XFFA0AEC0)),
                      ),
                      icon: const Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.black,
                      ),
                      iconSize: 30,
                      buttonHeight: 60,
                      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
                      dropdownDecoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      items: zoneItems
                          .map((item) => DropdownMenuItem<String>(
                                value: item,
                                child: Text(
                                  item,
                                  style: const TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                              ))
                          .toList(),
                      onChanged: (value) {
                        zone = value.toString();
                      },
                      onSaved: (value) {
                        selectedValue = value.toString();
                      },
                    ),
                  ))
                ],
              ),
              Row(
                children: [
                  const Text(
                    "X",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: InputText(
                      autoFocus: false,
                      title: "",
                      hintText: "ระบุพิกัด X",
                      isRequire: false,
                      controller: xCtrl,
                    ),
                  )
                ],
              ),
              Row(
                children: [
                  const Text(
                    "Y",
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: InputText(
                      autoFocus: false,
                      title: "",
                      hintText: "ระบุพิกัด Y",
                      isRequire: false,
                      controller: yCtrl,
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.32,
                            child: ElevatedButton(
                              onPressed: () => Navigator.of(context).pop(),
                              child: const Text("ยกเลิก"),
                              style: ElevatedButton.styleFrom(
                                  side: const BorderSide(color: Colors.black),
                                  padding: const EdgeInsets.all(15),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.5),
                                  ),
                                  textStyle: const TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                  ),
                                  backgroundColor: Colors.white,
                                  foregroundColor: Colors.black),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.32,
                            child: ElevatedButton(
                              onPressed: () {
                                yCtrl.text == "" ||
                                        xCtrl.text == "" ||
                                        zone == ""
                                    ? null
                                    : convertUtmToLatLng(mapProv);
                              },
                              child: const Text("ตกลง"),
                              style: ElevatedButton.styleFrom(
                                  side: const BorderSide(color: Colors.black),
                                  padding: const EdgeInsets.all(15),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.5),
                                  ),
                                  textStyle: const TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                  ),
                                  backgroundColor: Colors.black,
                                  foregroundColor: Colors.white),
                            ),
                          )
                        ]),
                  )
                ],
              )
            ],
          ),
        );
      },
    );
  }

  Widget latLongInput(StateSetter setState) {
    validateLatLng(DrawFarmViewModel mapProv) {
      setState(() {
        List latLongList = [];
        String lat = "";
        String lng = "";

        if (latLngCtrl.text.indexOf(',') > 0) {
          lat = latLngCtrl.text.split(",")[0].trim();
          lng = latLngCtrl.text.split(",")[1].trim();
          if (latLngCtrl.text.split(",").length != 2) {
            invalidLatLng = true;
            return;
          }
        } else {
          latLongList = latLngCtrl.text.split(" ");
          latLongList.removeWhere((item) => [
                "",
                " ",
              ].contains(item));
          if (latLongList.length != 2) {
            invalidLatLng = true;
            return;
          }
        }

        if (double.tryParse(lat) != null && double.tryParse(lng) != null) {
          invalidLatLng = false;
          mapProv.setNewLocation(LatLng(double.parse(lat), double.parse(lng)));
          Navigator.of(context).pop();
          mapProv.setSearchResult("${double.parse(lat)}, ${double.parse(lng)}");
        } else {
          invalidLatLng = true;
        }
      });
    }

    return Consumer<DrawFarmViewModel>(
      builder: (context, mapProv, _) {
        return Container(
          margin: const EdgeInsets.only(top: 20),
          child: Column(
            children: [
              Row(
                children: [
                  const Text(
                    "Lat,Long",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w300,
                        color: Color(0XFFA0AEC0)),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  Expanded(
                      child: Column(
                    children: [
                      InputText(
                          focusedBorderColor: invalidLatLng
                              ? Colors.red
                              : const Color(0xFF101010),
                          enabledBorder: invalidLatLng
                              ? Colors.red
                              : const Color(0XFFEDF2F7),
                          autoFocus: false,
                          title: "",
                          hintText: "ระบุพิกัด Lat,Long",
                          isRequire: false,
                          controller: latLngCtrl,
                          onChanged: (value) {
                            if (value == "") {
                              setState(() {
                                invalidLatLng = false;
                              });
                            }
                          }),
                      if (invalidLatLng)
                        const SizedBox(
                          height: 6,
                        ),
                      if (invalidLatLng)
                        const Align(
                          alignment: Alignment.topLeft,
                          child: Text(
                            "ไม่สามารถระบุพิกัดได้",
                            style: TextStyle(color: Colors.red, fontSize: 12),
                            textAlign: TextAlign.left,
                          ),
                        )
                    ],
                  )),
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 20),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.32,
                            child: ElevatedButton(
                              onPressed: () => Navigator.of(context).pop(),
                              child: const Text("ยกเลิก"),
                              style: ElevatedButton.styleFrom(
                                  side: const BorderSide(color: Colors.black),
                                  padding: const EdgeInsets.all(15),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.5),
                                  ),
                                  textStyle: const TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                  ),
                                  backgroundColor: Colors.white,
                                  foregroundColor: Colors.black),
                            ),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          SizedBox(
                            width: MediaQuery.of(context).size.width * 0.32,
                            child: ElevatedButton(
                              onPressed: () {
                                setState(() {
                                  if (latLngCtrl.text == "") {
                                    null;
                                  } else {
                                    validateLatLng(mapProv);

                                    //Navigator.of(context).pop();
                                  }
                                });
                              },
                              child: const Text("ตกลง"),
                              style: ElevatedButton.styleFrom(
                                  side: const BorderSide(color: Colors.black),
                                  padding: const EdgeInsets.all(15),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.5),
                                  ),
                                  textStyle: const TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,
                                  ),
                                  backgroundColor: Colors.black,
                                  foregroundColor: Colors.white),
                            ),
                          )
                        ]),
                  )
                ],
              )
            ],
          ),
        );
      },
    );
  }

  Future<void> searchCoordinates() {
    return showDialog(
        context: context,
        barrierColor: Colors.transparent,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (context, StateSetter setState) {
            return GestureDetector(
              onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
              child: Align(
                alignment: const Alignment(0, 0),
                child: Material(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Container(
                      margin: MediaQuery.of(context).viewInsets,
                      padding: const EdgeInsets.only(
                        left: 20,
                        right: 20,
                      ),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: SingleChildScrollView(
                        physics: const NeverScrollableScrollPhysics(),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const SizedBox(
                              height: 20,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text("ค้นหาจากพิกัด",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.black)),
                                InkWell(
                                    onTap: () {
                                      invalidLatLng = false;
                                      Navigator.of(context).pop();
                                    },
                                    child: const Icon(
                                      Icons.close,
                                      color: Colors.black,
                                      size: 30,
                                    ))
                              ],
                            ),
                            tabBar(setState),
                            selectLatLng
                                ? latLongInput(setState)
                                : coordinateInput()
                          ],
                        ),
                      )),
                ),
              ),
            );
          });
        }).then((value) {
      xCtrl.text = "";
      yCtrl.text = "";
      latLngCtrl.text = "";
      selectLatLng = false;
      invalidLatLng = false;
    });
  }

  Widget searchPlace() {
    return Builder(builder: (context) {
      return Consumer<DrawFarmViewModel>(builder: (context, mapProv, _) {
        return Container(
          decoration: const BoxDecoration(color: Colors.white),
          child: SafeArea(
              child: Stack(children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 5),
                child: ListView.builder(
                    itemCount: mapProv.getPlaceList.length,
                    itemBuilder: (context, index) {
                      return Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5.0),
                            child: InkWell(
                              onTap: () {
                                FocusManager.instance.primaryFocus?.unfocus();
                                mapProv.setSelectedLocation(
                                    mapProv.getPlaceList[index].placeId!);
                              },
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: SvgPicture.asset(
                                        "assets/images/ic_location.svg"),
                                  ),
                                  Expanded(
                                    flex: 6,
                                    child: Text(
                                      mapProv.getPlaceList[index].description!,
                                      style: const TextStyle(
                                        color: Colors.black,
                                        fontSize: 16,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          const Divider(
                            height: 0,
                            thickness: 1,
                          ),
                        ],
                      );
                    }),
              ),
            )
          ])),
        );
      });
    });
  }

  Widget totalArea() {
    double paddingTop = MediaQuery.of(context).size.height * 0.13;
    return Builder(builder: (context) {
      return Consumer<DrawFarmViewModel>(builder: (context, mapProv, _) {
        return mapProv.totalArea != ""
            ? SafeArea(
                child: Stack(children: <Widget>[
                Align(
                    alignment: Alignment.topCenter,
                    child: Padding(
                        padding: EdgeInsets.only(top: paddingTop),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(12),
                              decoration: BoxDecoration(
                                  color: widget.toolColor.withOpacity(0.7),
                                  borderRadius: BorderRadius.circular(15)),
                              child: Text(mapProv.totalArea,
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.white)),
                            ),
                          ],
                        )))
              ]))
            : const SizedBox();
      });
    });
  }

  Widget mapType(BuildContext context) {
    return Builder(builder: (context) {
      return Consumer<DrawFarmViewModel>(builder: (context, mapProv, _) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              onTap: () {
                showModalBottomSheet(
                    isDismissible: false,
                    shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20)),
                    ),
                    context: context,
                    builder: (BuildContext context) {
                      return BottomSheetMap(
                        valueChanged: (value) {
                          mapProv.setMaptype(value);
                        },
                        indexSelected: mapProv.indexSelected
                      );
                    });
              },
              child: Container(
                width: 45,
                height: 45,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(50)),
                child: const Icon(
                  Icons.map_outlined,
                  color: Colors.black,
                  size: 30,
                ),
              ),
            )
          ],
        );
      });
    });
  }

  Widget xyPin(BuildContext context) {
    return Builder(builder: (context) {
      return Consumer<DrawFarmViewModel>(builder: (context, provider, _) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              onTap: () {
                provider.setXy();
                LatLng point =
                    isCameraMove ? currentLocation! : provider.sourceLocation!;
                provider.markXYPin(point);
              },
              child: Container(
                  width: 45,
                  height: 45,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(50)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "X,Y",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 14,
                            color: provider.showXY
                                ? const Color(0xFF25C8A8)
                                : Colors.black),
                        textAlign: TextAlign.center,
                      )
                    ],
                  )),
            )
          ],
        );
      });
    });
  }

  Widget currentLoc() {
    //double paddingTop = MediaQuery.of(context).size.height * 0.19;
    return Builder(builder: (context) {
      return Consumer<DrawFarmViewModel>(builder: (context, mapProv, _) {
        return Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              onTap: () =>
                  mapProv.changeCameraPosition(mapProv.sourceLocation!),
              child: Container(
                width: 45,
                height: 45,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(50)),
                child: Icon(
                  widget.iconLocation,
                  color: Colors.black,
                  size: 30,
                ),
              ),
            )
          ],
        );
      });
    });
  }

  Widget toolsList() {
    return Builder(builder: (context) {
      return Consumer<DrawFarmViewModel>(builder: (context, mapProv, _) {
        return SafeArea(
            child: Stack(
          children: <Widget>[
            Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 30),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                        onTap: () => mapProv.undoLocationNew(),
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              color: mapProv.tempLocation.length == 0
                                  ? Colors.black.withOpacity(0.7)
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(50)),
                          child: Icon(
                            widget.iconUndoEdit,
                            color: mapProv.tempLocation.length == 0
                                ? Colors.white
                                : Color(0xFF0AC898),
                            size: 40,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      InkWell(
                        onTap: () {
                          LatLng point = isCameraMove
                              ? currentLocation!
                              : mapProv.sourceLocation!;
                          mapProv.onTapMap(point);
                        },
                        child: Container(
                          width: 70,
                          height: 70,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(50)),
                          child: const Icon(
                            Icons.add,
                            color: Color(0xFF0AC898),
                            size: 60,
                          ),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      InkWell(
                        onTap: () => mapProv.saveTracking(context),
                        child: Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                              color: mapProv.tempLocation.length < 3
                                  ? Colors.black.withOpacity(0.7)
                                  : Colors.white,
                              borderRadius: BorderRadius.circular(50)),
                          child: Icon(
                            widget.iconDoneEdit,
                            color: mapProv.tempLocation.length < 3
                                ? Colors.white
                                : const Color(0xFF0AC898),
                            size: 40,
                          ),
                        ),
                      ),
                    ],
                  ),
                ))
          ],
        ));
      });
    });
  }
}
