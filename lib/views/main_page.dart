import 'dart:async';
import 'package:auto_route/auto_route.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/view_models/navigate_index_view_model.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';
import 'package:varun_kanna_app/widget/round_bottom_navigatetion_bar.dart';

class MainPage extends StatefulWidget {
  final int? selectedPage;
  const MainPage({this.selectedPage, Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  int selectedIndex = 0;
  bool isCurrentVersion = false;
  @override
  void initState() {
    if (widget.selectedPage != null) {
      selectedIndex = widget.selectedPage!;
    }
    initData();
    super.initState();
  }

  initData() async {
    SharedPreferences? prefs = await _prefs;
    setState(() {
      isCurrentVersion = prefs.getBool("isCurrentVersion")!;
    });
  }

  @override
  Widget build(BuildContext context) {
    final router = context.router;
    NavigateIndexViewModel navigateIndexService =
        Provider.of<NavigateIndexViewModel>(context, listen: true);
    return AutoTabsScaffold(
        resizeToAvoidBottomInset: false,
        routes: [
          const HomeRouter(),
          WeatherRouter(),
          FarmsRouter(),
          const MarketRouter(),
          const ProfileRouter(),
        ],
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Transform.translate(
          offset: const Offset(0, 2),
          child: FloatingActionButton(
            onPressed: () async {
              // put condition show Alert update App here
              if (isCurrentVersion) {
                final SharedPreferences prefs = await _prefs;
                String userId = prefs.getString('userId') ?? "";
                if (userId != "") {
                  router.navigate(FarmsRouter());
                  setState(() {
                    navigateIndexService.setActiveIndex(2);
                    selectedIndex = 2;
                  });
                } else {
                  final action = await ViewDialogs.ConfirmOrCancelDialog(
                      context,
                      "ยังไม่ได้เข้าสู่ระบบใช่ไหม ?",
                      "กรุณาเข้าสู่ระบบเพื่อใช้งานฟังก์ชันนี้\nเข้าสู่ระบบกด ยืนยัน",
                      "ยกเลิก",
                      "ยืนยัน",
                      Colors.black);
                  if (action == ViewDialogsAction.confirm) {
                    router.navigate(const ProfileRouter());
                    navigateIndexService.setActiveIndex(4);
                  }
                }
              } else {}
            },
            child: (SvgPicture.asset(
              "assets/images/ic_new_farm.svg",
              color: navigateIndexService.getActiveIndex() == 2
                  ? const Color(0xFF0AC898)
                  : Colors.white,
            )),
            backgroundColor: Colors.black,
          ),
        ),
        extendBody: true,
        backgroundColor: Colors.white,
        bottomNavigationBuilder: (_, tabsRouter) {
          return RoundBottomNavigationBar(
              selectedIndex: navigateIndexService.getActiveIndex(),
              onItemTapped: (index) async {
                final SharedPreferences prefs = await _prefs;
                String userId = prefs.getString('userId') ?? "";
                // put condition show Alert update App here
                if (isCurrentVersion) {
                  if (index == 2) {
                    if (userId != "") {
                      // router.navigate(const MarketRouter());
                      tabsRouter.setActiveIndex(index);
                      setState(() {
                        selectedIndex = index;
                        navigateIndexService.setActiveIndex(index);
                      });
                    } else {
                      final action = await ViewDialogs.ConfirmOrCancelDialog(
                          context,
                          "ยังไม่ได้เข้าสู่ระบบใช่ไหม ?",
                          "กรุณาเข้าสู่ระบบเพื่อใช้งานฟังก์ชันนี้\nเข้าสู่ระบบกด ยืนยัน",
                          "ยกเลิก",
                          "ยืนยัน",
                          Colors.black);
                      if (action == ViewDialogsAction.confirm) {
                        router.navigate(const ProfileRouter());
                        navigateIndexService.setActiveIndex(4);
                      }
                    }
                  } else if (index == 1) {
                    if (userId != "") {
                      // router.navigate(const MarketRouter());
                      tabsRouter.setActiveIndex(index);
                      setState(() {
                        selectedIndex = index;
                        navigateIndexService.setActiveIndex(index);
                      });
                    } else {
                      final action = await ViewDialogs.ConfirmOrCancelDialog(
                          context,
                          "ยังไม่ได้เข้าสู่ระบบใช่ไหม ?",
                          "กรุณาเข้าสู่ระบบเพื่อใช้งานฟังก์ชันนี้\nเข้าสู่ระบบกด ยืนยัน",
                          "ยกเลิก",
                          "ยืนยัน",
                          Colors.black);
                      if (action == ViewDialogsAction.confirm) {
                        router.navigate(const ProfileRouter());
                        navigateIndexService.setActiveIndex(4);
                      }
                    }
                  } else if (index == 3) {
                    if (userId != "") {
                      // final test =
                      //     await CustomCupertinoPopUp.showActionSheet(context);

                      // print("🤣🤣🤣🤣🤣🤣🤣🤣🤣🤣🤣 ${test}");

                      router.navigate(const MarketRouter());
                      setState(() {
                        navigateIndexService.setActiveIndex(3);
                        selectedIndex = 3;
                      });
                    } else {
                      final action = await ViewDialogs.ConfirmOrCancelDialog(
                          context,
                          "ยังไม่ได้เข้าสู่ระบบใช่ไหม ?",
                          "กรุณาเข้าสู่ระบบเพื่อใช้งานฟังก์ชันนี้\nเข้าสู่ระบบกด ยืนยัน",
                          "ยกเลิก",
                          "ยืนยัน",
                          Colors.black);
                      if (action == ViewDialogsAction.confirm) {
                        router.navigate(const ProfileRouter());
                        navigateIndexService.setActiveIndex(4);
                      }
                    }
                  } else {
                    tabsRouter.setActiveIndex(index);
                    setState(() {
                      selectedIndex = index;
                      navigateIndexService.setActiveIndex(index);
                    });
                  }
                } else {}
              });
        });
  }
}
