import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/weather/weather_model.dart';
import 'package:varun_kanna_app/services/news_content_service.dart';
import 'package:varun_kanna_app/utils/casting.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';
import 'package:varun_kanna_app/views/weather/weather_page_provider.dart';
import 'package:varun_kanna_app/widget/custom_dot_indicator.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';

import '../../../widget/app_bar_text.dart';

class WeatherPage extends StatelessWidget {
  WeatherPage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double headerSize = size.width * 0.046;
    double weatherSizeDetail = size.width * 0.052;
    double translationHeight = size.height * -0.012;

    final width = MediaQuery.of(context).size.width;

    ScrollController scrollController = ScrollController();

    Widget weatherBox(WeatherModelModel? weatherModelModel) {
      double maxTemp = 0;
      double minTemp = 0;
      double rainEverage = 0;
      double windEverage = 0;
      String weartherDesc = "";
      String iconName = "";
      if (weatherModelModel != null) {
        maxTemp = Cast.fahrnhitToClss(weatherModelModel.main.temp_max!);
        minTemp = Cast.fahrnhitToClss(weatherModelModel.main.temp_min!);
        rainEverage = weatherModelModel.rain?.h3 ?? rainEverage;
        windEverage = weatherModelModel.wind.speed ?? windEverage;
        weartherDesc = weatherModelModel.weather[0].description!;
        iconName = weatherModelModel.weather[0].icon!;
      }

      return Container(
          height: MediaQuery.of(context).size.height * 0.3,
          width: MediaQuery.of(context).size.width / 1.08,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: const Offset(0, 1),
                blurRadius: 5,
                spreadRadius: 0,
              ),
            ],
            borderRadius: BorderRadius.circular(15.5),
            color: Colors.white,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Row(
                            children: [
                              Container(
                                transform: Matrix4.translationValues(
                                    translationHeight * 0.3,
                                    translationHeight * -1,
                                    0.0),
                                child: CachedNetworkImage(
                                  height:
                                      MediaQuery.of(context).size.height * 0.16,
                                  imageUrl:
                                      'https://varuna-kanna-public-dev.s3.ap-southeast-1.amazonaws.com/public/weather/icon/${iconName}.png',
                                ),
                              ),
                              Column(
                                children: [
                                  Text(
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    "อุณหภูมิสูงสุด",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: headerSize),
                                  ),
                                  SizedBox(
                                    height: size.width * 0.014,
                                  ),
                                  Row(
                                    children: [
                                      const Icon(
                                        Icons.keyboard_arrow_up,
                                        color: Colors.red,
                                      ),
                                      Text(
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        maxTemp.toStringAsFixed(0) + " °",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: headerSize * 1.6),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Column(
                                children: [
                                  Text(
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    "อุณหภูมิตํ่าสุด",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: headerSize),
                                  ),
                                  SizedBox(
                                    height: size.width * 0.014,
                                  ),
                                  Row(
                                    children: [
                                      const Icon(
                                        Icons.keyboard_arrow_down,
                                        color: Colors.green,
                                      ),
                                      Text(
                                        maxLines: 2,
                                        overflow: TextOverflow.ellipsis,
                                        minTemp.toStringAsFixed(0) + " °",
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: headerSize * 1.6),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      Container(
                        transform: Matrix4.translationValues(
                            0.0, translationHeight, 0.0),
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width / 4),
                        child: Column(
                          children: [
                            Text(
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              Cast.mapWeatherToThaiWord(
                                  iconName.substring(0, 2)),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: weatherSizeDetail * 0.9),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 0),
                        child: Row(
                          children: [
                            Column(
                              children: [
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 2.1,
                                  child: Text(
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    "ปริมาณนํ้าฝน (สะสมตลอดวัน)            ",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: size.width * 0.038),
                                  ),
                                ),
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width / 2.1,
                                  child: Text(
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    "ความเร็วลม (เฉลี่ยตลอดวัน)",
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: size.width * 0.038),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  width: MediaQuery.of(context).size.width / 9,
                                  child: Icon(
                                    Icons.local_drink,
                                    color: Colors.black,
                                    size: size.width * 0.038,
                                  ),
                                ),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width / 9,
                                  child: Icon(
                                    Icons.air,
                                    color: Colors.black,
                                    size: size.width * 0.038,
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              children: [
                                SizedBox(
                                  width: MediaQuery.of(context).size.width / 4,
                                  child: Text(
                                    "${rainEverage.toStringAsFixed(2)} มม.",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: size.width * 0.038),
                                  ),
                                ),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width / 4,
                                  child: Text(
                                    "${windEverage.toStringAsFixed(2)}  กม./ชม.",
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: size.width * 0.038),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),

                      ///
                    ],
                  ),
                ),
              ),
            ],
          ));
    }

    Widget currentLocationBox(String? currentLocation) {
      currentLocation = currentLocation != "" ? currentLocation : "";
      return Container(
          margin: EdgeInsets.only(top: 5, bottom: 10),
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: const Offset(0, 1),
                blurRadius: 5,
                spreadRadius: 0,
              ),
            ],
            borderRadius: BorderRadius.circular(15.5),
            color: Colors.white,
          ),
          child: Row(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    transform: Matrix4.translationValues(0.0, -2.0, 0.0),
                    margin: EdgeInsets.all(5),
                    width: MediaQuery.of(context).size.width * 0.6,
                    child: Text(
                      textAlign: TextAlign.left,
                      "ตำแหน่งปัจจุบัน",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: size.width * 0.038),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(10),

                    width: MediaQuery.of(context).size.width * 0.6,
                    //width of the recommendation text
                    child: Text(
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                      "" + currentLocation! != "" ? currentLocation : "",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: size.width * 0.038),
                    ),
                  ),
                ],
              ),
              Container(
                child: SvgPicture.network(
                  placeholderBuilder: (context) {
                    return SvgPicture.asset(
                        width: size.width * 0.2,
                        height: size.width * 0.2,
                        'assets/images/current_location.svg');
                  },
                  'assets/images/ic_rice.svg',
                ),
              ),
            ],
          ));
    }

///////
    return ChangeNotifierProvider(
      create: (context) => WeatherPageProvider()..initialData(context),
      child:
          Consumer3<WeatherPageProvider, NewsContentService, DrawFarmViewModel>(
              builder: (context, weatherProv, indicatorProv, provider, _) {
        // render
        if (weatherProv.isLoading) {
          return SizedBox(
            height: MediaQuery.of(context).size.height,
            child: const CustomLoadingWidget(),
          );
        } else {
          return Scaffold(
              body: Column(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width,
                // height: MediaQuery.of(context).size.height / 5,
                child: const AppBarText(
                  textHeader: "อากาศ",
                  isback: false,
                ),
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.8,
                width: MediaQuery.of(context).size.width,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      SizedBox(
                          width: MediaQuery.of(context).size.width,
                          // color: Colors.amber,
                          child: Column(
                            children: [
                              CarouselSlider(
                                  options: CarouselOptions(
                                    enableInfiniteScroll: false,
                                    viewportFraction: 0.93,
                                    // autoPlayInterval: const Duration(seconds: 3),
                                    // autoPlay: true,
                                    enlargeCenterPage: true,
                                    onPageChanged: (index, reason) async => {
                                      await weatherProv
                                          .switchWeatherByFarmIndex(index),
                                      // weatherProv.initialData(context),
                                      indicatorProv.onPageChange(index),
                                    },
                                    aspectRatio: 2.2,
                                    initialPage: 0,
                                  ),
                                  items: List.generate(
                                    1,
                                    (index) {
                                      if (index == 0) {
                                        return currentLocationBox(
                                            Cast.formatPlusCode(weatherProv
                                                .currentLocationList[0]
                                                .formatted_address!));
                                      } else {
                                        return Container();
                                      }
                                      //
                                      // else {
                                      //   index = index - 1;
                                      //   return Container(
                                      //     margin: EdgeInsets.only(
                                      //         top: index == 0 ? 2 : 10,
                                      //         left: 2,
                                      //         right: 2,
                                      //         bottom: 10),
                                      //     decoration: BoxDecoration(
                                      //       color: Colors.white,
                                      //       borderRadius:
                                      //           BorderRadius.circular(20),
                                      //       boxShadow: [
                                      //         BoxShadow(
                                      //           color: Colors.black
                                      //               .withOpacity(0.3),
                                      //           offset: const Offset(0, 1),
                                      //           blurRadius: 5,
                                      //           spreadRadius: 0,
                                      //         ),
                                      //       ],
                                      //     ),
                                      //     child: Row(
                                      //       children: [
                                      //         width > 800
                                      //             ? const Expanded(
                                      //                 child: SizedBox())
                                      //             : const SizedBox(),
                                      //         Expanded(
                                      //           flex: width > 800 ? 2 : 1,
                                      //           child: Column(
                                      //             crossAxisAlignment:
                                      //                 CrossAxisAlignment
                                      //                     .start,
                                      //             children: [
                                      //               Padding(
                                      //                 padding:
                                      //                     EdgeInsets.only(
                                      //                         top: 20,
                                      //                         left:
                                      //                             width > 800
                                      //                                 ? 0
                                      //                                 : 15),
                                      //                 child: Text(
                                      //                     provider
                                      //                         .getFarmList[
                                      //                             index]
                                      //                         .farmName!,
                                      //                     style:
                                      //                         const TextStyle(
                                      //                       color:
                                      //                           Colors.black,
                                      //                       fontWeight:
                                      //                           FontWeight
                                      //                               .w700,
                                      //                       fontSize: 18,
                                      //                     )),
                                      //               ),
                                      //               const SizedBox(
                                      //                 height: 10,
                                      //               ),
                                      //               Padding(
                                      //                 padding:
                                      //                     EdgeInsets.only(
                                      //                         left:
                                      //                             width > 800
                                      //                                 ? 0
                                      //                                 : 15),
                                      //                 child: Text(
                                      //                   maxLines: 3,
                                      //                   overflow: TextOverflow
                                      //                       .ellipsis,
                                      //                   'พื้นที่ ${provider.getFarmList[index].displayArea}',
                                      //                   textAlign:
                                      //                       TextAlign.left,
                                      //                   style:
                                      //                       const TextStyle(
                                      //                           color: Colors
                                      //                               .black,
                                      //                           fontWeight:
                                      //                               FontWeight
                                      //                                   .w500,
                                      //                           fontSize: 17),
                                      //                 ),
                                      //               ),
                                      //               Padding(
                                      //                 padding:
                                      //                     EdgeInsets.only(
                                      //                         left:
                                      //                             width > 800
                                      //                                 ? 0
                                      //                                 : 15),
                                      //                 child: Text(
                                      //                     overflow:
                                      //                         TextOverflow
                                      //                             .ellipsis,
                                      //                     maxLines: 2,
                                      //                     '${provider.getFarmList[index].address}',
                                      //                     style:
                                      //                         const TextStyle(
                                      //                       color:
                                      //                           Colors.black,
                                      //                       fontSize: 18,
                                      //                     )),
                                      //               ),
                                      //             ],
                                      //           ),
                                      //         ),
                                      //         // width > 800
                                      //         //     ? const Expanded(child: SizedBox())
                                      //         //     : const SizedBox(),
                                      //         provider.getFarmList[index]
                                      //                     .farmImg! ==
                                      //                 "https://s3.ap-southeast-1.amazonaws.com/"
                                      //             ? const SizedBox()
                                      //             : Container(
                                      //                 // padding:
                                      //                 //     const EdgeInsets.fromLTRB(5, 20, 0, 20),
                                      //                 margin: const EdgeInsets
                                      //                         .fromLTRB(
                                      //                     20, 20, 25, 20),
                                      //                 child: ClipRRect(
                                      //                   borderRadius:
                                      //                       BorderRadius
                                      //                           .circular(20),
                                      //                   child:
                                      //                       SizedBox.fromSize(
                                      //                     size: const Size
                                      //                         .fromRadius(60),
                                      //                     child: Image.network(
                                      //                         provider
                                      //                             .getFarmList[
                                      //                                 index]
                                      //                             .farmImg!,
                                      //                         fit: BoxFit
                                      //                             .cover),
                                      //                   ),
                                      //                 ),
                                      //               ),
                                      //         width > 800
                                      //             ? const Expanded(
                                      //                 child: SizedBox())
                                      //             : const SizedBox(),
                                      //       ],
                                      //     ),
                                      //   );
                                      // }
                                    },
                                  )),
                              Padding(
                                  padding: const EdgeInsets.all(4),
                                  child: CustomCircleDotIndicators(
                                    dotSpacing: 12,
                                    selectedSize: 9,
                                    dotsSize: 9,
                                    itemCount: 1,
                                    index: indicatorProv.index,
                                  )),
                              const SizedBox(
                                height: 20,
                              ),

                              Column(
                                children: [
                                  weatherBox(weatherProv.weatherModelModel),
                                ],
                              ),
                              // allFarm(provider, context),
                            ],
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ));
        }
      }),
    );
  }
}
