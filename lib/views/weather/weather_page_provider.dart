import 'dart:convert';
import 'dart:ffi';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/main.dart';
import 'package:varun_kanna_app/model/farm_response.dart';
import 'package:varun_kanna_app/model/weather/weather_model.dart';
import 'package:varun_kanna_app/services/farm_service.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:varun_kanna_app/view_models/draw_farm_view_model.dart';
import 'package:provider/provider.dart';

class WeatherPageProvider with ChangeNotifier {
  bool isLoading = true;
  List<LatLngModel> listLatLng = [];
  String unitType = "Imperial";
  String formattedDate =
      DateFormat('kk:mm:ss \n EEE d MMM').format(DateTime.now());
  // currentDate this variable for compare day for fetch weather API if value has change
  String currentDate = DateFormat('yyyy-MM-dd').format(DateTime.now());

  WeatherModelModel? weatherModelModel;

  List<List<WeatherModelModel>>? listWeatherByLatLng = [];

  List<LocationDetail> currentLocationList = [
    LocationDetail(formatted_address: "พื้นที่"),
    LocationDetail(formatted_address: "")
  ];

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  Future<void> getFarmList(BuildContext context) async {
    final SharedPreferences prefs = await _prefs;
    DrawFarmViewModel drawFarmViewModel = context.read<DrawFarmViewModel>();
    await drawFarmViewModel.clearFarmList();
    await drawFarmViewModel.getFarm(prefs.getString('userId').toString());
  }

  initialData(BuildContext context) async {
    isLoading = true;
    notifyListeners();

    double userLat = prefs!.getDouble('userLat') ?? 0.1;
    double userLng = prefs!.getDouble('userLng') ?? 0.1;

    LatLng _sourceLocation = await initLocation();
    double userCurrLat = _sourceLocation.latitude;
    double userCurrLng = _sourceLocation.longitude;

    String intitialDate = "";
    intitialDate = prefs!.getString('intitialDate') ?? "";

    bool isChangeLocation = ((userCurrLat - userLat).abs() >= 0.1 ||
        (userCurrLng - userLng).abs() >= 0.1);
    bool isNewDay = intitialDate != currentDate;

    print("(userCurrLat - userLat).abs() " +
        (userCurrLat - userLat).abs().toString());
    print(" (userCurrLng - userLng).abs() >= 0.1 " +
        (userCurrLng - userLng).abs().toString());

    listLatLng.insert(
        0,
        LatLngModel(
            lat: _sourceLocation.latitude, lng: _sourceLocation.longitude));

    if (isNewDay || isChangeLocation) {
      await Future.wait(listLatLng.map((element) async {
        LatLng latlng = LatLng(element.lat, element.lng);

        prefs!.setString(
            "intitialDate", DateFormat('yyyy-MM-dd').format(DateTime.now()));
        prefs!.setDouble("userLat", userCurrLat);
        prefs!.setDouble("userLng", userCurrLng);
        listWeatherByLatLng!.add(await getWeatherByLatLng(latlng));
      }));

      String stringList = jsonEncode({"list": listWeatherByLatLng});
      prefs!.setString("listWeatherAllFarm", stringList);
    }

    var weatherForcastTdyList = WeatherForcastAllFarmResponseModel.fromJson(
        jsonDecode(prefs!.getString("listWeatherAllFarm")!));

    // int time =
    //     ((((int.parse(formattedDate.substring(0, 2)) / 3).floor()) + 1) * 3);
    // // int time = 24;
    // if (time == 24) {
    //   time = 21;
    // }

    await setupWeaterDisplayValue(weatherForcastTdyList, 0);

    await setUplocationDetail(_sourceLocation);

    notifyListeners();
  }

  Future<void> setupWeaterDisplayValue(
      var weatherForcastTdyList, int index) async {
    int timeNow = int.parse(formattedDate.substring(0, 2));
    try {
      weatherForcastTdyList.list[index].forEach((element) {
        int timeStr = int.parse(element.dt_txt.substring(0, 2)) + 7;

        if (timeStr > timeNow) {
          String dt_text = element.dt_txt;
          Main main = element.main;
          List<Weather> weather = element.weather;
          Wind wind = element.wind;
          Rain rain = element.rain!;
          weatherModelModel =
              WeatherModelModel(main, weather, wind, rain, dt_text);
          throw new Exception();
        }
      });
    } catch (e) {}
  }

  Future<void> setUplocationDetail(LatLng _sourceLocation) async {
    String latlngString = _sourceLocation.latitude.toString() +
        "," +
        _sourceLocation.longitude.toString();
    final currLocationDetail = await getDetailLocationByLatLng(latlngString);
    currLocationDetail.when(
      success: ((data) => {
            currentLocationList = data,
            isLoading = false,
          }),
      failure: (error) => {},
    );
  }

  Future<List<WeatherModelModel>> getWeatherByLatLng(LatLng latlng) async {
    List<WeatherModelModel>? list = [];
    final weatherForcast = await getWeatherForcast(latlng);
    weatherForcast.when(
      success: ((data) {
        try {
          data.list.asMap().forEach((i, element) {
            String dateFormatStr = element.dt_txt.substring(0, 10);
            int time = int.parse(element.dt_txt.substring(11).substring(0, 2));
            if (time == 21) {
              throw new Exception();
            } else {
              list.add(element);
            }
          });
        } catch (e) {
          print(e);
        }
      }),
      failure: (error) {},
    );
    return list;
  }

  Future<LatLng> initLocation() async {
    LatLng _sourceLocation = LatLng(44.968046, -94.420307);

    try {
      Position currentPos;
      final serviceEnable = await Geolocator.isLocationServiceEnabled();
      if (serviceEnable) {
        var permission = await Geolocator.checkPermission();
        if ((permission != LocationPermission.always ||
            permission != LocationPermission.whileInUse)) {
          permission = await Geolocator.requestPermission();
        }
        currentPos = await Geolocator.getCurrentPosition();
      } else {
        currentPos = Position(
            latitude: Platform.isAndroid ? 0 : -6.215412,
            longitude: Platform.isAndroid ? 0 : 106.777773,
            timestamp: DateTime.now(),
            accuracy: 0,
            altitude: 0,
            heading: 0,
            speed: 0,
            speedAccuracy: 0);
      }

      _sourceLocation = LatLng(currentPos.latitude, currentPos.longitude);
    } catch (e) {
      print(e.toString());
      await initLocation();
    }

    notifyListeners();

    return _sourceLocation;
  }

  Future<void> switchWeatherByFarmIndex(int index) async {
    var weatherForcastTdyList = WeatherForcastAllFarmResponseModel.fromJson(
        jsonDecode(prefs!.getString("listWeatherAllFarm")!));
    int dateFormatStr;
    weatherForcastTdyList.list[index].forEach((element) {
      dateFormatStr = int.parse(element.dt_txt.substring(0, 2));
      int time =
          ((((int.parse(formattedDate.substring(0, 2)) / 3).ceil()) + 1) * 3);
      if (time == 24) {
        time = 21;
      }
      if (dateFormatStr == time) {
        String dt_text = element.dt_txt;
        Main main = element.main;
        List<Weather> weather = element.weather;
        Wind wind = element.wind;
        Rain rain = element.rain!;
        weatherModelModel =
            WeatherModelModel(main, weather, wind, rain, dt_text);
      }
    });
    notifyListeners();
  }

  Future<ApiResult<WeatherForcastResponseModel>> getWeatherForcast(
      LatLng location) async {
    String appid = "d397bebd497e27551b75c107aebefb1c";

    try {
      final response = await dioClient.get(
          "http://api.openweathermap.org/data/2.5/forecast",
          queryParameters: {
            "lat": location.latitude,
            "lon": location.longitude,
            "appid": appid,
            "units": unitType
          });
      WeatherForcastResponseModel provinceList =
          WeatherForcastResponseModel.fromJson(response);
      return ApiResult.success(provinceList);
    } catch (e) {
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<WeatherModelModel>> getCurrentWeather(
      LatLng location) async {
    String appid = "d397bebd497e27551b75c107aebefb1c";

    try {
      final response = await dioClient.get(
          "https://api.openweathermap.org/data/2.5/weather",
          queryParameters: {
            "lat": location.latitude,
            "lon": location.longitude,
            "appid": appid,
            "units": unitType
          });
      WeatherModelModel responseModel = WeatherModelModel.fromJson(response);
      return ApiResult.success(responseModel);
    } catch (e) {
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }
}
