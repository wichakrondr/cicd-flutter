import 'dart:async';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/model/term_and_condition_response.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/view_models/term_and_condition_view_model.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  void initState() {
    Provider.of<TermAndContionViewModel>(context, listen: false)
        .getTermAndCondtionInfo();
    super.initState();
    startTime();
  }

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  startTime() async {
    var _duration = const Duration(seconds: 2);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() async {
    TermAndContionViewModel termAndContionViewModel =
        Provider.of<TermAndContionViewModel>(context, listen: false);
    final SharedPreferences prefs = await _prefs;
    await termAndContionViewModel.getTermAndCondtionDeviceServive();
    await termAndContionViewModel.getCheckLastConsentService("device");
    String? consentsCurrentVersion =
        termAndContionViewModel.getCheckLastConsentInfo()!.version!;

    String? signedVersion = prefs.getString("signedVersion");

    bool? isDeviceConsent = signedVersion == consentsCurrentVersion;
    if (isDeviceConsent) {
      context.router.replace(MainRoute());
    } else {
      context.router.replace(const TermAndConditionDeviceRoute());
    }
    //context.router.navigate(MainRoute());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(left: 30, right: 30),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(color: Colors.black),
        child: SvgPicture.asset("assets/images/ic_kanna.svg"),
      ),
    );
  }
}
