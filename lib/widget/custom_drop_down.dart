import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';

class CustomDropDown extends StatelessWidget {
  CustomDropDown(
      {super.key,
      required this.items,
      required this.onChanged,
      required this.onSaved,
      required this.placeholder});
  List<dynamic> items;
  String placeholder;
  Function(String value) onChanged;
  Function(String value) onSaved;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField2(
      decoration: InputDecoration(
        isDense: true,
        contentPadding: EdgeInsets.zero,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        enabledBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: Color(0XFFEDF2F7)),
        ),
        focusedBorder: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          borderSide: BorderSide(color: Color(0XFFEDF2F7)),
        ),
      ),
      isExpanded: true,
      hint: Text(
        placeholder,
        style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w300,
            color: Color(0XFFA0AEC0)),
      ),
      icon: const Icon(
        Icons.keyboard_arrow_down,
        color: Colors.black,
      ),
      iconSize: 30,
      buttonHeight: 60,
      buttonPadding: const EdgeInsets.only(left: 20, right: 10),
      dropdownDecoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
      ),
      items: items
          .map((item) => DropdownMenuItem<String>(
                value: item,
                child: Text(
                  item,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
              ))
          .toList(),
      onChanged: (value) {
        onChanged(value.toString());
      },
      onSaved: (value) {
        onSaved(value.toString());
      },
    );
  }
}
