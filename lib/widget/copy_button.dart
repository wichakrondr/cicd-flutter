import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CopyButton extends StatelessWidget {
  const CopyButton({super.key, required this.copyText});
  final String copyText;

  @override
  Widget build(BuildContext context) {
    FToast fToast = FToast();
    fToast.init(context);
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.28,
        child: ElevatedButton(
          onPressed: () {
            Clipboard.setData(ClipboardData(text: copyText));

            fToast.showToast(
              child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 24.0, vertical: 12.0),
                  decoration: BoxDecoration(
                    color: const Color(0XFFE5EAEF),
                    borderRadius: BorderRadius.circular(12.5),
                  ),
                  child: const Text(
                    "คัดลอกสำเร็จ",
                    style: TextStyle(fontSize: 16),
                  )),
              gravity: ToastGravity.BOTTOM,
              toastDuration: const Duration(seconds: 2),
            );
          },
          child: const Text("คัดลอก"),
          style: ElevatedButton.styleFrom(
              side: const BorderSide(color: Colors.black),
              padding: const EdgeInsets.all(15),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(12.5),
              ),
              textStyle: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 18,
                  fontFamily: 'Sarabun',
                  height: 1.4),
              backgroundColor: Colors.black,
              foregroundColor: Colors.white),
        ),
      ),
    );
  }
}
