import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class AppBarImage extends StatelessWidget {
  final bool isback;
  final String pathImage = "assets/images/ic_kanna_black.svg";
  final Function()? onBackPress;

  const AppBarImage({Key? key, this.isback = true, this.onBackPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
      height: MediaQuery.of(context).size.height * 0.13,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          isback
              ? (TextButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                  child: Container(
                      child: const Icon(
                    Icons.arrow_back,
                    color: Colors.white,
                    size: 30,
                  )),
                  onPressed: onBackPress))
              : Container(),
          TextButton(
              style: TextButton.styleFrom(
                padding: EdgeInsets.zero,
                tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              ),
              child: Container(
                  child: const Icon(
                Icons.arrow_back,
                color: Colors.black,
                size: 30,
              )),
              onPressed: () {
                Navigator.pop(context);
              }),
          Expanded(child: LayoutBuilder(builder: (context, constrait) {
            final padding =
                (MediaQuery.of(context).size.width - constrait.maxWidth);
            return Padding(
              padding: EdgeInsets.only(right: padding),
              child: Center(
                child: SvgPicture.asset(
                  pathImage,
                  width: 30,
                  height: 30,
                ),
              ),
            );
          })),
        ],
      ),
    );
  }
}
