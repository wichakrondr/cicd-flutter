import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RoundBottomNavigationBar extends StatelessWidget {
  final int selectedIndex;
  final Function(int) onItemTapped;
  const RoundBottomNavigationBar(
      {Key? key, required this.selectedIndex, required this.onItemTapped})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: ThemeData(
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(32), topLeft: Radius.circular(32)),
              boxShadow: [
                BoxShadow(
                    color: Colors.black.withOpacity(0.15), blurRadius: 10),
              ],
            ),
            child: ClipRRect(
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(32.0),
                topRight: Radius.circular(32.0),
              ),
              child: BottomNavigationBar(
                backgroundColor: Colors.white,
                selectedItemColor: Color(0xFF0AC898),
                unselectedItemColor: Colors.black,
                type: BottomNavigationBarType.fixed,
                selectedFontSize: 12,
                items: [
                  BottomNavigationBarItem(
                      icon: Padding(
                          padding: const EdgeInsets.only(
                            top: 2,
                            bottom: 4,
                          ),
                          child: SvgPicture.asset(
                            'assets/images/ic_home.svg',
                            color: selectedIndex == 0
                                ? Color(0xFF0AC898)
                                : Colors.black,
                            width: 18,
                            height: 18,
                          )),
                      label: 'หน้าแรก'),
                  BottomNavigationBarItem(
                      icon: Padding(
                          padding: const EdgeInsets.only(
                            top: 2,
                            bottom: 4,
                          ),
                          child: SvgPicture.asset(
                            'assets/images/ic_weather.svg',
                            color: selectedIndex == 1
                                ? Color(0xFF0AC898)
                                : Colors.black,
                            width: 18,
                            height: 18,
                          )),
                      label: 'อากาศ'),
                  BottomNavigationBarItem(
                      icon: Padding(
                          padding: const EdgeInsets.only(
                            top: 2,
                            bottom: 4,
                          ),
                          child: SvgPicture.asset(
                            'assets/images/ic_weather.svg',
                            color: selectedIndex == 2
                                ? Color(0xFF0AC898)
                                : Colors.black,
                            width: 18,
                            height: 18,
                          )),
                      label: 'แปลงปลูก'),
                  BottomNavigationBarItem(
                      icon: Padding(
                          padding: const EdgeInsets.only(
                            top: 2,
                            bottom: 4,
                          ),
                          child: SvgPicture.asset(
                            'assets/images/ic_market.svg',
                            color: selectedIndex == 3
                                ? Color(0xFF0AC898)
                                : Colors.black,
                            width: 18,
                            height: 18,
                          )),
                      label: 'โครงการ'),
                  BottomNavigationBarItem(
                      icon: Padding(
                          padding: const EdgeInsets.only(
                            top: 2,
                            bottom: 4,
                          ),
                          child: SvgPicture.asset(
                            'assets/images/ic_profile.svg',
                            color: selectedIndex == 4
                                ? Color(0xFF0AC898)
                                : Colors.black,
                            width: 18,
                            height: 18,
                          )),
                      label: 'โปรไฟล์'),
                ],
                currentIndex: selectedIndex,
                onTap: onItemTapped,
              ),
            )));
  }
}
