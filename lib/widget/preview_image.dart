import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PreviewImageWidget extends StatelessWidget {
  PreviewImageWidget({
    Key? key,
    required this.clearImage,
    required this.imagePath,
    this.isInfoTapped = false,
    this.isEnable = true,
    this.infoPath = '',
    this.imagePathString,
  }) : super(key: key);
  Uint8List imagePath;
  Function clearImage;
  bool isInfoTapped;
  bool isEnable;
  String infoPath;
  String? imagePathString;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        InkWell(
            onTap: () {
              dialogPreview(context);
            },
            child: Stack(
              children: [
                const Padding(
                  padding: EdgeInsets.only(left: 40, top: 40),
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(
                      Color(0xFF25C8A8),
                    ),
                  ),
                ),
                Container(
                  width: 100,
                  height: 100,
                  margin: const EdgeInsets.only(top: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                      image: imagePath.isEmpty
                          ? CachedNetworkImageProvider(
                              imagePathString!,
                            )
                          : Image.memory(imagePath).image,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ],
            )),
        SizedBox(
          width: 107,
          child: Align(
              alignment: Alignment.topRight,
              child: isEnable
                  ? Container(
                      width: 20,
                      height: 20,
                      child: InkWell(
                        onTap: () {
                          isEnable ? clearImage() : null;
                        },
                        child: const Icon(
                          Icons.close,
                          color: Colors.white,
                          size: 20,
                        ),
                      ),
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.black,
                      ),
                    )
                  : Container()),
        ),
      ],
    );
  }

  Future<dynamic> dialogPreview(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          return Center(
              child: Container(
            margin: EdgeInsets.symmetric(horizontal: isInfoTapped ? 20 : 0),
            width: isInfoTapped
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width * 0.8,
            height: isInfoTapped
                ? MediaQuery.of(context).size.height * 0.8
                : MediaQuery.of(context).size.height * 0.8,
            child: Image(
              image: isInfoTapped
                  ? Image.asset(infoPath).image
                  : imagePath.isEmpty
                      ? CachedNetworkImageProvider(imagePathString!)
                      : Image.memory(imagePath).image,
              // fit: BoxFit.cover,
            ),
          ));
        });
  }
}
