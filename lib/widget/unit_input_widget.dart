import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:dropdown_textfield/dropdown_textfield.dart';
import 'package:flutter/material.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_widget_model/activity_fertilize_model.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_widget_model/activity_model.dart';
import 'package:varun_kanna_app/widget/input_text.dart';

class UnitInputWidget extends StatelessWidget {
  const UnitInputWidget({
    super.key,
    required this.title,
    required this.placeHolder,
    required this.onChanged,
    required this.onUpdate,
    this.items,
    this.itemsFert,
    required this.isRequired,
    required this.autoFocus,
    this.isReadOnly = false,
    this.isEditing = false,
    required this.controller,
    this.unitValueController,
    this.value = '',
    this.unitValue = '',
  });
  final String title;
  final String placeHolder;
  final Function(String value) onChanged;
  final Function(String value) onUpdate;
  final List<ItemResponseModel>? items;
  final List<ItemFertResponseModel>? itemsFert;
  final bool isRequired;
  final bool autoFocus;
  final bool isReadOnly;
  final bool isEditing;
  final String value;
  final String unitValue;
  final TextEditingController controller;
  final SingleValueDropDownController? unitValueController;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Row(
              children: [
                Text(
                  title,
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                isRequired
                    ? const Text("*",
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: Colors.red))
                    : Container()
              ],
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(width: 10),
              Expanded(
                child: InputText(
                  isEnable: !isReadOnly,
                  readOnly: isReadOnly,
                  controller: controller,
                  hintText: placeHolder,
                  autoFocus: autoFocus,
                  isNumber: true,
                  onChanged: (v) {
                    onChanged(v);
                  },
                ),
              ),
              const SizedBox(width: 30),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Container(
                    decoration: BoxDecoration(
                      // border: Border.all(width: 0.3, color: Colors.grey),
                      color: isReadOnly
                          ? Colors.grey.shade400.withAlpha(99)
                          : Colors.transparent,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: DropDownTextField(
                      controller: unitValueController,
                      clearOption: false,
                      dropdownRadius: 12,
                      enableSearch: false,
                      dropDownIconProperty: IconProperty(
                        icon: Icons.keyboard_arrow_down,
                        color: Colors.black,
                        size: 30,
                      ),
                      searchShowCursor: false,
                      padding: EdgeInsets.zero,
                      textStyle: TextStyle(
                          color: isReadOnly ? Colors.grey[600] : Colors.black),
                      textFieldDecoration: InputDecoration(
                        isDense: true,
                        contentPadding: const EdgeInsets.only(
                          bottom: 16,
                          top: 16,
                          left: 10,
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(12),
                        ),
                        enabledBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          borderSide: BorderSide(color: Color(0XFFEDF2F7)),
                        ),
                      ),
                      isEnabled: !isReadOnly,
                      dropDownList: items != null
                          ? items!
                              .map(
                                (e) => DropDownValueModel(
                                  value: e.nameTh,
                                  name: e.nameTh!,
                                ),
                              )
                              .toList()
                          : itemsFert!
                              .map(
                                (e) => DropDownValueModel(
                                  value: e.nameTh,
                                  name: e.nameTh!,
                                ),
                              )
                              .toList(),
                      onChanged: isReadOnly
                          ? null
                          : (v) {
                              onUpdate(v.value.toString());
                            },
                    ),
                  ),
                ),
              ),

              // Expanded(
              //   child: Padding(
              //     padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
              //     child: Container(
              //       decoration: BoxDecoration(
              //         color: isReadOnly ? Colors.grey[300] : Colors.transparent,
              //         borderRadius: BorderRadius.circular(12),
              //       ),
              //       child: DropdownButtonFormField2(
              //         decoration: InputDecoration(
              //           isDense: true,
              //           contentPadding: EdgeInsets.zero,
              //           border: OutlineInputBorder(
              //             borderRadius: BorderRadius.circular(12),
              //           ),
              //           enabledBorder: const OutlineInputBorder(
              //             borderRadius: BorderRadius.all(Radius.circular(12)),
              //             borderSide: BorderSide(color: Color(0XFFEDF2F7)),
              //           ),
              //           focusedBorder: const OutlineInputBorder(
              //             borderRadius: BorderRadius.all(Radius.circular(12)),
              //             borderSide: BorderSide(color: Color(0XFFEDF2F7)),
              //           ),
              //         ),
              //         isExpanded: true,
              //         icon: const Icon(
              //           Icons.keyboard_arrow_down,
              //           color: Colors.black,
              //         ),
              //         iconSize: 30,
              //         buttonHeight: 53,
              //         buttonPadding: const EdgeInsets.only(left: 20, right: 10),
              //         dropdownDecoration: BoxDecoration(
              //           borderRadius: BorderRadius.circular(12),
              //         ),
              //         value: isReadOnly || isEditing
              //             ? unitValue
              //             : items != null
              //                 ? items![0].nameTh
              //                 : itemsFert![0].nameTh,
              // items: items != null
              //     ? items!
              //         .map(
              //           (e) => DropdownMenuItem(
              //             value: e.nameTh,
              //             child: e.nameTh!,
              //           ),
              //         )
              //         .toList()
              //     : itemsFert!
              //         .map(
              //           (e) => DropdownMenuItem(
              //             value: e.nameTh,
              //             child: e.nameTh!,
              //           ),
              //         )
              //         .toList(),
              //         onChanged: isReadOnly
              //             ? null
              //             : (v) {
              //                 onUpdate(v.toString());
              //               },
              //       ),
              //     ),
              //   ),
              // ),

              const SizedBox(width: 10),
            ],
          ),
        ],
      ),
    );
  }
}
