import 'package:flutter/material.dart';

class RadioButton extends StatelessWidget {
  final int selectedIndex;
  final bool isRow;
  final bool isRequire;

  final Function(int?)? onChange;
  final List<String> radioBtn;
  final Color activeColor;
  final String title;
  final double titleSize;

  final bool? isEnable;

  const RadioButton(
      {Key? key,
      required this.selectedIndex,
      this.onChange,
      required this.title,
      this.isRow = true,
      this.isEnable = true,
      this.isRequire = false,
      this.titleSize = 16,
      this.activeColor = Colors.black,
      required this.radioBtn})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> _radioList() {
      List<Widget> children = [];
      for (int i = 0; i < radioBtn.length; i++) {
        children.add(Expanded(
          child: Row(
            children: [
              Radio(
                  fillColor: MaterialStateColor.resolveWith(
                      (states) => isEnable! ? activeColor : Colors.grey),
                  value: i + 1,
                  groupValue: selectedIndex,
                  onChanged: isEnable! ? onChange : null),
              Expanded(
                child: Text(radioBtn[i]),
              )
            ],
          ),
          flex: 1,
        ));
      }
      return children;
    }

//render
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
              child: Row(
            children: [
              Text(title,
                  style: TextStyle(
                      fontSize: titleSize, fontWeight: FontWeight.w500)),
              if (isRequire) ...[
                (Text("*",
                    style: TextStyle(
                        fontSize: titleSize,
                        fontWeight: FontWeight.w500,
                        color: Colors.red)))
              ]
            ],
          )),
        ),
        isRow
            ? Row(
                children: _radioList(),
              )
            : Column(
                children: _radioList(),
              )
      ],
    );
  }
}
