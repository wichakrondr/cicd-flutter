// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:varun_kanna_app/widget/fav_icon.dart';
import 'package:varun_kanna_app/model/news_content.dart';

class CarouselCards extends StatelessWidget {
  final List cardList;
  final String title;
  final Function(NewsContent cardDetail) onPressed;
  final Function() onSeeAllPressed;

  const CarouselCards(
      {required this.cardList,
      required this.title,
      required this.onPressed,
      required this.onSeeAllPressed,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    // print(width);
    // print(height);

    return Column(
      children: [
        Padding(
            padding: EdgeInsets.only(
              top: 20,
              left: width > 800 ? 100 : 20,
              right: width > 800 ? 100 : 20,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  // margin
                  child: Text(title,
                      style: const TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 24,
                          fontFamily: 'Sarabun')),
                ),
                GestureDetector(
                  onTap: onSeeAllPressed,
                  child: (Row(
                    children: [
                      Text('ดูทั้งหมด',
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              fontSize: width > 800 ? 18 : 14,
                              fontFamily: 'Sarabun')),
                      Center(
                        child: Container(
                          margin: const EdgeInsets.only(left: 5),
                          width: 25,
                          height: 25,
                          child: const Icon(
                            Icons.navigate_next,
                            color: Colors.white,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: const Color(0xFF0AC898),
                          ),
                        ),
                      ),
                    ],
                  )),
                )
              ],
            )),
        const SizedBox(height: 10),
        Container(
          width: width > 800 ? width * 0.8 : width,
          height: 220,
          child: ListView(
            // options: CarouselOptions(
            //   height: 230,
            //   scrollPhysics: const ClampingScrollPhysics(),
            //   enableInfiniteScroll: false,
            //   aspectRatio: width > 800 ? 2.9 : 1.7,
            //   viewportFraction: width > 800 ? 0.41 : 0.7,
            //   pageSnapping: true,
            // ),
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            children: cardList
                .map((item) => Container(
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: Cards(
                          text: item.text,
                          imageName: item.imageName,
                          title: item.title,
                          isFav: item.isFav,
                          onPressed: () => onPressed(item)),
                    ))
                .toList(),
          ),
        ),
      ],
    );
  }
}

class Cards extends StatelessWidget {
  final String text;
  final String imageName;
  final String title;
  final bool isFav;
  final Function() onPressed;

  const Cards(
      {required this.text,
      required this.imageName,
      required this.title,
      required this.onPressed,
      required this.isFav,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onPressed,
        child: Container(
          margin: const EdgeInsets.only(top: 5, bottom: 5),
          width: 250,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: const Offset(0, 1),
                blurRadius: 5,
                spreadRadius: 0,
              ),
            ],
          ),
          child: Column(
            children: [
              Container(
                height: 130,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20),
                    ),
                    image: DecorationImage(
                        fit: BoxFit.cover, image: AssetImage(imageName))),
                // child: Container(
                //     alignment: Alignment.topRight,
                //     child: FavIcon(isFav: isFav)),
              ),
              Container(
                height: 80,
                margin: const EdgeInsets.only(right: 10, left: 10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30),
                  ),
                  color: Colors.white,
                ),
                child: (Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 8, bottom: 5, right: 8, top: 5),
                      child: Text(title,
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: const TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w700,
                              fontSize: 16,
                              fontFamily: 'Sarabun')),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 8, bottom: 5, right: 8),
                      child: Text(
                        text,
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: const TextStyle(
                            color: Colors.black,
                            fontFamily: 'Sarabun',
                            fontSize: 14),
                      ),
                    )
                  ],
                )),
              ),
            ],
          ),
        ));
  }
}

class CardList extends StatelessWidget {
  final String text;
  final String imageName;
  final String title;
  final bool isFav;
  final Function() onPressed;
  final Function()? onFavPressed;

  const CardList(
      {required this.text,
      required this.imageName,
      required this.title,
      required this.onPressed,
      required this.isFav,
      this.onFavPressed,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print("Building isFave is $isFav");
    return GestureDetector(
        onTap: onPressed,
        child: Container(
          margin:
              const EdgeInsets.only(top: 10, bottom: 10, left: 10, right: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                offset: const Offset(0, 1),
                blurRadius: 5,
                spreadRadius: 0,
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 130,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                      topRight: Radius.circular(20),
                      topLeft: Radius.circular(20),
                    ),
                    image: DecorationImage(
                        fit: BoxFit.cover, image: AssetImage(imageName))),
                // child: Container(
                //     alignment: Alignment.topRight,
                //     child: FavIcon(isFav: isFav)),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 8, bottom: 5, right: 8, top: 10),
                child: Text(title,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w700,
                      fontFamily: 'Sarabun',
                      fontSize: 16,
                    )),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 8, bottom: 5, right: 8, top: 10),
                child: Text(
                  text,
                  textAlign: TextAlign.left,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                      fontFamily: 'Sarabun',
                      fontSize: 14),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
            ],
          ),
        ));
  }
}
