import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:varun_kanna_app/utils/constants.dart';

class InputText extends StatelessWidget {
  final String? title;
  final bool? isRequire;
  final bool isNumber;
  final bool isOnlyLetters;
  final bool isNumberAndLetters;
  final String? hintText;
  final int? maxLines;
  final int? maxLength;
  final String? counterText;
  final TextEditingController? controller;
  final ValueChanged<String>? onChanged;
  final bool? readOnly;
  final Icon? suffixIcon;
  final Widget? suffix;
  final Icon? prefixIcon;
  final Function()? onTap;
  final ValueChanged<String>? onSubmitted;
  final Color? focusedBorderColor;
  final Color? fillColor;
  final bool isEnable;
  final bool autoFocus;
  final Color? enabledBorder;
  final bool isEdit;
  const InputText({
    Key? key,
    this.title,
    this.isRequire = false,
    this.isNumber = false,
    this.isOnlyLetters = false,
    this.isNumberAndLetters = false,
    this.controller,
    this.hintText = '',
    this.maxLines = 1,
    this.maxLength = 9999999,
    this.counterText = '',
    this.onChanged,
    this.readOnly = false,
    this.suffix,
    this.onTap,
    this.onSubmitted,
    this.suffixIcon,
    this.prefixIcon,
    this.autoFocus = true,
    this.focusedBorderColor,
    this.isEnable = true,
    this.fillColor,
    this.enabledBorder,
    this.isEdit = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10, bottom: 10),
      child: Column(
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Row(
              children: [
                title == null
                    ? Container()
                    : Text(title!,
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500)),
                if (isRequire!) ...[
                  (const Text("*",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w500,
                          color: Colors.red)))
                ]
              ],
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          TextField(
            enabled: isEnable,
            autofocus: autoFocus,
            textInputAction: TextInputAction.next,
            style: TextStyle(
                height: 0.8,
                color: isEnable ? Colors.black : Colors.grey,
                fontSize: 16),
            decoration: InputDecoration(
                counterText: counterText,
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(12.5),
                ),
                filled: true,
                fillColor: !isEnable
                    ? ColorsUtils.disableColor.withOpacity(0.2)
                    : fillColor ?? Colors.white,
                focusedBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(12.5)),
                  borderSide: BorderSide(
                      color: focusedBorderColor ?? const Color(0xFF101010)),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: const BorderRadius.all(Radius.circular(12.5)),
                  borderSide: BorderSide(
                      color: enabledBorder ?? const Color(0XFFEDF2F7)),
                ),
                hintText: hintText,
                suffix: suffix,
                suffixIcon: suffixIcon,
                prefixIcon: prefixIcon),
            controller: controller,
            cursorColor: Colors.black,
            keyboardType: isNumber ? TextInputType.number : TextInputType.text,
            inputFormatters: isNumber
                ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
                : isOnlyLetters
                    ? <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(
                            RegExp("[ก-๙-a-zA-Z]")),
                      ]
                    : isNumberAndLetters
                        ? <TextInputFormatter>[
                            FilteringTextInputFormatter.allow(
                                RegExp("[ก-๙-a-zA-Z0-9]")),
                          ]
                        : null,
            maxLines: maxLines,
            maxLength: maxLength,
            onChanged: onChanged,
            readOnly: readOnly ?? false,
            onTap: onTap,
            onSubmitted: onSubmitted,
          ),
        ],
      ),
    );
  }
}
