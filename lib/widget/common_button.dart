import 'package:flutter/material.dart';

class CommonButton extends StatelessWidget {
  const CommonButton({
    super.key,
    this.onPressed,
    required this.title,
    this.color,
    this.icon,
    this.borderRadius = 12.5,
    this.textStyle,
  });
  final Function? onPressed;
  final String title;
  final Color? color;
  final double borderRadius;
  final Icon? icon;
  final TextStyle? textStyle;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed == null ? null : () => onPressed!.call(),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          icon ?? const SizedBox(),
          icon != null ? const SizedBox(width: 10) : const SizedBox(),
          Text(
            title,
            style: textStyle ?? const TextStyle(),
          ),
        ],
      ),
      style: ElevatedButton.styleFrom(
        // disabledBackgroundColor: const Color(0xffE5EAEF),
        side: BorderSide(
            color: color != null
                ? color!
                : onPressed == null
                    ? const Color(0xffCBD5E0)
                    : Colors.black),
        padding: const EdgeInsets.all(15),
        // backgroundColor: Colors.black,
        primary: color ?? Colors.black,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius),
        ),
        textStyle: const TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 18,
            fontFamily: 'Sarabun',
            height: 1.4),
      ),
    );
  }
}
