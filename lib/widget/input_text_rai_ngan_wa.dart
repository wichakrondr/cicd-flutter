import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:varun_kanna_app/utils/constants.dart';

class InputTextRaiNganWa extends StatelessWidget {
  final String title;
  final bool? isRequire;
  final bool isNumber;
  final bool isOnlyLetters;
  final bool isNumberAndLetters;
  final String? hintText1;
  final String? hintText2;
  final String? hintText3;
  final int? maxLines;
  final int? maxLength;
  final String? counterText;
  final TextEditingController? controllerRai;
  final TextEditingController? controllerNgan;
  final TextEditingController? controllerWa;
  final ValueChanged<String>? onChangedRai;
  final ValueChanged<String>? onChangedNgan;
  final ValueChanged<String>? onChangedWa;
  final bool? readOnly;
  final Icon? suffixIcon;
  final Widget? suffix;
  final Icon? prefixIcon;
  final Function()? onTap;
  final ValueChanged<String>? onSubmitted;
  final Color? focusedBorderColor;
  final Color? fillColor;
  final bool? isEnable;
  final bool autoFocus;
  final Color? enabledBorder;
  final bool validNgan;
  final bool validWa;
  const InputTextRaiNganWa(
      {Key? key,
      required this.title,
      this.isRequire = false,
      this.isNumber = false,
      this.isOnlyLetters = false,
      this.isNumberAndLetters = false,
      this.controllerRai,
      this.controllerNgan,
      this.controllerWa,
      this.hintText1 = '',
      this.hintText2 = '',
      this.hintText3 = '',
      this.maxLines = 1,
      this.maxLength = 9999999,
      this.counterText = '',
      this.onChangedRai,
      this.onChangedNgan,
      this.onChangedWa,
      this.readOnly,
      this.suffix,
      this.onTap,
      this.onSubmitted,
      this.suffixIcon,
      this.prefixIcon,
      this.autoFocus = true,
      this.validNgan = true,
      this.validWa = true,
      this.focusedBorderColor,
      this.isEnable = true,
      this.fillColor,
      this.enabledBorder})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
              child: Row(
            children: [
              Text(title,
                  style: const TextStyle(
                      fontSize: 16, fontWeight: FontWeight.w500)),
              if (isRequire!) ...[
                (const Text("*",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                        color: Colors.red)))
              ]
            ],
          )),
        ),
        const SizedBox(
          height: 5,
        ),
        Row(
          children: [
            Expanded(
                child: Padding(
                    padding: EdgeInsets.all(3),
                    child: Column(
                      children: [
                        TextField(
                          enabled: isEnable,
                          autofocus: autoFocus,
                          textInputAction: TextInputAction.next,
                          style: const TextStyle(
                              height: 1.3, color: Colors.black, fontSize: 16),
                          decoration: InputDecoration(
                              counterText: counterText,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12.5),
                              ),
                              filled: true,
                              fillColor: !isEnable!
                                  ? ColorsUtils.disableColor.withOpacity(0.2)
                                  : fillColor ?? Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12.5)),
                                borderSide: BorderSide(
                                    color: focusedBorderColor ??
                                        const Color(0xFF101010)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12.5)),
                                borderSide: BorderSide(
                                    color: enabledBorder ??
                                        const Color(0XFFEDF2F7)),
                              ),
                              hintText: hintText1,
                              suffix: suffix,
                              suffixIcon: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  mainAxisSize:
                                      MainAxisSize.min, // <-- important
                                  children: const [Text("ไร่")]),
                              prefixIcon: prefixIcon),
                          controller: controllerRai,
                          cursorColor: Colors.black,
                          keyboardType: isNumber
                              ? TextInputType.number
                              : TextInputType.text,
                          inputFormatters: isNumber
                              ? <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ]
                              : isOnlyLetters
                                  ? <TextInputFormatter>[
                                      FilteringTextInputFormatter.allow(
                                          RegExp("[ก-๙-a-zA-Z]")),
                                    ]
                                  : isNumberAndLetters
                                      ? <TextInputFormatter>[
                                          FilteringTextInputFormatter.allow(
                                              RegExp("[ก-๙-a-zA-Z0-9]")),
                                        ]
                                      : null,
                          maxLines: maxLines,
                          maxLength: maxLength,
                          onChanged: onChangedRai,
                          readOnly: readOnly ?? false,
                          onTap: onTap,
                          onSubmitted: onSubmitted,
                        ),
                        Text(""),
                      ],
                    ))),
            //
            Expanded(
                child: Padding(
                    padding: EdgeInsets.all(3),
                    child: Column(
                      children: [
                        TextField(
                          enabled: isEnable,
                          autofocus: autoFocus,
                          textInputAction: TextInputAction.next,
                          style: const TextStyle(
                              height: 1.3, color: Colors.black, fontSize: 16),
                          decoration: InputDecoration(
                              counterText: counterText,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12.5),
                              ),
                              filled: true,
                              fillColor: !isEnable!
                                  ? ColorsUtils.disableColor.withOpacity(0.2)
                                  : fillColor ?? Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12.5)),
                                borderSide: BorderSide(
                                    color: validNgan
                                        ? const Color(0xFF101010)
                                        : Color.fromARGB(255, 220, 13, 13)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12.5)),
                                borderSide: BorderSide(
                                    color: validNgan
                                        ? const Color(0XFFEDF2F7)
                                        : Color.fromARGB(255, 220, 13, 13)),
                              ),
                              hintText: hintText2,
                              suffix: suffix,
                              suffixIcon: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  mainAxisSize:
                                      MainAxisSize.min, // <-- important
                                  children: const [Text("งาน")]),
                              prefixIcon: prefixIcon),
                          controller: controllerNgan,
                          cursorColor: Colors.black,
                          keyboardType: isNumber
                              ? TextInputType.number
                              : TextInputType.text,
                          inputFormatters: isNumber
                              ? <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ]
                              : isOnlyLetters
                                  ? <TextInputFormatter>[
                                      FilteringTextInputFormatter.allow(
                                          RegExp("[ก-๙-a-zA-Z]")),
                                    ]
                                  : isNumberAndLetters
                                      ? <TextInputFormatter>[
                                          FilteringTextInputFormatter.allow(
                                              RegExp("[ก-๙-a-zA-Z0-9]")),
                                        ]
                                      : null,
                          maxLines: maxLines,
                          maxLength: 1,
                          onChanged: onChangedNgan,
                          readOnly: readOnly ?? false,
                          onTap: onTap,
                          onSubmitted: onSubmitted,
                        ),
                        Text("" + (!validNgan ? "ไม่เกิน 3 งาน" : ""),
                            style: TextStyle(fontSize: 14, color: Colors.red))
                      ],
                    ))),
            Expanded(
                child: Padding(
                    padding: EdgeInsets.all(3),
                    child: Column(
                      children: [
                        TextField(
                          enabled: isEnable,
                          autofocus: autoFocus,
                          textInputAction: TextInputAction.next,
                          style: const TextStyle(
                              height: 1.3, color: Colors.black, fontSize: 16),
                          decoration: InputDecoration(
                              counterText: counterText,
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(12.5),
                              ),
                              filled: true,
                              fillColor: !isEnable!
                                  ? ColorsUtils.disableColor.withOpacity(0.2)
                                  : fillColor ?? Colors.white,
                              focusedBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12.5)),
                                borderSide: BorderSide(
                                    color: validWa
                                        ? const Color(0xFF101010)
                                        : Color.fromARGB(255, 220, 13, 13)),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: const BorderRadius.all(
                                    Radius.circular(12.5)),
                                borderSide: BorderSide(
                                    color: validWa
                                        ? const Color(0XFFEDF2F7)
                                        : Color.fromARGB(255, 220, 13, 13)),
                              ),
                              hintText: hintText3,
                              suffix: suffix,
                              suffixIcon: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  mainAxisSize:
                                      MainAxisSize.min, // <-- important
                                  children: const [Text("ตร.วา")]),
                              prefixIcon: prefixIcon),
                          controller: controllerWa,
                          cursorColor: Colors.black,
                          keyboardType: isNumber
                              ? TextInputType.number
                              : TextInputType.text,
                          inputFormatters: isNumber
                              ? <TextInputFormatter>[
                                  FilteringTextInputFormatter.digitsOnly
                                ]
                              : isOnlyLetters
                                  ? <TextInputFormatter>[
                                      FilteringTextInputFormatter.allow(
                                          RegExp("[ก-๙-a-zA-Z]")),
                                    ]
                                  : isNumberAndLetters
                                      ? <TextInputFormatter>[
                                          FilteringTextInputFormatter.allow(
                                              RegExp("[ก-๙-a-zA-Z0-9]")),
                                        ]
                                      : null,
                          maxLines: maxLines,
                          maxLength: 2,
                          onChanged: onChangedWa,
                          readOnly: readOnly ?? false,
                          onTap: onTap,
                          onSubmitted: onSubmitted,
                        ),
                        Text("" + (!validWa ? "ไม่เกิน 99 ตร.วา" : ""),
                            textAlign: TextAlign.center,
                            style: TextStyle(fontSize: 14, color: Colors.red))
                      ],
                    ))),
          ],
        ),
      ],
    );
  }
}
