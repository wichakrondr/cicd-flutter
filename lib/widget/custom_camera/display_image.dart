import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:screenshot/screenshot.dart';
import 'package:varun_kanna_app/widget/custom_loading_widget.dart';

import '../common_button.dart';

class DisplayImage extends StatefulWidget {
  DisplayImage(
      {super.key,
      required this.imagePath,
      required this.imageSize,
      required this.projectName,
      this.latlng = ""});
  final File imagePath;
  final double imageSize;
  final String projectName;
  final String? latlng;
  @override
  State<DisplayImage> createState() => _DisplayImageState();
}

class _DisplayImageState extends State<DisplayImage> {
  final ImagePicker _picker = ImagePicker();
  ScreenshotController screenShotController = ScreenshotController();
  bool isLoading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          buildImage(),
          Align(
            alignment: Alignment.bottomRight,
            child: Container(
              height: 60,
              margin: const EdgeInsets.only(bottom: 20, left: 10, right: 10),
              width: MediaQuery.of(context).size.width * 0.25,
              child: CommonButton(
                color: const Color(0xff0AC898),
                title: "บันทึก",
                onPressed: () async {
                  setState(() {
                    isLoading = true;
                  });
                  // TAKE THIS "bytes" TO MAKE PREVIEW IMAGE
                  final bytes = await screenShotController.captureFromWidget(
                    Material(
                      child: buildImage(),
                    ),
                    delay: const Duration(milliseconds: 1200),
                  );

                  // final byte = bytes.lengthInBytes;
                  // final kb = byte / 1024;
                  // final mb = kb / 1024;
                  setState(() {
                    isLoading = false;
                  });

                  Navigator.pop(context, bytes);
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: SafeArea(
              child: Container(
                margin: const EdgeInsets.only(right: 10),
                child: IconButton(
                  onPressed: () async {
                    Navigator.pop(context, Uint8List(0));
                  },
                  icon: const Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topCenter,
            child: SafeArea(
              child: Container(
                margin: EdgeInsets.only(top: 10),
                child: const Text(
                  "รูปภาพ",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                  ),
                ),
              ),
            ),
          ),
          isLoading ? const Center(child: CustomLoadingWidget()) : Container()
        ],
      ),
    );
  }

  Stack buildImage() {
    return Stack(
      children: [
        Container(
          padding: EdgeInsets.zero,
          width: double.infinity,
          height: double.infinity,
          child: Center(
            child: Image(
              image: Image.file(File(widget.imagePath.path)).image,
            ),
          ),
        ),
        widget.latlng!.isEmpty
            ? Container(
                margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.15),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SvgPicture.asset(
                    "assets/images/water_mark_join_project.svg",
                    height: MediaQuery.of(context).size.height * 0.15,
                  ),
                ),
              )
            : Container(
                margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.19),
                child: Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.5),
                          borderRadius: BorderRadius.circular(12.5)),
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        widget.latlng!,
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.normal),
                        textAlign: TextAlign.center,
                      ),
                    )),
              ),
      ],
    );
  }
}
