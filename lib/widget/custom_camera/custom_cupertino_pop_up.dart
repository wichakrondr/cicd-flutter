import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:native_exif/native_exif.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/views/market/activity/activity_page_provider.dart';
import 'package:varun_kanna_app/widget/view_dialogs.dart';
import 'display_image.dart';

class CustomCupertinoPopUp {
  static Future<Uint8List> showActionSheet(BuildContext context,
      {Position? currentPos,
      bool? showLatlng = false,
      ActivityPageProvider? provider}) async {
    ExifLatLong? coordinates;
    DateTime? shootingDate;
    String latlngTxt = "";
    final f = DateFormat('dd/MM/yyyy, HH:mm');

    if (showLatlng!) {
      final serviceEnable = await Geolocator.isLocationServiceEnabled();
      if (serviceEnable) {
        var permission = await Geolocator.checkPermission();
        if ((permission != LocationPermission.always ||
            permission != LocationPermission.whileInUse)) {
          permission = await Geolocator.requestPermission();
        }
        currentPos = await Geolocator.getCurrentPosition();
      }
    }

    final ImagePicker _picker = ImagePicker();
    final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;

    final bytes = await showCupertinoModalPopup<Uint8List>(
      context: context,
      builder: (BuildContext context) => CupertinoActionSheet(
        actions: <CupertinoActionSheetAction>[
          CupertinoActionSheetAction(
            onPressed: () async {
              XFile? rawImage =
                  await _picker.pickImage(source: ImageSource.camera);

              if (showLatlng) {
                Exif? exif = await Exif.fromPath(rawImage!.path);
                coordinates = await exif.getLatLong();
                shootingDate = await exif.getOriginalDate();
                if (coordinates == null) {
                  await exif.writeAttributes({
                    'GPSLatitude': currentPos!.latitude.toString(),
                    'GPSLongitude': currentPos.longitude.toString(),
                    'DateTimeOriginal': f.format(DateTime.now()).toString()
                  });
                  latlngTxt = "Lat,Long " +
                      currentPos.latitude.toStringAsFixed(6).toString() +
                      ", " +
                      currentPos.longitude.toStringAsFixed(6).toString() +
                      "\n" +
                      f.format(DateTime.now()).toString();
                  provider!.setImageInfo(
                      LatLng(currentPos.latitude, currentPos.longitude),
                      DateTime.now());
                } else {
                  latlngTxt = "Lat,Long " +
                      coordinates!.latitude.toStringAsFixed(6).toString() +
                      ", " +
                      coordinates!.longitude.toStringAsFixed(6).toString() +
                      "\n" +
                      f.format(shootingDate!).toString();
                  provider!.setImageInfo(
                      LatLng(coordinates!.latitude, coordinates!.longitude),
                      shootingDate!);
                }
              }

              File imageFile = File(rawImage!.path);
              final byte = imageFile.readAsBytesSync().lengthInBytes;
              final kb = byte / 1024;
              final mb = kb / 1024;
              // print("PICTURE ! 😜😜😜😜 $mb");
              int currentUnix = DateTime.now().millisecondsSinceEpoch;
              final directory = await getApplicationDocumentsDirectory();
              String fileFormat = imageFile.path.split('.').last;

              final file = await imageFile.copy(
                '${directory.path}/$currentUnix.$fileFormat',
              );

              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => DisplayImage(
                    imagePath: file,
                    imageSize: mb,
                    projectName: prefs.getString("projectName").toString(),
                    latlng: latlngTxt,
                  ),
                ),
              ).then((value) {
                Navigator.pop(context, value as Uint8List);
              });
            },
            child: const Text('ถ่ายรูป'),
          ),
          CupertinoActionSheetAction(
            onPressed: () async {
              XFile? rawImage =
                  await _picker.pickImage(source: ImageSource.gallery);

              if (showLatlng) {
                Exif? exif = await Exif.fromPath(rawImage!.path);
                coordinates = await exif.getLatLong();
                shootingDate = await exif.getOriginalDate();

                if (coordinates == null) {
                  await exif.writeAttributes({
                    'GPSLatitude': currentPos!.latitude.toString(),
                    'GPSLongitude': currentPos.longitude.toString(),
                    'DateTimeOriginal': f.format(DateTime.now()).toString()
                  });
                  latlngTxt = "Lat,Long " +
                      currentPos.latitude.toStringAsFixed(6).toString() +
                      ", " +
                      currentPos.longitude.toStringAsFixed(6).toString() +
                      "\n" +
                      f.format(DateTime.now()).toString();
                  provider!.setImageInfo(
                      LatLng(currentPos.latitude, currentPos.longitude),
                      DateTime.now());
                } else {
                  latlngTxt = "Lat,Long " +
                      coordinates!.latitude.toStringAsFixed(6).toString() +
                      ", " +
                      coordinates!.longitude.toStringAsFixed(6).toString() +
                      "\n" +
                      f.format(shootingDate!).toString();
                  provider!.setImageInfo(
                      LatLng(coordinates!.latitude, coordinates!.longitude),
                      shootingDate!);
                }
              }

              File imageFile = File(rawImage!.path);
              final byte = imageFile.readAsBytesSync().lengthInBytes;
              final kb = byte / 1024;
              final mb = kb / 1024;
              // print("PICTURE ! 😜😜😜😜 $mb");
              int currentUnix = DateTime.now().millisecondsSinceEpoch;
              final directory = await getApplicationDocumentsDirectory();
              String fileFormat = imageFile.path.split('.').last;

              final file = await imageFile.copy(
                '${directory.path}/$currentUnix.$fileFormat',
              );
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => DisplayImage(
                    imagePath: file,
                    imageSize: mb,
                    projectName: prefs.getString("projectName").toString(),
                    latlng: latlngTxt,
                  ),
                ),
              ).then((value) {
                Navigator.pop(context, value as Uint8List);
              });
            },
            child: const Text('เลือกจากอัลบั้ม'),
          ),
        ],
        cancelButton: CupertinoActionSheetAction(
          isDestructiveAction: true,
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('ยกเลิก'),
        ),
      ),
    );
    int? size = bytes?.lengthInBytes ?? 0;
    final kb = size / 1024;
    double mb = kb / 1024;
    late Timer _timer;
    if (mb > 9.5) {
      await ViewDialogs.commonDialogWithIcon(
        context: context,
        text: "ขนาดรูปภาพใหญ่เกินไป กรุณาลองใหม่อีกครั้ง",
        autoDismiss: () {
          _timer = Timer(const Duration(seconds: 2), () {
            Navigator.of(context).pop();
          });
        },
        isAutoDismiss: true,
        icon: const Icon(
          size: 50,
          Icons.close_outlined,
          color: Colors.red,
        ),
      ).then((value) {
        if (_timer.isActive) {
          _timer.cancel();
        }
      });
      return Uint8List(0);
    } else if (bytes != null) {
      return bytes;
    } else {
      return Uint8List(0);
    }
  }
}
