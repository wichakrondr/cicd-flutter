import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:timelines/timelines.dart';

const kTileHeight = 50.0;

const completeColor = Colors.black;
// const completeColor = Color(0xff5e6172);
// const inProgressColor = Color(0xff5ec792);
// const inProgressColor = Color(0xff5e6172);
const inProgressColor = Colors.black;
// const todoColor = Colors.black;
const todoColor = Color(0xffd1d2d7);

enum TimeLinesWidgetType { horizontal, warning, danger, question, info }

class TimeLinesWidget extends StatelessWidget {
  final int itemCount;
  final double? dotSize;
  final List<String>? listProcess;
  final int processIndex;
  final double? boxHeight;
  final double extent;

  const TimeLinesWidget({
    required this.itemCount,
    this.dotSize = 20,
    this.boxHeight = 0.05,
    this.listProcess,
    this.extent = 4.0,
    required this.processIndex,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(4),
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * boxHeight!,
              alignment: Alignment.topCenter,
              child: Timeline.tileBuilder(
                shrinkWrap: true,
                padding: EdgeInsets.zero,
                theme: TimelineThemeData(
                  nodePosition: 0.1,
                  direction: Axis.horizontal,
                  connectorTheme:
                      const ConnectorThemeData(space: 5.0, thickness: 2.0),
                ),
                builder: TimelineTileBuilder.connected(
                  connectionDirection: ConnectionDirection.before,
                  itemCount: itemCount,
                  itemExtentBuilder: (_, __) {
                    return (MediaQuery.of(context).size.width - 150) / extent;
                  },
                  oppositeContentsBuilder: (context, index) {
                    return Container();
                  },
                  contentsBuilder: (context, index) {
                    var listProcessLocal = listProcess ?? [];
                    return Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: listProcessLocal.isNotEmpty
                          ? Text(listProcess![index])
                          : Container(),
                    );
                  },
                  indicatorBuilder: (_, index) {
                    if (index <= processIndex - 1) {
                      return DotIndicator(
                        border: Border.all(
                            color: Colors.grey, // Set border color
                            width: 3),
                        size: dotSize,
                        color: Colors.black,
                      );
                    } else {
                      return OutlinedDotIndicator(
                        backgroundColor: Colors.grey,
                        borderWidth: 3.0,
                        color: const Color.fromARGB(255, 233, 233, 233),
                        size: dotSize,
                      );
                    }
                  },
                  connectorBuilder: (_, index, type) {
                    if (index < processIndex) {
                      return const SolidLineConnector(
                        thickness: 10,
                        color: Colors.black,
                      );
                    } else {
                      return const SolidLineConnector(
                        thickness: 10,
                        color: Colors.grey,
                      );
                    }
                  },
                ),
              ),
            ),
          ],
        ));
  }
}

class ProcessTimeline extends StatelessWidget {
  final int itemCount;
  final double? dotSize;
  final List<String> listProcess;
  final int processIndex;

  final double extent;
  final double space;
  final double height;

  Color getColor(int index) {
    if (index == processIndex) {
      return inProgressColor;
    } else if (index < processIndex) {
      return completeColor;
    } else {
      return todoColor;
    }
  }

  const ProcessTimeline({
    required this.itemCount,
    this.dotSize = 20,
    this.space = 1,
    this.height = 0.1,
    required this.listProcess,
    this.extent = 4.0,
    required this.processIndex,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(0),
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * height,
              alignment: Alignment.topCenter,
              child: Timeline.tileBuilder(
                theme: TimelineThemeData(
                  nodePosition: 0.15,
                  direction: Axis.horizontal,
                  connectorTheme: ConnectorThemeData(
                    space: space,
                    thickness: 5.0,
                  ),
                ),
                builder: TimelineTileBuilder.connected(
                  connectionDirection: ConnectionDirection.before,
                  itemExtentBuilder: (_, __) =>
                      MediaQuery.of(context).size.width / listProcess.length,
                  oppositeContentsBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 15.0),
                    );
                  },
                  contentsBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 15.0),
                      child: Text(
                        listProcess[index],
                        style: TextStyle(
                          color: getColor(index),
                        ),
                      ),
                    );
                  },
                  indicatorBuilder: (_, index) {
                    var color;
                    var child;
                    if (index == processIndex) {
                      color = inProgressColor;
                      child = Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: SvgPicture.asset(
                            'assets/images/circle_dot_black.svg',
                            width: 40,
                            height: 20,
                          ));
                    } else if (index < processIndex) {
                      color = completeColor;
                      child = const Icon(
                        Icons.check,
                        color: Colors.white,
                        size: 15.0,
                      );
                    } else {
                      color = todoColor;
                    }

                    if (index <= processIndex) {
                      return Stack(
                        children: [
                          CustomPaint(
                            size: Size(30.0, 30.0),
                            // painter: _BezierPainter(
                            //   color: color,
                            //   drawStart: index > 0,
                            //   drawEnd: index < processIndex,
                            // ),
                          ),
                          DotIndicator(
                            size: 30.0,
                            color: color,
                            child: child,
                          ),
                        ],
                      );
                    } else {
                      return Stack(
                        children: [
                          CustomPaint(
                              // size: Size(15.0, 15.0),
                              // painter: _BezierPainter(
                              //   color: color,
                              //   drawEnd: index < listProcess!.length - 1,
                              // ),
                              ),
                          OutlinedDotIndicator(
                            borderWidth: 4.0,
                            color: color,
                            size: 30,
                          ),
                        ],
                      );
                    }
                  },
                  connectorBuilder: (_, index, type) {
                    if (index > 0) {
                      if (index == processIndex) {
                        final prevColor = getColor(index - 1);
                        final color = getColor(index);
                        List<Color> gradientColors;
                        if (type == ConnectorType.start) {
                          gradientColors = [
                            Color.lerp(prevColor, color, 0.5)!,
                            color
                          ];
                        } else {
                          gradientColors = [
                            prevColor,
                            Color.lerp(prevColor, color, 0.5)!
                          ];
                        }
                        return DecoratedLineConnector(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: gradientColors,
                            ),
                          ),
                        );
                      } else {
                        return SolidLineConnector(
                          color: getColor(index),
                        );
                      }
                    } else {
                      return null;
                    }
                  },
                  itemCount: itemCount,
                ),
              ),
            ),
          ],
        ));
  }
}

// hardcoded bezier painter
/// TODO: Bezier curve into package component
class _BezierPainter extends CustomPainter {
  const _BezierPainter({
    required this.color,
    this.drawStart = true,
    this.drawEnd = true,
  });

  final Color color;
  final bool drawStart;
  final bool drawEnd;

  Offset _offset(double radius, double angle) {
    return Offset(
      radius * cos(angle) + radius,
      radius * sin(angle) + radius,
    );
  }

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..style = PaintingStyle.fill
      ..color = color;

    final radius = size.width / 2;

    var angle;
    var offset1;
    var offset2;

    var path;

    if (drawStart) {
      angle = 3 * pi / 4;
      offset1 = _offset(radius, angle);
      offset2 = _offset(radius, -angle);
      path = Path()
        ..moveTo(offset1.dx, offset1.dy)
        ..quadraticBezierTo(0.0, size.height / 2, -radius,
            radius) // TODO connector start & gradient
        ..quadraticBezierTo(0.0, size.height / 2, offset2.dx, offset2.dy)
        ..close();

      canvas.drawPath(path, paint);
    }
    if (drawEnd) {
      angle = -pi / 4;
      offset1 = _offset(radius, angle);
      offset2 = _offset(radius, -angle);

      path = Path()
        ..moveTo(offset1.dx, offset1.dy)
        ..quadraticBezierTo(size.width, size.height / 2, size.width + radius,
            radius) // TODO connector end & gradient
        ..quadraticBezierTo(size.width, size.height / 2, offset2.dx, offset2.dy)
        ..close();

      canvas.drawPath(path, paint);
    }
  }

  @override
  bool shouldRepaint(_BezierPainter oldDelegate) {
    return oldDelegate.color != color ||
        oldDelegate.drawStart != drawStart ||
        oldDelegate.drawEnd != drawEnd;
  }
}
