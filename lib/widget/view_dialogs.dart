import 'package:flutter/material.dart';
import 'package:varun_kanna_app/widget/art_dialog.dart';
import 'package:varun_kanna_app/widget/art_sweetAlert.dart';

enum ViewDialogsAction { confirm, cancel }

class ViewDialogs {
  static Future<ViewDialogsAction> ConfirmOrCancelDialog(
    BuildContext context,
    String title,
    String body,
    String textButtonCancel,
    String textButtonConfirm,
    Color? themeColor,
  ) async {
    final action = await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          title: Center(
              child: Text(
            title,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
          )),
          content: Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Text(
                    body,
                    textAlign: TextAlign.center,
                    style: const TextStyle(fontSize: 16, height: 1.5),
                  ),
                )
              ],
            ),
          ),
          actions: [
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: ElevatedButton(
                        onPressed: () =>
                            Navigator.of(context).pop(ViewDialogsAction.cancel),
                        child: Text(textButtonCancel),
                        style: ElevatedButton.styleFrom(
                            side: themeColor != null
                                ? BorderSide(color: themeColor)
                                : const BorderSide(color: Colors.black),
                            padding: const EdgeInsets.all(15),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.5),
                            ),
                            textStyle: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                            ),
                            backgroundColor: Colors.white,
                            foregroundColor: themeColor ?? Colors.black),
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: ElevatedButton(
                        onPressed: () => Navigator.of(context)
                            .pop(ViewDialogsAction.confirm),
                        child: Text(textButtonConfirm),
                        style: ElevatedButton.styleFrom(
                            side: themeColor != null
                                ? BorderSide(color: themeColor)
                                : const BorderSide(color: Colors.black),
                            padding: const EdgeInsets.all(15),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.5),
                            ),
                            textStyle: const TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                            ),
                            backgroundColor: themeColor ?? Colors.black,
                            foregroundColor: Colors.white),
                      ),
                    )
                  ]),
            )
          ],
        );
      },
    );
    return (action != null) ? action : ViewDialogsAction.cancel;
  }

  static Future successDialog(
    BuildContext context,
    String title,
    String text,
  ) async {
    await ArtSweetAlert.show(
        context: context,
        artDialogArgs: ArtDialogArgs(
            type: ArtSweetAlertType.success,
            showOkBtn: false,
            title: title.isNotEmpty ? title : null,
            text: text.isNotEmpty ? text : null));
  }

  static Future commonDialogWithIcon(
      {required BuildContext context,
      required String text,
      Icon? icon,
      bool isAutoDismiss = false,
      Function? autoDismiss}) async {
    await await showDialog(
        context: context,
        barrierDismissible: true,
        builder: (c) {
          if (isAutoDismiss) {
            autoDismiss?.call();
          }
          return Center(
            child: SizedBox(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.25,
              child: Card(
                color: Colors.white,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.5)),
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      icon ??
                          const Icon(
                            Icons.check_circle_outline_outlined,
                            color: Color(0xff0AC898),
                            size: 100,
                          ),
                      const SizedBox(height: 20),
                      Text(
                        text,
                        style: const TextStyle(fontSize: 18),
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  static Future<void> confirmDialog({
    required BuildContext context,
    required String title,
    required String text,
    Text? customText,
    required String textButton,
    Color? themeColor,
  }) async {
    await showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          scrollable: true,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(32.0))),
          title: Center(
              child: Text(
            title,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
          )),
          content: Container(
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  text,
                  textAlign: TextAlign.center,
                  style: const TextStyle(fontSize: 16, height: 1.5),
                ),
                customText!
              ],
            ),
          ),
          actions: [
            Container(
              padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              child:
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: ElevatedButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text(textButton),
                    style: ElevatedButton.styleFrom(
                        side: themeColor != null
                            ? BorderSide(color: themeColor)
                            : const BorderSide(color: Colors.black),
                        padding: const EdgeInsets.all(15),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(12.5),
                        ),
                        textStyle: const TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 18,
                        ),
                        backgroundColor: themeColor ?? Colors.black,
                        foregroundColor: Colors.white),
                  ),
                ),
              ]),
            )
          ],
        );
      },
    );
  }
}
