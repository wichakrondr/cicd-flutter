import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/view_models/navigate_index_view_model.dart';

// ignore: must_be_immutable
class FavIcon extends StatefulWidget {
  bool isFav;

  FavIcon({required this.isFav, Key? key}) : super(key: key);

  @override
  State<FavIcon> createState() => _FavIconState();
}

class _FavIconState extends State<FavIcon> {
  bool isFav = false;
  bool isLogin = false; //check later when having api

  void gotoLoginScreen(BuildContext context) {
    NavigateIndexViewModel navigateIndexService =
        Provider.of<NavigateIndexViewModel>(context, listen: false);
    navigateIndexService.setActiveIndex(4);
    context.router.navigate(const ProfileRouter());
    // showDialog(
    //     context: context,
    //     builder: (BuildContext context) {
    //       return const CupertinoAlertDialog(content: Text("Login first"));
    //     });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    isFav = widget.isFav;

    return ElevatedButton(
        onPressed: () {
          isLogin
              ? setState(() {
                  isFav = !isFav;
                })
              : gotoLoginScreen(context);
        },
        child: Icon(isFav ? Icons.favorite : Icons.favorite_border_outlined,
            color: isFav ? Colors.red : Colors.black),
        style: ElevatedButton.styleFrom(
            shape: const CircleBorder(),
            padding: const EdgeInsets.all(5),
            primary: Colors.white, // <-- Button color
            onPrimary: Colors.red) // <-- Splash color
        );
  }
}
