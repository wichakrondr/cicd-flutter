import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

class SlidableButton extends StatelessWidget {
  //const SlidableButton({Key? key}) : super(key: key);
  SlidableButton(
      {this.icon,
      required this.label,
      this.autoClose = true,
      required this.onPressed});

  final IconData? icon;
  final String label;
  final bool autoClose;
  final SlidableActionCallback? onPressed;

  void _handleTap(BuildContext context) {
    onPressed?.call(context);
  }

  @override
  Widget build(BuildContext context) {
    final controller = Slidable.of(context);
    return ValueListenableBuilder<double>(
        valueListenable: controller!.animation,
        builder: (context, value, child) {
          return Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(15),
              decoration: const BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(20)),
              ),
              child: InkWell(
                onTap: () => _handleTap(context),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (icon != null) ...[
                      Icon(
                        icon,
                        color: Colors.white,
                        size: 30,
                      ),
                    ],
                    const SizedBox(
                      height: 5,
                    ),
                    Text(label,
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w500)),
                  ],
                ),
              ));
        });
  }
}
