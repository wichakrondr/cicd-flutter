import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DatePickerThai extends StatefulWidget {
  final ValueChanged<DateTime> valueChanged;
  final int defaultAtleastYear;
  DateTime initialDateTime;
  DatePickerThai(
      {Key? key,
      required this.valueChanged,
      required this.initialDateTime,
      this.defaultAtleastYear = 20})
      : super(key: key);

  @override
  State<DatePickerThai> createState() => _DatePickerThaiState();
}

class _DatePickerThaiState extends State<DatePickerThai> {
  late DateTime selectedDate = widget.initialDateTime;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: MediaQuery.of(context).size.height * 0.45,
        child: Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            children: [
              const SizedBox(
                height: 10,
              ),
              Container(
                height: MediaQuery.of(context).size.height * 0.2,
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12.5),
                    topRight: Radius.circular(12.5),
                  ),
                ),
                child: CupertinoDatePicker(
                    mode: CupertinoDatePickerMode.date,
                    onDateTimeChanged: (DateTime dateTime) {
                      setState(() {
                        selectedDate = dateTime;
                      });
                    },
                    maximumYear: widget.defaultAtleastYear != 0
                        ? DateTime.now().year - widget.defaultAtleastYear
                        : null,
                    minimumYear: DateTime(DateTime.now().year - 100,
                            DateTime.now().month, DateTime.now().day)
                        .year,
                    maximumDate:
                        widget.defaultAtleastYear != 0 ? DateTime.now() : null,
                    //initialDateTime cannot be greater than maximumDate
                    initialDateTime: widget.initialDateTime),
              ),
              const Divider(
                height: 0,
                thickness: 1,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Container(
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(12.5),
                      bottomRight: Radius.circular(12.5),
                    ),
                  ),
                  child: CupertinoButton(
                    child: const Text('ยืนยัน'),
                    onPressed: () {
                      setState(() {
                        widget.initialDateTime = selectedDate;
                      });
                      widget.valueChanged(selectedDate);
                      Navigator.of(context).pop(selectedDate);
                      // Navigator.of(context).pop(tempPickedDate);
                    },
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                child: CupertinoButton(
                    child: const Text(
                      'ยกเลิก',
                      style: TextStyle(color: Colors.blue),
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    borderRadius: const BorderRadius.all(Radius.circular(12.5)),
                    color: CupertinoColors.white),
              )
            ],
          ),
        ));
  }
}
