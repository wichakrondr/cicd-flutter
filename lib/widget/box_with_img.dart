import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../utils/constants.dart';

class BoxWithImg extends StatelessWidget {
  final String title;
  final String titleLine2;
  final bool? isRequire;

  final bool? isEnable;

  final String hintText;
  final String imgPath;
  final bool showInfoOutlineIcon;
  final Widget? widgetImage;
  final Function()? onTap;
  final Function()? onTapInFoIcon;
  final bool showInFo;

  const BoxWithImg({
    Key? key,
    required this.title,
    this.titleLine2 = "",
    this.isRequire = false,
    this.isEnable = true,
    this.showInfoOutlineIcon = false,
    this.hintText = '',
    this.onTap,
    this.onTapInFoIcon,
    this.imgPath = "",
    this.widgetImage,
    this.showInFo = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 18;
    return GestureDetector(
      child: Padding(
          padding: const EdgeInsets.only(left: 5, right: 5, top: 15, bottom: 0),
          child: Column(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Row(
                  children: [
                    Text(title,
                        style: TextStyle(
                            fontSize: title.length <= 40 ? size : size * 0.9,
                            fontWeight: FontWeight.w500)),
                    if (isRequire!) ...[
                      (Text("*",
                          style: TextStyle(
                              fontSize: size,
                              fontWeight: FontWeight.w500,
                              color: Colors.red)))
                    ],
                    if (showInFo) ...[
                      InkWell(
                        onTap: onTapInFoIcon,
                        child: const Icon(
                          Icons.info_outline,
                          size: 20,
                        ),
                      )
                    ]
                  ],
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(titleLine2,
                    style:
                        TextStyle(fontSize: size, fontWeight: FontWeight.w500)),
              ),
              const SizedBox(
                height: 5,
              ),
              widgetImage != null
                  ? Align(alignment: Alignment.centerLeft, child: widgetImage!)
                  : InkWell(
                      child: Container(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                margin: const EdgeInsets.only(right: 5),
                                height:
                                    MediaQuery.of(context).size.height * 0.2,
                                child: SvgPicture.asset(
                                  imgPath,
                                  height: 55,
                                ),
                              ),
                              Text(
                                hintText,
                                style: TextStyle(
                                  fontSize: size,
                                  color: Color(0xFFCBD5E0),
                                ),
                              ),
                            ]),
                        height: MediaQuery.of(context).size.height * 0.1,
                        width: MediaQuery.of(context).size.width * 0.9,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(22),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.05),
                              offset: const Offset(0, 1),
                              blurRadius: 5,
                              spreadRadius: 0,
                            ),
                          ],
                        ),
                      ),
                      onTap: isEnable! ? onTap : null,
                    ),
            ],
          )),
    );
  }
}
