import 'package:flutter/material.dart';
import 'package:page_view_indicators/circle_page_indicator.dart';

class CustomCircleDotIndicators extends StatelessWidget {
  const CustomCircleDotIndicators({
    Key? key,
    required this.itemCount,
    required this.index,
    this.dotsSize = 12,
    this.selectedSize = 12,
    this.dotSpacing = 8,
  }) : super(key: key);

  final int itemCount;
  final ValueNotifier<int> index;
  final double dotsSize;
  final double selectedSize;
  final double dotSpacing;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0),
      child: CirclePageIndicator(
        dotSpacing: dotSpacing,
        itemCount: itemCount,
        currentPageNotifier: index,
        selectedSize: selectedSize,
        size: dotsSize,
        dotColor: Color(0xff101010),
        borderColor: Color(0xff101010),
        selectedDotColor: Color(0xff0AC898),
        selectedBorderColor: Color(0xff0AC898),
      ),
    );
  }
}
