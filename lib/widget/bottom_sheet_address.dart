import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:varun_kanna_app/widget/input_text.dart';

class BottomSheetAddress extends StatefulWidget {
  final List item;
  Object? groupValue;
  final ValueChanged<Object> valueChanged;
  final String title;
  final Function()? onClosePress;
  bool autoFocus;
  BottomSheetAddress(
      {Key? key,
      required this.item,
      this.autoFocus = true,
      this.groupValue,
      required this.valueChanged,
      required this.title,
      this.onClosePress})
      : super(key: key);

  @override
  State<BottomSheetAddress> createState() => _BottomSheetAddressState();
}

class _BottomSheetAddressState extends State<BottomSheetAddress> {
  TextEditingController searchController = TextEditingController();
  late List itemResult;
  @override
  void initState() {
    super.initState();
    itemResult = widget.item;
  }

  @override
  Widget build(BuildContext context) {
    UnfocusDisposition disposition = UnfocusDisposition.scope;
    void searchText(String query) {
      final items = widget.item.where((text) {
        final searchLower = query.toString();
        final nameLower = text["name_th"].toString();
        return nameLower.contains(searchLower);
      }).toList();

      setState(() {
        itemResult = items;
      });
    }

    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Column(
          children: [
            Row(
              children: [
                const SizedBox(
                  width: 60,
                ),
                Expanded(
                    child: Center(
                        child: Container(
                  height: 5,
                  width: 100,
                  decoration: BoxDecoration(
                    color: const Color(0XFFA4AEB9),
                    borderRadius: BorderRadius.circular(20),
                  ),
                ))),
                TextButton(
                    style: TextButton.styleFrom(
                      padding: EdgeInsets.zero,
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    ),
                    child: const Icon(
                      Icons.close,
                      color: Colors.black,
                      size: 30,
                    ),
                    onPressed: widget.onClosePress),
              ],
            ),
            Text(widget.title),
            InputText(
              autoFocus: widget.autoFocus,
              title: "",
              isRequire: false,
              controller: searchController,
              hintText: "ค้นหา",
              prefixIcon: const Icon(
                Icons.search,
                color: Color(0XFFA0AEC0),
              ),
              onChanged: searchText,
            ),
            Expanded(
              child: ListView.builder(
                  physics: const ClampingScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: itemResult.length,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        RadioListTile(
                          value: int.parse(itemResult[index]["id"].toString()),
                          groupValue: widget.groupValue,
                          secondary:
                              itemResult[index]["icon_path"].toString() != ""
                                  ? SvgPicture.network(
                                      itemResult[index]["icon_path"].toString(),
                                      height: 25,
                                    )
                                  : null,
                          title: Text(
                            itemResult[index]["name_th"],
                            textAlign: TextAlign.left,
                          ),
                          onChanged: (value) {
                            setState(() {
                              widget.groupValue = value;
                            });
                            widget.valueChanged(value!);
                          },
                          toggleable: true,
                          activeColor: Colors.black,
                        ),
                        const Divider(
                          height: 0,
                          thickness: 1,
                        ),
                      ],
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
