import 'package:flutter/material.dart';

class AppBarText extends StatelessWidget {
  final String textHeader;
  final bool isback;
  final Function()? onBackPress;
  final Widget? action;
  final double fontSize;

  const AppBarText(
      {Key? key,
      required this.textHeader,
      this.isback = true,
      this.action,
      this.fontSize = 24,
      this.onBackPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
      height: MediaQuery.of(context).size.height * 0.13,
      decoration: const BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _backArrow(isback),
          const Spacer(),
          _titleText(textHeader),
          const Spacer(),
          action == null
              ? const Expanded(
                  child: SizedBox(),
                )
              : Expanded(
                  child: action!,
                ),
        ],
      ),
    );
  }

  Widget _titleText(String textHeader) {
    return SizedBox(child: LayoutBuilder(builder: (context, constrait) {
      final padding = (MediaQuery.of(context).size.width - constrait.maxWidth);
      return Padding(
        padding: const EdgeInsets.only(),
        child: Center(
            child: Text(textHeader,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontSize: fontSize,
                  fontFamily: 'Sarabun',
                ))),
      );
    }));
  }

  Widget _backArrow(bool isBack) {
    return isBack
        ? (Expanded(
            child: TextButton(
                style: TextButton.styleFrom(
                  tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                ),
                child: Container(
                    child: const Icon(
                  Icons.arrow_back,
                  color: Colors.white,
                  size: 30,
                )),
                onPressed: onBackPress)))
        : const Expanded(
            child: SizedBox(),
          );
  }
}
