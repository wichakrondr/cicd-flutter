import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class BottomSheetMap extends StatefulWidget {
  final ValueChanged<MapType> valueChanged;
  int indexSelected;
  BottomSheetMap(
      {Key? key, required this.valueChanged, required this.indexSelected})
      : super(key: key);

  @override
  State<BottomSheetMap> createState() => _BottomSheetMapState();
}

class _BottomSheetMapState extends State<BottomSheetMap> {
  TextEditingController searchController = TextEditingController();
  int mapIndex = 1;
  @override
  void initState() {
    super.initState();
    setState(() {
      mapIndex = widget.indexSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(0, 10, 0, 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              const SizedBox(
                width: 60,
              ),
              Expanded(
                  child: Center(
                      child: Container(
                height: 5,
                width: 100,
                decoration: BoxDecoration(
                  color: const Color(0XFFA4AEB9),
                  borderRadius: BorderRadius.circular(20),
                ),
              ))),
              TextButton(
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                  child: const Icon(
                    Icons.close,
                    color: Colors.black,
                    size: 30,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }),
            ],
          ),
          const Text("เลือกประเภทแผนที่",
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                  color: Colors.black)),
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () {
                        widget.valueChanged(MapType.satellite);
                        setState(() {
                          mapIndex = 1;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(1.5),
                        decoration: BoxDecoration(
                          color: mapIndex == 1
                              ? const Color(0xFF0AC898)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(12.5),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.5),
                          child: Image.asset(
                            'assets/images/ic_satellite_map.jpg',
                            width: 70,
                            height: 70,
                          ),
                        ),
                      )),
                  const SizedBox(
                    height: 5,
                  ),
                  Text("Satellite",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: mapIndex == 1
                              ? const Color(0xFF0AC898)
                              : Colors.black))
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () {
                        widget.valueChanged(MapType.normal);
                        setState(() {
                          mapIndex = 2;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(1.5),
                        decoration: BoxDecoration(
                          color: mapIndex == 2
                              ? const Color(0xFF0AC898)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(12.5),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.5),
                          child: Image.asset(
                            'assets/images/ic_road_map.jpg',
                            width: 70,
                            height: 70,
                          ),
                        ),
                      )),
                  const SizedBox(
                    height: 5,
                  ),
                  Text("Roadmap",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: mapIndex == 2
                              ? const Color(0xFF0AC898)
                              : Colors.black))
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () {
                        widget.valueChanged(MapType.hybrid);
                        setState(() {
                          mapIndex = 3;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(1.5),
                        decoration: BoxDecoration(
                          color: mapIndex == 3
                              ? const Color(0xFF0AC898)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(12.5),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.5),
                          child: Image.asset(
                            'assets/images/ic_hybrid_map.jpg',
                            width: 70,
                            height: 70,
                          ),
                        ),
                      )),
                  const SizedBox(
                    height: 5,
                  ),
                  Text("Hybrid",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: mapIndex == 3
                              ? const Color(0xFF0AC898)
                              : Colors.black))
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () {
                        widget.valueChanged(MapType.terrain);
                        setState(() {
                          mapIndex = 4;
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(1.5),
                        decoration: BoxDecoration(
                          color: mapIndex == 4
                              ? const Color(0xFF0AC898)
                              : Colors.white,
                          borderRadius: BorderRadius.circular(12.5),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.5),
                          child: Image.asset(
                            'assets/images/ic_terrain_map.jpg',
                            width: 70,
                            height: 70,
                          ),
                        ),
                      )),
                  const SizedBox(
                    height: 5,
                  ),
                  Text("Terrain",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: mapIndex == 4
                              ? const Color(0xFF0AC898)
                              : Colors.black))
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
