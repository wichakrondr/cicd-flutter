import 'package:dio/dio.dart';
import 'package:varun_kanna_app/model/home_page_model/banner_ads_model.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';

import '../utils/api/network_exceptions.dart';

class HomeService {
  Future<ApiResult<BannerAdsResponse>> getBannerAds() async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio);
    try {
      final response = await dioClient.get("/banner-ads");
      BannerAdsResponse bannerResponse = BannerAdsResponse.fromJson(response);

      return ApiResult.success(bannerResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }
}
