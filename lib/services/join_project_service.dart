import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/market_page_model/document_model.dart';
import 'package:varun_kanna_app/model/market_page_model/farm_list_model.dart';
import 'package:varun_kanna_app/model/market_page_model/form_model.dart';
import 'package:varun_kanna_app/model/market_page_model/join_project_list_model.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';

import '../model/market_page_model/consent_project_model.dart';
import '../utils/api/network_exceptions.dart';

class JoinProjectService {
  DioClient dioClient = DioClient(Dio(), baseUrl: "");
  Future<ApiResult<JoinProjectListResponse>> getProjectList() async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio);
    try {
      final response = await dioClient.get("/projects");
      JoinProjectListResponse joinProjectListResponse =
          JoinProjectListResponse.fromJson(response);

      return ApiResult.success(joinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<FormResponse>> getFormByProjectId(String projectId) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      final response = await dioClient.get("/verify-identity/form/$projectId");
      FormResponse joinProjectListResponse = FormResponse.fromJson(response);
      return ApiResult.success(joinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<JoinProjectListResponse>> submitForm(
      FormModel paramObject) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      final response = await dioClient.post("/verify-identity/submit-form",
          data: paramObject);
      JoinProjectListResponse joinProjectListResponse =
          JoinProjectListResponse.fromJson(response);
      return ApiResult.success(joinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<JoinProjectListResponse>> updateForm(
      FormModel paramObject) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      // String jsonList = json.encode(paramObject);
      // log('data:===>  $jsonList');

      final response =
          await dioClient.put("/verify-identity", data: paramObject);
      JoinProjectListResponse joinProjectListResponse =
          JoinProjectListResponse.fromJson(response);
      return ApiResult.success(joinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<FormResponse>> getForm(
      String req_join_project_id, String project_id) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      final response = await dioClient.get("/verify-identity",
          queryParameters: {
            "project_id": project_id,
            "req_join_project_id": req_join_project_id
          });
      FormResponse joinProjectListResponse = FormResponse.fromJson(response);
      return ApiResult.success(joinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<ProjectConsentResponse>> getProjectConsent(
      {required String projectId, required String userId}) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio);
    try {
      final response = await dioClient.get(
          "/consents/project-consent?project_id=$projectId&user_id=$userId");

      ProjectConsentResponse consentProjectResponse =
          ProjectConsentResponse.fromJson(response);

      return ApiResult.success(consentProjectResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<CommonResponse>> postSignConsent(
      {required String consentId, required String userId}) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio);
    try {
      final response = await dioClient.post("/consents/project-consent/sign",
          data: {"user_id": userId, "consent_id": consentId});

      CommonResponse commonResponse = CommonResponse.fromJson(response);

      return ApiResult.success(commonResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<JoinProjectFarmListResponse>> getFarmList({
    required String projectId,
    required String userId,
    String limit = "10",
    String offset = "0",
  }) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio);
    try {
      final response = await dioClient.get(
          "/farms/project?user_id=$userId&project_id=$projectId&limit=$limit&offset=$offset");
      JoinProjectFarmListResponse joinProjectListResponse =
          JoinProjectFarmListResponse.fromJson(response);

      return ApiResult.success(joinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<CommonResponse>> postCreateJoinProject({
    required String projectId,
    required String userId,
    required List farmList,
  }) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio);
    try {
      final response = await dioClient.post("/request-join-projects", data: {
        "project_id": projectId,
        "user_id": userId,
        "farm_id_list": farmList,
      });
      CommonResponse createJoinProjectListResponse =
          CommonResponse.fromJson(response);

      return ApiResult.success(createJoinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<RequestJoinProjectListResponse>> requestJoinProjectList(
    String status,
    String projectId,
    String userId,
    int limit,
    int offset,
  ) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio);
    try {
      final response =
          await dioClient.get("/request-join-projects", queryParameters: {
        "user_id": userId,
        "project_id": projectId,
        "status": status,
        "offset": offset,
        "limit": limit
      });
      log(json.encode(response));
      RequestJoinProjectListResponse requestJoinProjectListResponse =
          RequestJoinProjectListResponse.fromJson(response);

      return ApiResult.success(requestJoinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<DocumentResponse>> getDocumentList() async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      final response = await dioClient
          .get("/verify-identity/list-type-of-certificate-of-ownership");
      DocumentResponse joinProjectListResponse =
          DocumentResponse.fromJson(response);

      return ApiResult.success(joinProjectListResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<RequestJoinProjectListResponse>> requestJoinProjectById(
    String requestJoinProjectID,
  ) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      final response =
          await dioClient.get("/request-join-projects/" + requestJoinProjectID);
      RequestJoinProjectListResponse joinProjectResponse =
          RequestJoinProjectListResponse.fromJson(response);

      return ApiResult.success(joinProjectResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<CommonResponse>> cancelJoinProject(
      String requestJoinProjectID) async {
    try {
      final response = await dioClient.put(
          "request-join-projects/cancel/id/" + requestJoinProjectID,
          data: {"id": requestJoinProjectID});

      CommonResponse skipActivityResponse = CommonResponse.fromJson(response);
      return ApiResult.success(skipActivityResponse);
    } catch (e) {
      print("     e: " + e.toString());
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }
}
