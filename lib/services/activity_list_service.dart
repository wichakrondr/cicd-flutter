import 'package:dio/dio.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_selection_model.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';

class ActivityListService {
  DioClient dioClient = DioClient(Dio(), baseUrl: "");
  Future<ApiResult<ActivitySelectionResponse>> fetchActivityList(
      String projectId) async {
    try {
      final response =
          await dioClient.get("activities/request_join_project_id/$projectId");

      ActivitySelectionResponse activityResponse =
          ActivitySelectionResponse.fromJson(response);

      return ApiResult.success(activityResponse);
    } catch (e) {
      print("     e: " + e.toString());
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<CommonResponse>> skipActivity(String id) async {
    try {
      final response = await dioClient.put("activities/skip",
          queryParameters: {"id": id}, data: {"id": id});
      CommonResponse skipActivityResponse = CommonResponse.fromJson(response);
      return ApiResult.success(skipActivityResponse);
    } catch (e) {
      print("     e: " + e.toString());
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }
}
