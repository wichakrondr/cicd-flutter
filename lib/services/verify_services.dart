import 'package:dio/dio.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/market_page_model/verify_page_model/verify_response.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';

class VerifyService {
  Future<ApiResult<VerifyResponse>> getVerifyIdentity(
      String projectId, String reqJointProjectId) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      final response = await dioClient.get("/verify-identity",
          queryParameters: {
            "project_id": projectId,
            "req_join_project_id": reqJointProjectId
          });
      VerifyResponse verifyResponse = VerifyResponse.fromJson(response);

      return ApiResult.success(verifyResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<CommonResponse>> deleteVerifyIdentityImage(
      String userId, List<String> deletePath) async {
    final dio = Dio();
    DioClient dioClient = DioClient(dio, baseUrl: "");
    try {
      final response = await dioClient.post(
        "/verify-identity/upload/${userId}/remove",
        data: {"imgs_list": deletePath},
      );

      CommonResponse deleteResponse = CommonResponse.fromJson(response);

      return ApiResult.success(deleteResponse);
    } catch (e) {
      print(e);
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }
}
