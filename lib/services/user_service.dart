import 'package:varun_kanna_app/model/otp_response.dart';
import 'package:varun_kanna_app/model/profile_model.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:dio/dio.dart';

var dio = Dio();
const String _apiKey = "";
const String _baseUrl = "";
DioClient dioClient = DioClient(dio, baseUrl: _baseUrl);

Future<ApiResult<RegisterResponse>> loginWithPhoneNumber(String tel) async {
  try {
    final response = await dioClient.post(
      "users/login",
      data: {"tel": tel},
    );
    RegisterResponse? registerResponse = RegisterResponse.fromJson(response);

    return ApiResult.success(registerResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<RegisterResponse>> verifyOTP(
  String tel,
  String otp,
  String refCode,
) async {
  try {
    final response = await dioClient.post(
      "users/login/verify-otp",
      data: {"tel": tel, "otp": otp, "refCode": refCode},
    );
    RegisterResponse? registerResponse = RegisterResponse.fromJson(response);

    return ApiResult.success(registerResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<RegisterResponse>> registerWithPhoneNumber(String tel) async {
  try {
    final response = await dioClient.post(
      "users/register",
      data: {
        "tel": tel,
        "typeUser": "user",
      },
    );
    RegisterResponse? registerResponse = RegisterResponse.fromJson(response);

    return ApiResult.success(registerResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<RegisterResponse>> resentOTP(String tel) async {
  try {
    final response = await dioClient.post(
      "users/requestOtp",
      data: {"tel": tel},
    );
    RegisterResponse? registerResponse = RegisterResponse.fromJson(response);

    return ApiResult.success(registerResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<RegisterResponse>> verifyOTPNewUser(
  String tel,
  String otp,
  String refCode,
) async {
  try {
    final response = await dioClient.post(
      "users/register/verify",
      data: {"tel": tel, "otp": otp, "refCode": refCode},
    );
    RegisterResponse? registerResponse = RegisterResponse.fromJson(response);

    return ApiResult.success(registerResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<ProfileResponse>> signConsent(
    ProfileModel profileModel) async {
  try {
    final response = await dioClient.post(
      "users/register/signConsent",
      data: {
        "tel": profileModel.tel,
        "consentId": profileModel.consentId,
        "isAcceptedLegalAge": profileModel.isAcceptedLegalAge,
        "firstname": profileModel.firstName,
        "lastname": profileModel.lastName
      },
    );
    ProfileResponse? registerResponse = ProfileResponse.fromJson(response);

    return ApiResult.success(registerResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<ProfileResponse>> signConsentDevice(
    ProfileModel profileModel) async {
  try {
    final response = await dioClient.post(
      "consents/sign-device-consent",
      data: {
        "device_id": profileModel.deviceId,
        "consent_id": profileModel.consentDeviceId,
      },
    );
    ProfileResponse? registerResponse = ProfileResponse.fromJson(response);

    return ApiResult.success(registerResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}
