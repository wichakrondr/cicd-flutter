import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:varun_kanna_app/model/image_response.dart';
import 'package:varun_kanna_app/model/profile_model.dart';
import 'package:http/http.dart' as http;
import 'package:varun_kanna_app/services/image_service.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';

class ProfileService {
  static Future<List> getProvince() async {
    var url = Uri.parse(
        'https://kanna-dev.varunatech.co/api/v1/users/addresses/province');
    var response2 = await http.get(url);
    print("****************response: " + response2.body.toString());

    String response = await rootBundle.loadString('assets/province.json');
    Map<String, dynamic> provinceJson = await json.decode(response);
    List<dynamic> data = provinceJson["data"];

    return data;
  }

  static Future<ProfileModel> getProfile() async {
    ProfileModel profile = ProfileModel(
      firstName: "",
      lastName: "",
      birthDay: "",
      address: null,
      province: "",
      district: "",
      subDistrict: "",
    );
    return profile;
  }
}

var dio = Dio();
const String _baseUrl = "";
DioClient dioClient = DioClient(dio, baseUrl: _baseUrl);

Future<ApiResult<ProfileModel>> fetchProfile(String phoneNumber) async {
  try {
    final response = await dioClient.get("users/" + phoneNumber);
    ProfileModel profileModel = ProfileResponse.fromJson(response).data;
    ApiResult<ImageModel> imageModel =
        await fetchImage(profileModel.url_img ?? "");
    imageModel.when(
        success: (data) => {profileModel.url_img = data.path},
        failure: (error) => {print("error " + error.toString())});
    return ApiResult.success(profileModel);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<ProfileDeleteResponse>> deleteProfile(String userId) async {
  try {
    final response = await dioClient
        .post("requests/delete-account", data: {"user_id": userId});
    ProfileDeleteResponse profileModel =
        ProfileDeleteResponse.fromJson(response);
    return ApiResult.success(profileModel);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<ProfileDeleteResponse>> updateUserAddress(
    ProfileModel profileModel) async {
  try {
    final response = await dioClient.put("users", data: {
      "id": profileModel.id,
      "firstname": profileModel.firstName,
      "lastname": profileModel.lastName,
      "birthday": profileModel.birthDay,
      "email": profileModel.email,
      "addresses": {
        "address": profileModel.address?.address,
        "district": profileModel.address?.district,
        "subdistrict": profileModel.address?.subdistrict,
        "province": profileModel.address?.province,
        "postcode": profileModel.address?.postcode
      }
    });
    ProfileDeleteResponse profileResponse =
        ProfileDeleteResponse.fromJson(response);
    return ApiResult.success(profileResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}
