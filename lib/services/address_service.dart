import 'package:varun_kanna_app/model/sub_district_response.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:dio/dio.dart';
import 'package:varun_kanna_app/model/district_response.dart';

import '../model/province_response.dart';

var dio = Dio();
const String _apiKey = "";
const String _baseUrl = "";
DioClient dioClient = new DioClient(dio, baseUrl: _baseUrl);

Future<ApiResult<List<Province>>> fetchProvince() async {
  try {
    final response = await dioClient.get("users/addresses/province");
    List<Province> provinceList = ProvinceResponse.fromJson(response).data;
    return ApiResult.success(provinceList);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<List<DistrictModel>>> fetchDistrict(int provinceId) async {
  try {
    final response = await dioClient.get("users/addresses/district",
        queryParameters: {"provinceId": provinceId});
    List<DistrictModel> distrinctList =
        DistrictResponse.fromJson(response).data;

    return ApiResult.success(distrinctList);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<List<SubDistrictModel>>> fetchSubDistrict(
    int amphureId) async {
  try {
    final response = await dioClient.get("users/addresses/sub-district",
        queryParameters: {"amphureId": amphureId});
    List<SubDistrictModel> subDistrinctList =
        SubDistrictResponse.fromJson(response).data;

    return ApiResult.success(subDistrinctList);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}
