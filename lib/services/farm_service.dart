import 'dart:io';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:http_parser/http_parser.dart';
import 'package:varun_kanna_app/model/farm_list_response.dart';
import 'package:varun_kanna_app/model/farm_response.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:path_provider/path_provider.dart';

import '../model/common_response.dart';

var dio = Dio();
const key = 'AIzaSyBGzc6YxG_jlHxJxgFXrC0onbrNaQ8Qljw';
DioClient dioClient = DioClient(dio);

Future<ApiResult<FarmResponse>> saveFarmService(FarmModel farm) async {
  try {
    final response = await dioClient.post("/farms", data: farm.toJson());
    FarmResponse farmResponse = FarmResponse.fromJson(response);
    return ApiResult.success(farmResponse);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<FarmResponse>> savePolygonImgageService(
    String userId, String farmId, Uint8List img) async {
  try {
    Uint8List imageInUnit8List = img; // store unit8List image here ;
    final tempDir = await getTemporaryDirectory();

    File file = await File('${tempDir.path}/image.png').create();
    file.writeAsBytesSync(imageInUnit8List);

    String fileName = file.path.split('/').last;

    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        file.path,
        filename: fileName,
        contentType: MediaType("image", "png"),
      ),
    });

    final response = await dioClient.post(
      "/farms/picture/upload/" + userId,
      queryParameters: {"farm_id": farmId},
      data: formData,
    );
    FarmResponse farmImgResponse = FarmResponse.fromJson(response);

    return ApiResult.success(farmImgResponse);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<FarmListResponse>> fetchFarmList(String userId) async {
  try {
    final response = await dioClient.get("/farms/user_id/v2/" + userId);
    FarmListResponse farmResponse = FarmListResponse.fromJson(response);
    return ApiResult.success(farmResponse);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<FarmDetailResponse>> fetchFarmById(
    String farmId, String type) async {
  try {
    final response = await dioClient
        .get("/farms/base-data", queryParameters: {"id": farmId, "type": type});
    FarmDetailResponse farmResponse = FarmDetailResponse.fromJson(response);
    return ApiResult.success(farmResponse);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<CommonResponse>> updateFarmName(
    {required String farmId, required String farmName}) async {
  try {
    final response = await dioClient
        .put("/farms/name", data: {"farm_id": farmId, "farm_name": farmName});
    CommonResponse farmResponse = CommonResponse.fromJson(response);
    return ApiResult.success(farmResponse);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<CommonResponse>> deleteFarm({required String farmId}) async {
  try {
    final response = await dioClient.delete("/farms/farm_id/$farmId");
    CommonResponse farmResponse = CommonResponse.fromJson(response);
    return ApiResult.success(farmResponse);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<List<PlaceSearch>>> getAutocomplete(String search) async {
  try {
    final response = await dioClient.get(
        "https://maps.googleapis.com/maps/api/place/autocomplete/json",
        queryParameters: {"input": search, "key": key});
    var jsonResults = response["predictions"] as List;
    return ApiResult.success(
        jsonResults.map((place) => PlaceSearch.fromJson(place)).toList());
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<Place>> searchLocationById(String Id) async {
  try {
    final response = await dioClient.get(
        "https://maps.googleapis.com/maps/api/place/details/json",
        queryParameters: {"place_id": Id, "key": key});
    var jsonResult = response['result'] as Map<String, dynamic>;
    return ApiResult.success(Place.fromJson(jsonResult));
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<List<LocationDetail>>> getDetailLocationByLatLng(
    String latLng) async {
  try {
    final response = await dioClient.get(
        "https://maps.googleapis.com/maps/api/geocode/json",
        queryParameters: {"latlng": latLng, "key": key, "language": "TH"});
    // LocationDetailRes farmResponse = CommonResponse.fromJson(response);
    List<LocationDetail> res = LocationDetailRes.fromJson(response).results;

    return ApiResult.success(res);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<CommonResponse>> updatepolygon(
    FarmModel farm, String farmId, String farmImg) async {
  try {
    final response = await dioClient.put("/farms/polygon", data: {
      "farm_id": farmId,
      "square_wah": farm.squareWah,
      "ngan": farm.ngan,
      "rai": farm.rai,
      "polygon": farm.polygon,
      "centroid_UTM": farm.centroidUTM,
      "centroid_GLO": farm.centroidGLO,
      "farm_img": farmImg
    });
    CommonResponse farmResponse = CommonResponse.fromJson(response);
    return ApiResult.success(farmResponse);
  } catch (e) {
    print("     e: " + e.toString());
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}
