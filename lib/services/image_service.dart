import 'package:http_parser/http_parser.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/image_response.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:dio/dio.dart';

var dio = Dio();
const String _apiKey = "";
const String _baseUrl = "";
DioClient dioClient = DioClient(dio, baseUrl: _baseUrl);
Future<ApiResult<String>> uploadImage(file, String userId) async {
  try {
    String fileName = file.path.split('/').last;
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        file.path,
        filename: fileName,
        contentType: new MediaType("image", "jpeg"),
      ),
    });

    final response = await dioClient.post(
      "users/profile-picture/upload/" + userId,
      data: formData,
    );

    return ApiResult.success(response);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<ImageModel>> fetchImage(String path) async {
  try {
    final response = await dioClient.post(
      "users/get-signed-url",
      data: {"path": path},
    );
    ImageModel? imgRes = ImageResponse.fromJson(response).data;

    return ApiResult.success(imgRes!);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<UploadResponse>> uploadImageUtils(
    file, String userId, String path) async {
  try {
    FormData formData = FormData.fromMap({
      "file": await MultipartFile.fromFile(
        file.path,
        contentType: new MediaType("image", "PNG"),
      ),
    });

    final response = await dioClient.post(
      path + userId,
      data: formData,
    );
    UploadResponse uploadRes = UploadResponse.fromJson(response);

    return ApiResult.success(uploadRes);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}
