import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:varun_kanna_app/model/common_response.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_widget_model/activity_fertilize_model.dart';
import 'package:varun_kanna_app/model/market_page_model/activity_widget_model/activity_model.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';

import '../utils/api/dio_clien.dart';
import '../utils/api/network_exceptions.dart';

class AcctivitiComponentsService {
  DioClient dioClient = DioClient(Dio(), baseUrl: "");
  Future<ApiResult<ActivityWidgetResponse>> fetchActivityCommonComponents(
      String activityId) async {
    try {
      final response =
          await dioClient.get("activity-components/activity_id/$activityId");

      ActivityWidgetResponse activityResponse =
          ActivityWidgetResponse.fromJson(response);
      log(json.encode(activityResponse.data));
      return ApiResult.success(activityResponse);
    } catch (e) {
      print("     e: " + e.toString());
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ActivityFertilizeWidgetResponse> fetchActivityFertComponents(
      String activityId) async {
    try {
      final response =
          await dioClient.get("activity-components/activity_id/$activityId");

      ActivityFertilizeWidgetResponse activityResponse =
          ActivityFertilizeWidgetResponse.fromJson(response);

      return activityResponse;
    } catch (e) {
      print("     e: " + e.toString());
      return ActivityFertilizeWidgetResponse(
          status: false, data: [], message: "500");
      // return NetworkExceptions.getDioException(e);
    }
  }

  Future<ApiResult<CommonResponse>> saveFormActivity(
      String activityId, List<ActivityResponseModel> model) async {
    try {
      model.sort(
        (a, b) => a.index.compareTo(b.index),
      );

      final response = await dioClient.put("activities", data: {
        "activity_id": activityId,
        "components": model,
      });
      log(json.encode(model));
      CommonResponse commonResponse = CommonResponse.fromJson(response);

      return ApiResult.success(commonResponse);
    } catch (e) {
      print("     e: " + e.toString());
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }

  Future<ApiResult<CommonResponse>> saveFormFertActivity(
      String activityId, List<List<ActivityFertResponseModel>> model) async {
    try {
      List<List<ActivityFertResponseModel>> temp = [];
      temp.addAll(model);
      temp.asMap().forEach((index, data) {
        temp[index].sort(
          (a, b) => a.index.compareTo(b.index),
        );

        temp[index].asMap().forEach((key, value) {
          if (value.inputType != "image_box") {
            value.uploadEndpoint = null;
            // temp[index][key].title = "${temp[index][key].title}  ";
          }
        });
      });
      log(json.encode(temp));
      final response = await dioClient.put("activities", data: {
        "activity_id": activityId,
        "components": temp,
      });

      CommonResponse commonResponse = CommonResponse.fromJson(response);

      return ApiResult.success(commonResponse);
    } catch (e) {
      print("     e: " + e.toString());
      return ApiResult.failure(NetworkExceptions.getDioException(e));
    }
  }
}
