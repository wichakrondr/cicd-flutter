import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/model/term_and_condition_response.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:dio/dio.dart';

var dio = Dio();
const String _apiKey = "";
const String _baseUrl = "";
DioClient dioClient = new DioClient(dio, baseUrl: _baseUrl);

Future<ApiResult<List<TermAndConditionModel>>> fetchTermAndCondition() async {
  try {
    final response = await dioClient.get("consents");
    List<TermAndConditionModel> TermAndConditionList =
        TermAndConditionResponse.fromJson(response).data;
    return ApiResult.success(TermAndConditionList);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<TermAndConditionModel>> fetchTermAndConditionDevice() async {
  try {
    final response = await dioClient.get("consents/device-consent");
    TermAndConditionModel TermAndConditionList =
        TermAndConditionDeviceResponse.fromJson(response).data;
    return ApiResult.success(TermAndConditionList);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<List<TermAndConditionModel>>> fetchTermAndConditionById(
    String version, String type) async {
  try {
    final response = await dioClient.get(
      "consents/by-version-type",
      queryParameters: {"version": version, "type": type},
    );
    List<TermAndConditionModel> TermAndConditionList =
        TermAndConditionResponse.fromJson(response).data;
    return ApiResult.success(TermAndConditionList);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<ConsentModel>> fetchCheckUserConsent(
    String tel, String type) async {
  try {
    final response = await dioClient.get(
      "consents/check-sign-consent",
      queryParameters: {"tel": tel, "type": type},
    );
    ConsentModel checkConsentResponse =
        CheckConsentResponse.fromJson(response).data;
    return ApiResult.success(checkConsentResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<TermAndConditionModel>> fetchCheckLastConsent(
    String type) async {
  try {
    final response = await dioClient.get(
      "consents/lasted-consent",
      queryParameters: {"type": type},
    );
    TermAndConditionModel checkConsentResponse =
        ConsentResponse.fromJson(response).data;
    return ApiResult.success(checkConsentResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}
