import 'package:varun_kanna_app/model/farm_page_model/land_recomendation_model/land_recomendation_model.dart';
import 'package:varun_kanna_app/utils/api/dio_clien.dart';
import 'package:varun_kanna_app/utils/api/api_result.dart';
import 'package:varun_kanna_app/utils/api/network_exceptions.dart';
import 'package:dio/dio.dart';

var dio = Dio();
const String _baseUrl = "";
DioClient dioClient = DioClient(dio, baseUrl: _baseUrl);

Future<ApiResult<List<RecommendationResponseModel>>>
    fetchRecommendationListService(
        String farmId, LandRecomendationType type) async {
  try {
    final response = await dioClient.get("farms/base-data",
        queryParameters: {"id": farmId, "type": type.name});
    List<RecommendationResponseModel> recommendationResponse =
        LandRecomnModel.fromJson(response).data;
    return ApiResult.success(recommendationResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<List<PlantModel>>> fetchPlantListService() async {
  try {
    final response = await dioClient.get(
      "projects/list-plants",
    );
    List<PlantModel> plantListResponse =
        PlantListResponse.fromJson(response).data;
    return ApiResult.success(plantListResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}

Future<ApiResult<List<PlantModel>>> fetchPlantListByProjectService(
    String project_name) async {
  try {
    final response = await dioClient.get("projects/list-plants/project_name",
        queryParameters: {"project_name": project_name});
    List<PlantModel> plantListResponse =
        PlantListResponse.fromJson(response).data;
    return ApiResult.success(plantListResponse);
  } catch (e) {
    return ApiResult.failure(NetworkExceptions.getDioException(e));
  }
}
