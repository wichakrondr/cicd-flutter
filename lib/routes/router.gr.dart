// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i12;

import '../views/farms/farm_list_page.dart' as _i6;
import '../views/home/details_list_page.dart' as _i10;
import '../views/home/details_page.dart' as _i9;
import '../views/home/home_page.dart' as _i8;
import '../views/main_page.dart' as _i3;
import '../views/market/market_page.dart' as _i7;
import '../views/profile/profile_page.dart' as _i11;
import '../views/term_and_condition_device_page.dart' as _i2;
import '../views/weather/weather_page.dart' as _i5;
import '../views/welcome_page.dart' as _i1;

class AppRoute extends _i4.RootStackRouter {
  AppRoute([_i12.GlobalKey<_i12.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    WelcomeRoute.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i1.WelcomePage());
    },
    TermAndConditionDeviceRoute.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i2.TermAndConditionDevicePage());
    },
    MainRoute.name: (routeData) {
      final args =
          routeData.argsAs<MainRouteArgs>(orElse: () => const MainRouteArgs());
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i3.MainPage(selectedPage: args.selectedPage, key: args.key));
    },
    HomeRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i4.EmptyRouterPage());
    },
    WeatherRouter.name: (routeData) {
      final args = routeData.argsAs<WeatherRouterArgs>(
          orElse: () => const WeatherRouterArgs());
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: _i5.WeatherPage(key: args.key));
    },
    FarmsRouter.name: (routeData) {
      final args = routeData.argsAs<FarmsRouterArgs>(
          orElse: () => const FarmsRouterArgs());
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: _i6.FarmListPage(key: args.key));
    },
    MarketRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i7.MarketPage());
    },
    ProfileRouter.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i4.EmptyRouterPage());
    },
    HomeRoute.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i8.HomePage());
    },
    DetailsRoute.name: (routeData) {
      final args = routeData.argsAs<DetailsRouteArgs>(
          orElse: () => const DetailsRouteArgs());
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData,
          child:
              _i9.DetailsPage(onFavPressed: args.onFavPressed, key: args.key));
    },
    DetailsListRoute.name: (routeData) {
      final args = routeData.argsAs<DetailsListRouteArgs>();
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData,
          child: _i10.DetailsListPage(title: args.title, key: args.key));
    },
    ProfileRoute.name: (routeData) {
      return _i4.MaterialPageX<dynamic>(
          routeData: routeData, child: const _i11.ProfilePage());
    }
  };

  @override
  List<_i4.RouteConfig> get routes => [
        _i4.RouteConfig(WelcomeRoute.name, path: '/'),
        _i4.RouteConfig(TermAndConditionDeviceRoute.name,
            path: '/termAndConditionDevicePage'),
        _i4.RouteConfig(MainRoute.name, path: '/main-page', children: [
          _i4.RouteConfig(HomeRouter.name,
              path: 'home',
              parent: MainRoute.name,
              children: [
                _i4.RouteConfig(HomeRoute.name,
                    path: '', parent: HomeRouter.name),
                _i4.RouteConfig(DetailsRoute.name,
                    path: 'onFavPressed', parent: HomeRouter.name),
                _i4.RouteConfig(DetailsListRoute.name,
                    path: 'title', parent: HomeRouter.name)
              ]),
          _i4.RouteConfig(WeatherRouter.name,
              path: 'weather', parent: MainRoute.name),
          _i4.RouteConfig(FarmsRouter.name,
              path: 'farms', parent: MainRoute.name),
          _i4.RouteConfig(MarketRouter.name,
              path: 'marker', parent: MainRoute.name),
          _i4.RouteConfig(ProfileRouter.name,
              path: 'profile',
              parent: MainRoute.name,
              children: [
                _i4.RouteConfig(ProfileRoute.name,
                    path: '', parent: ProfileRouter.name)
              ])
        ])
      ];
}

/// generated route for
/// [_i1.WelcomePage]
class WelcomeRoute extends _i4.PageRouteInfo<void> {
  const WelcomeRoute() : super(WelcomeRoute.name, path: '/');

  static const String name = 'WelcomeRoute';
}

/// generated route for
/// [_i2.TermAndConditionDevicePage]
class TermAndConditionDeviceRoute extends _i4.PageRouteInfo<void> {
  const TermAndConditionDeviceRoute()
      : super(TermAndConditionDeviceRoute.name,
            path: '/termAndConditionDevicePage');

  static const String name = 'TermAndConditionDeviceRoute';
}

/// generated route for
/// [_i3.MainPage]
class MainRoute extends _i4.PageRouteInfo<MainRouteArgs> {
  MainRoute(
      {int? selectedPage, _i12.Key? key, List<_i4.PageRouteInfo>? children})
      : super(MainRoute.name,
            path: '/main-page',
            args: MainRouteArgs(selectedPage: selectedPage, key: key),
            initialChildren: children);

  static const String name = 'MainRoute';
}

class MainRouteArgs {
  const MainRouteArgs({this.selectedPage, this.key});

  final int? selectedPage;

  final _i12.Key? key;

  @override
  String toString() {
    return 'MainRouteArgs{selectedPage: $selectedPage, key: $key}';
  }
}

/// generated route for
/// [_i4.EmptyRouterPage]
class HomeRouter extends _i4.PageRouteInfo<void> {
  const HomeRouter({List<_i4.PageRouteInfo>? children})
      : super(HomeRouter.name, path: 'home', initialChildren: children);

  static const String name = 'HomeRouter';
}

/// generated route for
/// [_i5.WeatherPage]
class WeatherRouter extends _i4.PageRouteInfo<WeatherRouterArgs> {
  WeatherRouter({_i12.Key? key})
      : super(WeatherRouter.name,
            path: 'weather', args: WeatherRouterArgs(key: key));

  static const String name = 'WeatherRouter';
}

class WeatherRouterArgs {
  const WeatherRouterArgs({this.key});

  final _i12.Key? key;

  @override
  String toString() {
    return 'WeatherRouterArgs{key: $key}';
  }
}

/// generated route for
/// [_i6.FarmListPage]
class FarmsRouter extends _i4.PageRouteInfo<FarmsRouterArgs> {
  FarmsRouter({_i12.Key? key})
      : super(FarmsRouter.name, path: 'farms', args: FarmsRouterArgs(key: key));

  static const String name = 'FarmsRouter';
}

class FarmsRouterArgs {
  const FarmsRouterArgs({this.key});

  final _i12.Key? key;

  @override
  String toString() {
    return 'FarmsRouterArgs{key: $key}';
  }
}

/// generated route for
/// [_i7.MarketPage]
class MarketRouter extends _i4.PageRouteInfo<void> {
  const MarketRouter() : super(MarketRouter.name, path: 'marker');

  static const String name = 'MarketRouter';
}

/// generated route for
/// [_i4.EmptyRouterPage]
class ProfileRouter extends _i4.PageRouteInfo<void> {
  const ProfileRouter({List<_i4.PageRouteInfo>? children})
      : super(ProfileRouter.name, path: 'profile', initialChildren: children);

  static const String name = 'ProfileRouter';
}

/// generated route for
/// [_i8.HomePage]
class HomeRoute extends _i4.PageRouteInfo<void> {
  const HomeRoute() : super(HomeRoute.name, path: '');

  static const String name = 'HomeRoute';
}

/// generated route for
/// [_i9.DetailsPage]
class DetailsRoute extends _i4.PageRouteInfo<DetailsRouteArgs> {
  DetailsRoute({dynamic Function()? onFavPressed, _i12.Key? key})
      : super(DetailsRoute.name,
            path: 'onFavPressed',
            args: DetailsRouteArgs(onFavPressed: onFavPressed, key: key));

  static const String name = 'DetailsRoute';
}

class DetailsRouteArgs {
  const DetailsRouteArgs({this.onFavPressed, this.key});

  final dynamic Function()? onFavPressed;

  final _i12.Key? key;

  @override
  String toString() {
    return 'DetailsRouteArgs{onFavPressed: $onFavPressed, key: $key}';
  }
}

/// generated route for
/// [_i10.DetailsListPage]
class DetailsListRoute extends _i4.PageRouteInfo<DetailsListRouteArgs> {
  DetailsListRoute({required String title, _i12.Key? key})
      : super(DetailsListRoute.name,
            path: 'title', args: DetailsListRouteArgs(title: title, key: key));

  static const String name = 'DetailsListRoute';
}

class DetailsListRouteArgs {
  const DetailsListRouteArgs({required this.title, this.key});

  final String title;

  final _i12.Key? key;

  @override
  String toString() {
    return 'DetailsListRouteArgs{title: $title, key: $key}';
  }
}

/// generated route for
/// [_i11.ProfilePage]
class ProfileRoute extends _i4.PageRouteInfo<void> {
  const ProfileRoute() : super(ProfileRoute.name, path: '');

  static const String name = 'ProfileRoute';
}
