import 'package:auto_route/auto_route.dart';
import 'package:varun_kanna_app/views/farms/farm_list_page.dart';
import 'package:varun_kanna_app/views/home/details_list_page.dart';
import 'package:varun_kanna_app/views/home/details_page.dart';
import 'package:varun_kanna_app/views/home/home_page.dart';
import 'package:varun_kanna_app/views/main_page.dart';
import 'package:varun_kanna_app/views/market/market_page.dart';
import 'package:varun_kanna_app/views/profile/login_page.dart';
import 'package:varun_kanna_app/views/profile/profile_page.dart';
import 'package:varun_kanna_app/views/term_and_condition_device_page.dart';
import 'package:varun_kanna_app/views/weather/weather_page.dart';
import 'package:varun_kanna_app/views/welcome_page.dart';
import 'package:varun_kanna_app/views/profile/login/login_with_phoneNumber.dart';

@MaterialAutoRouter(replaceInRouteName: 'Page,Route', routes: <AutoRoute>[
  AutoRoute(path: '/', page: WelcomePage),
  AutoRoute(
      path: '/termAndConditionDevicePage', page: TermAndConditionDevicePage),
  AutoRoute(page: MainPage, children: [
    AutoRoute(
      path: 'home',
      name: 'HomeRouter',
      page: EmptyRouterPage,
      children: [
        AutoRoute(path: '', page: HomePage),
        AutoRoute(path: 'onFavPressed', page: DetailsPage),
        AutoRoute(path: 'title', page: DetailsListPage),
      ],
    ),
    AutoRoute(
      path: 'weather',
      name: 'WeatherRouter',
      page: WeatherPage,
    ),
    AutoRoute(
      path: 'farms',
      name: 'FarmsRouter',
      page: FarmListPage,
    ),
    AutoRoute(
      path: 'marker',
      name: 'MarketRouter',
      page: MarketPage,
    ),
    AutoRoute(
        path: 'profile',
        name: 'ProfileRouter',
        page: EmptyRouterPage,
        children: [
          AutoRoute(path: '', page: ProfilePage),
        ])
  ])
])
class $AppRoute {}
