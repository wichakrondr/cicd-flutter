// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'term_and_condition_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TermAndConditionResponse _$TermAndConditionResponseFromJson(
        Map<String, dynamic> json) =>
    TermAndConditionResponse(
      json['response_model'] == null
          ? null
          : ResponseModel.fromJson(
              json['response_model'] as Map<String, dynamic>),
      (json['data'] as List<dynamic>)
          .map((e) => TermAndConditionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$TermAndConditionResponseToJson(
        TermAndConditionResponse instance) =>
    <String, dynamic>{
      'response_model': instance.responseModel,
      'data': instance.data,
    };

TermAndConditionDeviceResponse _$TermAndConditionDeviceResponseFromJson(
        Map<String, dynamic> json) =>
    TermAndConditionDeviceResponse(
      json['response_model'] == null
          ? null
          : ResponseModel.fromJson(
              json['response_model'] as Map<String, dynamic>),
      TermAndConditionModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TermAndConditionDeviceResponseToJson(
        TermAndConditionDeviceResponse instance) =>
    <String, dynamic>{
      'response_model': instance.responseModel,
      'data': instance.data,
    };

TermAndConditionModel _$TermAndConditionModelFromJson(
        Map<String, dynamic> json) =>
    TermAndConditionModel(
      id: json['id'] as String?,
      version: json['version'] as String?,
      created_at: json['created_at'] as String?,
      type: json['type'] as String?,
      title: json['title'] as String?,
      content: json['content'] as String?,
    );

Map<String, dynamic> _$TermAndConditionModelToJson(
        TermAndConditionModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'version': instance.version,
      'created_at': instance.created_at,
      'type': instance.type,
      'title': instance.title,
      'content': instance.content,
    };

CheckConsentResponse _$CheckConsentResponseFromJson(
        Map<String, dynamic> json) =>
    CheckConsentResponse(
      json['response_model'] == null
          ? null
          : ResponseModel.fromJson(
              json['response_model'] as Map<String, dynamic>),
      ConsentModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CheckConsentResponseToJson(
        CheckConsentResponse instance) =>
    <String, dynamic>{
      'response_model': instance.responseModel,
      'data': instance.data,
    };

ConsentModel _$ConsentModelFromJson(Map<String, dynamic> json) => ConsentModel(
      consentsCurrentVersion: json['consentsCurrentVersion'] as String?,
      userSignVersion: json['userSignVersion'] as String?,
      isUserSignCurrentVersion:
          json['isUserSignCurrentVersion'] as bool? ?? false,
    );

Map<String, dynamic> _$ConsentModelToJson(ConsentModel instance) =>
    <String, dynamic>{
      'consentsCurrentVersion': instance.consentsCurrentVersion,
      'userSignVersion': instance.userSignVersion,
      'isUserSignCurrentVersion': instance.isUserSignCurrentVersion,
    };

ConsentResponse _$ConsentResponseFromJson(Map<String, dynamic> json) =>
    ConsentResponse(
      json['response_model'] == null
          ? null
          : ResponseModel.fromJson(
              json['response_model'] as Map<String, dynamic>),
      TermAndConditionModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ConsentResponseToJson(ConsentResponse instance) =>
    <String, dynamic>{
      'response_model': instance.responseModel,
      'data': instance.data,
    };
