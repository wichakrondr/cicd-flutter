// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'farm_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FarmResponse _$FarmResponseFromJson(Map<String, dynamic> json) => FarmResponse(
      json['status'] as bool?,
      json['message'] as String?,
      json['data'] == null
          ? null
          : FarmModelRes.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FarmResponseToJson(FarmResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

FarmModelRes _$FarmModelResFromJson(Map<String, dynamic> json) => FarmModelRes(
      id: json['id'] as String?,
      path: json['path'] as String?,
    );

Map<String, dynamic> _$FarmModelResToJson(FarmModelRes instance) =>
    <String, dynamic>{
      'id': instance.id,
      'path': instance.path,
    };

FarmModel _$FarmModelFromJson(Map<String, dynamic> json) => FarmModel(
      userId: json['user_id'] as String,
      farmName: json['farm_name'] as String,
      centroidUTM:
          UtmModel.fromJson(json['centroid_UTM'] as Map<String, dynamic>),
      centroidGLO:
          LatLngModel.fromJson(json['centroid_GLO'] as Map<String, dynamic>),
      squareWah: (json['square_wah'] as num).toDouble(),
      ngan: json['ngan'] as int,
      rai: json['rai'] as int,
      polygon: json['polygon'] as List<dynamic>,
      farmId: json['farm_id'] as String?,
    );

Map<String, dynamic> _$FarmModelToJson(FarmModel instance) => <String, dynamic>{
      'user_id': instance.userId,
      'farm_name': instance.farmName,
      'centroid_UTM': instance.centroidUTM,
      'centroid_GLO': instance.centroidGLO,
      'square_wah': instance.squareWah,
      'ngan': instance.ngan,
      'rai': instance.rai,
      'polygon': instance.polygon,
      'farm_id': instance.farmId,
    };

LatLngModel _$LatLngModelFromJson(Map<String, dynamic> json) => LatLngModel(
      lat: (json['lat'] as num).toDouble(),
      lng: (json['lng'] as num).toDouble(),
    );

Map<String, dynamic> _$LatLngModelToJson(LatLngModel instance) =>
    <String, dynamic>{
      'lat': instance.lat,
      'lng': instance.lng,
    };

UtmModel _$UtmModelFromJson(Map<String, dynamic> json) => UtmModel(
      grid: json['grid'] as String,
      zone: json['zone'] as int,
      x: (json['x'] as num).toDouble(),
      y: (json['y'] as num).toDouble(),
    );

Map<String, dynamic> _$UtmModelToJson(UtmModel instance) => <String, dynamic>{
      'grid': instance.grid,
      'zone': instance.zone,
      'x': instance.x,
      'y': instance.y,
    };

FarmDetailResponse _$FarmDetailResponseFromJson(Map<String, dynamic> json) =>
    FarmDetailResponse(
      json['status'] as bool?,
      json['message'] as String?,
      (json['data'] as List<dynamic>)
          .map((e) => e == null
              ? null
              : FarmDetailModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FarmDetailResponseToJson(FarmDetailResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

FarmDetailModel _$FarmDetailModelFromJson(Map<String, dynamic> json) =>
    FarmDetailModel(
      id: json['id'] as String?,
      farmName: json['farm_name'] as String?,
      centroidGLO: json['centroid_GLO'] == null
          ? null
          : LatLngModel.fromJson(json['centroid_GLO'] as Map<String, dynamic>),
      centroidUTM: json['centroid_UTM'] == null
          ? null
          : UtmModel.fromJson(json['centroid_UTM'] as Map<String, dynamic>),
      displayArea: json['display_area'] as String?,
      refCode: json['ref_code'] as String?,
      polygon: json['polygon'] as List<dynamic>?,
    );

Map<String, dynamic> _$FarmDetailModelToJson(FarmDetailModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'farm_name': instance.farmName,
      'centroid_GLO': instance.centroidGLO,
      'centroid_UTM': instance.centroidUTM,
      'display_area': instance.displayArea,
      'ref_code': instance.refCode,
      'polygon': instance.polygon,
    };

PlaceSearch _$PlaceSearchFromJson(Map<String, dynamic> json) => PlaceSearch(
      description: json['description'] as String?,
      placeId: json['place_id'] as String?,
    );

Map<String, dynamic> _$PlaceSearchToJson(PlaceSearch instance) =>
    <String, dynamic>{
      'description': instance.description,
      'place_id': instance.placeId,
    };

Location _$LocationFromJson(Map<String, dynamic> json) => Location(
      lat: (json['lat'] as num?)?.toDouble(),
      lng: (json['lng'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$LocationToJson(Location instance) => <String, dynamic>{
      'lat': instance.lat,
      'lng': instance.lng,
    };

Geometry _$GeometryFromJson(Map<String, dynamic> json) => Geometry(
      location: json['location'] == null
          ? null
          : Location.fromJson(json['location'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GeometryToJson(Geometry instance) => <String, dynamic>{
      'location': instance.location,
    };

Place _$PlaceFromJson(Map<String, dynamic> json) => Place(
      geometry: json['geometry'] == null
          ? null
          : Geometry.fromJson(json['geometry'] as Map<String, dynamic>),
      name: json['name'] as String?,
    );

Map<String, dynamic> _$PlaceToJson(Place instance) => <String, dynamic>{
      'geometry': instance.geometry,
      'name': instance.name,
    };

LocationDetail _$LocationDetailFromJson(Map<String, dynamic> json) =>
    LocationDetail(
      formatted_address: json['formatted_address'] as String?,
    );

Map<String, dynamic> _$LocationDetailToJson(LocationDetail instance) =>
    <String, dynamic>{
      'formatted_address': instance.formatted_address,
    };

LocationDetailRes _$LocationDetailResFromJson(Map<String, dynamic> json) =>
    LocationDetailRes(
      results: (json['results'] as List<dynamic>)
          .map((e) => LocationDetail.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LocationDetailResToJson(LocationDetailRes instance) =>
    <String, dynamic>{
      'results': instance.results,
    };
