// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'profile_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileResponse _$ProfileResponseFromJson(Map<String, dynamic> json) =>
    ProfileResponse(
      json['status'] as bool?,
      json['message'] as String?,
      ProfileModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProfileResponseToJson(ProfileResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

ProfileModel _$ProfileModelFromJson(Map<String, dynamic> json) => ProfileModel(
      firstName: json['firstname'] as String?,
      lastName: json['lastname'] as String?,
      id: json['id'] as String?,
      imageName: json['image_name'] as String?,
      birthDay: json['birthday'] as String?,
      email: json['email'] as String?,
      address: json['addresses'] == null
          ? null
          : AddressModel.fromJson(json['addresses'] as Map<String, dynamic>),
      province: json['province'] as String?,
      district: json['district'] as String?,
      subDistrict: json['sub_district'] as String?,
      postCode: json['post_code'] as String?,
      imgPath: json['img_path'] as String?,
      url_img: json['url_img'] as String?,
      tel: json['tel'] as String?,
      consentId: json['consentId'] as String?,
      consentDeviceId: json['consentDeviceId'] as String?,
      deviceId: json['deviceId'] as String?,
      isAcceptedLegalAge: json['is_accepted_legal_age'] as bool?,
    );

Map<String, dynamic> _$ProfileModelToJson(ProfileModel instance) =>
    <String, dynamic>{
      'firstname': instance.firstName,
      'lastname': instance.lastName,
      'id': instance.id,
      'image_name': instance.imageName,
      'birthday': instance.birthDay,
      'email': instance.email,
      'addresses': instance.address,
      'province': instance.province,
      'district': instance.district,
      'sub_district': instance.subDistrict,
      'post_code': instance.postCode,
      'img_path': instance.imgPath,
      'url_img': instance.url_img,
      'tel': instance.tel,
      'consentId': instance.consentId,
      'consentDeviceId': instance.consentDeviceId,
      'deviceId': instance.deviceId,
      'is_accepted_legal_age': instance.isAcceptedLegalAge,
    };

ProfileDeleteResponse _$ProfileDeleteResponseFromJson(
        Map<String, dynamic> json) =>
    ProfileDeleteResponse(
      json['status'] as bool?,
      json['message'] as String?,
    );

Map<String, dynamic> _$ProfileDeleteResponseToJson(
        ProfileDeleteResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
    };

AddressModel _$AddressModelFromJson(Map<String, dynamic> json) => AddressModel(
      address: json['address'] as String?,
      district: json['district'] as String?,
      subdistrict: json['subdistrict'] as String?,
      province: json['province'] as String?,
      postcode: json['postcode'] as String?,
    );

Map<String, dynamic> _$AddressModelToJson(AddressModel instance) =>
    <String, dynamic>{
      'address': instance.address,
      'district': instance.district,
      'subdistrict': instance.subdistrict,
      'province': instance.province,
      'postcode': instance.postcode,
    };
