import 'package:json_annotation/json_annotation.dart';
import 'package:varun_kanna_app/model/response_model.dart';
part 'term_and_condition_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class TermAndConditionResponse {
  ResponseModel? responseModel;
  List<TermAndConditionModel> data;

  TermAndConditionResponse(
    this.responseModel,
    this.data,
  );

  factory TermAndConditionResponse.fromJson(Map<String, dynamic> json) {
    return _$TermAndConditionResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TermAndConditionResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class TermAndConditionDeviceResponse {
  ResponseModel? responseModel;
  TermAndConditionModel data;

  TermAndConditionDeviceResponse(
    this.responseModel,
    this.data,
  );

  factory TermAndConditionDeviceResponse.fromJson(Map<String, dynamic> json) {
    return _$TermAndConditionDeviceResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$TermAndConditionDeviceResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class TermAndConditionModel {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'version')
  String? version;
  @JsonKey(name: 'created_at')
  String? created_at;
  @JsonKey(name: 'type')
  String? type;
  @JsonKey(name: 'title')
  String? title;
  @JsonKey(name: 'content')
  String? content;

  TermAndConditionModel({
    this.id,
    this.version,
    this.created_at,
    this.type,
    this.title,
    this.content,
  });

  factory TermAndConditionModel.fromJson(Map<String, dynamic> json) =>
      _$TermAndConditionModelFromJson(json);

  Map<String, dynamic> toJson() => _$TermAndConditionModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class CheckConsentResponse {
  ResponseModel? responseModel;
  ConsentModel data;

  CheckConsentResponse(
    this.responseModel,
    this.data,
  );

  factory CheckConsentResponse.fromJson(Map<String, dynamic> json) {
    return _$CheckConsentResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$CheckConsentResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ConsentModel {
  @JsonKey(name: 'consentsCurrentVersion')
  String? consentsCurrentVersion;
  @JsonKey(name: 'userSignVersion')
  String? userSignVersion;
  @JsonKey(name: 'isUserSignCurrentVersion')
  bool? isUserSignCurrentVersion;

  ConsentModel({
    this.consentsCurrentVersion,
    this.userSignVersion,
    this.isUserSignCurrentVersion = false,
  });

  factory ConsentModel.fromJson(Map<String, dynamic> json) =>
      _$ConsentModelFromJson(json);

  Map<String, dynamic> toJson() => _$ConsentModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ConsentResponse {
  ResponseModel? responseModel;
  TermAndConditionModel data;

  ConsentResponse(
    this.responseModel,
    this.data,
  );

  factory ConsentResponse.fromJson(Map<String, dynamic> json) {
    return _$ConsentResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ConsentResponseToJson(this);
}
