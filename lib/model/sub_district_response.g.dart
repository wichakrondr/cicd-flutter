// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_district_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubDistrictResponse _$SubDistrictResponseFromJson(Map<String, dynamic> json) =>
    SubDistrictResponse(
      json['status'] as bool?,
      json['message'] as String?,
      (json['data'] as List<dynamic>)
          .map((e) => SubDistrictModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SubDistrictResponseToJson(
        SubDistrictResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

SubDistrictModel _$SubDistrictModelFromJson(Map<String, dynamic> json) =>
    SubDistrictModel(
      id: json['id'] as int?,
      name_th: json['name_th'] as String?,
      name_en: json['name_en'] as String?,
      amphure_id: json['amphure_id'] as int?,
      zip_code: json['zip_code'] as int?,
    );

Map<String, dynamic> _$SubDistrictModelToJson(SubDistrictModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name_th': instance.name_th,
      'name_en': instance.name_en,
      'amphure_id': instance.amphure_id,
      'zip_code': instance.zip_code,
    };
