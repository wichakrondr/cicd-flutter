// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'farm_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JoinProjectFarmListResponse _$JoinProjectFarmListResponseFromJson(
        Map<String, dynamic> json) =>
    JoinProjectFarmListResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) =>
              JoinProjectFarmListModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$JoinProjectFarmListResponseToJson(
        JoinProjectFarmListResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

JoinProjectFarmListModel _$JoinProjectFarmListModelFromJson(
        Map<String, dynamic> json) =>
    JoinProjectFarmListModel(
      id: json['id'] as String?,
      farmName: json['farm_name'] as String?,
      displayArea: json['display_area'] as String?,
      farmImg: json['farm_img'] as String?,
      isSelected: json['is_selected'] as bool? ?? false,
    );

Map<String, dynamic> _$JoinProjectFarmListModelToJson(
        JoinProjectFarmListModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'farm_name': instance.farmName,
      'display_area': instance.displayArea,
      'farm_img': instance.farmImg,
      'is_selected': instance.isSelected,
    };
