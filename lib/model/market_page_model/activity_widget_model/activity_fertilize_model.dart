import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';

import 'unint8list_converter.dart';
part 'activity_fertilize_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ActivityFertilizeWidgetResponse {
  ActivityFertilizeWidgetResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  List<dynamic>? data;

  factory ActivityFertilizeWidgetResponse.fromJson(Map<String, dynamic> json) {
    return _$ActivityFertilizeWidgetResponseFromJson(json);
  }

  Map<String, dynamic> toJson() =>
      _$ActivityFertilizeWidgetResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ActivityFertResponseModel {
  ActivityFertResponseModel({
    required this.inputType,
    required this.title,
    required this.index,
    this.value = '',
    this.value2 = '',
    this.value3 = '',
    this.unitValue = "",
    this.placeHolder,
    this.disable = false,
    this.isRequired = false,
    this.autoFocus = false,
    this.items = const [],
    this.isValidate,
    this.errorText = '',
    this.uploadEndpoint = '',
    this.signedUrl = '',
    this.dataStamp = '',
    this.lat = '',
    this.lng = '',
  });

  String inputType;
  int index;
  String title;
  String? placeHolder;
  String value;
  String? value2;
  String? value3;
  String? unitValue;
  bool disable;
  bool isRequired;
  bool autoFocus;
  String errorText;
  bool? isValidate;
  List<ItemFertResponseModel>? items = [];
  String? uploadEndpoint;
  String? signedUrl;
  String? lat;
  String? lng;
  String? dataStamp;
  factory ActivityFertResponseModel.fromJson(Map<String, dynamic> json) {
    return _$ActivityFertResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ActivityFertResponseModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ItemFertResponseModel {
  ItemFertResponseModel({
    this.id,
    this.nameTh,
  });

  int? id;
  String? nameTh;
  factory ItemFertResponseModel.fromJson(Map<String, dynamic> json) {
    return _$ItemFertResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ItemFertResponseModelToJson(this);
}
