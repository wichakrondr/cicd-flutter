// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivityWidgetResponse _$ActivityWidgetResponseFromJson(
        Map<String, dynamic> json) =>
    ActivityWidgetResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map(
              (e) => ActivityResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ActivityWidgetResponseToJson(
        ActivityWidgetResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

ActivityResponseModel _$ActivityResponseModelFromJson(
        Map<String, dynamic> json) =>
    ActivityResponseModel(
      inputType: json['input_type'] as String,
      title: json['title'] as String,
      index: json['index'] as int,
      id: json['id'] as String,
      value: json['value'] as String? ?? '',
      value2: json['value2'] as String? ?? '',
      value3: json['value3'] as String? ?? '',
      unitValue: json['unit_value'] as String? ?? "",
      placeHolder: json['place_holder'] as String? ?? '',
      disable: json['disable'] as bool? ?? false,
      isRequired: json['is_required'] as bool? ?? false,
      autoFocus: json['auto_focus'] as bool? ?? false,
      items: (json['items'] as List<dynamic>?)
          ?.map((e) => ItemResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      isValidate: json['is_validate'] as bool?,
      errorText: json['error_text'] as String? ?? '',
      uploadEndpoint: json['upload_endpoint'] as String?,
      signedUrl: json['signed_url'] as String? ?? '',
      dataStamp: json['data_stamp'] as String? ?? '',
      lat: json['lat'] as String? ?? '',
      lng: json['lng'] as String? ?? '',
    );

Map<String, dynamic> _$ActivityResponseModelToJson(
        ActivityResponseModel instance) =>
    <String, dynamic>{
      'input_type': instance.inputType,
      'index': instance.index,
      'id': instance.id,
      'title': instance.title,
      'place_holder': instance.placeHolder,
      'value': instance.value,
      'value2': instance.value2,
      'value3': instance.value3,
      'unit_value': instance.unitValue,
      'disable': instance.disable,
      'is_required': instance.isRequired,
      'auto_focus': instance.autoFocus,
      'error_text': instance.errorText,
      'is_validate': instance.isValidate,
      'items': instance.items,
      'upload_endpoint': instance.uploadEndpoint,
      'signed_url': instance.signedUrl,
      'lat': instance.lat,
      'lng': instance.lng,
      'data_stamp': instance.dataStamp,
    };

ItemResponseModel _$ItemResponseModelFromJson(Map<String, dynamic> json) =>
    ItemResponseModel(
      id: json['id'] as int?,
      nameTh: json['name_th'] as String?,
    );

Map<String, dynamic> _$ItemResponseModelToJson(ItemResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name_th': instance.nameTh,
    };
