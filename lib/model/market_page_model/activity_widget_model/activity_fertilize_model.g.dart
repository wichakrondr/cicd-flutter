// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_fertilize_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivityFertilizeWidgetResponse _$ActivityFertilizeWidgetResponseFromJson(
        Map<String, dynamic> json) =>
    ActivityFertilizeWidgetResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: json['data'] as List<dynamic>?,
    );

Map<String, dynamic> _$ActivityFertilizeWidgetResponseToJson(
        ActivityFertilizeWidgetResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

ActivityFertResponseModel _$ActivityFertResponseModelFromJson(
        Map<String, dynamic> json) =>
    ActivityFertResponseModel(
      inputType: json['input_type'] as String,
      title: json['title'] as String,
      index: json['index'] as int,
      value: json['value'] as String? ?? '',
      value2: json['value2'] as String? ?? '',
      value3: json['value3'] as String? ?? '',
      unitValue: json['unit_value'] as String? ?? "",
      placeHolder: json['place_holder'] as String?,
      disable: json['disable'] as bool? ?? false,
      isRequired: json['is_required'] as bool? ?? false,
      autoFocus: json['auto_focus'] as bool? ?? false,
      items: (json['items'] as List<dynamic>?)
              ?.map((e) =>
                  ItemFertResponseModel.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      isValidate: json['is_validate'] as bool?,
      errorText: json['error_text'] as String? ?? '',
      uploadEndpoint: json['upload_endpoint'] as String? ?? '',
      signedUrl: json['signed_url'] as String? ?? '',
      dataStamp: json['data_stamp'] as String? ?? '',
      lat: json['lat'] as String? ?? '',
      lng: json['lng'] as String? ?? '',
    );

Map<String, dynamic> _$ActivityFertResponseModelToJson(
        ActivityFertResponseModel instance) =>
    <String, dynamic>{
      'input_type': instance.inputType,
      'index': instance.index,
      'title': instance.title,
      'place_holder': instance.placeHolder,
      'value': instance.value,
      'value2': instance.value2,
      'value3': instance.value3,
      'unit_value': instance.unitValue,
      'disable': instance.disable,
      'is_required': instance.isRequired,
      'auto_focus': instance.autoFocus,
      'error_text': instance.errorText,
      'is_validate': instance.isValidate,
      'items': instance.items,
      'upload_endpoint': instance.uploadEndpoint,
      'signed_url': instance.signedUrl,
      'lat': instance.lat,
      'lng': instance.lng,
      'data_stamp': instance.dataStamp,
    };

ItemFertResponseModel _$ItemFertResponseModelFromJson(
        Map<String, dynamic> json) =>
    ItemFertResponseModel(
      id: json['id'] as int?,
      nameTh: json['name_th'] as String?,
    );

Map<String, dynamic> _$ItemFertResponseModelToJson(
        ItemFertResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name_th': instance.nameTh,
    };
