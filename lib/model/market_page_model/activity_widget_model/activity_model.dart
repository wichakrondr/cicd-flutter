import 'dart:typed_data';

import 'package:json_annotation/json_annotation.dart';

import 'unint8list_converter.dart';
part 'activity_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ActivityWidgetResponse {
  ActivityWidgetResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  List<ActivityResponseModel>? data;

  factory ActivityWidgetResponse.fromJson(Map<String, dynamic> json) {
    return _$ActivityWidgetResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ActivityWidgetResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ActivityResponseModel {
  ActivityResponseModel({
    required this.inputType,
    required this.title,
    required this.index,
    required this.id,
    this.value = '',
    this.value2 = '',
    this.value3 = '',
    this.unitValue = "",
    this.placeHolder = '',
    this.disable = false,
    this.isRequired = false,
    this.autoFocus = false,
    this.items,
    this.isValidate,
    this.errorText = '',
    this.uploadEndpoint,
    this.signedUrl = '',
    this.dataStamp = '',
    this.lat = '',
    this.lng = '',
  });

  String inputType;
  int index;
  String id;
  String title;
  String? placeHolder;
  String value;
  String? value2;
  String? value3;
  String? unitValue;
  bool disable;
  bool isRequired;
  bool autoFocus;
  String errorText;
  bool? isValidate;
  List<ItemResponseModel>? items;
  String? uploadEndpoint;
  String? signedUrl;
  String? lat;
  String? lng;
  String? dataStamp;
  factory ActivityResponseModel.fromJson(Map<String, dynamic> json) {
    return _$ActivityResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ActivityResponseModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ItemResponseModel {
  ItemResponseModel({
    this.id,
    this.nameTh,
  });

  int? id;
  String? nameTh;
  factory ItemResponseModel.fromJson(Map<String, dynamic> json) {
    return _$ItemResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ItemResponseModelToJson(this);
}
