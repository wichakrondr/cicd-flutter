// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verify_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerifyResponse _$VerifyResponseFromJson(Map<String, dynamic> json) =>
    VerifyResponse(
      json['status'] as bool?,
      json['message'] as String?,
      (json['data'] as List<dynamic>?)
          ?.map((e) => VerifyResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$VerifyResponseToJson(VerifyResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

VerifyResponseModel _$VerifyResponseModelFromJson(Map<String, dynamic> json) =>
    VerifyResponseModel(
      personalId: json['personal_id'] as String,
      userId: json['user_id'] as String,
      reqJoinProjectId: json['req_join_project_id'] as String,
      projectId: json['project_id'] as String,
      firstName: json['first_name'] as String?,
      lastName: json['last_name'] as String?,
      idCardImgs: (json['id_card_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      idCardImgs_non_sign: (json['id_card_imgs_non_sign'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      copyHouseRegistrationImgs:
          (json['copy_house_registration_imgs'] as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      copyHouseRegistrationImgs_non_sign:
          (json['copy_house_registration_imgs_non_sign'] as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      typeOfRiceCultivation: json['type_of_rice_cultivation'] as String?,
      typeOfFarming: json['type_of_farming'] as String?,
      landId: json['land_id'] as String?,
      landRights: json['land_rights'] as String?,
      typeOfCertificateOwnership:
          json['type_of_certificate_ownership'] as String?,
      titleDeedNumber: json['title_deed_number'] as String?,
      province: json['province'] as String?,
      district: json['district'] as String?,
      subDistrict: json['sub_district'] as String?,
      ngan: json['ngan'] as int?,
      rai: json['rai'] as int?,
      squareWah: json['square_wah'] as String?,
      letterConsentParticipateProjectImgs:
          (json['letter_consent_participate_project_imgs'] as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      letterConsentParticipateProjectImgs_non_sign:
          (json['letter_consent_participate_project_imgs_non_sign']
                      as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      certificateLandUtilizationImgs:
          (json['certificate_land_utilization_imgs'] as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      certificateLandUtilizationImgs_non_sign:
          (json['certificate_land_utilization_imgs_non_sign'] as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      titleDeedImgs: (json['title_deed_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      titleDeedImgs_non_sign:
          (json['title_deed_imgs_non_sign'] as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      farmBookImgs: (json['farm_book_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      farmBookImgs_non_sign: (json['farm_book_imgs_non_sign'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      landBookImgs: (json['land_book_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      landBookImgs_non_sign: (json['land_book_imgs_non_sign'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      memberBookImgs: (json['member_book_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
      memberBookImgs_non_sign:
          (json['member_book_imgs_non_sign'] as List<dynamic>?)
                  ?.map((e) => e as String)
                  .toList() ??
              const [],
      plant: json['plant'] as String?,
      join: json['join'] as String?,
      cultivationId: json['cultivation_id'] as String?,
      irrigationArea: json['irrigation_area'] as String?,
      burnCultivationAreaV: json['burn_cultivation_area_v'] as String?,
      expectedDatePlantingV: json['expected_date_planting_v'] as String?,
      expectedDateHarvestV: json['expected_date_harvest_v'] as String?,
      startAtRiceAwd: json['start_at_rice_awd'] as String?,
      remark: json['remark'] as String?,
    );

Map<String, dynamic> _$VerifyResponseModelToJson(
        VerifyResponseModel instance) =>
    <String, dynamic>{
      'personal_id': instance.personalId,
      'user_id': instance.userId,
      'req_join_project_id': instance.reqJoinProjectId,
      'project_id': instance.projectId,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'id_card_imgs': instance.idCardImgs,
      'id_card_imgs_non_sign': instance.idCardImgs_non_sign,
      'copy_house_registration_imgs': instance.copyHouseRegistrationImgs,
      'copy_house_registration_imgs_non_sign':
          instance.copyHouseRegistrationImgs_non_sign,
      'land_id': instance.landId,
      'land_rights': instance.landRights,
      'type_of_certificate_ownership': instance.typeOfCertificateOwnership,
      'title_deed_number': instance.titleDeedNumber,
      'province': instance.province,
      'district': instance.district,
      'sub_district': instance.subDistrict,
      'ngan': instance.ngan,
      'rai': instance.rai,
      'square_wah': instance.squareWah,
      'letter_consent_participate_project_imgs':
          instance.letterConsentParticipateProjectImgs,
      'letter_consent_participate_project_imgs_non_sign':
          instance.letterConsentParticipateProjectImgs_non_sign,
      'certificate_land_utilization_imgs':
          instance.certificateLandUtilizationImgs,
      'certificate_land_utilization_imgs_non_sign':
          instance.certificateLandUtilizationImgs_non_sign,
      'title_deed_imgs': instance.titleDeedImgs,
      'title_deed_imgs_non_sign': instance.titleDeedImgs_non_sign,
      'farm_book_imgs': instance.farmBookImgs,
      'farm_book_imgs_non_sign': instance.farmBookImgs_non_sign,
      'land_book_imgs': instance.landBookImgs,
      'land_book_imgs_non_sign': instance.landBookImgs_non_sign,
      'member_book_imgs': instance.memberBookImgs,
      'member_book_imgs_non_sign': instance.memberBookImgs_non_sign,
      'plant': instance.plant,
      'join': instance.join,
      'cultivation_id': instance.cultivationId,
      'irrigation_area': instance.irrigationArea,
      'type_of_rice_cultivation': instance.typeOfRiceCultivation,
      'type_of_farming': instance.typeOfFarming,
      'burn_cultivation_area_v': instance.burnCultivationAreaV,
      'expected_date_planting_v': instance.expectedDatePlantingV,
      'expected_date_harvest_v': instance.expectedDateHarvestV,
      'start_at_rice_awd': instance.startAtRiceAwd,
      'remark': instance.remark,
    };
