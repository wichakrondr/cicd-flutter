import 'package:json_annotation/json_annotation.dart';
part 'verify_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class VerifyResponse {
  bool? status;
  String? message;
  List<VerifyResponseModel>? data;

  VerifyResponse(
    this.status,
    this.message,
    this.data,
  );

  factory VerifyResponse.fromJson(Map<String, dynamic> json) {
    return _$VerifyResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$VerifyResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class VerifyResponseModel {
  String personalId;
  String userId;
  String reqJoinProjectId;
  String projectId;
  String? firstName;
  String? lastName;
  List<String>? idCardImgs;
  List<String>? idCardImgs_non_sign;
  List<String>? copyHouseRegistrationImgs;
  List<String>? copyHouseRegistrationImgs_non_sign;
  String? landId;
  String? landRights;
  String? typeOfCertificateOwnership;
  String? titleDeedNumber;
  String? province;
  String? district;
  String? subDistrict;
  int? ngan;
  int? rai;
  String? squareWah;
  List<String>? letterConsentParticipateProjectImgs;
  List<String>? letterConsentParticipateProjectImgs_non_sign;
  List<String>? certificateLandUtilizationImgs;
  List<String>? certificateLandUtilizationImgs_non_sign;
  List<String>? titleDeedImgs;
  List<String>? titleDeedImgs_non_sign;
  List<String>? farmBookImgs;
  List<String>? farmBookImgs_non_sign;
  List<String>? landBookImgs;
  List<String>? landBookImgs_non_sign;
  List<String>? memberBookImgs;
  List<String>? memberBookImgs_non_sign;
  String? plant;
  String? join;
  String? cultivationId;
  String? irrigationArea;
  String? typeOfRiceCultivation;
  String? typeOfFarming;
  String? burnCultivationAreaV;
  String? expectedDatePlantingV;
  String? expectedDateHarvestV;
  String? startAtRiceAwd;
  String? remark;
  VerifyResponseModel({
    required this.personalId,
    required this.userId,
    required this.reqJoinProjectId,
    required this.projectId,
    this.firstName,
    this.lastName,
    this.idCardImgs = const [],
    this.idCardImgs_non_sign = const [],
    this.copyHouseRegistrationImgs = const [],
    this.copyHouseRegistrationImgs_non_sign = const [],
    this.typeOfRiceCultivation,
    this.typeOfFarming,
    this.landId,
    this.landRights,
    this.typeOfCertificateOwnership,
    this.titleDeedNumber,
    this.province,
    this.district,
    this.subDistrict,
    this.ngan,
    this.rai,
    this.squareWah,
    this.letterConsentParticipateProjectImgs = const [],
    this.letterConsentParticipateProjectImgs_non_sign = const [],
    this.certificateLandUtilizationImgs = const [],
    this.certificateLandUtilizationImgs_non_sign = const [],
    this.titleDeedImgs = const [],
    this.titleDeedImgs_non_sign = const [],
    this.farmBookImgs = const [],
    this.farmBookImgs_non_sign = const [],
    this.landBookImgs = const [],
    this.landBookImgs_non_sign = const [],
    this.memberBookImgs = const [],
    this.memberBookImgs_non_sign = const [],
    this.plant,
    this.join,
    this.cultivationId,
    this.irrigationArea,
    this.burnCultivationAreaV,
    this.expectedDatePlantingV,
    this.expectedDateHarvestV,
    this.startAtRiceAwd,
    this.remark,
  });

  factory VerifyResponseModel.fromJson(Map<String, dynamic> json) =>
      _$VerifyResponseModelFromJson(json);
  Map<String, dynamic> toJson() => _$VerifyResponseModelToJson(this);
}
