// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'form_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FormResponse _$FormResponseFromJson(Map<String, dynamic> json) => FormResponse(
      responseModel: json['responseModel'] == null
          ? null
          : ResponseModel.fromJson(
              json['responseModel'] as Map<String, dynamic>),
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => FormModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FormResponseToJson(FormResponse instance) =>
    <String, dynamic>{
      'responseModel': instance.responseModel,
      'data': instance.data,
    };

FormModel _$FormModelFromJson(Map<String, dynamic> json) => FormModel(
      id: json['id'] as String?,
      personal_id: json['personal_id'] as String?,
      name: json['name'] as String?,
      letter_consent_participate_project_imgs:
          (json['letter_consent_participate_project_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList(),
      form_id: json['form_id'] as String?,
      user_id: json['user_id'] as String?,
      first_name: json['first_name'] as String?,
      last_name: json['last_name'] as String?,
      id_card_imgs: (json['id_card_imgs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      land_rights: json['land_rights'] as String?,
      req_join_project_id: json['req_join_project_id'] as String?,
      project_id: json['project_id'] as String?,
      type_of_certificate_ownership:
          json['type_of_certificate_ownership'] as String?,
      title_deed_number: json['title_deed_number'] as String?,
      province: json['province'] as String?,
      district: json['district'] as String?,
      sub_district: json['sub_district'] as String?,
      square_wah: json['square_wah'] as int?,
      ngan: json['ngan'] as int?,
      rai: json['rai'] as int?,
      letter_attorney: json['letter_attorney'] as String?,
      title_deed_imgs: (json['title_deed_imgs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      certificate_land_utilization_imgs:
          (json['certificate_land_utilization_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList(),
      farm_book_imgs: (json['farm_book_imgs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      land_book_imgs: (json['land_book_imgs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      member_book_imgs: (json['member_book_imgs'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      irrigation_area: json['irrigation_area'] as String?,
      plant: json['plant'] as String?,
      icon_plant: json['icon_plant'] as String?,
      type_of_rice_cultivation: json['type_of_rice_cultivation'] as String?,
      type_of_farming: json['type_of_farming'] as String?,
      start_at_rice_awd: json['start_at_rice_awd'] as String?,
      join: json['join'] as String?,
      project_code: json['project_code'] as String?,
      burn_cultivation_area_v: json['burn_cultivation_area_v'] as String?,
      expected_date_planting_v: json['expected_date_planting_v'] as String?,
      expected_date_harvest_v: json['expected_date_harvest_v'] as String?,
      copy_house_registration_imgs:
          (json['copy_house_registration_imgs'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList(),
      project_name: json['project_name'] as String?,
      land_id: json['land_id'] as String?,
      cultivation_id: json['cultivation_id'] as String?,
    );

Map<String, dynamic> _$FormModelToJson(FormModel instance) => <String, dynamic>{
      'letter_consent_participate_project_imgs':
          instance.letter_consent_participate_project_imgs,
      'personal_id': instance.personal_id,
      'id': instance.id,
      'name': instance.name,
      'form_id': instance.form_id,
      'project_code': instance.project_code,
      'user_id': instance.user_id,
      'first_name': instance.first_name,
      'last_name': instance.last_name,
      'id_card_imgs': instance.id_card_imgs,
      'land_rights': instance.land_rights,
      'req_join_project_id': instance.req_join_project_id,
      'project_id': instance.project_id,
      'type_of_certificate_ownership': instance.type_of_certificate_ownership,
      'title_deed_number': instance.title_deed_number,
      'province': instance.province,
      'district': instance.district,
      'sub_district': instance.sub_district,
      'square_wah': instance.square_wah,
      'ngan': instance.ngan,
      'rai': instance.rai,
      'letter_attorney': instance.letter_attorney,
      'certificate_land_utilization_imgs':
          instance.certificate_land_utilization_imgs,
      'copy_house_registration_imgs': instance.copy_house_registration_imgs,
      'title_deed_imgs': instance.title_deed_imgs,
      'farm_book_imgs': instance.farm_book_imgs,
      'land_book_imgs': instance.land_book_imgs,
      'member_book_imgs': instance.member_book_imgs,
      'irrigation_area': instance.irrigation_area,
      'plant': instance.plant,
      'icon_plant': instance.icon_plant,
      'type_of_rice_cultivation': instance.type_of_rice_cultivation,
      'type_of_farming': instance.type_of_farming,
      'start_at_rice_awd': instance.start_at_rice_awd,
      'join': instance.join,
      'burn_cultivation_area_v': instance.burn_cultivation_area_v,
      'expected_date_planting_v': instance.expected_date_planting_v,
      'expected_date_harvest_v': instance.expected_date_harvest_v,
      'project_name': instance.project_name,
      'land_id': instance.land_id,
      'cultivation_id': instance.cultivation_id,
    };
