// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'join_project_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JoinProjectListResponse _$JoinProjectListResponseFromJson(
        Map<String, dynamic> json) =>
    JoinProjectListResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => JoinProjectListModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$JoinProjectListResponseToJson(
        JoinProjectListResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

JoinProjectListModel _$JoinProjectListModelFromJson(
        Map<String, dynamic> json) =>
    JoinProjectListModel(
      id: json['id'] as String?,
      bannerImage: json['banner_image'] as String?,
      name: json['name'] as String?,
      headerImage: json['header_image'] as String?,
    )..projectCode = json['project_code'] as String?;

Map<String, dynamic> _$JoinProjectListModelToJson(
        JoinProjectListModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'banner_image': instance.bannerImage,
      'name': instance.name,
      'header_image': instance.headerImage,
      'project_code': instance.projectCode,
    };
