// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_joint_project_list_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RequestJoinProjectListResponse _$RequestJoinProjectListResponseFromJson(
        Map<String, dynamic> json) =>
    RequestJoinProjectListResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) =>
              RequestJoinProjectListModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RequestJoinProjectListResponseToJson(
        RequestJoinProjectListResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

RequestJoinProjectListModel _$RequestJoinProjectListModelFromJson(
        Map<String, dynamic> json) =>
    RequestJoinProjectListModel(
      farmId: json['farm_id'] as String,
      farmName: json['farm_name'] as String,
      displayArea: json['display_area'] as String,
      farmImg: json['farm_img'] as String,
      status: json['status'] as String,
      reqJoinProjectId: json['req_join_project_id'] as String,
      statusValue: json['status_value'] as int,
      statusActivity: json['status_activity'] as String,
      statusVerify: json['status_verify'] as String,
    );

Map<String, dynamic> _$RequestJoinProjectListModelToJson(
        RequestJoinProjectListModel instance) =>
    <String, dynamic>{
      'farm_id': instance.farmId,
      'farm_name': instance.farmName,
      'display_area': instance.displayArea,
      'farm_img': instance.farmImg,
      'status': instance.status,
      'status_value': instance.statusValue,
      'req_join_project_id': instance.reqJoinProjectId,
      'status_verify': instance.statusVerify,
      'status_activity': instance.statusActivity,
    };
