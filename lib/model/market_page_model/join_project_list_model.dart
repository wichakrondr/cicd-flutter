import 'package:json_annotation/json_annotation.dart';
part 'join_project_list_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class JoinProjectListResponse {
  JoinProjectListResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  List<JoinProjectListModel>? data;
  factory JoinProjectListResponse.fromJson(Map<String, dynamic> json) {
    return _$JoinProjectListResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$JoinProjectListResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class JoinProjectListModel {
  JoinProjectListModel(
      {this.id, this.bannerImage, this.name, this.headerImage});

  String? id;
  String? bannerImage;
  String? name;
  String? headerImage;
  String? projectCode;

  factory JoinProjectListModel.fromJson(Map<String, dynamic> json) {
    return _$JoinProjectListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$JoinProjectListModelToJson(this);
}
