import 'package:json_annotation/json_annotation.dart';
import 'package:varun_kanna_app/model/response_model.dart';
part 'form_model.g.dart';

@JsonSerializable()
class FormResponse {
  FormResponse({
    this.responseModel,
    this.data,
  });

  ResponseModel? responseModel;
  List<FormModel>? data;
  factory FormResponse.fromJson(Map<String, dynamic> json) {
    return _$FormResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$FormResponseToJson(this);
}

@JsonSerializable()
class FormModel {
  FormModel({
    this.id,
    this.personal_id,
    this.name,
    this.letter_consent_participate_project_imgs,
    this.form_id,
    this.user_id,
    this.first_name,
    this.last_name,
    this.id_card_imgs,
    this.land_rights,
    this.req_join_project_id,
    this.project_id,
    this.type_of_certificate_ownership,
    this.title_deed_number,
    this.province,
    this.district,
    this.sub_district,
    this.square_wah,
    this.ngan,
    this.rai,
    this.letter_attorney,
    this.title_deed_imgs,
    this.certificate_land_utilization_imgs,
    this.farm_book_imgs,
    this.land_book_imgs,
    this.member_book_imgs,
    this.irrigation_area,
    this.plant,
    this.icon_plant,
    this.type_of_rice_cultivation,
    this.type_of_farming,
    this.start_at_rice_awd,
    this.join,
    this.project_code,
    this.burn_cultivation_area_v,
    this.expected_date_planting_v,
    this.expected_date_harvest_v,
    this.copy_house_registration_imgs,
    this.project_name,
    this.land_id,
    this.cultivation_id,
  });
  List<String>? letter_consent_participate_project_imgs;
  String? personal_id;
  String? id;
  String? name;
  String? form_id;
  String? project_code;
  String? user_id;
  String? first_name;
  String? last_name;
  List<String>? id_card_imgs;
  String? land_rights;
  String? req_join_project_id;
  String? project_id;
  String? type_of_certificate_ownership;
  String? title_deed_number;
  String? province;
  String? district;
  String? sub_district;
  int? square_wah;
  int? ngan;
  int? rai;
  String? letter_attorney;
  List<String>? certificate_land_utilization_imgs;
  List<String>? copy_house_registration_imgs;
  List<String>? title_deed_imgs;
  List<String>? farm_book_imgs;
  List<String>? land_book_imgs;
  List<String>? member_book_imgs;
  String? irrigation_area;
  String? plant;
  String? icon_plant;
  String? type_of_rice_cultivation;
  String? type_of_farming;
  String? start_at_rice_awd;
  String? join;
  String? burn_cultivation_area_v;
  String? expected_date_planting_v;
  String? expected_date_harvest_v;
  String? project_name;
  String? land_id;
  String? cultivation_id;
  factory FormModel.fromJson(Map<String, dynamic> json) {
    return _$FormModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$FormModelToJson(this);
}
