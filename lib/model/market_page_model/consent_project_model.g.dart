// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'consent_project_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProjectConsentResponse _$ProjectConsentResponseFromJson(
        Map<String, dynamic> json) =>
    ProjectConsentResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: json['data'] == null
          ? null
          : ProjectConsentModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProjectConsentResponseToJson(
        ProjectConsentResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

ProjectConsentModel _$ProjectConsentModelFromJson(Map<String, dynamic> json) =>
    ProjectConsentModel(
      signed: json['signed'] as bool?,
      currentVersion: json['currentVersion'] == null
          ? null
          : CurrentVersion.fromJson(
              json['currentVersion'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ProjectConsentModelToJson(
        ProjectConsentModel instance) =>
    <String, dynamic>{
      'signed': instance.signed,
      'currentVersion': instance.currentVersion,
    };

CurrentVersion _$CurrentVersionFromJson(Map<String, dynamic> json) =>
    CurrentVersion(
      id: json['id'] as String?,
      title: json['title'] as String?,
      content: json['content'] as String?,
    );

Map<String, dynamic> _$CurrentVersionToJson(CurrentVersion instance) =>
    <String, dynamic>{
      'id': instance.id,
      'title': instance.title,
      'content': instance.content,
    };
