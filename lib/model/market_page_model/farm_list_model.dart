import 'package:json_annotation/json_annotation.dart';
part 'farm_list_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class JoinProjectFarmListResponse {
  JoinProjectFarmListResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  List<JoinProjectFarmListModel>? data;

  factory JoinProjectFarmListResponse.fromJson(Map<String, dynamic> json) {
    return _$JoinProjectFarmListResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$JoinProjectFarmListResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class JoinProjectFarmListModel {
  JoinProjectFarmListModel({
    this.id,
    this.farmName,
    this.displayArea,
    this.farmImg,
    this.isSelected = false,
  });

  String? id;
  String? farmName;
  String? displayArea;
  String? farmImg;
  bool isSelected;

  factory JoinProjectFarmListModel.fromJson(Map<String, dynamic> json) {
    return _$JoinProjectFarmListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$JoinProjectFarmListModelToJson(this);
}
