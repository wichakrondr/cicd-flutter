import 'package:json_annotation/json_annotation.dart';
part 'request_joint_project_list_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class RequestJoinProjectListResponse {
  RequestJoinProjectListResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  List<RequestJoinProjectListModel>? data;
  factory RequestJoinProjectListResponse.fromJson(Map<String, dynamic> json) {
    return _$RequestJoinProjectListResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RequestJoinProjectListResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class RequestJoinProjectListModel {
  RequestJoinProjectListModel({
    required this.farmId,
    required this.farmName,
    required this.displayArea,
    required this.farmImg,
    required this.status,
    required this.reqJoinProjectId,
    required this.statusValue,
    required this.statusActivity,
    required this.statusVerify,
  });

  String farmId;
  String farmName;
  String displayArea;
  String farmImg;
  String status;
  int statusValue;
  String reqJoinProjectId;
  String statusVerify;
  String statusActivity;

  factory RequestJoinProjectListModel.fromJson(Map<String, dynamic> json) {
    return _$RequestJoinProjectListModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RequestJoinProjectListModelToJson(this);
}
