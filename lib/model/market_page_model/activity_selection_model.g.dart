// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_selection_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivitySelectionResponse _$ActivitySelectionResponseFromJson(
        Map<String, dynamic> json) =>
    ActivitySelectionResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map(
              (e) => ActivitySelectionModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ActivitySelectionResponseToJson(
        ActivitySelectionResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

ActivitySelectionModel _$ActivitySelectionModelFromJson(
        Map<String, dynamic> json) =>
    ActivitySelectionModel(
      id: json['id'] as String?,
      index: json['index'] as int?,
      name: json['name'] as String?,
      projectId: json['project_id'] as String?,
      status: json['status'] as String?,
      startPlanningPoint: json['start_planning_point'] as bool?,
      remark: json['remark'] as String?,
      required: json['required'] as bool?,
      requiredToVerify: json['required_to_verify'] as bool?,
      iconPath: json['icon_path'] as String?,
      startOn: json['start_on'] as int?,
      endOn: json['end_on'] as int?,
      requestJoinProjectId: json['request_join_project_id'] as String?,
      iconPathActive: json['icon_path_active'] as String?,
      periodDate: json['period_date'] as String?,
    );

Map<String, dynamic> _$ActivitySelectionModelToJson(
        ActivitySelectionModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'index': instance.index,
      'name': instance.name,
      'project_id': instance.projectId,
      'status': instance.status,
      'start_planning_point': instance.startPlanningPoint,
      'remark': instance.remark,
      'required': instance.required,
      'required_to_verify': instance.requiredToVerify,
      'icon_path': instance.iconPath,
      'start_on': instance.startOn,
      'end_on': instance.endOn,
      'request_join_project_id': instance.requestJoinProjectId,
      'icon_path_active': instance.iconPathActive,
      'period_date': instance.periodDate,
    };
