import 'package:json_annotation/json_annotation.dart';
import 'package:varun_kanna_app/model/response_model.dart';
part 'document_model.g.dart';

@JsonSerializable()
class DocumentResponse {
  DocumentResponse({
    this.responseModel,
    this.data,
  });

  ResponseModel? responseModel;
  List<DocumentModel>? data;
  factory DocumentResponse.fromJson(Map<String, dynamic> json) {
    return _$DocumentResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$DocumentResponseToJson(this);
}

@JsonSerializable()
class DocumentModel {
  DocumentModel({
    required this.id,
    this.short_name,
    this.signed,
  });
  String id;
  String? short_name;
  bool? signed;

  factory DocumentModel.fromJson(Map<String, dynamic> json) {
    return _$DocumentModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$DocumentModelToJson(this);
}
