import 'package:json_annotation/json_annotation.dart';
part 'consent_project_model.g.dart';

@JsonSerializable()
class ProjectConsentResponse {
  ProjectConsentResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  ProjectConsentModel? data;
  factory ProjectConsentResponse.fromJson(Map<String, dynamic> json) {
    return _$ProjectConsentResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ProjectConsentResponseToJson(this);
}

@JsonSerializable()
class ProjectConsentModel {
  ProjectConsentModel({
    this.signed,
    this.currentVersion,
  });

  bool? signed;
  CurrentVersion? currentVersion;

  factory ProjectConsentModel.fromJson(Map<String, dynamic> json) {
    return _$ProjectConsentModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ProjectConsentModelToJson(this);
}

@JsonSerializable()
class CurrentVersion {
  CurrentVersion({
    this.id,
    this.title,
    this.content,
  });

  String? id;
  String? title;
  String? content;

  factory CurrentVersion.fromJson(Map<String, dynamic> json) {
    return _$CurrentVersionFromJson(json);
  }

  Map<String, dynamic> toJson() => _$CurrentVersionToJson(this);
}
