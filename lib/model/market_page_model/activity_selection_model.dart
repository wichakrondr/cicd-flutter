import 'package:json_annotation/json_annotation.dart';
part 'activity_selection_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ActivitySelectionResponse {
  ActivitySelectionResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  List<ActivitySelectionModel>? data;
  factory ActivitySelectionResponse.fromJson(Map<String, dynamic> json) {
    return _$ActivitySelectionResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ActivitySelectionResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ActivitySelectionModel {
  ActivitySelectionModel({
     this.id,
     this.index,
     this.name,
     this.projectId,
    this.status,
    this.startPlanningPoint,
    this.remark,
    this.required,
    this.requiredToVerify,
    this.iconPath,
    this.startOn,
    this.endOn,
    this.requestJoinProjectId,
    this.iconPathActive,
    this.periodDate
  });

  String? id;
  int? index;
  String? name;
  String? projectId;
  String? status;
  bool? startPlanningPoint;
  String? remark;
  bool? required;
  bool? requiredToVerify;
  String? iconPath;
  int? startOn;
  int? endOn;
  String? requestJoinProjectId;
  String? iconPathActive;
  String? periodDate;

  factory ActivitySelectionModel.fromJson(Map<String, dynamic> json) {
    return _$ActivitySelectionModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ActivitySelectionModelToJson(this);
}
