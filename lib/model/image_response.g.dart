// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageResponse _$ImageResponseFromJson(Map<String, dynamic> json) =>
    ImageResponse(
      json['status'] as bool?,
      json['message'] as String?,
      json['data'] == null
          ? null
          : ImageModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$ImageResponseToJson(ImageResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

ImageModel _$ImageModelFromJson(Map<String, dynamic> json) => ImageModel(
      path: json['path'] as String,
    );

Map<String, dynamic> _$ImageModelToJson(ImageModel instance) =>
    <String, dynamic>{
      'path': instance.path,
    };
