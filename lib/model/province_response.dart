import 'package:json_annotation/json_annotation.dart';

part 'province_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ProvinceResponse {
  bool? status;
  String? message;
  List<Province> data;

  ProvinceResponse(
    this.status,
    this.message,
    this.data,
  );

  factory ProvinceResponse.fromJson(Map<String, dynamic> json) {
    return _$ProvinceResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ProvinceResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Province {
  int? id;
  String? name_th;
  String? name_en;

  Province({this.id, this.name_th, this.name_en});

  factory Province.fromJson(Map<String, dynamic> json) =>
      _$ProvinceFromJson(json);

  Map<String, dynamic> toJson() => _$ProvinceToJson(this);
}
