class Login {
  String firstName;
  String lastName;
  String? imageName;
  String birthDate;
  String? email;
  String? mobileNo;
  String address;
  String province;
  String district;
  String subDistrict;
  Login(
      {required this.firstName,
      required this.lastName,
      this.imageName,
      required this.birthDate,
      this.email,
      this.mobileNo,
      required this.address,
      required this.province,
      required this.district,
      required this.subDistrict});
}
