import 'package:json_annotation/json_annotation.dart';

part 'otp_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class RegisterResponse {
  bool? status;
  String? message;
  RegisterModel? data;

  RegisterResponse(
    this.status,
    this.message,
    this.data,
  );

  factory RegisterResponse.fromJson(Map<String, dynamic> json) {
    return _$RegisterResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RegisterResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class RegisterModel {
  @JsonKey(name: 'telephone')
  String? telephone;
  @JsonKey(name: 'refCode')
  String? refCode;
  @JsonKey(name: 'otp')
  String? otp;
  @JsonKey(name: 'userId')
  String? userId;
  @JsonKey(name: 'isActive')
  bool? isActive;
  @JsonKey(name: 'isExpire')
  bool? isExpire;
  RegisterModel(
      {this.telephone,
      this.refCode,
      this.otp,
      this.userId,
      this.isActive,
      this.isExpire});

  factory RegisterModel.fromJson(Map<String, dynamic> json) =>
      _$RegisterModelFromJson(json);

  Map<String, dynamic> toJson() => _$RegisterModelToJson(this);
}
