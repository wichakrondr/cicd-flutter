import 'package:json_annotation/json_annotation.dart';
part 'farm_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmResponse {
  bool? status;
  String? message;
  FarmModelRes? data;

  FarmResponse(
    this.status,
    this.message,
    this.data,
  );

  factory FarmResponse.fromJson(Map<String, dynamic> json) {
    return _$FarmResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$FarmResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmModelRes {
  String? id;
  String? path;

  FarmModelRes({this.id, this.path});

  factory FarmModelRes.fromJson(Map<String, dynamic> json) =>
      _$FarmModelResFromJson(json);
  Map<String, dynamic> toJson() => _$FarmModelResToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmModel {
  String userId;
  String farmName;
  @JsonKey(name: 'centroid_UTM')
  UtmModel centroidUTM;
  @JsonKey(name: 'centroid_GLO')
  LatLngModel centroidGLO;
  double squareWah;
  int ngan;
  int rai;
  List polygon;
  String? farmId;

  FarmModel(
      {required this.userId,
      required this.farmName,
      required this.centroidUTM,
      required this.centroidGLO,
      required this.squareWah,
      required this.ngan,
      required this.rai,
      required this.polygon,
      this.farmId});

  factory FarmModel.fromJson(Map<String, dynamic> json) =>
      _$FarmModelFromJson(json);
  Map<String, dynamic> toJson() => _$FarmModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class LatLngModel {
  double lat;
  double lng;

  LatLngModel({required this.lat, required this.lng});

  factory LatLngModel.fromJson(Map<String, dynamic> json) =>
      _$LatLngModelFromJson(json);
  Map<String, dynamic> toJson() => _$LatLngModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class UtmModel {
  String grid;
  int zone;
  double x;
  double y;

  UtmModel(
      {required this.grid,
      required this.zone,
      required this.x,
      required this.y});

  factory UtmModel.fromJson(Map<String, dynamic> json) =>
      _$UtmModelFromJson(json);
  Map<String, dynamic> toJson() => _$UtmModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmDetailResponse {
  bool? status;
  String? message;
  List<FarmDetailModel?> data;

  FarmDetailResponse(
    this.status,
    this.message,
    this.data,
  );

  factory FarmDetailResponse.fromJson(Map<String, dynamic> json) {
    return _$FarmDetailResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$FarmDetailResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmDetailModel {
  String? id;
  String? farmName;
  @JsonKey(name: 'centroid_GLO')
  LatLngModel? centroidGLO;
  @JsonKey(name: 'centroid_UTM')
  UtmModel? centroidUTM;
  String? displayArea;
  String? refCode;
  List? polygon;

  FarmDetailModel(
      {this.id,
      this.farmName,
      this.centroidGLO,
      this.centroidUTM,
      this.displayArea,
      this.refCode,
      this.polygon});

  factory FarmDetailModel.fromJson(Map<String, dynamic> json) =>
      _$FarmDetailModelFromJson(json);
  Map<String, dynamic> toJson() => _$FarmDetailModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class PlaceSearch {
  String? description;
  String? placeId;

  PlaceSearch({this.description, this.placeId});

  factory PlaceSearch.fromJson(Map<String, dynamic> json) =>
      _$PlaceSearchFromJson(json);
  Map<String, dynamic> toJson() => _$PlaceSearchToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Location {
  double? lat;
  double? lng;

  Location({this.lat, this.lng});

  factory Location.fromJson(Map<String, dynamic> json) =>
      _$LocationFromJson(json);
  Map<String, dynamic> toJson() => _$LocationToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Geometry {
  Location? location;

  Geometry({this.location});

  factory Geometry.fromJson(Map<String, dynamic> json) =>
      _$GeometryFromJson(json);
  Map<String, dynamic> toJson() => _$GeometryToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Place {
  Geometry? geometry;
  String? name;

  Place({this.geometry, this.name});

  factory Place.fromJson(Map<String, dynamic> json) => _$PlaceFromJson(json);
  Map<String, dynamic> toJson() => _$PlaceToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class LocationDetail {
  String? formatted_address;

  LocationDetail({this.formatted_address});

  factory LocationDetail.fromJson(Map<String, dynamic> json) =>
      _$LocationDetailFromJson(json);
  Map<String, dynamic> toJson() => _$LocationDetailToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class LocationDetailRes {
  List<LocationDetail> results;

  LocationDetailRes({required this.results});

  factory LocationDetailRes.fromJson(Map<String, dynamic> json) =>
      _$LocationDetailResFromJson(json);
  Map<String, dynamic> toJson() => _$LocationDetailResToJson(this);
}
