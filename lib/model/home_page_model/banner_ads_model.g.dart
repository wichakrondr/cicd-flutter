// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_ads_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BannerAdsResponse _$BannerAdsResponseFromJson(Map<String, dynamic> json) =>
    BannerAdsResponse(
      status: json['status'] as bool?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => BannerAdsModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BannerAdsResponseToJson(BannerAdsResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

BannerAdsModel _$BannerAdsModelFromJson(Map<String, dynamic> json) =>
    BannerAdsModel(
      id: json['id'] as String?,
      name: json['name'] as String?,
      path: json['path'] as String?,
      link: json['link'] as String?,
    );

Map<String, dynamic> _$BannerAdsModelToJson(BannerAdsModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'path': instance.path,
      'link': instance.link,
    };
