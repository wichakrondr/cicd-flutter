import 'package:json_annotation/json_annotation.dart';
part 'banner_ads_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class BannerAdsResponse {
  BannerAdsResponse({
    this.status,
    this.message,
    this.data,
  });

  bool? status;
  String? message;
  List<BannerAdsModel>? data;
  factory BannerAdsResponse.fromJson(Map<String, dynamic> json) {
    return _$BannerAdsResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$BannerAdsResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class BannerAdsModel {
  BannerAdsModel({
    this.id,
    this.name,
    this.path,
    this.link,
  });

  String? id;
  String? name;
  String? path;
  String? link;

  factory BannerAdsModel.fromJson(Map<String, dynamic> json) =>
      _$BannerAdsModelFromJson(json);

  Map<String, dynamic> toJson() => _$BannerAdsModelToJson(this);
}
