import 'package:json_annotation/json_annotation.dart';

part 'district_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class DistrictResponse {
  bool? status;
  String? message;
  List<DistrictModel> data;

  DistrictResponse(
    this.status,
    this.message,
    this.data,
  );

  factory DistrictResponse.fromJson(Map<String, dynamic> json) {
    return _$DistrictResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$DistrictResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class DistrictModel {
  int? id;
  String? name_th;
  String? name_en;
  int? province_id;

  DistrictModel({this.id, this.name_th, this.name_en});

  factory DistrictModel.fromJson(Map<String, dynamic> json) =>
      _$DistrictModelFromJson(json);

  Map<String, dynamic> toJson() => _$DistrictModelToJson(this);
}
