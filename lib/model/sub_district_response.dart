import 'package:json_annotation/json_annotation.dart';

part 'sub_district_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class SubDistrictResponse {
  bool? status;
  String? message;
  List<SubDistrictModel> data;

  SubDistrictResponse(
    this.status,
    this.message,
    this.data,
  );

  factory SubDistrictResponse.fromJson(Map<String, dynamic> json) {
    return _$SubDistrictResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$SubDistrictResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class SubDistrictModel {
  int? id;
  String? name_th;
  String? name_en;
  int? amphure_id;
  int? zip_code;

  SubDistrictModel(
      {this.id, this.name_th, this.name_en, this.amphure_id, this.zip_code});

  factory SubDistrictModel.fromJson(Map<String, dynamic> json) =>
      _$SubDistrictModelFromJson(json);

  Map<String, dynamic> toJson() => _$SubDistrictModelToJson(this);
}
