import 'package:json_annotation/json_annotation.dart';

part 'response_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ResponseModel {
  bool? status;
  String? message;

  ResponseModel(
    this.status,
    this.message,
  );

  factory ResponseModel.fromJson(Map<String, dynamic> json) {
    return _$ResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ResponseModelToJson(this);
}
