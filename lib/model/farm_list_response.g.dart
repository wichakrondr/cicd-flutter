// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'farm_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FarmListResponse _$FarmListResponseFromJson(Map<String, dynamic> json) =>
    FarmListResponse(
      json['status'] as bool?,
      json['message'] as String?,
      (json['data'] as List<dynamic>?)
          ?.map((e) => FarmListModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FarmListResponseToJson(FarmListResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

FarmListModel _$FarmListModelFromJson(Map<String, dynamic> json) =>
    FarmListModel(
      id: json['id'] as String?,
      farmName: json['farm_name'] as String?,
      displayArea: json['display_area'] as String?,
      farmImg: json['farm_img'] == null
          ? null
          : FarmImageModel.fromJson(json['farm_img'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$FarmListModelToJson(FarmListModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'farm_name': instance.farmName,
      'display_area': instance.displayArea,
      'farm_img': instance.farmImg,
    };

FarmImageModel _$FarmImageModelFromJson(Map<String, dynamic> json) =>
    FarmImageModel(
      path: json['path'] as String,
    );

Map<String, dynamic> _$FarmImageModelToJson(FarmImageModel instance) =>
    <String, dynamic>{
      'path': instance.path,
    };
