import 'package:flutter/foundation.dart';
import 'package:json_annotation/json_annotation.dart';

part 'common_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class CommonResponse {
  bool status;
  String message;

  CommonResponse({required this.status, required this.message});

  factory CommonResponse.fromJson(Map<String, dynamic> json) =>
      _$CommonResponseFromJson(json);
  Map<String, dynamic> toJson() => _$CommonResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class UploadResponse {
  CommonResponse? commonResponse;
  UploadModel? data;

  UploadResponse({this.data, this.commonResponse});

  factory UploadResponse.fromJson(Map<String, dynamic> json) =>
      _$UploadResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UploadResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class UploadModel {
  double size_mb;
  double size_kb;
  String path;
  String? pathSignUrl;

  UploadModel(
      {required this.size_mb, required this.size_kb, required this.path, this.pathSignUrl});

  factory UploadModel.fromJson(Map<String, dynamic> json) =>
      _$UploadModelFromJson(json);
  Map<String, dynamic> toJson() => _$UploadModelToJson(this);
}
