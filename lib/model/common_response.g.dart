// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'common_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommonResponse _$CommonResponseFromJson(Map<String, dynamic> json) =>
    CommonResponse(
      status: json['status'] as bool,
      message: json['message'] as String,
    );

Map<String, dynamic> _$CommonResponseToJson(CommonResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
    };

UploadResponse _$UploadResponseFromJson(Map<String, dynamic> json) =>
    UploadResponse(
      data: json['data'] == null
          ? null
          : UploadModel.fromJson(json['data'] as Map<String, dynamic>),
      commonResponse: json['common_response'] == null
          ? null
          : CommonResponse.fromJson(
              json['common_response'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UploadResponseToJson(UploadResponse instance) =>
    <String, dynamic>{
      'common_response': instance.commonResponse,
      'data': instance.data,
    };

UploadModel _$UploadModelFromJson(Map<String, dynamic> json) => UploadModel(
      size_mb: (json['size_mb'] as num).toDouble(),
      size_kb: (json['size_kb'] as num).toDouble(),
      path: json['path'] as String,
      pathSignUrl: json['path_sign_url'] as String?,
    );

Map<String, dynamic> _$UploadModelToJson(UploadModel instance) =>
    <String, dynamic>{
      'size_mb': instance.size_mb,
      'size_kb': instance.size_kb,
      'path': instance.path,
      'path_sign_url': instance.pathSignUrl,
    };
