class NewsContent {
  String text;
  String imageName;
  String title;
  bool isFav;
  String? createDate;
  String? writer;

  NewsContent(
      {required this.text,
      required this.imageName,
      required this.title,
      this.createDate,
      this.writer,
      this.isFav = false});
}
