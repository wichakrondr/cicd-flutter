// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'otp_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterResponse _$RegisterResponseFromJson(Map<String, dynamic> json) =>
    RegisterResponse(
      json['status'] as bool?,
      json['message'] as String?,
      json['data'] == null
          ? null
          : RegisterModel.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RegisterResponseToJson(RegisterResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

RegisterModel _$RegisterModelFromJson(Map<String, dynamic> json) =>
    RegisterModel(
      telephone: json['telephone'] as String?,
      refCode: json['refCode'] as String?,
      otp: json['otp'] as String?,
      userId: json['userId'] as String?,
      isActive: json['isActive'] as bool?,
      isExpire: json['isExpire'] as bool?,
    );

Map<String, dynamic> _$RegisterModelToJson(RegisterModel instance) =>
    <String, dynamic>{
      'telephone': instance.telephone,
      'refCode': instance.refCode,
      'otp': instance.otp,
      'userId': instance.userId,
      'isActive': instance.isActive,
      'isExpire': instance.isExpire,
    };
