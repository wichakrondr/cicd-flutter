import 'package:json_annotation/json_annotation.dart';
part 'profile_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ProfileResponse {
  bool? status;
  String? message;
  ProfileModel data;

  ProfileResponse(
    this.status,
    this.message,
    this.data,
  );

  factory ProfileResponse.fromJson(Map<String, dynamic> json) {
    return _$ProfileResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ProfileResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ProfileModel {
  @JsonKey(name: 'firstname')
  String? firstName;
  @JsonKey(name: 'lastname')
  String? lastName;
  @JsonKey(name: 'id')
  String? id;
  String? imageName;
  @JsonKey(name: 'birthday')
  String? birthDay;
  String? email;
  @JsonKey(name: 'addresses')
  AddressModel? address;
  String? province;
  String? district;
  String? subDistrict;
  String? postCode;
  String? imgPath;

  @JsonKey(name: 'url_img')
  String? url_img;

  @JsonKey(name: 'tel')
  String? tel;
  @JsonKey(name: 'consentId')
  String? consentId;
  @JsonKey(name: 'consentDeviceId')
  String? consentDeviceId;
  @JsonKey(name: 'deviceId')
  String? deviceId;
  bool? isAcceptedLegalAge;

  ProfileModel(
      {this.firstName,
      this.lastName,
      this.id,
      this.imageName,
      this.birthDay,
      this.email,
      this.address,
      this.province,
      this.district,
      this.subDistrict,
      this.postCode,
      this.imgPath,
      this.url_img,
      this.tel,
      this.consentId,
      this.consentDeviceId,
      this.deviceId,
      this.isAcceptedLegalAge});

  factory ProfileModel.fromJson(Map<String, dynamic> json) =>
      _$ProfileModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ProfileDeleteResponse {
  bool? status;
  String? message;
  // ProfileModel data;

  ProfileDeleteResponse(
    this.status,
    this.message,
    //this.data,
  );

  factory ProfileDeleteResponse.fromJson(Map<String, dynamic> json) {
    return _$ProfileDeleteResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ProfileDeleteResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class AddressModel {
  @JsonKey(name: 'address')
  String? address;
  @JsonKey(name: 'district')
  String? district;
  @JsonKey(name: 'subdistrict')
  String? subdistrict;
  @JsonKey(name: 'province')
  String? province;
  @JsonKey(name: 'postcode')
  String? postcode;

  AddressModel({
    required this.address,
    required this.district,
    required this.subdistrict,
    required this.province,
    required this.postcode,
  });

  factory AddressModel.fromJson(Map<String, dynamic> json) {
    return _$AddressModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$AddressModelToJson(this);
}
