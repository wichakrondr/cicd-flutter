import 'package:json_annotation/json_annotation.dart';
import 'package:varun_kanna_app/model/response_model.dart';

part 'land_recomendation_model.g.dart';

enum LandRecomendationType { suggest, base }

@JsonSerializable(fieldRename: FieldRename.snake)
class LandRecomnModel {
  ResponseModel? responseModel;
  List<RecommendationResponseModel> data;

  LandRecomnModel(
    this.responseModel,
    this.data,
  );

  factory LandRecomnModel.fromJson(Map<String, dynamic> json) {
    return _$LandRecomnModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$LandRecomnModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class PlantListResponse {
  ResponseModel? responseModel;
  List<PlantModel> data;

  PlantListResponse(
    this.responseModel,
    this.data,
  );

  factory PlantListResponse.fromJson(Map<String, dynamic> json) {
    return _$PlantListResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$PlantListResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class PlantModel {
  @JsonKey(name: 'id')
  String? id;
  @JsonKey(name: 'name_th')
  String? name_th;
  @JsonKey(name: 'name_en')
  String? name_en;
  @JsonKey(name: 'suitable_point')
  int? suitable_point;
  @JsonKey(name: 'suitable_status')
  String? suitable_status;
  @JsonKey(name: 'icon_path')
  String? iconPath;
  PlantModel(
      {this.name_en,
      this.name_th,
      this.suitable_point,
      this.suitable_status,
      this.id,
      this.iconPath});

  factory PlantModel.fromJson(Map<String, dynamic> json) =>
      _$PlantModelFromJson(json);

  Map<String, dynamic> toJson() => _$PlantModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class SoilSeriesModel {
  @JsonKey(name: 'soil_fertility')
  String? soil_fertility;
  @JsonKey(name: 'soil_ph_top')
  String? soil_ph_top;
  @JsonKey(name: 'soil_seriesname')
  String? soil_seriesname;
  @JsonKey(name: 'soil_soilgroup')
  String? soil_soilgroup;
  @JsonKey(name: 'soil_soilseries')
  String? soil_soilseries;
  @JsonKey(name: 'soil_texture_to')
  String? soil_texture_to;
  @JsonKey(name: 'soil_soil_id')
  String? soil_soil_id;
  SoilSeriesModel(
      {this.soil_fertility,
      this.soil_ph_top,
      this.soil_seriesname = "ไม่มีข้อมูล",
      this.soil_soilseries = "N/A",
      this.soil_soil_id,
      this.soil_texture_to,
      this.soil_soilgroup});

  factory SoilSeriesModel.fromJson(Map<String, dynamic> json) =>
      _$SoilSeriesModelFromJson(json);

  Map<String, dynamic> toJson() => _$SoilSeriesModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class RecommendationResponseModel {
  @JsonKey(name: 'id')
  String id;
  @JsonKey(name: 'soil_series')
  List<SoilSeriesModel> soil_series;
  @JsonKey(name: 'crop_recommendation')
  List<PlantModel> crop_recommendation;
  @JsonKey(name: 'polygon')
  List? polygon;
  RecommendationResponseModel({
    required this.id,
    required this.soil_series,
    required this.crop_recommendation,
  });

  factory RecommendationResponseModel.fromJson(Map<String, dynamic> json) =>
      _$RecommendationResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$RecommendationResponseModelToJson(this);
}
