// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'land_recomendation_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LandRecomnModel _$LandRecomnModelFromJson(Map<String, dynamic> json) =>
    LandRecomnModel(
      json['response_model'] == null
          ? null
          : ResponseModel.fromJson(
              json['response_model'] as Map<String, dynamic>),
      (json['data'] as List<dynamic>)
          .map((e) =>
              RecommendationResponseModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$LandRecomnModelToJson(LandRecomnModel instance) =>
    <String, dynamic>{
      'response_model': instance.responseModel,
      'data': instance.data,
    };

PlantListResponse _$PlantListResponseFromJson(Map<String, dynamic> json) =>
    PlantListResponse(
      json['response_model'] == null
          ? null
          : ResponseModel.fromJson(
              json['response_model'] as Map<String, dynamic>),
      (json['data'] as List<dynamic>)
          .map((e) => PlantModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$PlantListResponseToJson(PlantListResponse instance) =>
    <String, dynamic>{
      'response_model': instance.responseModel,
      'data': instance.data,
    };

PlantModel _$PlantModelFromJson(Map<String, dynamic> json) => PlantModel(
      name_en: json['name_en'] as String?,
      name_th: json['name_th'] as String?,
      suitable_point: json['suitable_point'] as int?,
      suitable_status: json['suitable_status'] as String?,
      id: json['id'] as String?,
      iconPath: json['icon_path'] as String?,
    );

Map<String, dynamic> _$PlantModelToJson(PlantModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name_th': instance.name_th,
      'name_en': instance.name_en,
      'suitable_point': instance.suitable_point,
      'suitable_status': instance.suitable_status,
      'icon_path': instance.iconPath,
    };

SoilSeriesModel _$SoilSeriesModelFromJson(Map<String, dynamic> json) =>
    SoilSeriesModel(
      soil_fertility: json['soil_fertility'] as String?,
      soil_ph_top: json['soil_ph_top'] as String?,
      soil_seriesname: json['soil_seriesname'] as String? ?? "ไม่มีข้อมูล",
      soil_soilseries: json['soil_soilseries'] as String? ?? "N/A",
      soil_soil_id: json['soil_soil_id'] as String?,
      soil_texture_to: json['soil_texture_to'] as String?,
      soil_soilgroup: json['soil_soilgroup'] as String?,
    );

Map<String, dynamic> _$SoilSeriesModelToJson(SoilSeriesModel instance) =>
    <String, dynamic>{
      'soil_fertility': instance.soil_fertility,
      'soil_ph_top': instance.soil_ph_top,
      'soil_seriesname': instance.soil_seriesname,
      'soil_soilgroup': instance.soil_soilgroup,
      'soil_soilseries': instance.soil_soilseries,
      'soil_texture_to': instance.soil_texture_to,
      'soil_soil_id': instance.soil_soil_id,
    };

RecommendationResponseModel _$RecommendationResponseModelFromJson(
        Map<String, dynamic> json) =>
    RecommendationResponseModel(
      id: json['id'] as String,
      soil_series: (json['soil_series'] as List<dynamic>)
          .map((e) => SoilSeriesModel.fromJson(e as Map<String, dynamic>))
          .toList(),
      crop_recommendation: (json['crop_recommendation'] as List<dynamic>)
          .map((e) => PlantModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    )..polygon = json['polygon'] as List<dynamic>?;

Map<String, dynamic> _$RecommendationResponseModelToJson(
        RecommendationResponseModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'soil_series': instance.soil_series,
      'crop_recommendation': instance.crop_recommendation,
      'polygon': instance.polygon,
    };
