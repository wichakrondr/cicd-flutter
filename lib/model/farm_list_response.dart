import 'package:json_annotation/json_annotation.dart';
import 'package:varun_kanna_app/model/farm_response.dart';
part 'farm_list_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmListResponse {
  bool? status;
  String? message;
  List<FarmListModel>? data;

  FarmListResponse(
    this.status,
    this.message,
    this.data,
  );

  factory FarmListResponse.fromJson(Map<String, dynamic> json) {
    return _$FarmListResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$FarmListResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmListModel {
  String? id;
  String? farmName;
  String? displayArea;
  FarmImageModel? farmImg;
  FarmListModel(
      {this.id,
      this.farmName,
      this.displayArea,
      this.farmImg,});

  factory FarmListModel.fromJson(Map<String, dynamic> json) =>
      _$FarmListModelFromJson(json);
  Map<String, dynamic> toJson() => _$FarmListModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class FarmImageModel {
  String path;

  FarmImageModel({required this.path});

  factory FarmImageModel.fromJson(Map<String, dynamic> json) =>
      _$FarmImageModelFromJson(json);
  Map<String, dynamic> toJson() => _$FarmImageModelToJson(this);
}
