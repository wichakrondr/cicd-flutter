import 'package:json_annotation/json_annotation.dart';
part 'image_response.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class ImageResponse {
  bool? status;
  String? message;
  ImageModel? data;

  ImageResponse(
    this.status,
    this.message,
    this.data,
  );

  factory ImageResponse.fromJson(Map<String, dynamic> json) {
    return _$ImageResponseFromJson(json);
  }

  Map<String, dynamic> toJson() => _$ImageResponseToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class ImageModel {
  String path;

  ImageModel({required this.path});

  factory ImageModel.fromJson(Map<String, dynamic> json) =>
      _$ImageModelFromJson(json);
  Map<String, dynamic> toJson() => _$ImageModelToJson(this);
}
