import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

part 'weather_model.g.dart';

@JsonSerializable(fieldRename: FieldRename.snake)
class WeatherModelModel {
  Main main;
  List<Weather> weather;
  Wind wind;
  String dt_txt;
  Rain? rain;
  WeatherModelModel(
    this.main,
    this.weather,
    this.wind,
    this.rain,
    this.dt_txt,
  );

  factory WeatherModelModel.fromJson(Map<String, dynamic> json) {
    return _$WeatherModelModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$WeatherModelModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class WeatherForcastAllFarmResponseModel {
  List<List<WeatherModelModel>> list;

  WeatherForcastAllFarmResponseModel(this.list);

  factory WeatherForcastAllFarmResponseModel.fromJson(
      Map<String, dynamic> json) {
    return _$WeatherForcastAllFarmResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() =>
      _$WeatherForcastAllFarmResponseModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class WeatherForcastResponseModel {
  List<WeatherModelModel> list;

  WeatherForcastResponseModel(this.list);

  factory WeatherForcastResponseModel.fromJson(Map<String, dynamic> json) {
    return _$WeatherForcastResponseModelFromJson(json);
  }

  Map<String, dynamic> toJson() => _$WeatherForcastResponseModelToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Main {
  double? temp;
  double? feels_like;
  double? temp_min;
  double? temp_max;
  double? pressure;
  double? sea_level;
  double? grnd_level;
  double? humidity;
  double? temp_kf;

  Main(
    this.temp,
    this.feels_like,
    this.temp_min,
    this.temp_max,
    this.pressure,
    this.sea_level,
    this.grnd_level,
    this.humidity,
    this.temp_kf,
  );

  factory Main.fromJson(Map<String, dynamic> json) {
    return _$MainFromJson(json);
  }

  Map<String, dynamic> toJson() => _$MainToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Weather {
  double? id;
  String? main;
  String? description;
  String? icon;

  Weather(
    this.id,
    this.main,
    this.description,
    this.icon,
  );

  factory Weather.fromJson(Map<String, dynamic> json) {
    return _$WeatherFromJson(json);
  }

  Map<String, dynamic> toJson() => _$WeatherToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Wind {
  double? speed;
  double? deg;
  double? gust;

  Wind(this.speed, this.deg, this.gust);

  factory Wind.fromJson(Map<String, dynamic> json) {
    return _$WindFromJson(json);
  }

  Map<String, dynamic> toJson() => _$WindToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake)
class Rain {
  double? h3;

  Rain(this.h3);

  factory Rain.fromJson(Map<String, dynamic> json) {
    return _$RainFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RainToJson(this);
}
