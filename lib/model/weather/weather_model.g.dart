// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'weather_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeatherModelModel _$WeatherModelModelFromJson(Map<String, dynamic> json) =>
    WeatherModelModel(
      Main.fromJson(json['main'] as Map<String, dynamic>),
      (json['weather'] as List<dynamic>)
          .map((e) => Weather.fromJson(e as Map<String, dynamic>))
          .toList(),
      Wind.fromJson(json['wind'] as Map<String, dynamic>),
      json['rain'] == null
          ? Rain(0)
          : Rain.fromJson(json['rain'] as Map<String, dynamic>),
      json['dt_txt'] as String,
    );

Map<String, dynamic> _$WeatherModelModelToJson(WeatherModelModel instance) =>
    <String, dynamic>{
      'main': instance.main,
      'weather': instance.weather,
      'wind': instance.wind,
      'dt_txt': DateFormat('kk:mm:ss \n EEE d MMM')
          .format(DateTime.parse(instance.dt_txt)),
      'rain': instance.rain,
    };

WeatherForcastAllFarmResponseModel _$WeatherForcastAllFarmResponseModelFromJson(
        Map<String, dynamic> json) =>
    WeatherForcastAllFarmResponseModel(
      (json['list'] as List<dynamic>)
          .map((e) => (e as List<dynamic>)
              .map((e) => WeatherModelModel.fromJson(e as Map<String, dynamic>))
              .toList())
          .toList(),
    );

Map<String, dynamic> _$WeatherForcastAllFarmResponseModelToJson(
        WeatherForcastAllFarmResponseModel instance) =>
    <String, dynamic>{
      'list': instance.list,
    };

WeatherForcastResponseModel _$WeatherForcastResponseModelFromJson(
        Map<String, dynamic> json) =>
    WeatherForcastResponseModel(
      (json['list'] as List<dynamic>)
          .map((e) => WeatherModelModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$WeatherForcastResponseModelToJson(
        WeatherForcastResponseModel instance) =>
    <String, dynamic>{
      'list': instance.list,
    };

Main _$MainFromJson(Map<String, dynamic> json) => Main(
      (json['temp'] as num?)?.toDouble(),
      (json['feels_like'] as num?)?.toDouble(),
      (json['temp_min'] as num?)?.toDouble(),
      (json['temp_max'] as num?)?.toDouble(),
      (json['pressure'] as num?)?.toDouble(),
      (json['sea_level'] as num?)?.toDouble(),
      (json['grnd_level'] as num?)?.toDouble(),
      (json['humidity'] as num?)?.toDouble(),
      (json['temp_kf'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$MainToJson(Main instance) => <String, dynamic>{
      'temp': instance.temp,
      'feels_like': instance.feels_like,
      'temp_min': instance.temp_min,
      'temp_max': instance.temp_max,
      'pressure': instance.pressure,
      'sea_level': instance.sea_level,
      'grnd_level': instance.grnd_level,
      'humidity': instance.humidity,
      'temp_kf': instance.temp_kf,
    };

Weather _$WeatherFromJson(Map<String, dynamic> json) => Weather(
      (json['id'] as num?)?.toDouble(),
      json['main'] as String?,
      json['description'] as String?,
      json['icon'] as String?,
    );

Map<String, dynamic> _$WeatherToJson(Weather instance) => <String, dynamic>{
      'id': instance.id,
      'main': instance.main,
      'description': instance.description,
      'icon': instance.icon,
    };

Wind _$WindFromJson(Map<String, dynamic> json) => Wind(
      (json['speed'] as num?)?.toDouble(),
      (json['deg'] as num?)?.toDouble(),
      (json['gust'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$WindToJson(Wind instance) => <String, dynamic>{
      'speed': instance.speed,
      'deg': instance.deg,
      'gust': instance.gust,
    };

Rain _$RainFromJson(Map<String, dynamic> json) => Rain(
      (json['3h'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$RainToJson(Rain instance) => <String, dynamic>{
      '3h': instance.h3,
    };
