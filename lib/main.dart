import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:varun_kanna_app/routes/router.gr.dart';
import 'package:varun_kanna_app/services/news_content_service.dart';
import 'package:varun_kanna_app/view_models/land_recomendation_view_model.dart';
import 'package:varun_kanna_app/view_models/navigate_index_view_model.dart';
import 'package:varun_kanna_app/view_models/otp_view_model.dart';
import 'package:varun_kanna_app/view_models/profile_view_model.dart';
import 'package:varun_kanna_app/view_models/term_and_condition_view_model.dart';
import 'package:varun_kanna_app/views/market/market_page_provider.dart';
import 'utils/thai_localize_intl.dart';
import 'view_models/draw_farm_view_model.dart';

class LineLogin {
  final String displayName;
  final String pictureUrl;
  final String statusMessage;
  final String userId;

  const LineLogin(
      this.displayName, this.pictureUrl, this.statusMessage, this.userId);
}

final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
final FirebaseRemoteConfig _remoteConfig = FirebaseRemoteConfig.instance;
SharedPreferences? prefs;
String current_version = "1.0.5";

// Fetching, caching, and activating remote config
_initConfig() async {
  await _remoteConfig.setConfigSettings(RemoteConfigSettings(
    // cache refresh time
    fetchTimeout: const Duration(seconds: 1),
    // a fetch will wait up to 10 seconds before timing out
    minimumFetchInterval: const Duration(seconds: 10),
  ));
  await _remoteConfig.fetchAndActivate();
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  prefs = await _prefs;
  await Firebase.initializeApp();
  await _initConfig();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final _appRouter = AppRoute();
  @override
  Widget build(BuildContext context) {
    String latest_version = _remoteConfig.getString('latest_version');
    prefs!.setString("current_version", current_version);
    bool isCurrentVersion = current_version == latest_version;
    prefs!.setBool("isCurrentVersion", isCurrentVersion);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
            create: (_) => NewsContentService()..initialData()),
        ChangeNotifierProvider(create: (_) => NavigateIndexViewModel()),
        ChangeNotifierProvider(create: (_) => TermAndContionViewModel()),
        ChangeNotifierProvider(create: (_) => ProfileViewModel()),
        ChangeNotifierProvider(create: (_) => DrawFarmViewModel()),
        ChangeNotifierProvider(create: (_) => LoginViewModel()),
        ChangeNotifierProvider(create: (_) => MarketPageProvider()),
        ChangeNotifierProvider(create: (_) => LandRecomnViewModel()),
      ],
      child: (MaterialApp.router(
          theme: ThemeData(fontFamily: 'Sarabun'),
          debugShowCheckedModeBanner: false,
          routerDelegate: _appRouter.delegate(),
          routeInformationParser: _appRouter.defaultRouteParser(),
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            ThaiCupertinoLocalizations.delegate,
          ],
          locale: const Locale('th', 'TH'),
          supportedLocales: const [
            Locale('th', 'TH'),
            Locale('en', 'US'),
          ],
          builder: (context, child) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: child!,
            );
          })),
    );
  }
}

extension BoolParsing on String {
  bool parseBool() {
    if (toLowerCase() == 'true') {
      return true;
    } else if (toLowerCase() == 'false') {
      return false;
    }

    throw '"$this" can not be parsed to boolean.';
  }
}
