import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:provider/provider.dart';
import 'package:varun_kanna_app/model/market_page_model/request_joint_project_list_model.dart';
import 'package:varun_kanna_app/views/market/activity_selection/activity_selection_page.dart';
import 'package:varun_kanna_app/views/market/activity_selection/activity_selection_page_provider.dart';

void main() {
  group("activity selection widget test", () {
    final activitySelectionPageProvider = ActivitySelectionPageProvider();
    final selectedFarm = RequestJoinProjectListModel(
        farmId: 'c800e69f-a946-4f25-b825-6da320746d96',
        farmName: 'สามเหลี่ยม',
        displayArea: '3 ไร่ 1 งาน 51.65 ตารางวา',
        farmImg:
            'https://varuna-kanna-dev.s3.ap-southeast-1.amazonaws.com/public/image/user/caf2adc9-0fdb-4b52-b146-88274530d041/farm/c800e69f-a',
        status: 'verified',
        reqJoinProjectId: 'cd8f605f-0267-44d5-8c92-2f2577d0adf1',
        statusValue: 3);

    String projectId = '0d6dd87b-eab7-43c2-809b-dec652ac5f57';
    String reqJoinProjectId = 'cd8f605f-0267-44d5-8c92-2f2577d0adf1';
    testWidgets(
      'when status is not waiting for information should be disable verify button',
      (WidgetTester tester) async {
        await tester.pumpWidget(MaterialApp(
          home: ChangeNotifierProvider(
            create: (context) => activitySelectionPageProvider,
            child: ActivitySelectionPage(
              projectName: "",
              farmItem: selectedFarm,
              projectId: projectId,
              reqJoinProjectId: reqJoinProjectId, 
              headerImage: ""
            ),
          ),
        ));

        activitySelectionPageProvider.initialData(selectedFarm);
        await tester.pump();

        var verifyButton = find.text("สำเร็จ");
        expect(verifyButton, findsOneWidget);

        var verifyMainPageButtonKey =
            find.byKey(ActivitySelectionPage.navigateToVerifyMainPageButtonKey);
        await tester.ensureVisible(verifyMainPageButtonKey);
        await tester.tap(verifyMainPageButtonKey);
        await tester.pumpAndSettle();
        expect(find.byType(ActivitySelectionPage), findsOneWidget);
      },
    );

    testWidgets(
      'when status is not verified should be disable activity button',
      (WidgetTester tester) async {
        await tester.pumpWidget(MaterialApp(
          home: ChangeNotifierProvider(
            create: (context) => activitySelectionPageProvider,
            child: ActivitySelectionPage(
              projectName: "",
              farmItem: selectedFarm,
              projectId: projectId,
              reqJoinProjectId: reqJoinProjectId,
              headerImage: ""
            ),
          ),
        ));

        activitySelectionPageProvider.initialData(selectedFarm);
        await tester.pump();

        var verifyButton = find.text("สำเร็จ");
        expect(verifyButton, findsOneWidget);

        var activityButton = find.text("รอบันทึกกิจกรรม");
        expect(activityButton, findsOneWidget);

        var activityListPageButtonKey = find
            .byKey(ActivitySelectionPage.navigateToActivityListPageButtonKey);
        await tester.ensureVisible(activityListPageButtonKey);
        await tester.tap(activityListPageButtonKey);
        await tester.pumpAndSettle();
        expect(find.byType(ActivitySelectionPage), findsOneWidget);
      },
    );
  });
}
