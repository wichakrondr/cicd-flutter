import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:varun_kanna_app/views/market/activity_selection/activity_selection_page_provider.dart';

void main() {
  late ActivitySelectionPageProvider provider;

  setUp(() {
    provider = ActivitySelectionPageProvider();
  });

  group("activity selection login", () {
    test("check farm item is empty", () {
      expect(provider, provider);
    });

    test("when status is waiting for information should be render correctly",
        () {
      provider.manageStatus("waiting for information");
      expect(provider.statusText, "รอบันทึกข้อมูล");
      expect(provider.fontColor, Color(0xff3273E4));
      expect(provider.backgroundColor, Color(0xffE4ECFA));
      expect(provider.activityStatusText, "รออนุมัติแปลงปลูก");
      expect(provider.activityFontColor, Color(0XFF787F88));
      expect(provider.activityBackgroundColor, Color(0XFFCBD5E0));
    });

    test("when status is waiting for edit identity should be render correctly",
        () {
      provider.manageStatus("waiting for edit identity");
      expect(provider.statusText, "รอแก้ไขข้อมูล");
      expect(provider.fontColor, Color(0xffEF7E21));
      expect(provider.backgroundColor, Color(0xffFCEADB));
      expect(provider.activityStatusText, "รออนุมัติแปลงปลูก");
      expect(provider.activityFontColor, Color(0XFF787F88));
      expect(provider.activityBackgroundColor, Color(0XFFCBD5E0));
    });

    test(
        "when status is waiting for verify identity should be render correctly",
        () {
      provider.manageStatus("waiting for verify identity");
      expect(provider.statusText, "รอตรวจสอบ");
      expect(provider.fontColor, Color(0xffF8C309));
      expect(provider.backgroundColor, Color(0xffFDF6DD));
      expect(provider.activityStatusText, "รออนุมัติแปลงปลูก");
      expect(provider.activityFontColor, Color(0XFF787F88));
      expect(provider.activityBackgroundColor, Color(0XFFCBD5E0));
    });

    test("when status is verified should be render correctly", () {
      provider.manageStatus("verified");
      expect(provider.statusText, "สำเร็จ");
      expect(provider.fontColor, Color(0xff0AC898));
      expect(provider.backgroundColor, Color(0xffD8F6EF));
      expect(provider.activityStatusText, "รอบันทึกกิจกรรม");
      expect(provider.activityFontColor, Color(0XFF3D75D6));
      expect(provider.activityBackgroundColor, Color(0XFFE4ECFA));
    });

    test("when status is verify rejected should be render correctly", () {
      provider.manageStatus("verify rejected");
      expect(provider.statusText, "ไม่สำเร็จ");
      expect(provider.fontColor, Color(0xffFF3E1D));
      expect(provider.backgroundColor, Color(0xffFFE0DB));
      expect(provider.activityStatusText, "รออนุมัติแปลงปลูก");
      expect(provider.activityFontColor, Color(0XFF787F88));
      expect(provider.activityBackgroundColor, Color(0XFFCBD5E0));
    });

    test(
        "when status is waiting for the activity log should be render correctly",
        () {
      provider.manageStatus("waiting for the activity log");
      expect(provider.statusText, "สำเร็จ");
      expect(provider.fontColor, Color(0xff0AC898));
      expect(provider.backgroundColor, Color(0xffD8F6EF));
      expect(provider.activityStatusText, "รอบันทึกกิจกรรม");
      expect(provider.activityFontColor, Color(0XFF3D75D6));
      expect(provider.activityBackgroundColor, Color(0XFFE4ECFA));
    });

    test(
        "when status is waiting for edit the activity should be render correctly",
        () {
      provider.manageStatus("waiting for edit the activity");
      expect(provider.statusText, "สำเร็จ");
      expect(provider.fontColor, Color(0xff0AC898));
      expect(provider.backgroundColor, Color(0xffD8F6EF));
      expect(provider.activityStatusText, "รอแก้ไขกิจกรรม");
      expect(provider.activityFontColor, Color(0xffEF7E21));
      expect(provider.activityBackgroundColor, Color(0xffFCEADB));
    });

    test(
        "when status is waiting for approve the activity should be render correctly",
        () {
      provider.manageStatus("waiting for approve the activity");
      expect(provider.statusText, "สำเร็จ");
      expect(provider.fontColor, Color(0xff0AC898));
      expect(provider.backgroundColor, Color(0xffD8F6EF));
      expect(provider.activityStatusText, "รอตรวจสอบ");
      expect(provider.activityFontColor, Color(0xffF8C309));
      expect(provider.activityBackgroundColor, Color(0xffFDF6DD));
    });

    test("when status is successful activity log should be render correctly",
        () {
      provider.manageStatus("successful activity log");
      expect(provider.statusText, "สำเร็จ");
      expect(provider.fontColor, Color(0xff0AC898));
      expect(provider.backgroundColor, Color(0xffD8F6EF));
      expect(provider.activityStatusText, "สำเร็จ");
      expect(provider.activityFontColor, Color(0xFF0AC898));
      expect(provider.activityBackgroundColor, Color(0xffD8F6EF));
    });

    test("when status is activity rejected should be render correctly", () {
      provider.manageStatus("activity rejected");
      expect(provider.statusText, "สำเร็จ");
      expect(provider.fontColor, Color(0xff0AC898));
      expect(provider.backgroundColor, Color(0xffD8F6EF));
      expect(provider.activityStatusText, "ไม่สำเร็จ");
      expect(provider.activityFontColor, Color(0xffFF3E1D));
      expect(provider.activityBackgroundColor, Color(0xffFFE0DB));
    });
  });
}